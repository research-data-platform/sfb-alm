CREATE TABLE `alm_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `readd` datetime DEFAULT NULL,
  `recipient` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `message` text,
  `responseto` int(11) DEFAULT NULL COMMENT 'Response to message <message id>',
  `archived` int(1) DEFAULT '0',
  `deleted` int(1) DEFAULT '0',
  `sender_hide` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
);

/* assumed to hook_schema */
CREATE TABLE `alm_requests` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`userid` int (11),   /*Hier muss noch ein FOREIGN key zur User datenbank hinzugefügt werden und das format entsprechend angepasst werden */
	`position` varchar(100),
	`institut` varchar(100),
	`workinggroup_id` int (11),
	`subproject_id` int (11),
	`email` varchar(100),
	`phone` varchar(100),
	`imaging_goal` TEXT,
	`sample_type` varchar(100),
	`target_structure` varchar(100),
	`conditions` varchar(100),
	`staining_method` varchar(100),
	`data_required` TEXT,

	`imaging_support` varchar(50), #array as semicolon separated field
	`sample_health` int (2),  #radio button
	`imaging_module` varchar(50), #array as semicolon separated field
	`analysis_module` varchar(50),  #array as semicolon separated field
	`introduction_to_alm` varchar(50),  #array as semicolon separated field
	
	`consultation_attending`  VARCHAR(250),
  `consultation_prev_practical_experiences`  TEXT,
  `consultation_responsible_s02_member`  int (11), #user ID
  `consultation_imaging_support`  TEXT,
  `consultation_imaging_module`  TEXT,
  `consultation_sample_health`  TEXT,
  `consultation_analysis_module`  TEXT,

	`request_state` int (11),
  `locked_by` int(11), #The userID of the last user that opens a edit page of these request

	PRIMARY KEY (`id`)
);

/* assumed to hook_schema */
CREATE TABLE `alm_request_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11),
  `date` DATETIME,
  `message` TEXT,
  `type` int(6),

  PRIMARY KEY (`id`)
);

/* assumed to hook_schema */
CREATE TABLE `alm_request_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11),
  `date` DATETIME,
  `action` VARCHAR(100),
  `message` VARCHAR (100),
  `user_id` int (11),
  `is_visible` TINYINT(1),

  PRIMARY KEY (`id`)
);

/* assumed to hook_schema */
CREATE TABLE `alm_request_samples`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11),
  `sample_id` VARCHAR(40),
  `serial_description` VARCHAR(200),
  `serial_abbreviation` VARCHAR(5),
  `species_description` VARCHAR(200),
  `species_abbreviation` VARCHAR(2),
  `id_description` VARCHAR(200),
  `id_abbreviation` VARCHAR(5),
  `type_description` VARCHAR(200),
  `type_abbreviation` VARCHAR(5),
  `genotype_description` VARCHAR(200),
  `genotype_abbreviation` VARCHAR(7),
  `treatment_description` VARCHAR(200),
  `treatment_abbreviation` VARCHAR(5),
  `staining_type_description` VARCHAR(200),
  `staining_type_abbreviation` VARCHAR(5),
  `user_initials_description` VARCHAR(200),
  `user_initials_abbreviation` VARCHAR(3),
  `coverslip_description` VARCHAR(200),
  `coverslip_abbreviation` VARCHAR(3),
  `highest_assigned_coverslip_no` INT(3),

  `editable` TINYINT(1) DEFAULT 1,
  PRIMARY KEY (`id`)
);

/* deprecated */
CREATE TABLE `alm_request_stainings`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`request_id` int(11),
`type` int(5),
`label_name` VARCHAR (100),
`primary_antibody_1` VARCHAR (100),
`prim_dilution_1` VARCHAR (10),
`secondary_antibody_1` VARCHAR (100),
`sec_dilution_1` VARCHAR (10),
`primary_antibody_2` VARCHAR (100),
`prim_dilution_2` VARCHAR (10),
`secondary_antibody_2` VARCHAR (100),
`sec_dilution_2` VARCHAR (10),
`primary_antibody_3` VARCHAR (100),
`prim_dilution_3` VARCHAR (10),
`secondary_antibody_3` VARCHAR (100),
`sec_dilution_3` VARCHAR (10),
`primary_antibody_4` VARCHAR (100),
`prim_dilution_4` VARCHAR (10),
`secondary_antibody_4` VARCHAR (100),
`sec_dilution_4` VARCHAR (10),
`primary_antibody_5` VARCHAR (100),
`prim_dilution_5` VARCHAR (10),
`secondary_antibody_5` VARCHAR (100),
`sec_dilution_5` VARCHAR (10),

PRIMARY KEY (`id`)
);

/* assumed to hook_schema */
CREATE TABLE `alm_request_stainings`(

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11),
  `type` int(5),
  `label_name` VARCHAR (100),
  `owner` int(11),
  `editable` TINYINT(1) DEFAULT 1,
  PRIMARY KEY (`id`) /*,
  FOREIGN KEY (`owner`) REFERENCES users(`uid`) */
);

/* assumed to hook_schema */
CREATE TABLE `alm_staining_antibody`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staining_id` int (11),
  `primary_antibody_id` int(11)      COMMENT 'Foreign Key Antibody ID',
  `primary_antibody_dilution` VARCHAR (10),
  `secondary_antibody_id` int(11)   COMMENT 'Foreign Key Antibody ID',
  `secondary_antibody_dilution` VARCHAR (10),
  `orderNo` int (2)     COMMENT 'The order of the Row in the staining Table (only antibodies)',

  PRIMARY KEY (`id`)#,
  #FOREIGN KEY (staining_id) REFERENCES alm_stainings(staining_id)
);

/* assumed to hook_schema */
CREATE TABLE `alm_Staining_additional_staining`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staining_id` int(11),
  `primary_additional_staining` VARCHAR (100),
  `primary_add_staining_dilution` VARCHAR (10),
  `secondary_additional_staining` VARCHAR (100),
  `secondary_add_staining_dilution` VARCHAR (10),
  `orderNo` int (2) COMMENT 'The order of the Row in the staining Table (only additional stainings)',

  PRIMARY KEY (`id`)#,
  #FOREIGN KEY (staining_id) REFERENCES alm_stainings(staining_id)
);

CREATE TABLE `alm_request_coverslip_labels`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(5),
  `request_id` int(11) NOT NULL,
  `sample_id` int(11),
  `coverslip` int(2),
  `staining_id` int(11),
  `state` int(11) DEFAULT 10 COMMENT '10: Visible, 30: Hidden, 100: Deleted',
  `printed_flag` TINYINT(1) DEFAULT 0 COMMENT '0: Not printed, 1:printed',

  PRIMARY KEY (`id`)
);

CREATE TABLE `alm_print_coverslip_label_jobs` (
  `id`VARCHAR(6) UNIQUE,
  `request_id` INT(11),
  `state` int(11),

  PRIMARY KEY (`id`)
);

CREATE TABLE `alm_print_coverslip_label_job_values` (
  #`id` INT(11) NOT NULL AUTO_INCREMENT, #EIGENTLICH GAR NICHT NÖTIG
  `job_id` VARCHAR (6),
  `sample_name`VARCHAR(100),
  `project_name` VARCHAR(100), #Länge prüfen
  `user_name`VARCHAR(100), #Länge prüfen
  `date` DATETIME,
  `staining_name` VARCHAR(100)
);


/*Drop Tables:
DROP TABLE alm_requests;
DROP TABLE alm_request_comments;
DROP TABLE alm_request_log;
DROP TABLE alm_request_samples;
DROP TABLE alm_request_stainings;
DROP TABLE alm_staining_antibody;
DROP TABLE alm_Staining_additional_staining;

*/