<?php

define('ALM_ADMIN_MANAGER_MODE_DESCRIPTION_TEXT', '
<b>Attention: Read carefully before changing this option!</b>
<p>Changing this option significantly changes the behavior of the alm module!</p>
<p>Active Manager Mode:</br>
- All requests have to be reviewed by an alm manager and accepted manually.</br>
- All requests of users who don\'t have the "Advanced User Status" can only be consulted by an alm manager.</p>

<p>No Manager Mode:</br>
- All new requests are automatically accepted.</br>
- Every user who starts a request automatically becomes an "Advanced User".</p>

<p>Changing of this option does not influence the user status. This means, if you activate the manager mode all 
users who have previously been assinged "Advanced User Status" (manually or automatically) will keep this status. 
They still can consult their own requests without an alm manager.</p>
');

/**
 * Page callback: Advanced Light Microscopy settings
 *
 * @see sfb_alm_menu()
 *
 * @return mixed
 */
function sfb_alm_admin() {
  $form = [];

  $form['alm_request_pid_prefix'] = [
    '#type' => 'textfield',
    '#title' => t('ALM Request PID prefix'),
    '#default_value' => variable_get('alm_request_pid_prefix', ALMRequest::DEFAULT_PID_PREFIX),
    '#description' => t("This field defines the prefix of the pid, which will be used in the url. For example: /alm/request/1 will be converted to /alm/request/sfb-0000-111-1."),
    '#required' => TRUE,
  ];

  $form['alm_coverslip_label_pid_prefix'] = [
    '#type' => 'textfield',
    '#title' => t('ALM Coverslip Label PID prefix'),
    '#default_value' => variable_get('alm_coverslip_label_pid_prefix', ALMRequestCoverslipLabel::DEFAULT_PID_PREFIX),
    '#description' => t("This field defines the prefix of the pid for the coverslip Labels."),
    '#required' => TRUE,
  ];

  $form['alm_request_email_request_notification_recipients'] = [
    '#type' => 'textarea',
    '#title' => t('Request email notification recipients'),
    '#default_value' => variable_get('alm_request_email_request_notification_recipients', ''),
    '#description' => t("This field defines the email adresses for the manager notifications. Adresses can be separated by ; or new lines"),
    '#cols' => '62',
    '#attributes' => ['style' => ['width: auto;']],
    '#resizable' => FALSE,
    '#required' => TRUE,
  ];

  $form['alm_request_limit'] = [
    '#type' => 'textfield',
    '#title' => t('Number of requests per page in the overview'),
    '#default_value' => variable_get('alm_request_limit', 25),
    '#description' => t(""), //TODO: description
    '#required' => TRUE,
  ];

  $form['manager_mode-fieldset'] = [
    '#type' => 'fieldset',
    '#title' => t('Manager Mode'),
    '#description' => t(ALM_ADMIN_MANAGER_MODE_DESCRIPTION_TEXT),
  ];

  $form['manager_mode-fieldset'][SFB_ALM_VAR_CONFIG_MANAGER_MODE] = [
    '#type' => 'checkbox',
    '#title' => t('Use Manager Mode'),
    '#default_value' => variable_get(SFB_ALM_VAR_CONFIG_MANAGER_MODE, TRUE),
  ];

  return system_settings_form($form);
}