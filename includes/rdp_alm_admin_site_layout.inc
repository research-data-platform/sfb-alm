<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 23.09.2019
 * Time: 12:26
 */

function rdp_alm_admin_site_layout() {
  $form = [];

  $form[SFB_ALM_VAR_CONFIG_MODULE_TITLE] = [
    '#type' => 'textfield',
    '#title' => t('ALM Module Title'),
    '#default_value' => variable_get(SFB_ALM_VAR_CONFIG_MODULE_TITLE, 'Advanced Light Microscopy'),
    '#description' => t('Title of the module that will be used for references in menus.'),
  ];

  $banner_image_default_path = base_path() . drupal_get_path('theme',
      'sfb_alm_dashboard') . '/img/alm_logo_full.png';
  $form[SFB_ALM_VAR_CONFIG_BANNER_IMAGE_PATH] = [
    '#type' => 'textfield',
    '#title' => t('ALM Module Banner Image'),
    '#default_value' => variable_get(SFB_ALM_VAR_CONFIG_BANNER_IMAGE_PATH, $banner_image_default_path),
    '#description' => t('Specify the path (relative to webroot) for a 
    banner image displayed in the ALM menu bar. 
    Recommended format: PNG, maximum width: 200px, maximum height: 40px.'),
  ];

  $form['alm_requests_overview_introduction_text'] = [
    '#type' => 'textarea',
    '#title' => t('ALM Requests Overview Introduction Text'),
    '#default_value' => variable_get('alm_requests_overview_introduction_text', ''),
    '#description' => t('This field sets the introduction (head) text of the "User Request Overview Page". Html can be used'),
  ];

  $form['alm_requests_overview_introduction_image'] = [
    '#type' => 'textfield',
    '#title' => t('ALM Requests Overview Introduction Image'),
    '#default_value' => variable_get('alm_requests_overview_introduction_image', ''),
    '#description' => t('This field sets the introduction image of the "User Request Overview Page". The folder of the alm modul is the root'),
  ];

  $form['alm_request_new_introduction_text'] = [
    '#type' => 'textarea',
    '#title' => t('New Request Introduction Text'),
    '#default_value' => variable_get('alm_request_new_introduction_text', ''),
    '#description' => t('This field sets the introduction (head) text of the "New Request Page". Html can be used'),
  ];

  $form['alm_request_new_introduction_image'] = [
    '#type' => 'textfield',
    '#title' => t('New Request Introduction Image'),
    '#default_value' => variable_get('alm_request_new_introduction_image', ''),
    '#description' => t('This field sets the introduction image of the "New Request Page". The folder of the alm modul is the root. 
    Attention: The image should not be larger than the text'),
  ];

  $form['alm_request_manager_overview_introduction_text'] = [
    '#type' => 'textarea',
    '#title' => t('Manager Requests Overview Introduction Text'),
    '#default_value' => variable_get('alm_request_manager_overview_introduction_text', ''),
    '#description' => t('This field sets the introduction (head) text of the "Manager Request Overview Page". Html can be used'),
  ];

  $form['alm_request_manager_overview_introduction_image'] = [
    '#type' => 'textfield',
    '#title' => t('Manager Requests Overview Introduction Image'),
    '#default_value' => variable_get('alm_request_manager_overview_introduction_image', ''),
    '#description' => t('This field sets the introduction image of the "Manager Request Overview Page". The folder of the alm modul is the root'),
  ];

  return system_settings_form($form);
}