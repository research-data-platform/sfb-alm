<?php

/**
 * @file
 * Create new ALM request
 *
 * @author Christian Henke (christian.henke@stud.uni-goettingen.de)
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 */
function sfb_alm_request_new($form = [], &$form_state, $pid = NULL) {

  // get drupal's user object
  global $user;

  // create an empty ALMRequest object
  $request = new ALMRequest();
  $request->setUserID($user->uid);
  $request->setEmail($request->getUser()->getMail());

  // add request id to form state to use it later in save function
  $form_state[ALM_REQUEST_ID] = $request->getRequestID();

  // change default form theme to alm_dashboard theme box output
  // this form uses two-columns-form-theme from alm-dashboard
  $form['#theme'] = [
    '#theme' => 'alm_dashboard_box_form',
  ];

  //
  //
  // set form fields
  //
  //

  $introduction_image = '';
  if (!empty(variable_get('alm_request_new_introduction_image', ''))) {
    $image_path = base_path() . drupal_get_path('module',
        'sfb_alm') . '/' . variable_get('alm_request_new_introduction_image', '');
    $introduction_image = '<img src="' . $image_path . '" style="float:left; padding-right: 30px;" alt="Advanced Light Microscopy" />';
  }
  $introduction_text = variable_get('alm_request_new_introduction_text', '');

  $form['introduction'] = [
    '#type' => 'markup',
    '#markup' =>
      $introduction_image .
      $introduction_text,
    '#prefix' => '<div class="no-print"><p style="margin-top: 10px; margin-bottom: 20px;">',
    '#suffix' => '</p></div>',
  ];

  //
  // Researcher
  //

  $form['fieldset-userprofile'] = [
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-user"></i> ' . t('User Profile'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['fieldset-userprofile'][ALM_REQUEST_USER_USER] = $request->getFormFieldUserAccountName();
  $form['fieldset-userprofile'][ALM_REQUEST_USER_NAME] = $request->getFormFieldUserFullname();
  $form['fieldset-userprofile'][ALM_REQUEST_USER_POSITION] = $request->getFormFieldPosition();
  $form['fieldset-userprofile'][ALM_REQUEST_USER_INSTITUTE] = $request->getFormFieldInstitute();
  $form['fieldset-userprofile'][ALM_REQUEST_USER_RESEARCHGROUP] = $request->getFormFieldResearchgroup();
  $form['fieldset-userprofile'][ALM_REQUEST_USER_SUBPROJECT] = $request->getFormFieldSubprojects();
  $form['fieldset-userprofile'][ALM_REQUEST_USER_EMAIL] = $request->getFormFieldEmail();
  $form['fieldset-userprofile'][ALM_REQUEST_USER_PHONE] = $request->getFormFieldPhoneNumber();

  //
  // Imaging Goal and Scientific Question
  //

  $form['fieldset-imaging_goal_and_scientific_question'] = [
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-lightbulb-o"></i> ' . t('Imaging Goal and Scientific Question'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => '',
  ];

  $form['fieldset-imaging_goal_and_scientific_question'][ALM_REQUEST_IMAGING_GOAL_AND_SCIENTIFIC_QUESTION] = $request->getFormFieldImagingGoal();

  //
  // Sample Information
  //

  $form['fieldset-sample_information'] = [
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-info"></i> ' . t('Sample Information'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    //'#description' => 'Please describe here the imaging goal and the scientific question',
  ];

  $form['fieldset-sample_information'][ALM_REQUEST_SAMPLE_TYPE] = $request->getFormFieldSampleType();
  $form['fieldset-sample_information'][ALM_REQUEST_SAMPLE_TARGET_STRUCTURE] = $request->getFormFieldTargetStructure();
  $form['fieldset-sample_information'][ALM_REQUEST_SAMPLE_CONDITIONS] = $request->getFormFieldSampleConditions();
  $form['fieldset-sample_information'][ALM_REQUEST_SAMPLE_STAINING_METHOD] = $request->getFormFieldStainingMethod();
  $form['fieldset-sample_information'][ALM_REQUEST_SAMPLE_DESCRIPTIVE_OR_QUANTITATIVE_DATA_REQUIRED] = $request->getFormFieldDataRequired();

  //
  // Requested Support and Services
  //


  $form['fieldset-requested_support_and_services'] = [
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-edit"></i> ' . t('Requested Support and Service '),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    //'#description' => 'Please describe here the imaging goal and the scientific question',
  ];

  //todo (Nov.2019) Changed to one column because of scaling problems if the number of checkboxes are varyin
  //The markup elements generates a view with 3 rows and two colums
  //$form['fieldset-requested_support_and_services']['row_1_begin'] = ['#markup' => '<div class="row"><div class="col-md-4">',];
  $form['fieldset-requested_support_and_services'][ALM_REQUEST_IMAGING_SUPPORT] = $request->getFormFieldImagingSupport();
  //$form['fieldset-requested_support_and_services']['row_1_column_2'] = ['#markup' => '</div><div class="col-md-4">',];
  $form['fieldset-requested_support_and_services'][ALM_REQUEST_INTRODUCTION_TO_ALM] = $request->getFormFieldIntroductionToAlm();
  //$form['fieldset-requested_support_and_services']['row_1_end_row_2_begin'] = ['#markup' => '</div></div><div class="row"><div class="col-md-4">',];
  $form['fieldset-requested_support_and_services'][ALM_REQUEST_IMAGING_MODULE] = $request->getFormFieldImagingModule();
  //$form['fieldset-requested_support_and_services']['row_2_column_2'] = ['#markup' => '</div><div class="col-md-4">',];
  $form['fieldset-requested_support_and_services'][ALM_REQUEST_ANALYSIS_MODULE] = $request->getFormFieldAnalysisModule();
  //$form['fieldset-requested_support_and_services']['row_2_end'] = ['#markup' => '</div></div>',];
  $form['fieldset-requested_support_and_services'][ALM_REQUEST_SAMPLE_HEALTH] = $request->getFormFieldSampleHealth();


  //
  // User Comment
  //

  $form['fieldset-user_comment'] = [
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-comment"></i> ' . t('User Comment'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => 'Place for additional user comments',
  ];


  $form['fieldset-user_comment'][ALM_REQUEST_USER_COMMENT] = $request->getFormFieldComment(ALMCommentType::SubmitEdit);

  //
  // Buttons
  //

  // submit button to submit this request to alm team
  // after submit user is redirected to requests overview (/alm/request/
  $form['btn-submit'] = [
    '#type' => 'submit',
    '#value' => '<i class="fa fa-send-o"></i>' . t(' Submit request'),
    '#description' => t('Submits the request to the ALM Team. After submitting the request it will not be possible to edit the request again!'),
    '#attributes' => ['class' => ['btn-primary', 'btn-lg'], 'style' => ['float: right;']],
  ];

  // cancel this form
  /*$form['btn-cancel'] = array(
    '#type' => 'markup',
    '#description' => t('Discard your changes and go back to requests overview.'),
    '#markup' => '<a href="'. sfb_alm_url(SFB_ALM_URL_REQUEST) .'" class="btn btn-default"><i class="fa fa-remove"></i> Cancel</a>',
  );*/
  $form['btn-cancel'] = [
    '#type' => 'submit',
    '#description' => t('Discard the request and go back to requests overview.'),
    '#value' => t('Cancel'),
    '#attributes' => ['class' => ['btn-default']],
    '#submit' => ['sfb_alm_request_new_submit_cancel'],
  ];

  // stores this form into database and redirects to edit request page
  $form['btn-save'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
    '#description' => t('Save the request and go to the view page'),
    '#attributes' => ['class' => ['btn-default']],
    '#submit' => ['sfb_alm_request_new_submit_save'],
  ];

  // stores this form into database and redirects to edit request page
  $form['btn-continue'] = [
    '#type' => 'submit',
    '#value' => t('Save and continue'),
    '#description' => t('Save the request and stay on the edit page'),
    '#attributes' => ['class' => ['btn-default']],
    '#submit' => ['sfb_alm_request_new_submit_continue'],
  ];

  return $form;
}


/**
 * Implements hook_drupal_form_submit()
 *
 * TODO: dokumentation
 * Submits the ALM request to the ALM Managaer
 *
 * @param $form
 * @param $form_state
 */
function sfb_alm_request_new_submit($form, &$form_state) {

  // create new request instance to store it into database
  $request = new ALMRequest();

  // Initial new request (set RequestState, userID, requestID)
  $request->setNewRequestAttributes();


  // fill this request object with input data from form_state
  $request = sfb_alm_merge_request_with_form_state($form_state, $request);

  //Set the user Comment to the updated comment
  $comment = new ALMComment();
  $comment->setTypeID(ALMCommentType::Submit);
  $comment->setMessage($form_state['values'][ALM_REQUEST_USER_COMMENT]);
  $comment->setCurrentDate();

  $request->setUpdatedComment($comment);
  // store this request into database
  $request_id = $request->save(FALSE);

  // Load new request with request ID
  $request = ALMRequestsRepository::findById($request_id);

  // This must be AFTER the first save because in this version (with no PID) the request ID is set first by the first save and the submitAttributes function needs the request id
  //  Sets submitDate and RequestState
  $request->setSubmitAttributes();

  /*//Update the User Comment
  $comment = $request->getCommentWithType(ALMCommentType::SubmitEdit);
  $comment->setTypeID(ALMCommentType::Submit);
  $comment->setMessage($form_state['values'][ALM_REQUEST_USER_COMMENT]);
  $comment->setCurrentDate();

  $request->setUpdatedComment($comment);*/

  $request->save();


  $msg = 'Request ' . l($request_id, sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request_id))
    . ' submitted successfully.';
  if (variable_get(SFB_ALM_VAR_CONFIG_MANAGER_MODE, 1)) {
    // Only send notifications to service unit team, if module is configured in "active manager mode"
    $msg .= ' E-mail notifications have been sent to the service team.';
  }
  else {
    /**
     * Auto-accept request if "active manager mode" is deactivated
     */
    if ($request->getRequestState() == ALMRequestState::Pending) {
      $request->setRequestState(ALMRequestState::Accepted);
      $request->save(FALSE);
      ALMUserQualificationRepository::setQualificationLevelOfUserId($request->getUserID(),
        ALMUserQualificationRepository::$QUALIFICATION_LEVEL_USER_CONSULTATION);
    }
  }
  drupal_set_message($msg);
  // Send an e-mail to user (and alm team
  sfb_alm_mail_send_new_request_notification($request);

  //redirect to overview
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST)];
}

/**
 * Saves the ALM request with the values of the form_state array to the database and redirect to the edit page
 *
 * @param $form
 * @param $form_state
 */
function sfb_alm_request_new_submit_continue($form = NULL, &$form_state) {
  $insertID = save($form_state);

  // redirect to request view
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $insertID)];
}

function sfb_alm_request_new_submit_cancel($form, &$form_state) {
  // redirect to the request view
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST)];
}

/**
 * Saves the ALM request with the values of the form_state array to the database and redirect to the view
 *
 * @param $form
 * @param $form_state
 */
function sfb_alm_request_new_submit_save($form = NULL, &$form_state) {
  $insertID = save($form_state);
  // redirect to request view
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $insertID)];
}

//TODO: diese funktion muss umbenannt werden!
/**
 * Saves the the request with the values of the form_state array to the database
 *
 * @param $form_state
 *
 * @return integer the request ID of the new request
 */
function save($form_state) {

  //TODO: wozu ist diese Schleife da?
  //foreach ($form_state['values'][ALM_REQUEST_IMAGING_SUPPORT] as $num => $item) {
  //}

  // get global user
  global $user;

  // create new alm request instance
  $request = new ALMRequest();

  // Initial new request (set RequestState, userID, requestID)
  $request->setNewRequestAttributes();

  // fill this request obejct with input data from form_state
  $request = sfb_alm_merge_request_with_form_state($form_state, $request);

  //Update the User Comment
  $comment = $request->getCommentWithType(ALMCommentType::SubmitEdit);
  $comment->setMessage($form_state['values'][ALM_REQUEST_USER_COMMENT]);
  $comment->setCurrentDate();

  //Set the updated user Comment to the updatedComment Variable, so the next save invoke will store it to the DB
  $request->setUpdatedComment($comment);

  // save this request object into database and get its id
  $insertID = $request->save();

  drupal_set_message('Request ' . l($request->getRequestID(),
      sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request->getRequestID())) . ' saved successfully.');

  return $insertID;
}

function sfb_alm_request_new_validate($form, &$form_state) {
  /*if (strlen($form_state['values']['user-surname']) <= 1) {
    form_set_error('user-surname', t('word not long enough'));
  }
  */
}