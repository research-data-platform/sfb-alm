<?php
/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 04.08.2016
 * Time: 16:06
 */

/**
 * Implements hook_title_callback()
 *
 * @param int $alm_request_id
 * @return string
 */
function sfb_alm_management_request_view_title_callback($alm_request_id = 0) {
  $request = ALMRequestsRepository::findById($alm_request_id);
  $request_display_name = 'unknown';

  if (!$request->isEmpty()) {
    $request_display_name = 'umg-sfb1002-alm-request-' . $request->getRequestID();
  }

  return 'Manage Request ' . $alm_request_id;
}

function sfb_alm_management_request_view($requestID = null) {
  $requestObj = ALMRequestsRepository::findById($requestID);
  $form_state[ALM_REQUEST_ID] = $requestObj->getRequestID();

  //
  // Check access
  //
  if ($requestObj->isEmpty()) {
    drupal_not_found();
    return;
  }

  // Check if the user has permission to view the managerview of  the request
  if (!$requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW)) {
    drupal_access_denied();
    return;
  }

  // Create a (hidden) log entry, that the manager view the request.
  sfb_alm_create_log_entry(ALMLog::$ACTION_MANAGER_VIEW, "View request", $requestID, 0);

  $page_buttons = '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST) .
    '" class="btn btn-default" style="margin-right:5px"><span class="fa fa-arrow-left" ></span> Back to Overview</a>';
  if ($requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_ACCEPT)) {

    //Button accept
    $page_buttons .= '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_ACCEPT, $requestID) .
      '" class="btn btn-primary btn-lg" style="margin-right:5px; float: right; " ><span class="fa fa-thumbs-o-up"></span> Accept request</a>';
  }
  if ($requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_WITHDRAW)) {
    //Button withdraw
    $page_buttons .= '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_WITHDRAW, $requestID) .
      '" class="btn btn-default" style="margin-right:5px" ><span class="fa fa-undo"></span> Withdraw request</a>';
  }

  if ($requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_EDIT)) {
    //Button revise
    $page_buttons .= '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_REVISE, $requestID) .
      '" class="btn btn-default" style="margin-right:5px" ><span class="fa fa-retweet"></span> Revise request</a>';

    if ($requestObj->getRequestState() == ALMRequestState::Accepted) {
      // $edit_consult_btn_text =  ? 'Consult request' : 'Edit request';
      //Button edit
      $page_buttons .= '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $requestID) .
        '?active=1" class="btn btn-primary btn-lg" style="float: right;" ><span class="fa fa-edit"></span> ' . t('Consult request') . '</a>';
    } else {
      $page_buttons .= '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $requestID) .
        '" class="btn btn-default"><span class="fa fa-edit"></span> ' . t('Edit request') . '</a>';
    }
  }

  //
  // Consultation
  //
  $view_consultation = 'The request is not consulted yet';

  $request_data_tab = new AlmRequestDataViewTab($requestObj);
  $consultation_tab = new AlmManagementConsultationViewTab($requestObj);
  $comments_and_log_tab = new AlmCommentsAndLogsTab($requestObj);

  $tabs = array(
    array(
      'title' => 'Request Overview',
      'content' => $request_data_tab->getHtml(),//$request->getViewTabRequestDatas(),
    ),
    array(
      'title' => 'Consultation',
      'content' => $consultation_tab->getHtml(),//$request->getViewTabConsultation(),
    ),
    array(
      'title' => 'Comments and Log',
      'content' => $comments_and_log_tab->getHtml(),
    ),
  );
  
  // Add the pdf download button
  $button_tabbed_box= array(
    "title" => "Request form PDF",
    "url" => sfb_alm_url(RDP_ALM_URL_DOWNLOAD_PDF_REQUEST, $requestObj->getRequestID()),
    "icon-class" => "fa fa-file",
    "icon-color" => "#72afd2",
  );

  $tabbed_box_vars = array(
    'tabs' => $tabs,
    '#active' => isset($_GET['tab']) ? $_GET['tab'] : 0,
    'btn-right' => $button_tabbed_box);

  return theme_sfb_alm_tabbed_box($tabbed_box_vars) . $page_buttons;

}
