<?php

/**
 * Download handler.
 *
 * This function uses different calls that are defined by variable $option.
 *
 * Following download calls are valid:
 * - alm/download/request_form/{request_id}
 *                / $option    / arg(3)
 * - alm/download/pdf_form/{request_id}
 *
 * @param $option
 */
function sfb_alm_download($option)  {

  // if no option  is set, then return drupal not found page
  if($option == NULL) {
    drupal_not_found();
    return;
  }

  // check if called option exists and obligatory arguments are set
  if($option == 'request_form' && !empty(arg(3))) {
    return sfb_alm_download_request_form(arg(3));   //TODO: remove if not needed
  } else if($option =='pdf_request' && !empty(arg(3))) {
    return sfb_alm_download_pdf(arg(3));
  } else {
    drupal_not_found();
    return;
  }

}

/**
 * Creates new request pdf file.
 *
 * @param int $request_id
 *   Database request id
 */
function sfb_alm_download_pdf($request_id) {

  if(user_is_anonymous()) {
    drupal_access_denied();
    return;
  }

  // get request from database
  $request = ALMRequestsRepository::findById($request_id);

  // check if request exists
  if($request->isEmpty()) {
    drupal_not_found();
    exit();
  }

  // check permissions
  if(!($request->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_VIEW) || $request->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW))) {
    drupal_access_denied();
    exit();
  }

  // load pdf library
  module_load_include('php', 'sfb_commons', 'lib/tcpdf_min_6.2.12/tcpdf');
  module_load_include('php', 'sfb_commons', 'lib/fpdi_1.6.1/fpdi');

  class PDF extends FPDI
  {
    /**
     * "Remembers" the template id of the imported page
     */
    var $_tplIdx;

    var $requestId;

    var $templateFileName = 'request.pdf';

    /**
     * Draw an imported PDF logo on every page
     */
    function Header()
    {

      if (is_null($this->_tplIdx)) {
        $template_file = $_SERVER['DOCUMENT_ROOT'] . base_path().drupal_get_path('module', 'sfb_alm').'/resources/'.$this->templateFileName;
        $this->setSourceFile($template_file);
        $this->_tplIdx = $this->importPage(1);
      }
      $tplIdx = $this->importPage(1, '/MediaBox');
      $size = $this->useTemplate($tplIdx, 0, 0, 0, 0, true);
    }


    function Footer()
    {
      // position at 15 mm from bottom
      $this->SetY(-15);
      // set font
      $this->SetFont('helvetica', 'I', 8);
      // page number
      $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().' - ALM-Request '.$this->requestId , 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    function setRequestId($requestId) {
      $this->requestId = $requestId;
    }

    function setTemplateFileName($filename) {
      $this->templateFileName = $filename;
    }
  }

  // initiate PDF
  $pdf = new PDF();

  switch($request->getRequestState()) {
    case ALMRequestState::Edit:
      $pdf->setTemplateFileName('request-preview.pdf');
      break;
    case ALMRequestState::Revision:
      $pdf->setTemplateFileName('request-preview.pdf');
      break;
    case ALMRequestState::Pending:
      $pdf->setTemplateFileName('request-preview.pdf');
      break;
    case ALMRequestState::Withdraw:
      $pdf->setTemplateFileName('request-preview.pdf');
      break;
    case ALMRequestState::Accepted:
      break;
    case ALMRequestState::Running:
      break;
    case ALMRequestState::Finished:
      break;
    case ALMRequestState::Archived:
      break;
    case ALMRequestState::Deleted:
      $pdf->setTemplateFileName('request-preview.pdf');
      break;
    default:
      $pdf->setTemplateFileName('request-preview.pdf');
  }

  $pdf->setRequestId($request_id);
  $pdf->SetFont('sourcesanspro', '', 10, '', false);
  $pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
  $pdf->SetAutoPageBreak(true, 40);
  $pdf->setFontSubsetting(false);

  // add basis request data
  sfb_alm_download_pdf_add_request_data($pdf, $request);

  // add page with consultation data
  sfb_alm_download_pdf_add_consultation($pdf, $request);

  // add page with technical data
  //TODO: erstmal ist die ausgabe von technischen daten ausgeschaltet, da das nur für bestimmte zwecke verwendet werden soll. es wäre daher sinnvoll noch ein extra url argument zu definieren, das die technical details ausgabe erlaubt.
  //sfb_alm_download_pdf_add_technical_data($pdf, $request);

  // prepare drupal for pdf output
  drupal_add_http_header('Access-Control-Allow-Origin', "*");
  drupal_add_http_header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  header('Content-Type: application/pdf');
  header('Content-Disposition: attachment;filename="request.pdf"');

  // print pdf file
  $pdf->Output();
  exit();
}

/**
 * @param $request_id
 * @deprecated this excel file output should not be used anymore
 */
function sfb_alm_download_request_form($request_id) {

  if(user_is_anonymous()) {
    drupal_access_denied();
    return;
  }

  //TODO: read request from db
  //TODO: check permissions

  //
  //
  // BEGIN PHPEXCEL
  //
  //
  $template_file = $_SERVER['DOCUMENT_ROOT'] . base_path().drupal_get_path('module', 'sfb_alm').'/resources/template_alm_request.xlsx';

  date_default_timezone_set('Europe/London');
  module_load_include('php', 'sfb_commons', 'lib/phpexcel_1.8.0/PHPExcel');
  $objReader = PHPExcel_IOFactory::createReader('Excel2007');
  $objPHPExcel = $objReader->load($template_file);
  drupal_add_http_header('Access-Control-Allow-Origin', "*");
  drupal_add_http_header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');

  $objPHPExcel->getProperties()->setCreator("SFB 1002 - Service 02")
    ->setLastModifiedBy('SFB 1002 Research Data Platform - Advanced Light Microscopy (ALM)')
    ->setTitle("SFB 1002 - ALM Request")
    ->setSubject("")
    ->setDescription("")
    ->setKeywords("office 2007 openxml study")
    ->setCategory("ALM Request");

  $objPHPExcel->setActiveSheetIndex(0);

  // Redirect output to a client's web browser (Excel2007)
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="SFB1002-ALM-Request.xlsx"');
  header('Cache-Control: max-age=0');
  // If you're serving to IE 9, then the following may be needed
  header('Cache-Control: max-age=1');

  // If you're serving to IE over SSL, then the following may be needed
  header ('Expires: Mon, 26 Jul 2020 05:00:00 GMT'); // Date in the past
  header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
  header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
  header ('Pragma: public'); // HTTP/1.0

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->save('php://output');
}

/**
 * Overrides TCPDF object containing request pdf with data from request object.
 *
 * @param $pdf \TCPDF
 * @param $request \ALMRequest
 */
function sfb_alm_download_pdf_add_request_data(&$pdf, $request) {

  // prepare output data
  $request_url = sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request->getRequestID());

  $user_name = $request->getUserFullname();
  $user_rdp = $request->getUserAccountName();
  $user_email = $request->getEmail();
  $user_phone = $request->getPhoneNumber();
  $user_institute = $request->getInstitute();
  $user_researchgroup = $request->getResearchGroupName();
  $user_subproject = $request->getSubProjectName();

  $imaging_goal = $request->getImagingGoal();
  if(empty($imaging_goal))
    $imaging_goal = 'No imaging goal defined.';

  $img_sample_type = $request->getSampleType();
  $img_target_structures = $request->getTargetStructure();
  $img_conditions = $request->getConditions();
  $img_staining_method = $request->getStainingMethod();
  $img_data_required = $request->getDataRequired();

  //
  // prepare imaging support string
  //
  $imaging_support_string = '';
  $imaging_support_all_choices = $request->getAllImagingSupportChoices();
  $imaging_support_choices = $request->getImagingSupportIDs();

  foreach($imaging_support_all_choices as $choice_id => $choice_name) {

    $chosen = false;

    foreach($imaging_support_choices as $user_choice) {
      if($user_choice == $choice_id) {
        $chosen = true;
      }
    }

    if($chosen) {
      $imaging_support_string .= '<tt>[X]</tt> ';
    } else {
      $imaging_support_string .= '<tt>[ ]</tt> ';
    }

    $imaging_support_string .= $choice_name."<br />";
  }

  //
  // prepare sample health string
  //
  $sample_health_string = '';
  if($request->getSampleHealthID() == '2') {
    $sample_health_string .= "<tt>[ ]</tt> No documentation of sample health<br />";
    $sample_health_string .= "<tt>[X]</tt> Documentation of sample health<br />";
  } else {
    $sample_health_string .= "<tt>[X]</tt> No documentation of sample health<br />";
    $sample_health_string .= "<tt>[ ]</tt> Documentation of sample health<br />";
  }

  //
  // prepare imaging module string
  //
  $imaging_module_string = '';
  $imaging_module_all_choices = $request->getAllImagingModules();
  $imaging_module_choices = $request->getImagingModuleIDs();

  foreach($imaging_module_all_choices as $choice_id => $choice_name) {
    $chosen = false;

    foreach($imaging_module_choices as $user_choice)
      if($user_choice == $choice_id)
        $chosen = true;

    if($chosen)
      $imaging_module_string .= '<tt>[X]</tt> ';
    else
      $imaging_module_string .= '<tt>[ ]</tt> ';

    $imaging_module_string .= $choice_name."<br />";
  }

  //
  // prepare introduction alm string
  //
  $introduction_alm_string = '';
  $introduction_alm_all_choices = $request->getAllIntroductionsToAlmChoices();
  $introduction_alm_choices = $request->getIntroductionToAlmIDs();

  foreach($introduction_alm_all_choices as $choice_id => $choice_name) {
    $chosen = false;

    foreach($introduction_alm_choices as $user_choice)
      if($user_choice == $choice_id)
        $chosen = true;

    if($chosen)
      $introduction_alm_string .= '<tt>[X]</tt> ';
    else
      $introduction_alm_string .= '<tt>[ ]</tt> ';

    $introduction_alm_string .= $choice_name."<br />";
  }

  //
  // prepare analysis module string
  //
  $analysis_module_string = '';
  $analysis_module_all_choices = $request->getAllAnalysisModules();
  $analysis_module_choices = $request->getAnalysisModuleIDs();

  foreach($analysis_module_all_choices as $choice_id => $choice_name) {
    $chosen = false;

    foreach($analysis_module_choices as $user_choice)
      if($user_choice == $choice_id)
        $chosen = true;

    if($chosen)
      $analysis_module_string .= '<tt>[X]</tt> ';
    else
      $analysis_module_string .= '<tt>[ ]</tt> ';

    $analysis_module_string .= $choice_name."<br />";
  }

  //
  // get latest comment
  //
  $comment_string = '';
  $comment_data = ALMCommentRepository::findLatestCommentByRequestIDAndSmallerThanCommentType($request->getRequestID(), ALMCommentType::SubmitEdit);

  // If there is no comment, return empty HTML string, so in the output the complete field is not visible
  if ($comment_data != null && $comment_data->getMessage() != "") {
    $comment_string .= $comment_data->getMessage();
  }

  // add a page
  $pdf->AddPage();

  //
  // Request ID and Date
  //
  $html = '
    <table cellpadding="3">
      <tr>
      <td style="text-align:center; border: 0.5px dashed #e4dfcd; width: 45%">
        <span style="color: #626262;">Request ID</span><br /><br />
        <span style="font-size: 2em;">'.$request->getRequestID().'</span><br />
        <span style="font-family: sourcesansprolight"><small style="color:#565656">'.$request_url.'</small></span><br />
        <span style="color: #626262; font-size:0.8em">Current state:</span>
        <span style="font-size: 0.8em">'.ALMRequestState::getRequestStateName($request->getRequestState()).'</span>
      </td>
      <td style="width: 10%"></td>
      <td style="text-align:center; border: 0.5px dashed #e4dfcd; width: 45%">
        <span style="color: #626262;">Request date</span><br />
        <span>'.$request->getSubmitDate().'</span><br />
        <span style="color: #626262;">Accepted</span><br />
        <span>'.$request->getAcceptDate().'</span><br />
        <span style="color: #626262;">Consultation</span><br />
        <span>'.$request->getConsultationDate().'</span>
      </td>
      </tr>
    </table>
  ';
  $pdf->writeHTML($html, true, false, true, false, '');

  //
  // User Profile
  //
  $html = '
    <table cellpadding="10">
      <tr style="background-color: #f0ede3;">
        <td style="margin-top: 100px;">
          <span style="font-size: larger;">USER PROFILE</span><br />&nbsp;<br />
          <table cellpadding="2">
            <tr>
              <td style="text-align: right; font-size: small; color: #626262; width: 20%">Surname, First name </td>
              <td style="width: 2%"></td>
              <td style="width: 78%">'.$user_name.'</td>
            </tr>
            <tr>
              <td style="text-align: right; font-size: small; color: #626262; width: 20%">RDP User </td>
              <td style="width: 2%"></td>
              <td style="width: 78%">'.$user_rdp.'</td>
            </tr>
            <tr>
              <td style="text-align: right; font-size: small; color: #626262; width: 20%">E-Mail </td>
              <td style="width: 2%"></td>
              <td style="width: 78%">'.$user_email.'</td>
            </tr>
            <tr>
              <td style="text-align: right; font-size: small; color: #626262; width: 20%">Phone </td>
              <td style="width: 2%"></td>
              <td style="width: 78%">'.$user_phone.'</td>
            </tr>
            <tr>
              <td style="text-align: right; font-size: small; color: #626262; width: 20%">Institute </td>
              <td style="width: 2%"></td>
              <td style="width: 78%">'.$user_institute.'</td>
            </tr>
            <tr>
              <td style="text-align: right; font-size: small; color: #626262; width: 20%">Research Group </td>
              <td style="width: 2%"></td>
              <td style="width: 78%">'.$user_researchgroup.'</td>
            </tr>
            <tr>
              <td style="text-align: right; font-size: small; color: #626262; width: 20%">SFB 1002 Project </td>
              <td style="width: 2%"></td>
              <td style="width: 78%">'.$user_subproject.'</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  ';
  $pdf->writeHTML($html, true, false, true, false, '');

  //
  //Imaging goal and scientific question
  //
  $html = '
    <table cellpadding="10">
      <tr>
        <td  style="border-bottom: 0.5px dashed #e4dfcd; border-left: 0.5px dashed #e4dfcd; border-right: 0.5px dashed #e4dfcd; text-align: justify"><span style="font-size: larger;">IMAGING GOAL AND SCIENTIFIC QUESTION</span><br />&nbsp;<br />
          <span style="font-family: sourcesansprolight">
            '.$imaging_goal.'
          </span>
        </td>
      </tr>
    </table>
  ';

  $pdf->writeHTML($html, true, false, true, false, '');

  //
  // Sample Information
  //

  $html = '
    <span style="font-size: larger;">SAMPLE INFORMATION</span><br />&nbsp;<br />
    <table cellpadding="3">
    <tr><td style="width: 40%">Sample Type</td><td>'.$img_sample_type.'</td></tr>
    <tr><td>Target Structure(s)</td><td>'.$img_target_structures.'</td></tr>
    <tr><td>Conditions</td><td>'.$img_conditions.'</td></tr>
    <tr><td>Staining Method</td><td>'.$img_staining_method.'</td></tr>
    <tr><td>Descriptive or quantitative data required</td><td>'.$img_data_required.'</td></tr>
    </table>
    ';

  $pdf->writeHTML($html, true, false, true, false, '');

  //
  // Requested Support and Services
  //
  $html = '
    <span style="font-size: larger;">REQUESTED SUPPORT AND SERVICES</span><br />
    <table cellpadding="3">
      <tr>
        <td>
          <h4>Imaging support</h4>
          '.$imaging_support_string.'
        </td>
        <td>
          <h4>Sample health</h4>
          '.$sample_health_string.'
          <h4>Analysis module</h4>
          '.$analysis_module_string.'
        </td>
      </tr>
      <tr>
        <td>
          <h4>Imaging module</h4>
          '.$imaging_module_string.'
        </td>
        <td>
          <h4>Introduction to ALM</h4>
          '.$introduction_alm_string.'
        </td>
      </tr>
    </table>
    ';

  $pdf->writeHTML($html, true, false, true, false, '');

  //
  // COMMENTS
  //

  $html = '
    <table cellpadding="10">
      <tr>
        <td  style="border-bottom: 0.5px dashed #e4dfcd; border-left: 0.5px dashed #e4dfcd; border-right: 0.5px dashed #e4dfcd; text-align: justify"><span style="font-size: larger;">COMMENTS</span><br />&nbsp;<br />
          <span style="font-family: sourcesansprolight">
            '.$comment_string.'
          </span>
        </td>
      </tr>
    </table>
  ';

  $pdf->writeHTML($html, true, false, true, false, '');

}

/**
 * Overrides TCPDF object containing request pdf.
 * Depending on request state adds consultation details (running, finished,
 * archived) or info message (edit, revision, pending, withdraw, deleted).
 *
 * @param $pdf \TCPDF
 * @param $request \ALMRequest
 */
function sfb_alm_download_pdf_add_consultation(&$pdf, $request) {

  $pdf->AddPage();
  $html = '<p style="font-size: larger;">CONSULTATION</p>';

  switch($request->getRequestState()) {
    case ALMRequestState::Edit:
      $html .= '<p>This request has not been submitted yet.</p>';
      break;
    case ALMRequestState::Revision:
      $html .= '<p>This request has not been submitted yet.</p>';
      break;
    case ALMRequestState::Pending:
      $html .= '<p>This request is waiting for S02 validation.</p>';
      break;
    case ALMRequestState::Withdraw:
      $html .= '<p>This request has been withdrawed by S02 team.</p>';
      break;
    case ALMRequestState::Accepted:
      $html .= '<p>This request is waiting for consultation</p>';
      break;
    case ALMRequestState::Running:
      $html .= sfb_alm_download_pdf_add_consultation_get_detailed_data($request);
      break;
    case ALMRequestState::Finished:
      $html .= sfb_alm_download_pdf_add_consultation_get_detailed_data($request);
      break;
    case ALMRequestState::Archived:
      $html .= sfb_alm_download_pdf_add_consultation_get_detailed_data($request);
      break;
    case ALMRequestState::Deleted:
      $html .= '<p>This request was deleted by the user</p>';
      break;
    default:
      watchdog('sfb_alm', 'Unknown request state. Please check the code of sfb_alm_download function.');
      $html .= '<p>Unknown request state. Please contact Research Data Platform administrators.</p>';
  }

  $pdf->writeHTML($html, true, false, true, false, '');
}

/**
 * Returns a html formatted string with consultation details.
 *
 * @param $request \ALMRequest
 * @return string HTML formatted string
 */
function sfb_alm_download_pdf_add_consultation_get_detailed_data($request) {

  // prepare consultation strings
  $consultation_attending = $request->getConsultationAttending();
  if(empty($consultation_attending))
    $consultation_attending = '-';

  $consultation_experiences = $request->getConsultationPreviousPracticalExperiences();
  if(empty($consultation_experiences))
    $consultation_experiences = '-';

  $consultation_imagingsupport = $request->getConsultationImagingSupport();
  if(empty($consultation_imagingsupport))
    $consultation_imagingsupport = '-';

  $consultation_samplehealth = $request->getConsultationSampleHealth();
  if(empty($consultation_samplehealth))
    $consultation_samplehealth = '-';

  $consultation_imagingmodules = $request->getConsultationImagingModule();
  if(empty($consultation_imagingmodules))
    $consultation_imagingmodules = '-';

  $consultation_analysismodules = $request->getConsultationAnalysisModule();
  if(empty($consultation_analysismodules))
    $consultation_analysismodules = '-';

  $html = '<p style=""><b>Attending:</b> '.$consultation_attending.'</p>';
  $html .= '<p style=""><b>Previous practical experiences in sample preparation and imaging:</b> '.$consultation_experiences.'</p>';
  $html .= '<p style=""><b>Imaging Support:</b> '.$consultation_imagingsupport.'</p>';
  $html .= '<p style=""><b>Sample health:</b> '.$consultation_samplehealth.'</p>';
  $html .= '<p style=""><b>Imaging Module(s):</b> '.$consultation_imagingmodules.'</p>';
  $html .= '<p style=""><b>Analysis Module(s):</b> '.$consultation_analysismodules.'</p>';

  $html .= '<p style=""><b>SINGLE IMMUNOFLUORESCENCES:</b></p>';
  $single_immunofluorescences = $request->loadAllSingleStainings();
  if(count($single_immunofluorescences) > 0) {
    foreach($single_immunofluorescences as $sif) {
      $html .= '
    <p><span style="font-size:11px"><b>Staining name: </b>'. $sif->getLabelName() . '</span><br>
    <table border="1" cellpadding="3">
      <tr>
        <td width="41%"><strong>Primary antibody</strong></td>
        <td width="9%"><strong>Dilution</strong></td>
        <td width="41%"><strong>Secondary antibody</strong></td>
        <td width="9%"><strong>Dilution</strong></td>
      </tr>';

      $html .= '
        <tr>
          <td>' . sfb_alm_download_pdf_create_antibody_cell($sif->getAntibodyDefinitions()[0]['primaryAntibodyId']) . '</td>
          <td>' . $sif->getAntibodyDefinitions()[0]['primaryAntibodyDilution'] . '</td>
          <td>' . sfb_alm_download_pdf_create_antibody_cell($sif->getAntibodyDefinitions()[0]['secondaryAntibodyId']) . '</td>
          <td>' . $sif->getAntibodyDefinitions()[0]['secondaryAntibodyDilution'] . '</td>
        </tr>
        <tr>
          <td>' . $sif->getAdditionalStainingDefinitions()[0]['primaryAdditionalStaining'] . '</td>
          <td>' . $sif->getAdditionalStainingDefinitions()[0]['primaryAdditionalStainingDilution'] . '</td>
          <td>' . $sif->getAdditionalStainingDefinitions()[0]['secondaryAdditionalStaining'] . '</td>
          <td>' . $sif->getAdditionalStainingDefinitions()[0]['secondaryAdditionalStainingDilution'] . '</td>
        </tr>
      ';
      $html .= '</table></p>';
    }
  } else {
    $html .= '-';
  }

  $html .= '<p style=""><b>DUAL IMMUNOFLUORESCENCES:</b></p>';
  $dual_immunofluorescences = $request->loadAllDualStainings();
  if(count($dual_immunofluorescences) > 0) {
    foreach($dual_immunofluorescences as $dif) {
    $html .= '
      <p><span style="font-size:11px"><b>Staining name: </b>'. $dif->getLabelName() . '</span><br>
      <table border="1" cellpadding="3">
        <tr>
          <td width="41%"><strong>Primary antibody</strong></td>
          <td width="9%"><strong>Dilution</strong></td>
          <td width="41%"><strong>Secondary antibody</strong></td>
          <td width="9%"><strong>Dilution</strong></td>
        </tr>';

      $html .= '
        <tr>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($dif->getAntibodyDefinitions()[0]['primaryAntibodyId']).'</td>
          <td>'.$dif->getAntibodyDefinitions()[0]['primaryAntibodyDilution'].'</td>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($dif->getAntibodyDefinitions()[0]['secondaryAntibodyId']).'</td>
          <td>'.$dif->getAntibodyDefinitions()[0]['secondaryAntibodyDilution'].'</td>
        </tr>
        <tr>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($dif->getAntibodyDefinitions()[1]['primaryAntibodyId']).'</td>
          <td>'.$dif->getAntibodyDefinitions()[1]['primaryAntibodyDilution'].'</td>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($dif->getAntibodyDefinitions()[1]['secondaryAntibodyId']).'</td>
          <td>'.$dif->getAntibodyDefinitions()[1]['secondaryAntibodyDilution'].'</td>
        </tr>
        <tr>
          <td>'.$dif->getAdditionalStainingDefinitions()[0]['primaryAdditionalStaining'].'</td>
          <td>'.$dif->getAdditionalStainingDefinitions()[0]['primaryAdditionalStainingDilution'].'</td>
          <td>'.$dif->getAdditionalStainingDefinitions()[0]['secondaryAdditionalStaining'].'</td>
          <td>'.$dif->getAdditionalStainingDefinitions()[0]['secondaryAdditionalStainingDilution'].'</td>
        </tr>
      ';
      $html .= '</table></p>';
    }
  } else {
    $html .= '-';
  }

  $html .= '<p style=""><b>MULTI IMMUNOFLUORESCENCES:</b></p>';
  $mutli_immunofluorescences = $request->loadAllMultiStainings();
  if(count($mutli_immunofluorescences) > 0) {
    foreach($mutli_immunofluorescences as $mif) {
    $html .= '
      <p><span style="font-size:11px"><b>Staining: </b>'. $mif->getLabelName() . '</span><br>
      <table border="1" cellpadding="3">
        <tr>
          <td width="41%"><strong>Primary antibody</strong></td>
          <td width="9%"><strong>Dilution</strong></td>
          <td width="41%"><strong>Secondary antibody</strong></td>
          <td width="9%"><strong>Dilution</strong></td>
        </tr>';
      $html .= '
        <tr>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($mif->getAntibodyDefinitions()[0]['primaryAntibodyId']).'</td>
          <td>'.$mif->getAntibodyDefinitions()[0]['primaryAntibodyDilution'].'</td>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($mif->getAntibodyDefinitions()[0]['secondaryAntibodyId']).'</td>
          <td>'.$mif->getAntibodyDefinitions()[0]['secondaryAntibodyDilution'].'</td>
        </tr>
        <tr>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($mif->getAntibodyDefinitions()[1]['primaryAntibodyId']).'</td>
          <td>'.$mif->getAntibodyDefinitions()[1]['primaryAntibodyDilution'].'</td>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($mif->getAntibodyDefinitions()[1]['secondaryAntibodyId']).'</td>
          <td>'.$mif->getAntibodyDefinitions()[1]['secondaryAntibodyDilution'].'</td>
        </tr>
        <tr>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($mif->getAntibodyDefinitions()[2]['primaryAntibodyId']).'</td>
          <td>'.$mif->getAntibodyDefinitions()[2]['primaryAntibodyDilution'].'</td>
          <td>'.sfb_alm_download_pdf_create_antibody_cell($mif->getAntibodyDefinitions()[2]['secondaryAntibodyId']).'</td>
          <td>'.$mif->getAntibodyDefinitions()[2]['secondaryAntibodyDilution'].'</td>
        </tr>
        <tr>
          <td>'.$mif->getAdditionalStainingDefinitions()[0]['primaryAdditionalStaining'].'</td>
          <td>'.$mif->getAdditionalStainingDefinitions()[0]['primaryAdditionalStainingDilution'].'</td>
          <td>'.$mif->getAdditionalStainingDefinitions()[0]['secondaryAdditionalStaining'].'</td>
          <td>'.$mif->getAdditionalStainingDefinitions()[0]['secondaryAdditionalStainingDilution'].'</td>
        </tr>
        <tr>
          <td>'.$mif->getAdditionalStainingDefinitions()[1]['primaryAdditionalStaining'].'</td>
          <td>'.$mif->getAdditionalStainingDefinitions()[1]['primaryAdditionalStainingDilution'].'</td>
          <td>'.$mif->getAdditionalStainingDefinitions()[1]['secondaryAdditionalStaining'].'</td>
          <td>'.$mif->getAdditionalStainingDefinitions()[1]['secondaryAdditionalStainingDilution'].'</td>
        </tr>
      ';
      $html .= '</table></p>';
    }

  } else {
    $html .= '-';
  }

  $html .= '<p style=""><b>SAMPLES:</b></p>';
  $samples = $request->loadAllSamples();
  if(count($samples)) {
    $i = 0;
    foreach($samples as $sample) {
      $html .= '
      <p style="font-size:13px">Sample: '. $sample->getLabelName() . '</p>
      <p><table border="1" cellpadding="3">
        <tr>
          <td width="10%"><strong>No.</strong></td>
          <td width="15%"><strong>Field</strong></td>
          <td width="55%"><strong>Description</strong></td>
          <td width="20%"><strong>Abbreviation</strong></td>
        </tr>';


      $html .= '
      <tr><td rowspan="9">' . $i . '</td><td>Species</td><td>' . $sample->getSpeciesDescription() . '</td><td>' . $sample->getSpeciesAbbreviation() . '</td></tr>
      <tr><td>ID</td><td>' . $sample->getSpeciesIdDescription() . '</td><td>' . $sample->getSpeciesIdAbbreviation() . '</td></tr>
      <tr><td>Type</td><td>' . $sample->getTypeDescription() . '</td><td>' . $sample->getTypeAbbreviation() . '</td></tr>
      <tr><td>Genotype</td><td>' . $sample->getGenotypeDescription() . '</td><td>' . $sample->getGenotypeAbbreviation() . '</td></tr>
      <tr><td>Treatment</td><td>' . $sample->getTreatmentDescription() . '</td><td>' . $sample->getTreatmentAbbreviation() . '</td></tr>
      <tr><td>Staining type</td><td>' . $sample->getStainingTypeDescription() . '</td><td>' . $sample->getStainingTypeAbbreviation() . '</td></tr>
      ';
      $i++;

      $html .= '</table></p>';
    }
  } else {
    $html .= '-';
  }

  return $html;
}

/**
 * creates a cell content for one antibody as HTML
 * First line: "Antibody Symbol"  "Antibody Name"
 * Second line: Link to Antibodycatalogue
 *
 * Exception: If the selected antobodyID ist not in the database: returns "Antibody not found"
 *
 * @param $antibodyId
 * @return string html
 */
function sfb_alm_download_pdf_create_antibody_cell($antibodyId){
  if(!isset($antibodyId)){
    return '-';
  }

  try {
    $antibody = ALMAntibodyAdapter::getALMAntibodyObjectById($antibodyId);

    /*
    $html_cell_content = '<b>Symbol: </b>' . $antibody->getAntigenSymbol() . '   ' .
    '<b>Name: </b>' . $antibody->getName() . '<br>' .
      '<span style="font-size: 0.7em; text-decoration: underline; font-family: sourcesansprolight">'.$antibody->getViewUrl().'</span><br />';
     */
    $antigenHTML =  '<b> Antigen Symbol: </b>'.$antibody->getAntigenSymbol();
    $nameHTML = '<b> Name: </b>'.$antibody->getName();
    // empty string if RaisedIn is not set
    $raisedInHTML = empty($antibody->getRaisedIn()) ? '' : '<b> Raised in: </b>'.$antibody->getRaisedIn();
    // empty string  if catalogNo is not set
    $catalogNoHTML = empty($antibody->getCatalogNo()) ? '' : '<b>Catalog No.: </b>'.$antibody->getCatalogNo();
    $antibodyLinkHTML = '<a href="' . $antibody->getViewUrl() . '" style="font-size: 8px;" >'.$antibody->getViewUrl().'</a>';

    $html_cell_content =
      // line 1
      $antigenHTML.' '.$nameHTML.'<br>'.
      // line 2
      $raisedInHTML.' '.$catalogNoHTML.'<br>'.
      // line 3
      $antibodyLinkHTML;

    return $html_cell_content;

  } catch (NoAntibodyFoundException $e){
    drupal_set_message($e->getMessage(), 'warning');
    return 'Antibody not found!';
  }
}

/**
 * Overrides TCPDF object containing request pdf.
 *
 * @param $pdf \TCPDF
 * @param $request \ALMRequest
 */
function sfb_alm_download_pdf_add_technical_data(&$pdf, $request) {
  $pdf->AddPage();

  //TODO: request state wird derzeit als number dargestellt, da im ALMRequestState die Funktion getStateName fehlt... IMPLEMENTIEREN!
  $html = '
    <p><span style="font-size: larger;">TECHNICAL DETAILS</span></p>
    <p><b>Report created: </b> '.date('Y-m-d H:i:s').' </p>
    <p><b>Current state: </b>'.$request->getRequestState().'</p>
        ';

  $html .= '<table border="1" cellpadding="3">';
  $html .= '<tr>
        <td>Date</td>
        <td>Action</td>
        <td>Message</td>
        <td>User</td>
    </tr>';
  foreach($request->getLogs() as $log) {
    $html .= '
        <tr>
            <td>'.$log->getDate().'</td>
            <td>'.$log->getAction().'</td>
            <td><span style="font-family: sourcesansprolight">'.$log->getMessage().'</span></td>
            <td>'.$log->getUserName().'</td>
        </tr>';
  }
  $html .= '</table>';

  $pdf->writeHTML($html, true, false, true, false, '');
}
