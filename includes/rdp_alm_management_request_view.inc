<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 18.05.2019
 * Time: 23:03
 */

/**
 * Implements hook_title_callback()
 *
 * @param int $alm_request_id
 * @return string
 */
function sfb_alm_management_request_view_title_callback($alm_request_id = 0) {
  $request = ALMRequestsRepository::findById($alm_request_id);
  $request_display_name = 'unknown';

  if (!$request->isEmpty()) {
    $request_display_name = 'umg-sfb1002-alm-request-' . $request->getRequestID();
  }

  return 'Manage Request ' . $alm_request_id;
}

function rdp_alm_management_request_view_form($form, &$form_state, $request_id = null) {
  $page = new AlmRequestViewManagementPage();
  $form = $page->getPageForm($form, $form_state, $request_id);

  return $form;
}

//todo rename the ajax functions? (add the _management_ part)

/*
 * Ajax callback function for adding or deleting label rows
 * Function reloads the whole label_table fieldset
 */
function rdp_alm_coverslip_labels_create_ajax_links_callback($form, $form_state) {
  $page = new AlmRequestViewManagementPage();
  return $page->coverslipLabelsAjaxCallback($form, $form_state);
}

/**
 * (Ajax) Submit function for adding a new row for labels
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_coverslip_label_create_ajax_add($form, &$form_state) {
  $page = new AlmRequestViewManagementPage();
  $page->coverslipLabelAjaxAdd($form, $form_state);
}

/**
 * (Ajax) Submit function for deleting a row of the label table
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_coverslip_label_create_ajax_delete($form, &$form_state) {
  $page = new AlmRequestViewManagementPage();
  $page->CoverslipLabelAjaxDelete($form, $form_state);
}

/**
 * (Ajax) Submit function for deleting a row of the label table
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_coverslip_label_create_ajax_hide($form, &$form_state) {
  $page = new AlmRequestViewManagementPage();
  $page->coverslipLabelAjaxHide($form, $form_state);
}

/**
 * Creates for all selected values the template for the label printer
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_coverslip_label_create_submit_print($form, &$form_state) {
  $page = new AlmRequestViewManagementPage();
  $page->coverslipLabelSubmitPrint($form, $form_state);
}