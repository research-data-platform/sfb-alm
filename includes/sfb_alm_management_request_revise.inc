<?php
/**
 * User: Henke_Christian
 * Date: 08.08.2016
 * Time: 11:29
 */

define('ALM_REQUEST_REVISE_CPOMMENT','alm_request_revise_comment');

/**
 * @param null $form
 * @param $form_state
 */
function sfb_alm_management_request_revise($form = null, &$form_state, $requestID = null){

  $request = ALMRequestsRepository::findById($requestID);

  $form_state[ALM_REQUEST_ID] = $request->getRequestID();

  $form['fieldset-revise_comment'] = array(
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-user"></i> ' . t('Review Comment'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-revise_comment'][ALM_REQUEST_REVISE_CPOMMENT] = array(
    '#type' => 'textarea',
    '#default_value' => '',
    '#field_prefix' => '<div class="row"><div class="col-lg-8">',
    '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
  );

  // cancel this form
  $form['btn-cancel'] = array(
    '#type' => 'markup',
    '#markup' => '<a href="'. sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW,  $form_state[ALM_REQUEST_ID]) .'" class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-arrow-left"></i> Cancel</a>',
  );

  $form['btn-submit'] = array(
      '#type' => 'submit',
      '#value' => t('<i class="fa fa-retweet"></i>'.' Revise request'),
      '#description' => t('Revise the request and sends an email to the user!'),
      //Todo button färben
    );

  return $form;
}

function sfb_alm_management_request_revise_submit($form = null, &$form_state){

  global $user;

  $requestID = $form_state[ALM_REQUEST_ID];
  $request = ALMRequestsRepository::findById($requestID);
  //Check the permission
  if (!$request->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_EDIT)) {
    drupal_set_message('Error: A problem occured by processing request object. Please reload the page. (Code: ' . SFB_ALM_ERROR_REVISE_REQUEST_SUBMIT . ')', 'error');
    drupal_access_denied();
    return;
  }
  // Set requestState and reviseDate and reset the acceptDate
  $request->setReviseAttributes();

  //Update the Revise Comment
  $comment = $request->getCommentWithType(ALMCommentType::ReviseEdit);
  $comment->setMessage($form_state['values'][ALM_REQUEST_REVISE_CPOMMENT]);
  $comment->setCurrentDate();

  // Sets the CommentType to Submit because than it isn be possible to edit the comment again.
  $comment->setTypeID(ALMCommentType::Revise);

  //Set the updated user Comment to the updatedComment Variable, so the next save invoke will store it to the DB
  $request->setUpdatedComment($comment);

  // save request in database
  $request->save();


  drupal_set_message('The request was revised');

  // send notification email
  sfb_alm_mail_send_request_notification($request, 'revised', $form_state['values'][ALM_REQUEST_REVISE_CPOMMENT], $user->uid);

  $form_state['redirect'] = sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST);
}
