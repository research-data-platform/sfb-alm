<?php
/**
 * Page callback that triggers download of all files from an ALMDataset
 * (CDSTAR package) as a zip archive
 *
 * @param int $request_id
 * @param int $dataset_id
 */
function rdp_alm_request_dataset_download($request_id, $dataset_id) {
  $dataset = ALMDatasetRepository::findById($dataset_id);

  //Access when:
  // a) The User is archive owner
  // b) The User is part of the research group and the sharing lvl is group
  // c) The archive is public

  // Check access permission
  if (!$dataset->checkDownloadPermission()) {
    drupal_goto(sfb_alm_url(RDP_ALM_URL_REQUEST_LANDING, $request_id));
  }

  $object = CdstarObjectRepository::findById($dataset->getCdstarObjectId());
  $object->exportZipArchive();
}