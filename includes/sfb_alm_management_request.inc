<?php
/**
 * @file
 * Main management page for Service02 team members.
 *
 * @author Christian Henke (christian.henke@stud.uni-goettingen.de)
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 */

/**
 * @return string
 */
function sfb_alm_management_request() {

  if (!user_access(SFB_ALM_PERMISSION_MANAGE)) {
    drupal_access_denied();
    exit();
  }


  // The user variable will be use to check if the request is new for the user
  global $user;

  /**
   * Request filter
   */

  // Check if the requests variable in the URL is set and if the value is a request state
  $limit_of_requests_per_page = variable_get('alm_request_limit', 25);
  if (isset($_GET['requests']) && ALMRequestState::isRequestState($_GET['requests'])) {
    $requests = ALMRequestsRepository::findManageableRequestsByRequestStateAddPageLimit($_GET['requests'], $limit_of_requests_per_page);
    $visible_requests = t(ALMRequestState::getRequestStateName($_GET['requests']) . ' requests');
  }
  else {
    // Default value: all requests
    // Default value is used if no requestState is selected:
    // get all requests with following state: pending, accept, running finished
    $requests = ALMRequestsRepository::findAllManageableRequestsAddPageLimit($limit_of_requests_per_page);
    $visible_requests = t('All current requests');
  }

  // Select Field for request filter
  $show_options = [
    '<a href="' . '?requests=All' . '">' . t('All current requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Pending . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Pending) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Accepted . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Accepted) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Running . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Running) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Edit . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Edit) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Revision . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Revision) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Withdraw . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Withdraw) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Finished . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Finished) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Deleted . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Deleted) . ' requests') . '</a>',
  ];

  $view_select_field = '<div style="padding: 5px 5px 10px 5px">Filter: Show ' . alm_dashboard_dropdown_menu_html_items([
      'items' => $show_options,
      'selected_item' => $visible_requests,
    ]) . '</div>';

  // table header
  $header = [
    'Request ID',
    'Submitted date',
    'Accepted date',
    'Researcher',
    'Request State',
    'Quick Action',
    'Action',
    '',
  ];

  // table rows
  $rows = [];
  foreach ($requests as $request) {
    //Check if a manager has opened the request already by searching for a log entry with ALMLogRepository::ACTION_MANAGER_VIEW.
    $is_new_request = sfb_alm_management_request_is_new_request($request->getRequestID());

    /**
     * Highlight overdue requests (i.e. running more than 6 months, not status is not archived, deleted, withdrawn, or finished)
     */
    $request_submit_date = $request->getSubmitDate() == '' ? 'Not submitted yet!' : $request->getSubmitDate();
    $tz = new DateTimeZone(variable_get('date_default_timezone', 'Europe/Berlin'));
    if ($submit_date = new DateTime($request_submit_date, $tz)) {
      $now = new DateTime('now', $tz);
      $diff = $now->diff($submit_date);
      $status_whitelist = [
        ALMRequestState::Finished,
        ALMRequestState::Deleted,
        ALMRequestState::Archived,
        ALMRequestState::Withdraw,
      ];
      if ($diff->m + ($diff->y *12) > 6 && !in_array($request->getRequestState(), $status_whitelist)) {
        $request_submit_date .= '&nbsp;<span class="badge btn-danger"><span class="glyphicon glyphicon-time"></span> overdue</span>';
      }
    }


    $rows[] = [
      'data' => [
        // Request ID:
        // Add a "new" label, if the request was not viewed by a ALM manager
        $is_new_request ? l($request->getRequestID(), sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $request->getRequestID())) . ' <span class="label label-default">new</span>' :
          l($request->getRequestID(), sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $request->getRequestID())),
        // Request date
        $request_submit_date,
        // Accept date:
        $request->getElementAcceptDate(),
        // Researcher:
        $request->getUserFullname() . ' ' . $request->getElementResearchgroupIcon(),
        // Request State:
        ALMRequestState::getRequestStateIcon($request->getRequestState()),
        // Action:
        afb_alm_management_request_get_state_action($request),
        // Dropdown:
        sfb_alm_management_request_get_dropdown($request),
        // Documents
        '<a href="' . sfb_alm_url(RDP_ALM_URL_DOWNLOAD_PDF_REQUEST, $request->getRequestID()) . '" data-toggle="tooltip" title="Request form PDF"><span class="fa fa-file"></span></a>',
      ],
      // If the request is a new request, display the line bold.
      'style' => $is_new_request ? 'font-weight: bold;' : '',
    ];
  }

  // print message if there are no requests
  if (count($rows) == 0) {
    $rows[] = [
      'data' => [['data' => 'No requests', 'colspan' => 8]],
    ];
  }

  $box_header = ['title' => 'Overview'];

  $introduction_image = '';
  if(!empty(variable_get('alm_request_manager_overview_introduction_image', ''))) {
    $image_path = base_path() . drupal_get_path('module', 'sfb_alm') . '/' . variable_get('alm_request_manager_overview_introduction_image', '');
    $introduction_image =  '<p><img src="' . $image_path . '" style="float:left; padding:20px;" alt="Advanced Light Microscopy" /> </p>';
  }
  $introduction_text = variable_get('alm_request_manager_overview_introduction_text', '');

  $box_body = [
    'data' =>
        $introduction_image.
      $introduction_text .'
        <p style="clear: both"></p>' .
      $view_select_field .
      theme('table', ['header' => $header, 'rows' => $rows]),
    'class' => ['table-responsive', 'no-padding'],
  ];
  $box_footer = ['data' => ''];

  return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]) . theme('pager', ['tags' => []]);
}

/**
 * Check if the request with request_id has been viewed by any alm manager
 * since the last submit.
 *
 * Returns true if the last submit date is greater than last manager view date
 * or if last manager view is not set.
 *
 * @param $request_id
 *
 * @return bool
 */
function sfb_alm_management_request_is_new_request($request_id) {

  // Load the date of the last submit and the date of the last manager view from the Log Database
  $last_submit_date = ALMLogRepository::getLatestActionDateByRequestID($request_id, ALMLog::$ACTION_SUBMIT);
  $last_manager_view_date = ALMLogRepository::getLatestActionDateByRequestID($request_id, ALMLog::$ACTION_MANAGER_VIEW);

  if ($last_submit_date == ALMLogRepository::EMPTY_DATE) {
    watchdog('sfb_alm', 'Request ' . $request_id . ' without submit date detected');
  }

  // First check if the last view date is Empty_Date (-1) this is the case
  // when the request never has been seen by a manager
  if ($last_manager_view_date == ALMLogRepository::EMPTY_DATE) {
    return TRUE;
  }

  // Then check if the last manager view is newer then the last submit (if
  // the request was revised, the view date can be older then the submit date)
  if (strtotime($last_submit_date) - strtotime($last_manager_view_date) > 0) {
    return TRUE;
  }

  // else: The request was viewed by manager -> return false:
  return FALSE;
}


/**
 * @param $request ALMRequest
 *
 * @return string
 */
function afb_alm_management_request_get_state_action($request) {
  // define the quick action links
  $dropdown_view = l('View', sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $request->getRequestID()));
  $dropdown_consult = '<a href=' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $request->getRequestID()) . '?active=1>Consult request</a>';

  // select the qucik action depending the request state
  switch ($request->getRequestState()) {
    // State Pending
    case ALMRequestState::Pending:
      return $dropdown_view;
    // State Accepted
    case ALMRequestState::Accepted:
      return $dropdown_consult;
    // State Running
    case ALMRequestState::Running:
      return $dropdown_view;
    // All other states. Currently no other state should be visible
    default:
      return '';
  }
}

function sfb_alm_management_request_get_dropdown($request) {
  $requestID = $request->getRequestID();

  // Declare the links for the option buttons
  $dropdown_accept = '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_ACCEPT, $requestID) . '">Accept request</a>';
  $dropdown_revise = '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_REVISE, $requestID) . '">Revise request</a>';
  $dropdown_view = '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $requestID) . '">View request</a>';
  $dropdown_edit = '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $requestID) . '">Edit request</a>';
  $dropdown_download = '<a href="' . sfb_alm_url(RDP_ALM_URL_DOWNLOAD_PDF_REQUEST, $requestID) . '">Download PDF</a>';
  $dropdown_consult = '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $requestID) . '">Consult request</a>';
  $dropdown_withdraw = '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_WITHDRAW, $requestID) . '">Withdraw request</a>';
  $dropdown_close = '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_CLOSE, $requestID) . '">Close request</a>';


  // select the visible option-buttons depending on the request state
  switch ($request->getRequestState()) {
    // State Pending
    case ALMRequestState::Pending:
      // add the Buttons as HTML-link to the option array
      $options = [
        $dropdown_edit,
        $dropdown_view,
        $dropdown_download,
        $dropdown_accept,
        $dropdown_revise,
        $dropdown_close,
      ];
      break;
    // State Accepted
    case ALMRequestState::Accepted:
      // add the Buttons as HTML-link to the option array
      $options = [
        $dropdown_view,
        $dropdown_download,
        $dropdown_consult,
        $dropdown_revise,
        $dropdown_withdraw,
        $dropdown_close,
      ];
      break;
    // Inactive states
    case ALMRequestState::Finished:
    case ALMRequestState::Deleted:
    case ALMRequestState::Archived:
      $options = [
        $dropdown_view,
        $dropdown_download,
      ];
      break;
    // State running (the other states are not visible currently)
    default:
      // add the Buttons as HTML-link to the option array
      $options = [
        $dropdown_view,
        $dropdown_download,
        $dropdown_close,
      ];
      break;
  }

  // Theme the option array to a HTML Button with Dropdown menu
  return alm_dashboard_dropdown_menu_html_items(['items' => $options]);
}

/**
 * Implement hook_form
 *
 * @param null $form
 * @param $form_state
 *
 * @return null
 */
function sfb_alm_management_request_dropdown_delete_form($form = NULL, &$form_state) {
  $form['button'] = [
    '#type' => 'submit',
    '#title' => t('delete_btn'),
    '#value' => t('Delete request'),
    '#attributes' => [
      'class' => ['btn-link'],
      'style' => [
        'color: grey;',
        'width: 100%;',
        'text-align: left;',
        'padding: 0px 0px 3px 20px;',
      ],
    ],
    '#submit' => ['sfb_alm_management_request_dropdown_delete_form_submit'],
  ];

  return $form;
}

/**
 * Implement hook_form
 *
 * @param null $form
 * @param $form_state
 *
 * @return null
 */
function sfb_alm_management_request_dropdown_delete_form_submit($form, &$form_state) {
  $requestID = $form_state['build_info']['args'][0];
  $request = ALMRequestsRepository::findById($requestID);
  $request->setRequestState(ALMRequestState::Deleted);
  ALMRequestsRepository::save($request);

  drupal_set_message('Deleted request ' . $requestID);
}


