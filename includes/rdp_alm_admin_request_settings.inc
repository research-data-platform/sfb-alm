<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 25.11.2019
 * Time: 15:00
 */

/**
 *
 * @return array
 */
function rdp_alm_admin_request_settings_form($form, $form_state){

  $form['fieldset-imaging_support'] = rdp_alm_admin_request_settings_get_select_option_table_fieldset('alm_imaging_support','Imaging Support');
  $form['fieldset-imaging_modules'] = rdp_alm_admin_request_settings_get_select_option_table_fieldset('alm_imaging_modules','Imaging Modules');
  $form['fieldset-analysis_modules'] = rdp_alm_admin_request_settings_get_select_option_table_fieldset('alm_analysis_modules','Analysis Modules');
  $form['fieldset-introduction_to_alm'] = rdp_alm_admin_request_settings_get_select_option_table_fieldset('alm_introduction_to_alm','Introduction to ALM');

  return $form;
}

/**
 * Creates a renderable fieldset for all options of a selectField
 * @param $selectFieldName
 * @param $title
 * @return array
 */
function rdp_alm_admin_request_settings_get_select_option_table_fieldset($selectFieldName, $title){
  $selectOptions = new ALMSelectOptions($selectFieldName, AlmDefaultValues::DefaultSelectOptionsByName($selectFieldName));
  $select_Option_view = new AlmSelectOptionsAdminView($selectOptions);
  $table_fieldset = [
    '#type' => 'fieldset',
    '#title' => t($title),
  ];
  $htmlAddLink = l(
    t('Add'),
    sfb_alm_url(RDP_ALM_URL_CONFIG_REQUEST_SETTINGS_EDIT_SELECT_OPTION, $selectFieldName, 0),
    array(
      'attributes' => array(
        'class' => 'btn btn-xs btn-primary',
        'style' => 'float:right',
    )));

  $table_fieldset[$selectFieldName] = [
    '#markup' => $select_Option_view->getViewTable().$htmlAddLink,
  ];

  return $table_fieldset;
}

/**
 * for editing existing or creating a new select option
 * @param array $form
 * @param $form_state
 * @param $fieldName String Name of the select Field (This is NOT the text of the option)
 * @param $id int ID of the option in the selectField with the $fieldName
 * @return array
 */
function rdp_alm_admin_request_settings_edit_form($form = [], &$form_state, $fieldName, $id = 0) {
  $form_state['name'] = $fieldName;
  $form_state['id'] = $id;

  $selectOptions = new ALMSelectOptions($fieldName, AlmDefaultValues::DefaultSelectOptionsByName($fieldName));

  $form['option_text'] = [
    '#type' => 'textfield',
    '#title' => t('Text of the Option'),
    // If the id does not exists, it will be a new option
    '#default_value' => $selectOptions->existsId($id) ? $selectOptions->getAllOptions()[$id] : '',
  ];
  $form['status'] = [
    '#description' => 'Status',
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#prefix' => t('Status:'),
    // If the id does not exists, it will be a new option
    '#default_value' => $selectOptions->existsId($id) ? $selectOptions->isEnabled($id) : 0,
  ];

  $form['back-btn'] = [
    '#type' => 'submit',
    '#value' => t('Back'),
    '#submit' => ['rdp_alm_admin_request_settings_edit_submit_back'],
  ];
  $form['save_settings-btn'] = [
    '#type' => 'submit',
    '#value' => t('Save Settings'),
    '#submit' => ['rdp_alm_admin_request_settings_edit_submit'],
  ];

  return $form;
}

/**
 * Submit function for editing existing or creating a new select option
 * @param $form
 * @param $form_state
 */
function rdp_alm_admin_request_settings_edit_submit($form, &$form_state){
  $fieldName = $form_state['name'];
  $optionId = $form_state['id'];

  $optionText = $form_state['values']['option_text'];
  $optionStatus = $form_state['values']['status'];

  $selectOptions = new ALMSelectOptions($fieldName, AlmDefaultValues::DefaultSelectOptionsByName($fieldName));
  if ($selectOptions->existsId($optionId)){
    // edit option
    $selectOptions->setOptionName($optionId, $optionText);
    $selectOptions->setStatus($optionId, $optionStatus);

    drupal_set_message(t('Select Option "' . $optionText . '" of "' . $fieldName . '" was editabled successfully'));
  } else{
    // add new option
    $selectOptions->addOption($optionText, $optionStatus);

    drupal_set_message(t('New Select Option "' . $optionText . '" was added to "' . $fieldName . '"'));
  }

  $selectOptions->storeOptions();

  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_CONFIG_REQUEST_SETTINGS)];
}

function rdp_alm_admin_request_settings_edit_submit_back($form, &$form_state){
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_CONFIG_REQUEST_SETTINGS)];
}
