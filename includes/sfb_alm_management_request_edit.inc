<?php
/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 13.07.2016
 * Time: 13:46
 */

// test comment

define('ALM_MANAGEMENT_REQUEST_ADD_SINGLE_STAINING', 1);
define('ALM_MANAGEMENT_REQUEST_ADD_DUAL_STAINING', 2);
define('ALM_MANAGEMENT_REQUEST_ADD_MULTI_STAINING', 3);

function sfb_alm_management_request_edit_title_callback($alm_request_id = 0) {
  return 'Manage Request ' . $alm_request_id . ' (edit/consult)';
}


function sfb_alm_management_request_edit_form($form, &$form_state, $requestID = null) {
  $requestObj = ALMRequestsRepository::findById($requestID);
  $form_state[ALM_REQUEST_ID] = $requestID;

  // TODO: anti concurrent editing with force @Christian: KOMMENTAR NICHT LÖSCHEN!

  //
  // Check access
  //
  if ($requestObj->getRequestID() == ALMRequest::EMPTY_REQUEST_ID) {
    drupal_not_found();
    return;
  }
  if (!$requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_EDIT)) {
    drupal_access_denied();
    exit();
  }


  // if form_state['input'] ist NOT empty, the request gets only reloaded (e.g. after saving or validation)

  if($requestObj->isLocked() && !isset($_GET['unlock']) && empty($form_state['input'])){
    //Todo Auslageern in js File
    $locked_user_name = UsersRepository::findByUid($requestObj->getLockedBy())->getFullname();

    $form['block-dialog'] = sfb_alm_managment_request_edit_locked_dialog($locked_user_name);
  } else{
    $requestObj->lock();
  }

  // Create a (hidden) log entry, that the manager view the request.
  sfb_alm_create_log_entry(ALMLog::$ACTION_MANAGER_VIEW, "View request", $requestID, 0);

  //If a new staining or Sample is added, the page should scroll to the old position. By submitting, the old position is saved in the scrolLTo parameter in the URL
  if(isset($_GET['scrollTo'])){
    drupal_add_js('jQuery(document).ready(function () {
     window.scrollTo(0,' . $_GET['scrollTo'] . ');
     })', 'inline');
  }



//
  $form['tabs'] = array(
    '#tree' => TRUE,
    '#theme' => 'sfb_alm_dashboard_tab_form',
    '#active' => isset($_GET['active']) ? $_GET['active'] : 0,
  );

  $form['tabs']['User Request']['fieldset-userprofile'] = array(
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-user"></i> ' . t('User Profile'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['scrollPos'] = array(
    '#type' => 'hidden',
    '#default_value' => '0',
  );

  $form['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_USER] = $requestObj->getFormFieldUserAccountName();
  $form['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_NAME] = $requestObj->getFormFieldUserFullname();
  $form['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_POSITION] = $requestObj->getFormFieldPosition();
  $form['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_INSTITUTE] = $requestObj->getFormFieldInstitute();
  $form['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_RESEARCHGROUP] = $requestObj->getFormFieldResearchgroup();
  $form['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_SUBPROJECT] = $requestObj->getFormFieldSubprojects();
  $form['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_EMAIL] = $requestObj->getFormFieldEmail();
  $form['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_PHONE] = $requestObj->getFormFieldPhoneNumber();

  //
  // Imaging Goal and Scientific Question
  //

  $form['tabs']['User Request']['fieldset-imaging_goal_and_scientific_question'] = array(
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-lightbulb-o"></i> ' . t('Imaging Goal and Scientific Question'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => 'Please describe here the imaging goal and the scientific question',
  );

  $form['tabs']['User Request']['fieldset-imaging_goal_and_scientific_question'][ALM_REQUEST_IMAGING_GOAL_AND_SCIENTIFIC_QUESTION] = $requestObj->getFormFieldImagingGoal();

  // 
  // Sample Information
  //
  $form['tabs']['User Request']['fieldset-sample_information'] = array(
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-info"></i> ' . t('Sample Information'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    //'#description' => 'Please describe here the imaging goal and the scientific question',
  );

  $form['tabs']['User Request']['fieldset-sample_information'][ALM_REQUEST_SAMPLE_TYPE] = $requestObj->getFormFieldSampleType();
  $form['tabs']['User Request']['fieldset-sample_information'][ALM_REQUEST_SAMPLE_TARGET_STRUCTURE] = $requestObj->getFormFieldTargetStructure();
  $form['tabs']['User Request']['fieldset-sample_information'][ALM_REQUEST_SAMPLE_CONDITIONS] = $requestObj->getFormFieldSampleConditions();
  $form['tabs']['User Request']['fieldset-sample_information'][ALM_REQUEST_SAMPLE_STAINING_METHOD] = $requestObj->getFormFieldStainingMethod();
  $form['tabs']['User Request']['fieldset-sample_information'][ALM_REQUEST_SAMPLE_DESCRIPTIVE_OR_QUANTITATIVE_DATA_REQUIRED] = $requestObj->getFormFieldDataRequired();


  //
  // Requested Support and Services
  //

  $form['tabs']['User Request']['fieldset-requested_support_and_services'] = array(
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-edit"></i> ' . t('Requested Support and Service'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    //'#description' => 'Please describe here the imaging goal and the scientific question',
  );

  //todo (Nov.2019) Changed to one column because of scaling problems if the number of checkboxes are varyin
  //The markup elements generates a view with 3 rows and two colums
  // The markup elements generates a view with 3 rows and two colums
  //$form['tabs']['User Request']['fieldset-requested_support_and_services']['row_1_begin'] = array('#markup' => '<div class="row"><div class="col-md-4">',);
  $form['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_IMAGING_SUPPORT] = $requestObj->getFormFieldImagingSupport();
  //$form['tabs']['User Request']['fieldset-requested_support_and_services']['row_1_column_2'] = array('#markup' => '</div><div class="col-md-4">',);
  $form['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_INTRODUCTION_TO_ALM] = $requestObj->getFormFieldIntroductionToAlm();
  //$form['tabs']['User Request']['fieldset-requested_support_and_services']['row_1_end_row_2_begin'] = array('#markup' => '</div></div><div class="row"><div class="col-md-4">',);
  $form['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_IMAGING_MODULE] = $requestObj->getFormFieldImagingModule();
  //$form['tabs']['User Request']['fieldset-requested_support_and_services']['row_2_column_2'] = array('#markup' => '</div><div class="col-md-4">',);
  $form['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_ANALYSIS_MODULE] = $requestObj->getFormFieldAnalysisModule();
  //$form['tabs']['User Request']['fieldset-requested_support_and_services']['row_2_end'] = array('#markup' => '</div></div>',);
  $form['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_SAMPLE_HEALTH] = $requestObj->getFormFieldSampleHealth();


  if($requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_CONSULT)) {
    //
    // CONSULTATION TAB
    //
    $consultationForms = new ALMConsultationEditForms($requestObj);

    //
    // Fieldset General
    //
    $form['tabs']['Consultation']['fieldset-general'] = $consultationForms->getFieldsetGeneral();


    $form['tabs']['Consultation']['fieldset-stainings'] = array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-flask"></i> ' . t('Documentation of immunofluorescence stainings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    //
    // Single Staining Table
    //

    $form['tabs']['Consultation']['fieldset-stainings']['fieldset-single_staining'] = $consultationForms->getFieldsetSingleStaining('sfb_alm_management_request_edit_submit_add_single_staining');

    //
    // Dual IF Staining Table
    //

    $form['tabs']['Consultation']['fieldset-stainings']['fieldset-dual_staining'] = $consultationForms->getFieldsetDualStaining('sfb_alm_management_request_edit_submit_add_dual_staining');


    //
    // Multi IF Staining Table
    //

    $form['tabs']['Consultation']['fieldset-stainings']['fieldset-multi_staining'] = $consultationForms->getFieldsetMultiStaining('sfb_alm_management_request_edit_submit_add_multi_staining');
    //
    // Sample Documentation
    //

    $form['tabs']['Consultation']['fieldset-sample_documentation'] = $consultationForms->getFieldsetSampleDocumentation('sfb_alm_management_request_edit_submit_add_sample_documentation');
  }
  
  // Button cancel
  $form['btn-cancel'] = array(
    '#type' => 'submit',
    '#value' => t(' Cancel'),
    '#attributes' => array('class' => array('btn-default'),),
    '#submit' => array('sfb_alm_management_request_edit_submit_cancel'),
  );

  $form['btn-save'] = array(
    '#type' => 'submit',
    '#value' => t(' Save'),
    '#description' => t('Save the request'),
    '#attributes' => array('class' => array('btn-default'),),
    '#submit' => array('sfb_alm_management_request_edit_submit_save'),
  );

  $form['btn-continue'] = array(
    '#type' => 'submit',
    '#value' => t(' Save and continue'),
    '#description' => t('Save the request and stay on the edit page'),
    '#attributes' => array('class' => array('btn-default'),),
    '#submit' => array('sfb_alm_management_request_edit_submit_continue'),
  );

  if ($requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_CONSULT)) {
    $form['confirm-dialog'] = sfb_alm_management_request_edit_confirm_dialog();

    $form['btn-run'] = array(
      '#type' => 'submit',
      '#value' => '<i class="fa fa-thumbs-o-up"></i> ' . t(' Finish consultation'),
      '#description' => t('Finalize the consultation and set the request state to run'),
      '#attributes' => array('class' => array('btn-lg', 'btn-primary'), 'id' => 'btn-run', 'style' => array('float: right')),
      '#submit' => array('sfb_alm_management_request_edit_submit_run'),
      '#validate' => array('sfb_alm_management_request_edit_submit_run_validate'),

    );
  }
  if ($requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_ACCEPT)) {
    $form['btn-accept'] = array(
      '#type' => 'markup',
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_ACCEPT, $requestID) .
        '" class="btn btn-primary btn-lg" style="margin-right:5px; float: right; " ><span class="fa fa-thumbs-o-up"></span> Accept request</a>',
    );
  }
  //
  // COMMENTS AND LOG TAB
  //

  $commentsAndLogTab = new AlmCommentsAndLogsTab($requestObj);
  $form['tabs']['Comments and Log'] = $commentsAndLogTab->getForm();


  // fixes the format of the select2 (antibody) fields
  drupal_add_js('jQuery(document).ready(function() {
	jQuery(".select2").css("width", "100%");
});', 'inline');


  return $form;
}

/**
 * Returns a drupal_form array for the confirm dialog which is shown when the user tries to open an blocked request
 * (This dialog is will be shown automatically if this is in the code, it does not depend where this code is inserted)
 * @param $locked_user_name
 * @return array
 */
function sfb_alm_managment_request_edit_locked_dialog($locked_user_name){
  $locked_dialog = new RequestBlockedDialog();

  $dialogBody = '
      
      <h4>The request is currently locked. Do you want to unlock the request?</h4>
      <h4 class="small">The request is locked by: '. $locked_user_name .'</h4>
      <div class="panel panel-danger">
        <div class="panel-heading">
          <h3 class="panel-title">Attention:</h3> 
        </div>
        <div class="panel-body">
          <p>If you open the request several times at the same time, it can happen that some data gets overwritten.</p>
        </div>
      </div>
      <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" href="#lock-explanation">
          <h3 class="panel-title">Why do I get this notification?</h3> 
        </div>
        <div class="panel-body collapse" id="lock-explanation">
          <p>If you open a request in a browser tab (e.g. a Firefox tab), the system locks the request to ensure that 
          you don\'t open the request in another tab or on another device at the same time a second time. 
          This should prevent lose of data that can happen if you first save the request on one device and then save an 
          old version on another device.</p>
          <p><strong>If you don\'t close the request properly (with the "Save" or "Cancel"-Button) the system doesn\'t recognize that you 
          closed the request and cannot unlock the request for the next usage!<strong></p>
        </div>
      </div>
    ';

  $locked_dialog->setBody($dialogBody);
  $locked_dialog->setAnswTextYes('Unlock');

  return [
    '#type' => 'markup',
    '#markup' => $locked_dialog->getDialogAsHtml(),
  ];
}

/**
 * returns a drupal_form array for the confirm dialog of the submit button
 * (This content is normally not visible, that's why it does not depend where this code is inserted)
 * @return array
 */
function sfb_alm_management_request_edit_confirm_dialog(){
  $confirm_dialog =  new ConfirmDialog('btn-run');
  $confirm_dialog->setAnswTextYes('Continue');
  $confirm_dialog->setBody('
<h4>Are you sure that you want to finish the consultation?</h4>
<h4 class="small">This sets the request to running. The request and consultation data will be fixed then.<br>
<strong>Hint: To avoid lose of unsaved antibody information, ensure that no "short label name" field is empty</strong></h4>
<div class="panel panel-warning">
  <div class="panel-heading">
  <h3 class="panel-title">Attention:</h3> 
  </div>
  <div class="panel-body">
    <p>If you finish the consultation, this cannot be undone. Once the consultation is finished it will not be possible to edit any consultation data anymore!</p>
  </div>
</div>
    ');


  return [
    '#type' => 'markup',
    '#markup' => $confirm_dialog->getDialogAsHtml(),
  ];
}


/**
 * Removes a staining. The stainingId is collected from the '#name' field of the triggering element
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_submit_remove_staining($form, &$form_state){
  sfb_alm_management_request_save_all($form_state);

  // The name of the trigger event is the unique staining id
  //todo Check permission --> is the id really from the currently opend request and is the user allready allowed to delet it?
  ALMRequestStainingRepository::deleteById($form_state['triggering_element']['#name']);
  
  $scrollPosition = $form_state['values']['scrollPos'];
  $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1&scrollTo=' . $scrollPosition);
}

function rdp_alm_request_submit_remove_sample($form, &$form_state){
  sfb_alm_management_request_save_all($form_state);

  // The name of the trigger event is the unique Sample id
  //todo Check permission --> is the id really from the currently opend request and is the user allready allowed to delet it?
  ALMRequestSampleRepository::deleteById($form_state['triggering_element']['#name']);

  $scrollPosition = $form_state['values']['scrollPos'];
  $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1&scrollTo=' . $scrollPosition);
}

function rdp_alm_request_submit_copy_sample($form, &$form_state){
  // save the scroll position
  $scrollPosition = $form_state['values']['scrollPos'];

  try {
    sfb_alm_management_request_save_all($form_state);

    // Add new Sample
    $sample = ALMRequestSampleRepository::findBySampleId($form_state['triggering_element']['#name']);
    $sample->setEditable(true);
    $sample->assignNewSerialNumber();
    $sample->resetDatabaseId();
    $sample->save();

    $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1&scrollTo=' . $scrollPosition);
  } catch (WrongAntibodyInputException $e) {
    $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1');
  }
}

/**
 * Creates a new Staining (depending on $form_state['add_staining']) and returns the object
 * @param $form_state
 * @return ALMRequestStaining
 */
function sfb_alm_management_request_create_added_staining($form_state) {
  $requestID = $form_state[ALM_REQUEST_ID];

  $staining = new ALMRequestStaining();
  $staining->setRequestId($requestID);
  // Decide the type of the new IF
  switch ($form_state['add_staining']) {
    case ALM_MANAGEMENT_REQUEST_ADD_SINGLE_STAINING:
      $staining->setType(ALMRequestStaining::TYPE_SINGLE);
      break;
    case ALM_MANAGEMENT_REQUEST_ADD_DUAL_STAINING:
      $staining->setType(ALMRequestStaining::TYPE_DUAL);
      break;
    case ALM_MANAGEMENT_REQUEST_ADD_MULTI_STAINING:
      $staining->setType(ALMRequestStaining::TYPE_MULTI);
      break;
  }
  // Initial the staining rows (depending the staining type) to prevent "missing index errors"
  $staining->initialDefaultStainingRows();
  $staining->setEditable(true);

  return $staining;
}

/**
 * Saves the whole request in the database
 * @param $form_state
 * @throws WrongAntibodyInputException - One or more antibody inputs does not exist in the database. The wrong inputs are NOT saved.
 * @return bool
 */
function sfb_alm_management_request_save_all($form_state) {

  $requestID = $form_state[ALM_REQUEST_ID];
  $requestObj = ALMRequestsRepository::findById($requestID);

  //Check the permission
  if (!$requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_EDIT)) {
    drupal_set_message('Error: A problem occured by processing request staining_object. Please reload the page. (Code: ' . SFB_ALM_ERROR_EDIT_REQUEST_SAVE . ')', 'error');
    drupal_access_denied();
    return false;
  }


  if($requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_CONSULT)) {


    // generate a ALMConsultationEditForms object to save all values to the associated request Object (requestObj)
    $consultation_edit_forms_obj = new ALMConsultationEditForms($requestObj);

    // save the consultation values to the requestObj
    $consultation_edit_forms_obj->collectDataFromFieldsetGeneral($form_state['values']['tabs']['Consultation']['fieldset-general']);

    //save the staining values to the requestObj
    $consultation_edit_forms_obj->collectDataFromFieldsetSingleStaining($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-single_staining']);
    $consultation_edit_forms_obj->collectDataFromFieldsetDualStaining($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-dual_staining']);
    $consultation_edit_forms_obj->collectDataFromFieldsetMultiStaining($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-multi_staining']);

    // save the sample values to the requestObj
    $consultation_edit_forms_obj->collectDataFromFieldsetSample($form_state['values']['tabs']['Consultation']['fieldset-sample_documentation']);

    // Add a new staining if the add_staining variable is set.
    if(isset($form_state['add_staining'])){
      $requestObj->addStaining(sfb_alm_management_request_create_added_staining($form_state));
    }
  }

  //save the user request values
  $requestObj->setPosition($form_state['values']['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_POSITION]);
  $requestObj->setInstitut($form_state['values']['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_INSTITUTE]);
  $requestObj->setSubprojectID($form_state['values']['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_SUBPROJECT]);
  $requestObj->setResearchGroupID($form_state['values']['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_RESEARCHGROUP]);
  $requestObj->setEmail($form_state['values']['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_EMAIL]);
  $requestObj->setPhoneNumber($form_state['values']['tabs']['User Request']['fieldset-userprofile'][ALM_REQUEST_USER_PHONE]);
  $requestObj->setImagingGoal($form_state['values']['tabs']['User Request']['fieldset-imaging_goal_and_scientific_question'][ALM_REQUEST_IMAGING_GOAL_AND_SCIENTIFIC_QUESTION]);
  $requestObj->setSampleType($form_state['values']['tabs']['User Request']['fieldset-sample_information'] [ALM_REQUEST_SAMPLE_TYPE]);
  $requestObj->setTargetStructure($form_state['values']['tabs']['User Request']['fieldset-sample_information'] [ALM_REQUEST_SAMPLE_TARGET_STRUCTURE]);
  $requestObj->setConditions($form_state['values']['tabs']['User Request']['fieldset-sample_information'] [ALM_REQUEST_SAMPLE_CONDITIONS]);
  $requestObj->setStainingMethod($form_state['values']['tabs']['User Request']['fieldset-sample_information'] [ALM_REQUEST_SAMPLE_STAINING_METHOD]);
  $requestObj->setDataRequired($form_state['values']['tabs']['User Request']['fieldset-sample_information'] [ALM_REQUEST_SAMPLE_DESCRIPTIVE_OR_QUANTITATIVE_DATA_REQUIRED]);
  $requestObj->setImagingSupportIDs($form_state['values']['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_IMAGING_SUPPORT]);
  $requestObj->setSampleHealthID($form_state['values']['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_SAMPLE_HEALTH]);
  $requestObj->setImagingModuleIDs($form_state['values']['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_IMAGING_MODULE]);
  $requestObj->setAnalysisModuleIDs($form_state['values']['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_ANALYSIS_MODULE]);
  $requestObj->setIntroductionToAlmIDs($form_state['values']['tabs']['User Request']['fieldset-requested_support_and_services'][ALM_REQUEST_INTRODUCTION_TO_ALM]);

  // save whole request Object in the database
  $requestObj->save();
  // reload the requeste before unlock because sometimes it is a problem when the request is saved 2 times.
  $requestObj = ALMRequestsRepository::findById($requestID);
  $requestObj->unlock();


  // Check if all Antibodylinks are correct.
  if($requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_CONSULT)) {
    if (!sfb_alm_management_request_edit_check_antibodies($form_state)) {
      drupal_set_message('All correct values of Request ' . $requestObj->getRequestID() . ' were saved succefully', 'warning');
      throw new WrongAntibodyInputException('The selected Antibody is not in the database');
    }
  }

  drupal_set_message('Request '.$requestObj->getRequestID().' saved succefully');

}

/**
 * Checks, if the linked-antibodies are existing in the database.
 * @return true if ALL linked Antibodies existing in the database. Otherwise false.
 */
function sfb_alm_management_request_edit_check_antibodies($form_state)
{
  // Check single stainings
  $single_staining_tables = sfb_alm_management_request_edit_get_staining_tables_from_fieldset_stainings($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-single_staining']);
  $single_staining_antibodies_valid = sfb_alm_management_request_edit_check_antibodies_of_staining_type_fieldset($single_staining_tables, ALMRequestStaining::TYPE_SINGLE);

  // Check dual stainings
  $dual_staining_tables = sfb_alm_management_request_edit_get_staining_tables_from_fieldset_stainings($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-dual_staining']);
  $dual_staining_antibodies_valid = sfb_alm_management_request_edit_check_antibodies_of_staining_type_fieldset($dual_staining_tables, ALMRequestStaining::TYPE_DUAL);

  // Check multi stainings
  $multi_staining_tables = sfb_alm_management_request_edit_get_staining_tables_from_fieldset_stainings($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-multi_staining']);
  $multi_staining_antibodies_valid = sfb_alm_management_request_edit_check_antibodies_of_staining_type_fieldset($multi_staining_tables, ALMRequestStaining::TYPE_MULTI);

  if($single_staining_antibodies_valid && $dual_staining_antibodies_valid && $multi_staining_antibodies_valid){
    return true;
  }
  else{
    return false;
  }

  return $all_antibodies_valid;
}

function sfb_alm_management_request_edit_check_antibodies_of_staining_type_fieldset($staining_type_fieldset, $staining_type)
{
  $all_antibodies_valid = true;

  foreach ($staining_type_fieldset as $staining_fieldset) {
    $table_is_valid = sfb_alm_management_request_edit_check_staining_table($staining_fieldset, $staining_type);
    if (!$table_is_valid) {
      $all_antibodies_valid = false;
    }

  }
  return $all_antibodies_valid;
}

  /**
   * Extract an array only with (all) staining tables of a request. (For example without the pre_table_form)
   * @param $fieldset_stainings
   * @return
   */
  function sfb_alm_management_request_edit_get_staining_tables_from_fieldset_stainings($fieldset_stainings)
  {
    $staining_tables = array();
    $element_counter = 0;
    while (isset($fieldset_stainings['view_staining_table' . $element_counter])) {
      $staining_tables[$element_counter] = $fieldset_stainings['view_staining_table' . $element_counter];
      unset($staining_tables[$element_counter]['pre_table_form']);
      $element_counter++;
    }
    return $staining_tables;
  }

  function sfb_alm_management_request_edit_check_staining_table($staining_table, $staining_type)
  {
    $valid = true;

    $numberOfAntibodies = 0;
    // Depend on the Stainig Type, the number of antibodies in a staining table is variable.
    switch ($staining_type){
      case ALMRequestStaining::TYPE_SINGLE:
        $numberOfAntibodies = 1;
        break;
      case ALMRequestStaining::TYPE_DUAL:
        $numberOfAntibodies = 2;
        break;
      case ALMRequestStaining::TYPE_MULTI:
        $numberOfAntibodies = 3;
    }

    for ($index = 0; $index < $numberOfAntibodies; $index++) {
      $staining_row = $staining_table[$index];

      // The select2 field returns a associative array which contains the selected antibody id as key and value.
      // The reset function returns the first value if there is one, otherwise it returns false
      $prim_antibody_id = reset($staining_row['prim_anti']);
      $sec_antibody_id = reset($staining_row['sec_anti']);

      // If the antibodyField is empty, $prim_antibody_id will be false. Then it is not necessary to check if the id exists
      if ($prim_antibody_id && !ALMAntibodyAdapter::existsAntibdyId($prim_antibody_id)) {
        drupal_set_message(t('Wrong antibody input was detected: ' . $prim_antibody_id . ' <br> The false antibody was deleted. Please check your input.'), 'warning');
        $valid = false;
      }
      if ($sec_antibody_id && !ALMAntibodyAdapter::existsAntibdyId($sec_antibody_id)) {
        drupal_set_message(t('Wrong antibody input was detected: ' . $sec_antibody_id . ' <br> The false antibody was deleted. Please check your input.'), 'warning');
        $valid = false;
      }
    }

    return $valid;
  }

  function sfb_alm_management_request_edit_submit_add_single_staining($form, &$form_state)
  {
    // When the add_staining varible is set, the next save_all invoke will create a new immunofluorescence field. The value of the variable gives the type of the new IF
    $form_state['add_staining'] = ALM_MANAGEMENT_REQUEST_ADD_SINGLE_STAINING;
    $scrollPosition = $form_state['values']['scrollPos'];
    try {
      sfb_alm_management_request_save_all($form_state);
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1&scrollTo=' . $scrollPosition);
    } catch (WrongAntibodyInputException $e) {
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1');
    }
  }

  function sfb_alm_management_request_edit_submit_add_dual_staining($form, &$form_state)
  {
    // When the add_staining varible is set, the next save_all invoke will create a new immunofluorescence field. The value of the variable gives the type of the new IF
    $form_state['add_staining'] = ALM_MANAGEMENT_REQUEST_ADD_DUAL_STAINING;
    $scrollPosition = $form_state['values']['scrollPos'];

    try {
      sfb_alm_management_request_save_all($form_state);
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1&scrollTo=' . $scrollPosition);
    } catch (WrongAntibodyInputException $e) {
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1');
    }
  }

  function sfb_alm_management_request_edit_submit_add_multi_staining($form, &$form_state)
  {
    // When the add_staining varible is set, the next save_all invoke will create a new immunofluorescence field. The value of the variable gives the type of the new IF
    $form_state['add_staining'] = ALM_MANAGEMENT_REQUEST_ADD_MULTI_STAINING;
    $scrollPosition = $form_state['values']['scrollPos'];

    try {
      sfb_alm_management_request_save_all($form_state);
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1&scrollTo=' . $scrollPosition);
    } catch (WrongAntibodyInputException $e) {
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1');
    }
  }

  function sfb_alm_management_request_edit_submit_add_sample_documentation($form, &$form_state)
  {
    // save the scroll position
    $scrollPosition = $form_state['values']['scrollPos'];

    try {
      sfb_alm_management_request_save_all($form_state);

      // Add new Sample
      $sample = new ALMRequestSample();
      $sample->setRequestID($form_state[ALM_REQUEST_ID]);
      $sample->setEditable(true);
      $sample->assignSerialNumber();
      $sample->save();

      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1&scrollTo=' . $scrollPosition);
    } catch (WrongAntibodyInputException $e) {
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1');
    }
  }

  function sfb_alm_management_request_edit_submit_continue($form, &$form_state)
  {
    try {
      sfb_alm_management_request_save_all($form_state);
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]));
    } catch (WrongAntibodyInputException $e) {
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1');
    }

  }

  function sfb_alm_management_request_edit_submit_cancel($form, &$form_state)
  {
    //unlock the request
    ALMRequestsRepository::findById($form_state[ALM_REQUEST_ID])->unlock();
    $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $form_state[ALM_REQUEST_ID]));
  }

  function sfb_alm_management_request_edit_submit_save($form, &$form_state)
  {
    try {
      sfb_alm_management_request_save_all($form_state);
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $form_state[ALM_REQUEST_ID]));
    } catch (WrongAntibodyInputException $e) {
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1');
    }

  }

/**
 * Check that all Labels has a label name before the request can set to run
 * @param $form
 * @param $form_state
 */
function sfb_alm_management_request_edit_submit_run_validate($form, &$form_state){
  // Check that every Staining has a label name

  // single stainings
  foreach ($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-single_staining'] as $id => $staining_table) {
    // First Check if the $staining_table variable is an array (to sort the buttons out)
    // Second check if the short_label field contains any text, if not set an form error.
    if(is_array($staining_table) &&  empty($staining_table['pre_table_form']['short_label'])){
      $element_id = 'tabs][Consultation][fieldset-stainings][fieldset-single_staining]['.$id.'][pre_table_form][short_label';
      form_set_error($element_id, 'Please enter a name for the "short label name field" in the consultation form <b>and check your Antibodies!</b>');
    }
  }

  // dual stainings
  foreach ($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-dual_staining'] as $id => $staining_table) {
    // First Check if the $staining_table variable is an array (to sort the buttons out)
    // Second check if the short_label field contains any text, if not set an form error.
    if(is_array($staining_table) &&  empty($staining_table['pre_table_form']['short_label'])){
      $element_id = 'tabs][Consultation][fieldset-stainings][fieldset-dual_staining]['.$id.'][pre_table_form][short_label';
      form_set_error($element_id, 'Please enter a name for the "short label name field" in the consultation form');
    }
  }

  // multi stainings
  foreach ($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-multi_staining'] as $id => $staining_table) {
    // First Check if the $staining_table variable is an array (to sort the buttons out)
    // Second check if the short_label field contains any text, if not set an form error.
    if(is_array($staining_table) &&  empty($staining_table['pre_table_form']['short_label'])){
      $element_id = 'tabs][Consultation][fieldset-stainings][fieldset-multi_staining]['.$id.'][pre_table_form][short_label';
      form_set_error($element_id, 'Please enter a name for the "short label name field" in the consultation form');
    }
  }
}

  /**
   * Tries to submit the request and change the state to running.
   * If the save function throws a WrongAntibodyInputException, the consultation will not be finished
   * @param $form
   * @param $form_state
   * @return bool
   */
  function sfb_alm_management_request_edit_submit_run($form, &$form_state)
  {

    try {
      sfb_alm_management_request_save_all($form_state);

      $requestID = $form_state[ALM_REQUEST_ID];
      $request = ALMRequestsRepository::findById($requestID);

      // Check permission
      if (!sfb_alm_management_request_edit_check_permission_consult($request)) {
        return false;
      }

      $request->setRunAttributes();
      $request->save();
      drupal_set_message('Consultation of request ' . $request->getRequestID() . ' was finished');

      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST, $form_state[ALM_REQUEST_ID]));
    } catch (WrongAntibodyInputException $e) {
      drupal_set_message(t('The consultation is NOT finish now!'), 'warning');
      $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1');
    }
  }

  function sfb_alm_management_request_edit_check_permission_consult($request)
  {
    // Check permission
    if (!$request->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_CONSULT)) {
      drupal_set_message('Error: A problem occured by processing request staining_object. Please reload the page. (Code: ' . SFB_ALM_ERROR_EDIT_REQUEST_SUBMIT . ')', 'error');
      drupal_access_denied();
      return false;
    }
    return true;
  }

  class WrongAntibodyInputException extends Exception
  {
  }


