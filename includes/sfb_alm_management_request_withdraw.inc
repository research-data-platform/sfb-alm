<?php
/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 10.08.2016
 * Time: 17:33
 */

define('ALM_REQUEST_WITHDRAW_CPOMMENT','alm_request_withdraw_comment');

/**
 * @param null $form
 * @param $form_state
 */
function sfb_alm_management_request_withdraw($form = null, &$form_state, $requestID = null){

  $request = ALMRequestsRepository::findById($requestID);

  $form_state[ALM_REQUEST_ID] = $request->getRequestID();

  $form['fieldset-withdraw_comment'] = array(
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-user"></i> ' . t('Withdraw Comment'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-withdraw_comment'][ALM_REQUEST_WITHDRAW_CPOMMENT] = array(
    '#type' => 'textarea',
    '#default_value' => '',
    '#field_prefix' => '<div class="row"><div class="col-lg-8">',
    '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
  );

  // cancel this form
  $form['btn-cancel'] = array(
    '#type' => 'markup',
    '#markup' => '<a href="'. sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW,  $form_state[ALM_REQUEST_ID]) .'" class="btn btn-default" style = "margin-right: 5px"><i class="fa fa-arrow-left"></i> Cancel</a>',
  );

  $form['btn-submit'] = array(
    '#type' => 'submit',
    '#value' => t('<i class="fa fa-remove"></i>'.' Withdraw request'),
    '#description' => t('Withdraws the request and sends an email to the user!'),
  );

  return $form;
}

function sfb_alm_management_request_withdraw_submit($form = null, &$form_state){
  
  $requestID = $form_state[ALM_REQUEST_ID];
  $request = ALMRequestsRepository::findById($requestID);

  //Check the permission
  if (!$request->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_WITHDRAW)) {
    drupal_set_message('Error: A problem occured by processing request object. Please reload the page. (Code: ' . SFB_ALM_ERROR_WITHDRAW_REQUEST_SUBMIT . ')', 'error');
    drupal_access_denied();
    return;
  }
  
  $request->setWithdrawAttributes();

  //Update the Withdraw Comment
  $comment = $request->getCommentWithType(ALMCommentType::WithdrawEdit);
  $comment->setMessage($form_state['values'][ALM_REQUEST_WITHDRAW_CPOMMENT]);
  $comment->setCurrentDate();

  // Sets the CommentType to Submit because than it isn be possible to edit the comment again.
  $comment->setTypeID(ALMCommentType::Withdraw);

  //Set the updated user Comment to the updatedComment Variable, so the next save invoke will store it to the DB
  $request->setUpdatedComment($comment);

  // save request in database
  $request->save();
  //TODO Send mail

  drupal_set_message('The request withdrawed successfully');
  $form_state['redirect'] = sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST);
}