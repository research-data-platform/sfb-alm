<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 08.01.2019
 * Time: 20:47
 *
 * Description: Creates a page that allows the user to add a sample to a running request
 */

function rdp_alm_request_view_add_sample_form($form = null, &$form_state, $request_id = null){
  $page = new AlmAddSamplePage();
  $form = $page->getPageForm($form = null, $form_state, $request_id);
  return $form;

}

/**
 * Closes the "new staining" view and redirected back to the request overview
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_view_add_sample_cancel($form, &$form_state){
  $page = new AlmAddSamplePage();
  $page->cancelSubmitFunction($form, $form_state);
}

/**
 * Creates a new staining with the user input and save it (linked to the request) in the db.
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_view_add_sample_submit($form, &$form_state){
  $page = new AlmAddSamplePage();
  $page->addSubmitFunction($form, $form_state);
}

