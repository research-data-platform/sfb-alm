<?php

/**
 * Page callback: ALM Persistent Identifier settings
 *
 * @see sfb_alm_menu()
 *
 * @return mixed
 */
function sfb_alm_admin_pid() {

  if (module_exists("epic_pid")) {
    // Show only "manual generation" services as options
    $services = EpicPidServiceRepository::findAllByMethod(EpicPidService::MANUAL);
    $options = array();
    foreach ($services as $service) {
      $options[$service->getId()] = $service->getName();
    }

    if(empty($options)){
      $options [] = 'No PID Service was defined';
    }

    $form['alm_coverslip_label_pid_service'] = [
      '#type' => 'select',
      '#title' => t('EPIC PID Service for ALM coverslip labels'),
      '#description' => t('Choose the EPIC PID service for coverslip PIDs.'),
      '#options' => $options,
      '#default_value' => variable_get("alm_coverslip_label_pid_service"),
      '#required' => TRUE,
    ];
  }

  return system_settings_form($form);

}

/**
 * Display healthy / unhealthy PIDs (i.e. PID value in Coverslip-Label table
 * exists but PID Object of class EpicPid is not registered.
 *
 * Allow batch-repair of unhealthy PIDs.
 *
 * ToDo
 *
 * @return string
 */
function sfb_alm_admin_pid_status() {

  $labels = ALMRequestCoverslipLabelRepository::findAllPid();

  $header = ["Request ID", "Sample ID", "Coverslip ID", "PID", "EPIC PID ID"];
  $data = [];

  $pidPrefix = variable_get("alm_coverslip_label_pid_prefix");
  //  $pidPrefix = "alm01-";

  foreach ($labels as $label) {

    $epicPid = EpicPidRepository::findById($pidPrefix . $label->getPid());

    if ($epicPid->isEmpty()) {
      $link = l("Register",
        RDP_ALM_URL_CONFIG_PID . '/register/' . $label->getDbId());
      $pid = 'N/A';
    }
    else {
      $link = $epicPid->getUrl();
      $pid = $epicPid->getPid();
    }

    $data[] = [
      $label->getRequestId(),
      $label->getSampleId(),
      $label->getCoverslipNo(),
      $label->getPid(),
      $pid,
      $link,
    ];
  }
  try {
    $output = "<h2>Coverslip PIDs in ALM Module</h2>";
    $output .= theme('table', ['header' => $header, 'rows' => $data]);
  } catch (Exception $e) {
    watchdog_exception('ALM', $e);
  }

  return $output;

}

/**
 * Register PID for a coverslip label.
 *
 * @param $label_id
 *
 * @throws \Exception
 */
function sfb_alm_admin_pid_register($label_id) {
  $label = ALMRequestCoverslipLabelRepository::findById($label_id);
  $label->createPid();

  // Redirect
  drupal_goto(RDP_ALM_URL_CONFIG_PID_STATUS);
}