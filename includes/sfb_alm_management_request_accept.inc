<?php
/**
 * @file
 * Form used for request accepting.
 *
 * @author Christian Henke (christian.henke@stud.uni-goettingen.de)
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 */

define('ALM_MANAGEMENT_REQUEST_ACCEPT_COMMENT','alm_management_request_accept_comment');

/**
 * @param null $form
 * @param $form_state
 * @param int $requestID
 * @return null|void form array
 */
function sfb_alm_management_request_accept($form = null, &$form_state, $requestID = null){

  $request = ALMRequestsRepository::findById($requestID);

  //Check the permission
  if (!$request->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_ACCEPT)) {
    drupal_access_denied();
    return;
  }

  $form_state[ALM_REQUEST_ID] = $request->getRequestID();

  $form['fieldset-accept_comment'] = array(
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-user"></i> ' . t('Manager comment'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-accept_comment'][ALM_MANAGEMENT_REQUEST_ACCEPT_COMMENT] = array(
    '#type' => 'textarea',
    '#default_value' => '',
    '#field_prefix' => '<div class="row"><div class="col-lg-8">',
    '#field_suffix' => '</div><div class="col-lg-4"><span class="badge"></span></div></div>',//TODO: field description fehlt
  );

  //determine the user qualification level
  $user_quali_lvl = ALMUserQualificationRepository::getQualificationLevelOfUserId($request->getUserID());
  $cur_user_opt  = 0;
  if ($user_quali_lvl == ALMUserQualificationRepository::$QUALIFICATION_LEVEL_USER_CONSULTATION){
    $cur_user_opt = 1;
  }

  $form['qualification_level'] = array(
    '#type' => 'radios',
    '#options' => array(t('ALM-Manager'), t('User')),
    '#title' => t('Consultation type:'),
    '#default_value' => $cur_user_opt,
    '#description' => t('With this radio button, you can give the user direct access to the consultation formular. Without an ALM-Manager'),
    '#field_suffix' => t('Attention: This setting is a <b>global user setting </b>which has effect for all requests of the user (including past requests). 
    If you choose "User", the user will be able to consult all his accepted requests without a manager'),

  );

  // cancel this form
  $form['btn-cancel'] = array(
    '#type' => 'markup',
    '#markup' => '<a href="'. sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW,  $form_state[ALM_REQUEST_ID]) .'" class="btn btn-default" style="margin-right:5px"><i class="fa fa-arrow-left"></i> Cancel</a>',
  );

  $form['btn-submit'] = array(
    '#type' => 'submit',
    '#value' => t('<i class="fa fa-thumbs-o-up"></i>'.' Accept request'),
    '#description' => t('Accepts the request and sends an email to the user!'),
    '#attributes' => array('class' => array('btn-success')),
  );

  return $form;
}

function sfb_alm_management_request_accept_submit($form = null, &$form_state){

  global $user;

  $request = ALMRequestsRepository::findById($form_state[ALM_REQUEST_ID]);

  //Check the permission
  if (!$request->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_ACCEPT)) {
    drupal_set_message('Error: A problem occured by processing request object. (Code: ' . SFB_ALM_ERROR_ACCEPT_REQUEST_SUBMIT . ')', 'error');
    drupal_access_denied();
    return;
  }
  
  //sets requestState and acceptDate 
  $request->setAcceptAttributes();

  //Update the Accept Comment
  $comment = $request->getCommentWithType(ALMCommentType::AcceptEdit);
  $comment->setMessage($form_state['values'][ALM_MANAGEMENT_REQUEST_ACCEPT_COMMENT]);
  $comment->setCurrentDate();

  // Sets the CommentType to Submit because than it isn be possible to edit the comment again.
  $comment->setTypeID(ALMCommentType::Accepted);

  //Set the updated user Comment to the updatedComment Variable, so the next save invoke will store it to the DB
  $request->setUpdatedComment($comment);

  // save request in database
  $request->save();

  //determine the user qualification level
  if($form_state['values']['qualification_level'] == 0){
    ALMUserQualificationRepository::setQualificationLevelOfUserId($request->getUserID(), ALMUserQualificationRepository::$QUALIFICATION_LEVEL_MANAGER_CONSULTATION);
  } else if($form_state['values']['qualification_level'] == 1){
    ALMUserQualificationRepository::setQualificationLevelOfUserId($request->getUserID(), ALMUserQualificationRepository::$QUALIFICATION_LEVEL_USER_CONSULTATION);
  }

  // add drupal message
  drupal_set_message('Request '.l($request->getRequestID(), sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $request->getRequestID())).' accepted.');

  // send notification email
  sfb_alm_mail_send_request_notification($request, 'accepted', $form_state['values'][ALM_MANAGEMENT_REQUEST_ACCEPT_COMMENT], $user->uid);

  // redirect to requests overview
  $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST));
}