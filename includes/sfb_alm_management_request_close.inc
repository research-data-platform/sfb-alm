<?php
/**
 * @file
 * Form used for closing requests (set status to "finished".
 *
 * @author Markus Suhr (markus.suhr@med.uni-goettingen.de)
 */

define('ALM_MANAGEMENT_REQUEST_CLOSE_COMMENT', 'alm_management_request_close_comment');

/**
 * @param null $form
 * @param $form_state
 * @param int $requestID
 *
 * @return null|void form array
 */
function sfb_alm_management_request_close($form = NULL, &$form_state, $requestID = NULL) {

  $request = ALMRequestsRepository::findById($requestID);

  //Check the permission
  // ToDo: dedicated close permission?
  if (!$request->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_CLOSE)) {
    drupal_access_denied();
  }

  $form_state[ALM_REQUEST_ID] = $request->getRequestID();

  $form['fieldset-close_comment'] = [
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-user"></i> ' . t('Manager comment'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['fieldset-close_comment'][ALM_MANAGEMENT_REQUEST_CLOSE_COMMENT] = [
    '#type' => 'textarea',
    '#default_value' => '',
    '#field_prefix' => '<div class="row"><div class="col-lg-8">',
    '#field_suffix' => '</div><div class="col-lg-4"><span class="badge">
        </span></div></div>',//TODO: field description fehlt
  ];

  // cancel this form
  $form['btn-cancel'] = [
    '#type' => 'markup',
    '#markup' => '<a href="' .
      sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $form_state[ALM_REQUEST_ID]) .
      '" class="btn btn-default" style="margin-right:5px">
            <i class="fa fa-arrow-left"></i> Cancel</a>',
  ];

  $form['btn-submit'] = [
    '#type' => 'submit',
    '#value' => t('<i class="fa fa-thumbs-o-up"></i>' . ' Close request'),
    '#description' => t('Close request'),
    '#attributes' => ['class' => ['btn-success']],
  ];

  return $form;
}

function sfb_alm_management_request_close_submit($form = NULL, &$form_state) {

  global $user;

  $request = ALMRequestsRepository::findById($form_state[ALM_REQUEST_ID]);

  //Check the permission
  if (!$request->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_CLOSE)) {
    drupal_set_message('Error: A problem occured by processing request object. (Code: ' . SFB_ALM_ERROR_ACCEPT_REQUEST_SUBMIT . ')', 'error');
    drupal_access_denied();
  }

  //sets requestState and acceptDate 
  $request->setFinishedAttributes();

  //Update the Accept Comment
  $comment = $request->getCommentWithType(ALMCommentType::CloseEdit);
  $comment->setMessage($form_state['values'][ALM_MANAGEMENT_REQUEST_CLOSE_COMMENT]);
  $comment->setCurrentDate();

  // Sets the CommentType to Submit because than it isn be possible to edit the comment again.
  $comment->setTypeID(ALMCommentType::CloseEdit);

  //Set the updated user Comment to the updatedComment Variable, so the next save invoke will store it to the DB
  $request->setUpdatedComment($comment);

  // save request in database
  $request->save();

  // add drupal message
  drupal_set_message('Request ' .
    l($request->getRequestID(),
      sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $request->getRequestID())) .
    ' closed.');

  // send notification email
  sfb_alm_mail_send_request_notification($request, 'closed', $form_state['values'][ALM_MANAGEMENT_REQUEST_CLOSE_COMMENT], $user->uid);

  // redirect to requests overview
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST)];
}