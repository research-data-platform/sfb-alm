<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 05.06.2018
 * Time: 21:09
 */


function rdp_alm_request_dataset_upload_form($form = [], &$form_state, $request_id = NULL) {

  $request = ALMRequestsRepository::findById($request_id);
  $user = User::getCurrent();

  $request_status_blacklist = [
    ALMRequestState::Archived,
    ALMRequestState::Deleted,
    ALMRequestState::Finished,
    ALMRequestState::Withdraw,
  ];
  if (in_array($request->getRequestState(), $request_status_blacklist)) {
    drupal_set_message("Permission to upload a new dataset for the request is denied.", "error");
    watchdog("ALM Module", "Permission to upload a new dataset for 
    the request is denied. Request ID: %req_id, Status: %req_state",
      [
        '%req_id' => $request_id,
        '%req_state' => ALMRequestState::getRequestStateName($request->getRequestState()),
      ], WATCHDOG_NOTICE);
    drupal_goto(sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request_id));
  }

  $form_renderer = new ALMDatasetUploadForms($request);
  try {
    $form = $form_renderer->getFormUpload();
  } catch (NoUserArchiveFoundException $e) {
    // Log the exception
    watchdog_exception(__CLASS__, $e);
    // Display an error message
    drupal_set_message($e->getMessage(), 'error');
    // Redirect to default display page for Request
    drupal_goto(sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request_id));
  }

  $archive = ArchiveRepository::findAllPersonalArchivesByUser($user->getUid())[0];
  $archive_id = $archive->getId();

  /**
   * Set initial parameters.
   */
  $form_state['archive_id'] = $archive_id;
  $form_state['request_id'] = $request_id;

  return $form;
}

function rdp_alm_request_dataset_upload_form_validate($form, &$form_state) {

  /**
   * Class method handles validation of file upload field.
   */
  ALMDataset::formCreateUploadValidate();
}

function rdp_alm_request_dataset_upload_form_submit($form, &$form_state) {

  /**
   * ToDo: better way to select user personal archive?
   */
  global $user;
  $archive = ArchiveRepository::findAllPersonalArchivesByUser($user->uid)[0];
  $archive_id = $archive->getId();

  $sharing_level = $form_state['values']['sharing_level'];
  $pid_id = $form_state['values']['select_coverslip'];
  $request_id = $form_state['request_id'];
  $comment_text = $form_state['values']['comment'];

  $alm_dataset = new ALMDataset();
  $alm_dataset->setPidId($pid_id);
  $alm_dataset->setRequestId($request_id);
  $alm_dataset->formCreateUploadSubmit($archive_id, $sharing_level);

  if (isset($form_state['values']['comment'])) {
    $comment = new ALMComment();
    $comment->setTypeID(ALMCommentType::DatasetUpload);
    $comment->setRequestID($request_id);
    $comment->setCurrentDate();
    $comment->setMessage($comment_text);
    $comment->save();
    $alm_dataset->setCommentId($comment->getCommentID());
    $alm_dataset->save();
  }

  /**
   * Redirect
   */
  $form_state['redirect'] = sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request_id, '3');

}
