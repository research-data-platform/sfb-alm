<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 19.11.2018
 * Time: 00:47
 */


function rdp_alm_request_view_add_staining_form($form = null, &$form_state, $request_id = null){
    $page = new AlmAddStainingPage();
    $form = $page->getPageForm($form, $form_state, $request_id);
    return $form;
}


/**
 * Closes the "new staining" view and redirected back to the request overview
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_view_add_staining_cancel($form, &$form_state){
  $page = new AlmAddStainingPage();
  $page->cancelSubmitFunction($form, $form_state);
}

/**
 * Check if the staining has a label name
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_view_add_staining_submit_validate($form, &$form_state) {
  $page = new AlmAddStainingPage();
  $page->addValidateFunction($form, $form_state);
}

/**
 * Creates a new staining with the user input and save it (linked to the request) in the db.
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_view_add_staining_submit($form, &$form_state){
  $page = new AlmAddStainingPage();
  $page->addSubmitFunction($form, $form_state);
}

