<?php
/**
 * Drupal confirmation form that deletes an ALMDataset
 */
function rdp_alm_request_dataset_delete(
  $form,
  &$form_state,
  $request_id,
  $dataset_id
) {

  $dataset = ALMDatasetRepository::findById($dataset_id);

  // Check access permission
  if (!$dataset->checkDeletePermission()) {
    drupal_goto(sfb_alm_url(RDP_ALM_URL_REQUEST_LANDING, $request_id));
  }

  $form_state['dataset_id'] = $dataset->getId();

  return confirm_form($form,
    t("Delete the Dataset %n?", ['%n' => $dataset->getDisplayName()]),
    sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW_IMAGES, $request_id));
}

/**
 * Drupal form handler. Triggers @see ALMDataset delete.
 */
function rdp_alm_request_dataset_delete_submit($form, &$form_state) {

  $dataset_id = $form_state['dataset_id'];
  $dataset = ALMDatasetRepository::findById($dataset_id);

  $request_id = $dataset->getRequestId();
  $dataset->delete();

  $form_state['redirect'] = sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW_IMAGES,
    $request_id);
}
