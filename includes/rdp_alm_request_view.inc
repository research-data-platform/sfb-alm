<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 26.01.2019
 * Time: 15:29
 */


/**
 * Implements hook_title_callback()
 *
 * @param int $alm_request_id
 * @return string
 */
function rdp_alm_request_view_title_callback($alm_request_id = 0) {

  // get pid prefix from config variables
  $request_pid_prefix = variable_get('alm_request_pid_prefix', ALMRequest::DEFAULT_PID_PREFIX);

  // get request data
  $request = ALMRequestsRepository::findById($alm_request_id);

  // if request with given id exists, than print request pid as a page title
  $request_display_name = 'Unknown request';
  if (!$request->isEmpty()) {
    $request_display_name = 'Request: ' . $request_pid_prefix . $request->getRequestID();
  }

  return $request_display_name;
}

function rdp_alm_request_view_form($form, &$form_state, $request_id = null) {
  $page = new AlmRequestViewPage();
  $form = $page->getPageForm($form, $form_state, $request_id);

  return $form;
}


/*
 * Ajax callback function for adding or deleting label rows
 * Function reloads the whole label_table fieldset
 */
function rdp_alm_coverslip_labels_create_ajax_links_callback($form, $form_state) {
  $page = new AlmRequestViewPage();
  return $page->coverslipLabelsAjaxCallback($form, $form_state);
}

/**
 * (Ajax) Submit function for adding a new row for labels
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_coverslip_label_create_ajax_add($form, &$form_state) {
  $page = new AlmRequestViewPage();
  $page->coverslipLabelAjaxAdd($form, $form_state);
}

/**
 * (Ajax) Submit function for deleting a row of the label table
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_coverslip_label_create_ajax_delete($form, &$form_state) {
  $page = new AlmRequestViewPage();
  $page->CoverslipLabelAjaxDelete($form, $form_state);
}

/**
 * (Ajax) Submit function for deleting a row of the label table
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_coverslip_label_create_ajax_hide($form, &$form_state) {
  $page = new AlmRequestViewPage();
  $page->coverslipLabelAjaxHide($form, $form_state);
}

/**
 * Creates for all selected values the template for the label printer
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_coverslip_label_create_submit_print($form, &$form_state) {
  $page = new AlmRequestViewPage();
  $page->coverslipLabelSubmitPrint($form, $form_state);
}

/**
 * Submit handler for sfb_alm_request_view_form.
 *
 * Submits the current request to ALM-Team.
 * Sets request to pending state.
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_view_form_submit($form, &$form_state) {
  $page = new AlmRequestViewPage();
  $page->submit($form, $form_state);
}
