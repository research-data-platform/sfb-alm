<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 18.08.2017
 * Time: 14:59
 */

/**
 * Form to adjust requests.
 * The first version can only reset a running request to the consultation state
 */
function sfb_alm_admin_reset_requests() {
  $form = array();

  // create the options-array for the selectField
  $running_requests = ALMRequestsRepository::findAllRequestsByState(ALMRequestState::Running);
  $select_field_options_requestIds = array();
  foreach ($running_requests as $request){
    $select_field_options_requestIds[$request->getRequestID()] = $request->getRequestID().' -- Researcher: '. $request->getUserFullname();
  }

  $form['select_alm_request'] = array(
    '#type' => 'select',
    '#title' => t('Reset ALM Request to consultation'),
    '#default_value' => t('none'),
    '#options' => $select_field_options_requestIds,
    '#description' => t('Select ALM-Request that should be resetted to "accepted" state.'),
    '#required' => TRUE,
  );

  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Reset the state of the selected request'),
    '#submit' => array('sfb_alm_admin_reset_requests_submit'),
    );

  return $form;
}



/**
 * Resets the state of the $form_state[values][select_alm_request]
 */
function sfb_alm_admin_reset_requests_submit($form = null, &$form_state){
  // load request
  $selected_request = ALMRequestsRepository::findById($form_state['values']['select_alm_request']);
  $selected_request->resetRunningToAccepted();
  $selected_request->save(false);
  drupal_set_message('Request' . $form_state['values']['select_alm_request'] . 'was resetted to accepted state');
}