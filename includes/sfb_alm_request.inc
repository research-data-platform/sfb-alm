<?php

/**
 *
 * @return string HTML String
 * @throws Exception
 */
function sfb_alm_request() {

  // get drupal's user object
  global $user;


  // requests table header (data: used for theme_table, field: used for table sort)
  $header = array(
      array('data' => 'Request ID', 'field' => 'r.' . ALMRequestsRepository::$databaseFields['id']),
      array('data' => 'Submitted date',),
      array('data' => 'Accepted date',),
      array('data' => 'Request State',),
      array('data' => '',),
      array('data' => 'Action',),
      array('data' => ''),
  );

  /**
   * Request filter
   */

  // Check if the requests variable in the URL is set and if the value is a request state
  $limit_of_requests_per_page = variable_get('alm_request_limit', 25);
  if (isset($_GET['requests']) && ALMRequestState::isRequestState($_GET['requests'])) {
      $requests = ALMRequestsRepository::findAllRequestsByUserIdAndStateAddPageLimit($user->uid, $_GET['requests'], $limit_of_requests_per_page);
      $visible_requests = t(ALMRequestState::getRequestStateName($_GET['requests']) . ' requests');
  } else{
    // Default value: all requests
    // Default value is used if no requeststate is selected
    $requests = ALMRequestsRepository::findAllCurrentRequestsByUserIDAddPageLimit($user->uid, $limit_of_requests_per_page);
    $visible_requests = t('All current requests');
    }

  // Select Field for request filter
  $show_options = array(
    '<a href="' . '?requests=All' . '">' . t('All current requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Edit . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Edit) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Pending . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Pending) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Accepted . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Accepted) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Running . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Running) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Revision . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Revision) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Withdraw . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Withdraw) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Finished . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Finished) . ' requests') . '</a>',
    '<a href="' . '?requests=' . ALMRequestState::Deleted . '">' . t(ALMRequestState::getRequestStateName(ALMRequestState::Deleted) . ' requests') . '</a>',
  );

  $view_select_field = '<div style="padding: 5px 5px 10px 5px">Filter: Show ' . alm_dashboard_dropdown_menu_html_items(array('items' => $show_options, 'selected_item' => $visible_requests)) . '</div>';


  /**
   * Request Table
   */



  // requests table rows
  $rows = array();
  foreach ($requests as $request) {
    $rows[]['data'] = array(
      // Request ID:
      '<a href="' . sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request->getRequestID()) . '">' . $request->getRequestID() . '</a>',
      // Submit date:
      $request->getSubmitDate(),
      // Accept Date
      $request->getAcceptDate(),
      // Request State:
      ALMRequestState::getRequestStateIcon($request->getRequestState()),
      // Request ID:
      '',
      // Validate_action:
      afb_alm_get_request_state_action($request),
      //Action:
      sfb_alm_request_get_dropdown($request),
    );
  }

  // Todo
  // Because of the responsive-table class of bootstrap  the dropdown menu of the last request is cut.
  // If the last row has a height of 100px, it is visible.
  if(!empty($rows)) {
    $rows[sizeof($rows) - 1]['style'] = 'height: 100px;';
  }
  // print message if current user has no requests
  if (count($rows) == 0) {
    $rows[] = array(
      'data' => array(array('data' => "You don't have any requests to display yet. " . l('Create new request', sfb_alm_url(RDP_ALM_URL_REQUEST_NEW)), 'colspan' => 7))
    );
  }

  $box_header = array('title' => 'Overview');
  $introduction_image = '';
  if(!empty(variable_get('alm_requests_overview_introduction_image', ''))) {
    $image_path = base_path() . drupal_get_path('module', 'sfb_alm') . '/' . variable_get('alm_requests_overview_introduction_image', '');
    $introduction_image =  '<p><img src="' . $image_path . '" style="float:left; padding:20px;" alt="Advanced Light Microscopy" /> </p>';
  }
  $introduction_text = variable_get('alm_requests_overview_introduction_text', '');
  $box_body = array(
    'data' =>
      $introduction_image.
      $introduction_text .'
          <p style="clear: both"></p>'
      . $view_select_field
      . theme('table', array('header' => $header, 'rows' => $rows)),
    'class' => array('table-responsive', 'no-padding'));
  $box_footer = array('data' => '<a role="button" class="btn btn-default" href="' . sfb_alm_url(RDP_ALM_URL_REQUEST_NEW) . '"><small><span class="fa fa-asterisk"></span></small> Create new request</a>');

  // Theme the table with the requests
  $view_request_table = theme('alm_dashboard_box', array('header' => $box_header, 'body' => $box_body, 'footer' => $box_footer)) . theme('pager', array('tags' => array()));

  return $view_request_table;
}

/**
 * @param $request ALMRequest
 * @return string
 */
function afb_alm_get_request_state_action($request) {
  // define the quick action links
  $quick_action_view = l('View', sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request->getRequestID()));
  $quick_action_edit = l('Edit', sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $request->getRequestID()));
  $quick_action_consult = l('Consult', sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $request->getRequestID()).'?active=1');

  // select the qucik action depending the request state
  switch ($request->getRequestState()) {
    // State Edit
    case ALMRequestState::Edit:
      return $quick_action_edit;
    // State Revision
    case ALMRequestState::Revision:
      return $quick_action_edit;
    // State Pending
    case ALMRequestState::Pending:
      return $quick_action_view;
    // State Accepted
    case ALMRequestState::Accepted:
      // Check if the user is allowed to consult his own request
      if ($request->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CONSULT)){
        return $quick_action_consult;
      }
      return $quick_action_view;
    // State Withdraw
    case ALMRequestState::Withdraw:
      return $quick_action_view;
    // State Running
    case ALMRequestState::Running:
      return $quick_action_view;
    // State Finished
    case ALMRequestState::Finished:
      return '';
    default:
      return '';
  }
}


/**
 * Creates a dropdown menu with buttons for the option of the request. The options depending on the request
 * for example to edit the request
 *
 * @param $request
 * @return string HTML
 */
function sfb_alm_request_get_dropdown($request) {
  $requestID = $request->getRequestID();

  // define the forms of the option buttons
  $dropdown_view = drupal_get_form(SFB_ALM_REQUEST_DROPDOWN_VIEW_FORM . '_' . $requestID, $requestID);
  $dropdown_edit = drupal_get_form(SFB_ALM_REQUEST_DROPDOWN_EDIT_FORM . '_' . $requestID, $requestID);
  $dropdown_consult = drupal_get_form(SFB_ALM_REQUEST_DROPDOWN_CONSULT_FORM . '_' . $requestID, $requestID);
  $dropdown_delete = drupal_get_form(SFB_ALM_REQUEST_DROPDOWN_DELETE_FORM . '_' . $requestID, $requestID);
  $dropdown_download = drupal_get_form(SFB_ALM_REQUEST_DROPDOWN_DOWNLOAD_FORM . '_' . $requestID, $requestID);
  // Todo: submit button form

  // select the visible option-buttons depending on the request state
  switch ($request->getRequestState()) {
    // State Edit
    case ALMRequestState::Edit:
      // add the Buttons as HTML to the option array
      $options = array(
        drupal_render($dropdown_edit),
        drupal_render($dropdown_view),
        drupal_render($dropdown_download),
        drupal_render($dropdown_delete),
      );
      break;
    // State Revision
    case ALMRequestState::Revision:
      // add the Buttons as HTML to the option array
      $options = array(
        drupal_render($dropdown_edit),
        drupal_render($dropdown_view),
        drupal_render($dropdown_download),
        drupal_render($dropdown_delete),
      );
      break;
    // State Pending
    case ALMRequestState::Pending:
      // add the Buttons as HTML to the option array
      $options = array(
        drupal_render($dropdown_view),
        drupal_render($dropdown_download),
        drupal_render($dropdown_delete),
      );
      break;
    // State Withdraw
    case ALMRequestState::Withdraw:
      // add the Buttons as HTML to the option array
      $options = array(
        drupal_render($dropdown_view),
        drupal_render($dropdown_download),
        drupal_render($dropdown_delete),
      );
      break;
    case ALMRequestState::Accepted:
      $options = array(drupal_render($dropdown_view));
      // Adds consult button if user is allowed to consult own requests
      if($request->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CONSULT)){
        $options[] = drupal_render($dropdown_consult);
      }
      $options[] = drupal_render($dropdown_download);
      break;
    case ALMRequestState::Running:
      // add the Buttons as HTML to the option array
      $options = array(
        drupal_render($dropdown_view),
        drupal_render($dropdown_download),
      );
      break;
    // States Accpeted, Finished, Deleted, Archived
    default:
      // add the Buttons as HTML to the option array
      $options = array(
        drupal_render($dropdown_view),
        drupal_render($dropdown_download),
      );
      break;
  }

  // Theme the option array to a HTML Button with Dropdown menu
  return alm_dashboard_dropdown_menu_html_items(array('items' => $options));
}

function sfb_alm_request_dropdown_view_form($form = null, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'sfb_alm') . ' / sfb_alm . css');
  $form['button'] = array(
    '#type' => 'submit',
    '#title' => t('view_btn'),
    '#value' => t('View request'),
    '#attributes' => array('class' => array('btn-link'), 'style' => array('color: grey;', 'width: 100%;', 'text-align: left;', 'padding: 0px 0px 3px 20px;')),
    '#submit' => array('sfb_alm_request_dropdown_view_form_submit'),
  );

  return $form;
}

function sfb_alm_request_dropdown_delete_form($form = null, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'sfb_alm') . ' / sfb_alm . css');
  $form['button'] = array(
    '#type' => 'submit',
    '#title' => t('delete_btn'),
    '#value' => t('Delete request'),
    '#attributes' => array('class' => array('btn-link'), 'style' => array('color: grey;', 'width: 100%;', 'text-align: left;', 'padding: 0px 0px 3px 20px;')),
    '#submit' => array('sfb_alm_request_dropdown_delete_form_submit'),
  );

  return $form;
}

function sfb_alm_request_dropdown_edit_form($form = null, &$form_state) {
  // Because of the button has the btn-link class, it can't get a second class, that's why the <li> Element is associate with the css file sfb_alm.css
  drupal_add_css(drupal_get_path('module', 'sfb_alm') . '/sfb_alm.css');
  $form['button'] = array(
    '#type' => 'submit',
    '#title' => t('edit_btn'),
    '#value' => t('Edit request'),
    '#attributes' => array('class' => array('btn-link'), 'style' => array('color: grey;', 'width: 100%;', 'text-align: left;', 'padding: 0px 0px 3px 20px;')),
    '#submit' => array('sfb_alm_request_dropdown_edit_form_submit'),

  );

  return $form;
}

function rdp_alm_request_dropdown_consult_form($form = null, &$form_state) {
  // Because of the button has the btn-link class, it can't get a second class, that's why the <li> Element is associate with the css file sfb_alm.css
  drupal_add_css(drupal_get_path('module', 'sfb_alm') . '/sfb_alm.css');
  $form['button'] = array(
    '#type' => 'submit',
    '#title' => t('edit_btn'),
    '#value' => t('Consult request'),
    '#attributes' => array('class' => array('btn-link'), 'style' => array('color: grey;', 'width: 100%;', 'text-align: left;', 'padding: 0px 0px 3px 20px;')),
    '#submit' => array('sfb_alm_request_dropdown_consult_form_submit'),

  );

  return $form;
}


function sfb_alm_request_dropdown_download_form($form = null, &$form_state) {
  drupal_add_css(drupal_get_path('module', 'sfb_alm') . '/sfb_alm.css');
  $form['button'] = array(
    '#type' => 'submit',
    '#title' => t('download_btn'),
    '#value' => t('Download request'),
    '#attributes' => array('class' => array('btn-link'), 'style' => array('color: grey;', 'width: 100%;', 'text-align: left;', 'padding: 0px 0px 3px 20px;')),
    '#submit' => array('sfb_alm_request_dropdown_download_form_submit'),
  );

  return $form;
}

/**
 * OLD FUNCTION!!!! CHECK BEFORE USING!!
 *
 * @deprecated
 * @param null $form
 * @param $form_state
 * @return null
 */
function sfb_alm_request_link_submit_form($form = null, &$form_state) {
  $form['button'] = array(
    '#type' => 'submit',
    '#title' => t('submit_btn'),
    '#value' => t('Submit request'),
    '#attributes' => array('class' => array('btn-link'), 'style' => array('text-align: left;', 'padding: 0px 0px 3px 0px;')),
    '#submit' => array('sfb_alm_request_link_submit_form_submit')
  );

  return $form;
}

/**
 * -------------------------------------------------------------------
 * Submit Functions:
 * -------------------------------------------------------------------
 */

function sfb_alm_request_dropdown_delete_form_submit($form, &$form_state) {
  $requestID = $form_state['build_info']['args'][0];
  $request = ALMRequestsRepository::findById($requestID);
  $old_state = $request->getRequestState();
  $request->setRequestState(ALMRequestState::Deleted);
  ALMRequestsRepository::save($request);

  global $user;
  if ($old_state == ALMRequestState::Pending) {
    sfb_alm_mail_send_request_notification($request, 'deleted', 'Pending request has been deleted', $user->uid);
    drupal_set_message('Request ' . $requestID . ' has been deleted. Notification e-mails have been sent to S02 team.');
  } else {
    drupal_set_message('Request ' . $requestID . ' has been deleted.');
  }
}

function sfb_alm_request_dropdown_view_form_submit($form, &$form_state) {
  $requestID = $form_state['build_info']['args'][0];
  $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $requestID));
}

function sfb_alm_request_dropdown_edit_form_submit($form, &$form_state) {
  $requestID = $form_state['build_info']['args'][0];
  $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $requestID));
}

function sfb_alm_request_dropdown_consult_form_submit($form, &$form_state) {
  $requestID = $form_state['build_info']['args'][0];
  $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $requestID). '?active=1');
}


/**
 * OLD FUNCTION!!!! CHECK BEFORE USING!!!
 * @deprecated
 * @param $form
 * @param $form_state
 */
function sfb_alm_request_link_submit_form_submit($form, &$form_state) {
  $requestID = $form_state['build_info']['args'][0];

  $request = ALMRequestsRepository::findById($requestID);

  //  Sets submitDate and RequestState
  $request->setSubmitAttributes();

  $comment = $request->getCommentWithType(ALMCommentType::SubmitEdit);
  $comment->setCurrentDate();

  // Sets the CommentType to Submit because then it isnt possible to edit the comment again.
  $comment->setTypeID(ALMCommentType::Submit);

  //Set the updated user Comment to the updatedComment Variable, so the next save invoke will store it to the DB
  $request->setUpdatedComment($comment);

  // save request in database
  $request->save(false);

  drupal_set_message('Request ' . $requestID . ' submitted successfully.');

  // redirect to overview form
  $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_REQUEST));
}

function sfb_alm_request_dropdown_download_form_submit($form, &$form_state) {
  $requestID = $form_state['build_info']['args'][0];
  $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_DOWNLOAD_PDF_REQUEST, $requestID));
}


