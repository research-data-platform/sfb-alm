<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 19.04.2018
 * Time: 23:55
 */

function sfb_alm_request_landing($id = NULL) {

  // get request data
  $request = ALMRequestsRepository::findById($id);

  //
  // Check if the requests exists
  //
  if ($request->isEmpty()) {
    drupal_not_found();
    return;
  }

  if ($request->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_VIEW)) {
    $link_url = sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request->getRequestID());
  }
  else {
    if ($request->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW)) {
      $link_url = sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $request->getRequestID());
    }
  }
  if (isset($link_url)) {
    $link = l('<span class="glyphicon glyphicon-edit"></span>&nbsp;Open Request',
      $link_url,
      ['html' => TRUE, 'attributes' => ['class' => ['btn btn-primary btn-md pull-right']]]);
  }

  $request_data_views = new AlmRequestDataViews($request);

  $logo_path = base_path() . drupal_get_path('module', 'sfb_alm')
    . '/' . SFB_ALM_PATH_RESOURCES . '/alm_logo.png';
  $description_box = '<p><img src="' . $logo_path . '" style="float:right; padding:20px;" 
            alt="Advanced Light Microscopy" /> </p>
          <blockquote>
              <p>Dear Visitor,<br />
              this is the research data management system for the Advanced Light 
              Microscopy Service Unit (S02) of the Collaborative Research Center 1002,
              "Modulatory Units in Heart Failure".<br />
              The following is a shortened public representation of the dataset 
              you requested by accessing this page. If you are entitled to full 
              access, please log into the system.<br />
              If you are otherwise interested to get full access to the dataset,
              please contact the responsible researcher.
              <footer>' . SFB_ALM_TEXT_SFB_TEAM . '</footer>
            </blockquote>
          <p style="clear: both"></p>';

  $box_header = ['title' => 'Overview'];
  $box_body = [
    'data' => $description_box . $request_data_views->getResearcherBox(),//$request->getFormattedBoxResearcher(),
    'class' => ['table-responsive', 'no-padding'],
  ];
  $box_footer = ['data' => ''];

  $page_html = theme('alm_dashboard_box', ['header' => $box_header, 'body' => $box_body, 'footer' => $box_footer]);

  $datasets = ALMDatasetRepository::findAllByRequestId($id);

  if (count($datasets)) {
    foreach ($datasets as $dataset) {

      if ($dataset->checkReadPermission()) {
        $accordion[$dataset->getPidId()][] = [
          // Dataset display name
          l($dataset->getDisplayName(), $dataset->url('landing')),
          // Number of files in dataset
          $dataset->getFileCount(),
          // Upload date
          $dataset->getCreated()->format(SFB_ALM_DATEFORMAT),
          // Visibility
          SharingLevel::getHtmlInline($dataset->getSharingLevel()),
        ];
      }
    }
    if (isset($accordion)) {
      $i = 1;
      $dataset_block = '<div class="container-fluid"><h3>Microscopy Datasets</h3><div class="panel-group" id="accordion">';
      foreach ($accordion as $key => $rows) {
        $css_id = "collapse" . (string) $i++;
        $dataset_block .= '<div class="panel panel-default">';
        $dataset_block .= '<div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#' . $css_id . '">
              <span class="glyphicon glyphicon-barcode"></span>&nbsp;Coverslip Label: ' . $key . '</a>
            </h4>
          </div>';
        $dataset_block .= '<div id="' . $css_id . '" class="panel-collapse collapse"><div class="panel-body">'
          . theme('table', ['header' => ['Dataset', 'Files', 'Upload date', 'Visibility'], 'rows' => $rows])
          . '</div></div>';
        $dataset_block .= '</div>';
        // ToDo: replace tableRow with specially formatted list (EPIC-PID, Dataset-name, date, nr. of files?)
        // ToDo: distinct view page, group by coverslip label, display sample & staining information
      }
      $dataset_block .= '</div></div>';
      $page_html .= $dataset_block;
    }
  }

  if (isset($link)) {
    $page_html = $link . $page_html;
  }

  return $page_html;
}