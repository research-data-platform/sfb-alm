<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 17.10.2017
 * Time: 16:34
 */

function sfb_alm_coverslip_labels_download($requestID = null){

  $print_job_id = $_GET['jobId'];
  $print_job = ALMCoverslipLabelsPrintJobRepository::findById($print_job_id);
  //
  // Check permission
  //

  $request_obj = ALMRequestsRepository::findById($print_job->getRequestId());
  if ($request_obj->getRequestID() == ALMRequest::EMPTY_REQUEST_ID) {
    drupal_not_found();
    return;
  }

  // an manager is also allowed to download the coverslip labels, if he is allowed to see the image overview
  if(!$request_obj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CREATE_COVERSLIPS) &&
      !$request_obj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW_IMAGES)){
    
    drupal_access_denied();
    exit();
  }


  // send response headers to the browser
  drupal_add_http_header('Content-Type', 'text/csv');
  drupal_add_http_header('Content-Disposition', 'attachment;filename=alm_labels.csv');

  $fp = fopen('php://output', 'w');

  // csv head
  fputcsv($fp, array('SampleID', 'Projectname', 'Username', 'Date', 'StainingShortname', 'handleUrl'), ';');
  // csv body
  foreach($print_job->getLabelsPrintValues() as $line){
    fputcsv($fp, $line, ';');
  }
  fclose($fp);

  ALMCoverslipLabelsPrintJobRepository::deleteById($print_job_id);

  drupal_exit();
}