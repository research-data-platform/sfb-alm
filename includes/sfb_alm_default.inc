<?php

/**
 * @file
 * Default page of Advanced Light Microscopy module.
 *
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 */
function sfb_alm_default() {

  // Set the title for this page
  $title = variable_get(SFB_ALM_VAR_CONFIG_MODULE_TITLE, 'Advanced Light Microscopy');
  drupal_set_title($title);

  global $user;

  // check whether user is a service 02 member
  // * only members of service group can see management view (content)
  $content = '';

  if (user_access(SFB_ALM_PERMISSION_MANAGE)) {
    // management view can see requested, running or finished requests of all users

    // get all pending requests
    $requests_pending = ALMRequestsRepository::findAllRequestsByState(ALMRequestState::Pending);
    $requests_accepted = ALMRequestsRepository::findAllRequestsByState(ALMRequestState::Accepted);
    $requests_running = ALMRequestsRepository::findAllRequestsByState(ALMRequestState::Running);

    //
    // overview boxes
    //
    //TODO: in theme verschieben
    $content .= '
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon ' . (count($requests_pending) == 0 ? 'bg-grey' : 'bg-yellow') . '"><i class="fa fa-question"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">PENDING REQUESTS</span>
              <span class="info-box-number">' . count($requests_pending) . '</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon ' . (count($requests_accepted) == 0 ? 'bg-grey' : 'bg-aqua') . '"><i class="fa fa-check"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">ACCEPTED</span>
              <span class="info-box-number">' . count($requests_accepted) . '</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon ' . (count($requests_running) == 0 ? 'bg-grey' : 'bg-green') . '"><i class="fa fa-battery-2"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">RUNNING</span>
              <span class="info-box-number">' . count($requests_running) . '</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    </div>
    ';

    //
    // print an overview with pending, accepted, and running requests
    //

    // pending requests table
    $tbl_requests_pending_header = ['ID', 'Researcher', 'Requested', ''];
    $tbl_requests_pending_rows = [];
    foreach ($requests_pending as $request_pending) {
      $tbl_requests_pending_rows[] = _render_request_table_row($request_pending, SFB_ALM_PERMISSION_MANAGE);
    }

    // accepted requests table
    $tbl_requests_accepted_header = ['ID', 'Researcher', 'Accepted', '',];
    $tbl_requests_accepted_rows = [];
    foreach ($requests_accepted as $request_accepted) {
      $tbl_requests_accepted_rows[] = _render_request_table_row($request_accepted,
        SFB_ALM_PERMISSION_MANAGE);
    }


    // running requests table
    $tbl_requests_running_header = [
      'ID',
      'Researcher',
      'Adviser',
      'Consulted',
      '',
    ];
    $tbl_requests_running_rows = [];
    foreach ($requests_running as $request_running) {
      $tbl_requests_running_rows[] = _render_request_table_row($request_running, SFB_ALM_PERMISSION_MANAGE, TRUE);
    }


    $content .= '
      <div class="row">
        <div class="col-xs-4">' . theme('alm_dashboard_box', [
        'header' => ['title' => 'New requests'],
        'body' => [
          'data' => theme('table', [
            'header' => $tbl_requests_pending_header,
            'rows' => $tbl_requests_pending_rows,
          ]),
        ],
        'footer' => ['data' => ''],
      ]) . '</div>
        <div class="col-xs-4">' . theme('alm_dashboard_box', [
        'header' => ['title' => 'Waiting for consultation'],
        'body' => [
          'data' => theme('table', [
            'header' => $tbl_requests_accepted_header,
            'rows' => $tbl_requests_accepted_rows,
          ]),
        ],
        'footer' => ['data' => ''],
      ]) . '</div>
        <div class="col-xs-4">' . theme('alm_dashboard_box', [
        'header' => ['title' => 'Running requests'],
        'body' => [
          'data' => theme('table', [
            'header' => $tbl_requests_running_header,
            'rows' => $tbl_requests_running_rows,
          ]),
        ],
        'footer' => ['data' => ''],
      ]) . '</div>
      </div>';
  }
  else {

    //TODO: code verdopplung von oben, muss noch ueberarbeitet werden

    // get all pending requests
    $requests_pending = ALMRequestsRepository::findAllRequestsByUserIdAndState($user->uid, ALMRequestState::Pending);
    $requests_revised = ALMRequestsRepository::findAllRequestsByUserIdAndState($user->uid, ALMRequestState::Revision);
    $requests_accepted = ALMRequestsRepository::findAllRequestsByUserIdAndState($user->uid, ALMRequestState::Accepted);
    $requests_running = ALMRequestsRepository::findAllRequestsByUserIdAndState($user->uid, ALMRequestState::Running);


    $content .= '
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon ' . (count($requests_pending) == 0 ? 'bg-grey' : 'bg-yellow') . '"><i class="fa fa-question"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">PENDING REQUESTS</span>
              <span class="info-box-number">' . count($requests_pending) . '</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon ' . (count($requests_accepted) == 0 ? 'bg-grey' : 'bg-aqua') . '"><i class="fa fa-check"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">ACCEPTED / WAITING FOR CONSULTATION</span>
              <span class="info-box-number">' . count($requests_accepted) . '</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon ' . (count($requests_running) == 0 ? 'bg-grey' : 'bg-green') . '"><i class="fa fa-battery-2"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">RUNNING</span>
              <span class="info-box-number">' . count($requests_running) . '</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    </div>
    ';

    //
    // print an overview with pending, accepted, and running requests
    //

    // pending requests table
    $tbl_requests_pending_header = ['ID', 'Researcher', 'Requested', ''];
    $tbl_requests_pending_rows = [];
    // revised requests
    foreach ($requests_revised as $request_revised) {
      $tbl_requests_pending_rows[] = _render_request_table_row($request_revised, SFB_ALM_PERMISSION_DEFAULT);
    }
    //pending requests
    foreach ($requests_pending as $request_pending) {
      $tbl_requests_pending_rows[] = _render_request_table_row($request_pending, SFB_ALM_PERMISSION_DEFAULT);
    }

    // accepted requests table
    $tbl_requests_accepted_header = ['ID', 'Researcher', 'Accepted', '',];
    $tbl_requests_accepted_rows = [];
    foreach ($requests_accepted as $request_accepted) {
      $tbl_requests_accepted_rows[] = _render_request_table_row($request_accepted, SFB_ALM_PERMISSION_DEFAULT);
    }


    // running requests table
    $tbl_requests_running_header = [
      'ID',
      'Researcher',
      'Adviser',
      'Consulted',
      '',
    ];
    $tbl_requests_running_rows = [];
    foreach ($requests_running as $request_running) {
      $tbl_requests_running_rows[] = _render_request_table_row($request_running, SFB_ALM_PERMISSION_DEFAULT, TRUE);
    }

    $content .= '
      <div class="row">
        <div class="col-xs-4">' . theme('alm_dashboard_box', [
        'header' => ['title' => 'Pending and revised requests'],
        'body' => [
          'data' => theme('table', [
            'header' => $tbl_requests_pending_header,
            'rows' => $tbl_requests_pending_rows,
          ]),
        ],
        'footer' => ['data' => '<a role="button" class="btn btn-default" href="' . sfb_alm_url(RDP_ALM_URL_REQUEST_NEW) . '"><small><span class="fa fa-asterisk"></span></small> Create new request</a>'],
      ]) . '</div>
        <div class="col-xs-4">' . theme('alm_dashboard_box', [
        'header' => ['title' => 'Waiting for consultation'],
        'body' => [
          'data' => theme('table', [
            'header' => $tbl_requests_accepted_header,
            'rows' => $tbl_requests_accepted_rows,
          ]),
        ],
        'footer' => ['data' => ''],
      ]) . '</div>
        <div class="col-xs-4">' . theme('alm_dashboard_box', [
        'header' => ['title' => 'Running requests'],
        'body' => [
          'data' => theme('table', [
            'header' => $tbl_requests_running_header,
            'rows' => $tbl_requests_running_rows,
          ]),
        ],
        'footer' => ['data' => ''],
      ]) . '</div>
      </div>';
  }

  return $content;
}

/**
 * Helper function to build overview page table rows.
 *
 * ToDo: move to new ALMRequestRenderer class or something
 *
 * @param \ALMRequest $request
 * @param $permission
 * @param bool $is_running
 *
 * @return array
 */
function _render_request_table_row(ALMRequest $request, $permission, $is_running = FALSE) {

  switch ($permission) {
    case SFB_ALM_PERMISSION_MANAGE:
      $url_view = RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW;
      break;
    default:
      $url_view = RDP_ALM_URL_REQUEST_VIEW;
      break;
  }

  $row = [];

  $revised = $request->getRequestState() == ALMRequestState::Revision ? '&nbsp;<span class="text-yellow"><i class="fa fa-retweet"></i> Revised!</span>' : '';
  $row[] = '<a href="' . sfb_alm_url($url_view, $request->getRequestID()) . '">' . $request->getRequestID() . '</a>' . $revised;
  $row[] = $request->getUserFullname();
  if ($is_running) {
    $row[] = $request->getConsultationResponsibleS02MemberName();
  }
  $row[] = $request->getSubmitDate();
  $row[] = '<a href="' . sfb_alm_url(RDP_ALM_URL_DOWNLOAD_PDF_REQUEST, $request->getRequestID()) . '" data-toggle="tooltip" title="Request form PDF"><span class="fa fa-file"></span></a>
        &nbsp;<a href="' . sfb_alm_url($url_view, $request->getRequestID()) . '" data-toggle="tooltip" title="View Request"><span class="fa fa-eye"></span></a>';

  return $row;
}