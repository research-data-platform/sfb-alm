<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 11.06.2019
 * Time: 23:52
 */

function rdp_alm_request_dataset_landing_form($form = [], &$form_state, $request_id = NULL, $dataset_id){
  $pageObj = new AlmDatasetLandingPage($request_id, $dataset_id);

  return $pageObj->getPageForm($form, $form_state);
}