<?php
/**
 * @file
 * Edit ALM request
 *
 * @author Christian Henke (christian.henke@stud.uni-goettingen.de)
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 */

define('ALM_REQUEST_ADD_SINGLE_STAINING', 1);
define('ALM_REQUEST_ADD_DUAL_STAINING', 2);
define('ALM_REQUEST_ADD_MULTI_STAINING', 3);

/**
 * @param array $form
 * @param $form_state
 * @param null $pid
 *
 * @return array
 */
function sfb_alm_request_edit_form($form = [], &$form_state, $pid = NULL) {
  // load external file to use
  module_load_include('inc', 'sfb_alm', 'includes/sfb_alm_request_view'); //todo 18.may.18 wird das noch gebraucht?

  // get request instance with data from database
  $requestObj = ALMRequestsRepository::findById($pid);

  //
  // Check access
  //

  // if request could not be found or user has no permission than return
  // drupal_not_found or drupal_access_denied response
  if ($requestObj->isEmpty()) {

    drupal_not_found();
    exit();
  }

  if (!$requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT) &&
    !$requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CONSULT)) {
    drupal_access_denied();
    exit();
  }

  if ($requestObj->isLocked() && !isset($_GET['unlock']) && empty($form_state['input'])) {
    $locked_user_name = UsersRepository::findByUid($requestObj->getLockedBy())->getFullname();
    $form['locked-dialog'] = sfb_alm_request_edit_locked_dialog($locked_user_name);
  }
  else {
    $requestObj->lock();
  }

  // if form_state['input'] ist NOT empty, the request gets only reloaded (e.g. after saving or validation)


  $form_state[ALM_REQUEST_ID] = $requestObj->getRequestID();

  //If a new staining or Sample is added, the page should scroll to the old position. By submitting, the old position is saved in the scrolLTo parameter in the URL
  if (isset($_GET['scrollTo'])) {
    drupal_add_js('jQuery(document).ready(function () {
     window.scrollTo(0,' . $_GET['scrollTo'] . ');
     })', 'inline');
  }

  // Add Variable to store the scroll Position
  $form['scrollPos'] = [
    '#type' => 'hidden',
    '#default_value' => '0',
  ];

  //
  $form['tabs'] = [
    '#tree' => TRUE,
    '#theme' => 'sfb_alm_dashboard_tab_form',
    '#active' => isset($_GET['active']) ? $_GET['active'] : 0,
  ];


  // set form fields //

  //
  // User Request Tab
  //
  $user_request_tab = new ALMRequestEditForms($requestObj);

  // user profile
  $form['tabs']['User Request']['fieldset-userprofile'] = $user_request_tab->getFieldsetUserProfile();
  // Imaging Goal and Scientific Question
  $form['tabs']['User Request']['fieldset-imaging_goal_and_scientific_question'] = $user_request_tab->getFieldsetImagingGoal();
  // Sample Information
  $form['tabs']['User Request']['fieldset-sample_information'] = $user_request_tab->getFieldsetSampleInformation();
  // Requested Support and Services
  $form['tabs']['User Request']['fieldset-requested_support_and_services'] = $user_request_tab->getFieldsetRequestedSupport();

  // User Comment
  if ($requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT)) {
    $form['tabs']['User Request']['fieldset-user_comment'] = $user_request_tab->getFieldsetUserComment();
  }

  //
  // Consultation Tab
  //
  if ($requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CONSULT)) {

    $consultation_forms = new ALMConsultationEditForms($requestObj);
    // Fieldset General
    $form['tabs']['Consultation']['fieldset-general'] = $consultation_forms->getFieldsetGeneral();
    //stainings
    $form['tabs']['Consultation']['fieldset-stainings'] = [
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-flask"></i> ' . t('Documentation of immunofluorescence stainings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['tabs']['Consultation']['fieldset-stainings']['fieldset-single_staining'] = $consultation_forms->getFieldsetSingleStaining('rdp_alm_request_edit_submit_add_single_staining');
    $form['tabs']['Consultation']['fieldset-stainings']['fieldset-dual_staining'] = $consultation_forms->getFieldsetDualStaining('rdp_alm_request_edit_submit_add_dual_staining');
    $form['tabs']['Consultation']['fieldset-stainings']['fieldset-multi_staining'] = $consultation_forms->getFieldsetMultiStaining('rdp_alm_request_edit_submit_add_multi_staining');

    // samples
    $form['tabs']['Consultation']['fieldset-sample_documentation'] = $consultation_forms->getFieldsetSampleDocumentation('rdp_alm_request_edit_submit_add_sample_doc');

  }

  drupal_add_js('jQuery(document).ready(function() {
	jQuery(".select2").css("width", "100%");
});', 'inline');

  //
  // COMMENTS AND LOG TAB
  //

  $commentsAndLogTab = new AlmCommentsAndLogsTab($requestObj);
  $form['tabs']['Comments and Log'] = $commentsAndLogTab->getForm();

  //
  // Image Tab
  //
  // Todo 05/June/18 verschieben in View, da Edit auf running gar nich erreichbar ist.
  if ($requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_VIEW_IMAGES)) {
    $image_tab = new ALMDatasesUploadForms($requestObj);
    $form['tabs']['images'] = $image_tab->getImageOverviewFieldset();
  }

  //
  // Buttons
  //

  if ($requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT)) {
    $form['btn-submit'] = [
      '#type' => 'submit',
      '#value' => t('<i class="fa fa-send-o"></i>' . ' Submit request'),
      '#description' => t('Submits the request to the ALM Team. After submitting the request it will not be possible to edit the request again!'),
      '#submit' => ['rdp_alm_request_edit_submit'],
      '#attributes' => ['class' => ['btn-primary', 'btn-lg'], 'style' => ['float: right;']],
    ];
  }

  if ($requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CONSULT)) {
    $form['run-confirm-dialog'] = sfb_alm_request_edit_confirm_dialog();

    $form['btn-run'] = [
      '#type' => 'submit',
      '#value' => '<i class="fa fa-thumbs-o-up"></i> ' . t(' Finish consultation'),
      '#description' => t('Finalize the consultation and set the request state to run'),
      '#attributes' => ['class' => ['btn-lg', 'btn-primary'], 'id' => 'btn-run', 'style' => ['float: right']],
      '#submit' => ['rdp_alm_request_edit_submit_run'],
      '#validate' => ['rdp_alm_request_edit_submit_run_validate'],

    ];
  }

  $form['btn-cancel'] = [
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#attributes' => ['class' => ['btn-default']],
    '#description' => t('Discard your changes and go back to requests overview.'),
    '#submit' => ['rdp_alm_request_edit_submit_cancel'],
  ];

  $form['btn-save'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
    '#description' => t('Save the request and go to the view page'),
    '#attributes' => ['class' => ['btn-default']],
    '#submit' => ['rdp_alm_request_edit_submit_save'],
  ];

  $form['btn-continue'] = [
    '#type' => 'submit',
    '#value' => t('Save and continue'),
    '#description' => t('Save the request and stay on the edit page'),
    '#attributes' => ['class' => ['btn-default']],
    '#submit' => ['rdp_alm_request_edit_submit_continue'],
  ];

  // breadcrumb must be overridden, because 'title callback' of request view isn't called
  /*try {
    $breadcrumb = drupal_get_breadcrumb();
    $breadcrumb[3] = str_replace('>Request<', '>' . sfb_alm_request_view_title_callback(20) . '<', $breadcrumb[3]); //TODO: 20 ist falsch, muss noch durch die id des requests ersetzt werden
    drupal_set_breadcrumb($breadcrumb);
  } catch (Exception $e) { }*/

  return $form;
}

/**
 * Returns a drupal_form array for the confirm dialog which is shown when the user tries to open an blocked request
 * (This dialog is will be shown automatically if this is in the code, it does not depend where this code is inserted)
 *
 * @param $locked_user_name
 *
 * @return array
 */
function sfb_alm_request_edit_locked_dialog($locked_user_name) {
  $locked_dialog = new RequestBlockedDialog();

  $dialogBody = '
      
      <h4>The request is currently locked. Do you want to unlock the request?</h4>
      <h4 class="small">The request is locked by: ' . $locked_user_name . '</h4>
      <div class="panel panel-danger">
        <div class="panel-heading">
          <h3 class="panel-title">Attention:</h3> 
        </div>
        <div class="panel-body">
          <p>If you open the request several times at the same time, it can happen that some data gets overwritten.</p>
        </div>
      </div>
      <div class="panel panel-info">
        <div class="panel-heading" data-toggle="collapse" href="#lock-explanation">
          <h3 class="panel-title">Why do I get this notification?</h3> 
        </div>
        <div class="panel-body collapse" id="lock-explanation">
          <p>If you open a request in a browser tab (e.g. a Firefox tab), the system locks the request to ensure that 
          you don\'t open the request in another tab or on another device at the same time a second time. 
          This should prevent lose of data that can happen if you first save the request on one device and then save an 
          old version on another device.</p>
          <p><strong>If you don\'t close the request properly (with the "Save" or "Cancel"-Button) the system doesn\'t recognize that you 
          closed the request and cannot unlock the request for the next usage!<strong></p>
        </div>
      </div>
    ';

  $locked_dialog->setBody($dialogBody);
  $locked_dialog->setAnswTextYes('Unlock');

  return [
    '#type' => 'markup',
    '#markup' => $locked_dialog->getDialogAsHtml(),
  ];
}

/**
 * Returns a drupal_form array for the confirm dialog of the submit button
 * (This content is normally not visible, that's why it does not depend where this code is inserted)
 *
 * @return array
 */
function sfb_alm_request_edit_confirm_dialog() {
  $confirm_dialog = new ConfirmDialog('btn-run');
  $confirm_dialog->setAnswTextYes('Continue');
  $confirm_dialog->setBody('
<h4>Are you sure that you want to finish the consultation?</h4>
<h4 class="small">This sets the request to running. The request and consultation data will be fixed then.<br>
<strong>Hint: To avoid lose of unsaved antibody information, ensure that no "short label name" field is empty</strong></h4>
<div class="panel panel-warning">
  <div class="panel-heading">
  <h3 class="panel-title">Attention:</h3> 
  </div>
  <div class="panel-body">
    <p>If you finish the consultation, this cannot be undone. Once the consultation is finished it will not be possible to edit any consultation data anymore!</p>
  </div>
</div>
    ');

  return [
    '#type' => 'markup',
    '#markup' => $confirm_dialog->getDialogAsHtml(),
  ];
}

/**
 * Merge the request with the values of the form and saves the request to the database.
 * Redirect to the request view page
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_edit_submit_save($form, &$form_state) {
  //Saves the request
  $requestID = rdp_alm_request_edit_save_to_database($form_state);

  // redirect to the request view
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $requestID)];
}

/**
 * Merge the request with the values of the form and saves the request to the database.
 * Stay at the request edit page
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_edit_submit_continue($form, &$form_state) {
  //Saves the request
  $requestID = rdp_alm_request_edit_save_to_database($form_state);

  // redirect to the request view
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $requestID)];
}

function rdp_alm_request_edit_submit_cancel($form, &$form_state) {
  $request = ALMRequestsRepository::findById($form_state[ALM_REQUEST_ID])->unlock();
  // redirect to the request view
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $form_state[ALM_REQUEST_ID])];
}

/**
 * Checks the permission and merge the request with the form values. After that the request will be saved to the
 * database.
 *
 * @param $form_state
 *
 * @return integer requestID
 */
function rdp_alm_request_edit_save_to_database($form_state) {

  // update request
  // get request from database
  $requestObj = ALMRequestsRepository::findById($form_state[ALM_REQUEST_ID]);

  // Check if the user has permission to save(edit) the request
  if (!$requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT || ALMRequest::ROLE_USER,
    ALMRequest::ACTION_CONSULT)) {
    drupal_set_message('Error: A problem occured by processing request object. Please reload the page. (Code: ' . SFB_ALM_ERROR_EDIT_REQUEST_SAVE . ')',
      'error');
    drupal_access_denied();
    return;
  }

  // writes form_state values into the request object
  rdp_alm_request_edit_write_form_state_values_to_request_obj($requestObj, $form_state);

  // save request in database
  $requestObj->save();

  // set the lockedBy id to null
  $requestObj = ALMRequestsRepository::findById($requestObj->getRequestID());
  $requestObj->unlock();

  drupal_set_message('Request ' . $form_state[ALM_REQUEST_ID] . ' saved successfully');
  return $requestObj->getRequestID();
}

/**
 * Saves the data (values) from the form_state array into the request object
 *
 * @param $requestObj ALMRequest
 */
function rdp_alm_request_edit_write_form_state_values_to_request_obj($requestObj, $form_state) {

  $request_edit_forms_obj = new ALMRequestEditForms($requestObj);
  $request_edit_forms_obj->collectDataFromFieldsetUserProfile($form_state['values']['tabs']['User Request']['fieldset-userprofile']);
  $request_edit_forms_obj->collectDataFromFieldsetImagingGoal($form_state['values']['tabs']['User Request']['fieldset-imaging_goal_and_scientific_question']);
  $request_edit_forms_obj->collectDataFromFieldsetSampleInformation($form_state['values']['tabs']['User Request']['fieldset-sample_information']);
  $request_edit_forms_obj->collectDataFromFIeldsetRequestedSupport($form_state['values']['tabs']['User Request']['fieldset-requested_support_and_services']);

  // User comment fieldset is only available if the request is in state edit
  if ($requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT)) {
    $request_edit_forms_obj->collectDataFromFieldsetUserComment($form_state['values']['tabs']['User Request']['fieldset-user_comment']);
  }

  ////////////// Consultation ///////////////////
  if ($requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CONSULT)) {

    // generate a ALMConsultationEditForms object to save all values to the associated request Object (requestObj)
    $consultation_edit_forms_obj = new ALMConsultationEditForms($requestObj);

    // save the consultation values to the requestObj
    $consultation_edit_forms_obj->collectDataFromFieldsetGeneral($form_state['values']['tabs']['Consultation']['fieldset-general']);

    //save the staining values to the requestObj
    $consultation_edit_forms_obj->collectDataFromFieldsetSingleStaining($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-single_staining']);
    $consultation_edit_forms_obj->collectDataFromFieldsetDualStaining($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-dual_staining']);
    $consultation_edit_forms_obj->collectDataFromFieldsetMultiStaining($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-multi_staining']);

    // save the sample values to the requestObj
    $consultation_edit_forms_obj->collectDataFromFieldsetSample($form_state['values']['tabs']['Consultation']['fieldset-sample_documentation']);

    // Add a new staining if the add_staining variable is set.
    if (isset($form_state['add_staining'])) {
      $requestObj->addStaining(rdp_alm_request_create_new_staining($form_state));
    }

    // Add a new sample documentation object if the add_sample variable is set.
    if (isset($form_state['add_sample'])) {
      $requestObj->addSample(rdp_alm_request_create_new_sample($form_state));
    }
  }
}

/**
 * Submits the request by setting the state variable to "pending" (@param $form
 *
 * @param $form_state
 *
 * @see ALMRequestState) and save the other fields to database
 */
function rdp_alm_request_edit_submit($form, &$form_state) {
  // update request
  // get request from database
  $request = ALMRequestsRepository::findById($form_state[ALM_REQUEST_ID]);

  // Check if the user has permission to submit(edit) the request
  if (!$request->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT)) {
    drupal_set_message('Error: A problem occured by processing request object. Please reload the page. (Code: ' . SFB_ALM_ERROR_EDIT_REQUEST_SUBMIT . ')',
      'error');
    drupal_access_denied();
    return;
  }

  // get input data from edit form and set the request values
  rdp_alm_request_edit_write_form_state_values_to_request_obj($request, $form_state);

  //  Sets submitDate and RequestState
  $request->setSubmitAttributes();

  //Update the User Comment
  // Sets the CommentType to Submit because than it isnt be possible to edit the comment again.
  $request->getUpdatedComment()->setTypeID(ALMCommentType::Submit);


  // save request in database
  $request->save();

  $request->unlock();

  $msg = 'Request ' . l($request->getRequestID(), sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request->getRequestID()))
    . ' submitted successfully.';
  if (variable_get(SFB_ALM_VAR_CONFIG_MANAGER_MODE, 1)) {
    // Only send notifications to service unit team, if module is configured in "active manager mode"
    $msg .= ' E-mail notifications have been sent to the service team.';
  }
  else {
    /**
     * Auto-accept request if "active manager mode" is deactivated
     */
    if ($request->getRequestState() == ALMRequestState::Pending) {
      $request->setRequestState(ALMRequestState::Accepted);
      $request->save(FALSE);
      ALMUserQualificationRepository::setQualificationLevelOfUserId($request->getUserID(),
        ALMUserQualificationRepository::$QUALIFICATION_LEVEL_USER_CONSULTATION);
    }
  }
  drupal_set_message($msg);
  // Send an e-mail to user (and alm team
  sfb_alm_mail_send_new_request_notification($request);

  //redirect to overview
  $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST)];
}

/**
 * Tries to submit the request and change the state to running.
 * If the save function throws a WrongAntibodyInputException, the consultation will not be finished
 *
 * @param $form
 * @param $form_state
 *
 * @return bool
 */
function rdp_alm_request_edit_submit_run($form, &$form_state) {
  try {
    $requestID = $form_state[ALM_REQUEST_ID];
    $requestObj = ALMRequestsRepository::findById($requestID);

    // Check permission
    if (!$requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CONSULT)) {
      drupal_set_message('Error: A problem occured by processing request staining_object. Please reload the page. (Code: ' . SFB_ALM_ERROR_EDIT_REQUEST_SUBMIT . ')',
        'error');
      drupal_access_denied();
      return FALSE;
    }

    rdp_alm_request_edit_write_form_state_values_to_request_obj($requestObj, $form_state);

    $requestObj->setRunAttributes();
    $requestObj->save();
    drupal_set_message('Consultation of request ' . $requestObj->getRequestID() . ' was finished');

    $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST, $form_state[ALM_REQUEST_ID])];
  } catch (WrongAntibodyInputException $e) {
    drupal_set_message(t('The consultation is NOT finish now!'), 'warning');
    $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1'];
  }
}


/////// Util functions //////////////////
///
/**
 * Creates a new Staining (depending on $form_state['add_staining']) and returns the object
 *
 * @param $form_state
 *
 * @return ALMRequestStaining
 */
function rdp_alm_request_create_new_staining($form_state) {
  $requestID = $form_state[ALM_REQUEST_ID];

  $staining = new ALMRequestStaining();
  $staining->setRequestId($requestID);
  // Choose the type of the new staining
  switch ($form_state['add_staining']) {
    case ALM_REQUEST_ADD_SINGLE_STAINING:
      $staining->setType(ALMRequestStaining::TYPE_SINGLE);
      break;
    case ALM_REQUEST_ADD_DUAL_STAINING:
      $staining->setType(ALMRequestStaining::TYPE_DUAL);
      break;
    case ALM_REQUEST_ADD_MULTI_STAINING:
      $staining->setType(ALMRequestStaining::TYPE_MULTI);
      break;
  }
  // Initial the staining rows (depending the staining type) to prevent "missing index errors"
  $staining->initialDefaultStainingRows();
  $staining->setEditable(TRUE);

  return $staining;
}

/**
 * Creates a new Sample Documentation which is assigned to
 *
 * @param $form_state
 *
 * @return ALMRequestSample
 */
function rdp_alm_request_create_new_sample($form_state) {
  $sample = new ALMRequestSample();
  $sample->setRequestID($form_state[ALM_REQUEST_ID]);
  $sample->setEditable(TRUE);
  $sample->assignSerialNumber();

  return $sample;
}

function rdp_alm_request_submit_copy_sample($form, &$form_state) {
  try {
    rdp_alm_request_edit_save_to_database($form_state);

    // Add new Sample
    $sample = ALMRequestSampleRepository::findBySampleId($form_state['triggering_element']['#name']);
    $sample->setEditable(TRUE);
    $sample->assignNewSerialNumber();
    $sample->resetDatabaseId();
    $sample->save();

    // save the scroll position
    $scrollPosition = $form_state['values']['scrollPos'];

    // not possible to use the redirect function because the request must be saved before the sample is copied.
    $form_state['redirect'] = [
      sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1&scrollTo=' . $scrollPosition,
    ];
  } catch (WrongAntibodyInputException $e) {
    $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1'];
  }
}

/**
 * Removes a sample. The sampleID it gathered from the '#name' field of the triggering element
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_submit_remove_sample($form, &$form_state) {
  //todo 30.may.18 Check permission --> is the id really from the currently opend request and is the user allready allowed to delet it?

  rdp_alm_request_edit_save_and_redirect_to_scroll_pos($form, $form_state);
  // The name of the trigger event is the unique Sample id
  ALMRequestSampleRepository::deleteById($form_state['triggering_element']['#name']);

}


/**
 * Removes a staining. The stainingId is collected from the '#name' field of the triggering element
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_submit_remove_staining($form, &$form_state) {

  //todo 30.may.18 Check permission --> is the id really from the currently opend request and is the user allready allowed to delet it?

  rdp_alm_request_edit_save_and_redirect_to_scroll_pos($form, $form_state);

  // The name of the trigger event is the unique staining id
  ALMRequestStainingRepository::deleteById($form_state['triggering_element']['#name']);

}

/**
 * Submit function to add a new single staining
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_edit_submit_add_single_staining($form, &$form_state) {
  // When the add_staining varible is set, the next save_all invoke will create a new staining field.
  // The value of the variable gives the type of the new staining
  $form_state['add_staining'] = ALM_REQUEST_ADD_SINGLE_STAINING;

  rdp_alm_request_edit_save_and_redirect_to_scroll_pos($form, $form_state);
}

/**
 * Submit function to add a new dual staining
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_edit_submit_add_dual_staining($form, &$form_state) {
  // When the add_staining varible is set, the next save_all invoke will create a new staining field.
  // The value of the variable gives the type of the new staining
  $form_state['add_staining'] = ALM_REQUEST_ADD_DUAL_STAINING;

  rdp_alm_request_edit_save_and_redirect_to_scroll_pos($form, $form_state);
}

/**
 * Submit function to add a new multi staining
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_edit_submit_add_multi_staining($form, &$form_state) {
  // When the add_staining varible is set, the next save_all invoke will create a new staining field.
  // The value of the variable gives the type of the new staining
  $form_state['add_staining'] = ALM_REQUEST_ADD_MULTI_STAINING;

  rdp_alm_request_edit_save_and_redirect_to_scroll_pos($form, $form_state);
}

function rdp_alm_request_edit_submit_add_sample_doc($form, &$form_state) {
  $form_state['add_sample'] = '';
  rdp_alm_request_edit_save_and_redirect_to_scroll_pos($form, $form_state);
}

function rdp_alm_request_edit_save_and_redirect_to_scroll_pos($form, &$form_state) {
  try {
    $scrollPosition = $form_state['values']['scrollPos'];
    rdp_alm_request_edit_save_to_database($form_state);
    $form_state['redirect'] = [
      sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1&scrollTo=' . $scrollPosition,
    ];
  } catch (WrongAntibodyInputException $e) {
    $form_state['redirect'] = [sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $form_state[ALM_REQUEST_ID]) . '?active=1'];
  }
}

/**
 * Check that all Labels has a label name before the request can set to run
 *
 * @param $form
 * @param $form_state
 */
function rdp_alm_request_edit_submit_run_validate($form, &$form_state) {
  // Check that every Staining has a label name

  // single stainings
  foreach ($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-single_staining'] as $id => $staining_table) {
    // First Check if the $staining_table variable is an array (to sort the buttons out)
    // Second check if the short_label field contains any text, if not set an form error.
    if (is_array($staining_table) && empty($staining_table['pre_table_form']['short_label'])) {
      $element_id = 'tabs][Consultation][fieldset-stainings][fieldset-single_staining][' . $id . '][pre_table_form][short_label';
      form_set_error($element_id, 'Please enter a name for the "short label name field" in the consultation form');
    }
  }

  // dual stainings
  foreach ($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-dual_staining'] as $id => $staining_table) {
    // First Check if the $staining_table variable is an array (to sort the buttons out)
    // Second check if the short_label field contains any text, if not set an form error.
    if (is_array($staining_table) && empty($staining_table['pre_table_form']['short_label'])) {
      $element_id = 'tabs][Consultation][fieldset-stainings][fieldset-dual_staining][' . $id . '][pre_table_form][short_label';
      form_set_error($element_id, 'Please enter a name for the "short label name field" in the consultation form');
    }
  }

  // multi stainings
  foreach ($form_state['values']['tabs']['Consultation']['fieldset-stainings']['fieldset-multi_staining'] as $id => $staining_table) {
    // First Check if the $staining_table variable is an array (to sort the buttons out)
    // Second check if the short_label field contains any text, if not set an form error.
    if (is_array($staining_table) && empty($staining_table['pre_table_form']['short_label'])) {
      $element_id = 'tabs][Consultation][fieldset-stainings][fieldset-multi_staining][' . $id . '][pre_table_form][short_label';
      form_set_error($element_id, 'Please enter a name for the "short label name field" in the consultation form');
    }
  }
}
