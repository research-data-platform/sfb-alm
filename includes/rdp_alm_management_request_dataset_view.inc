<?php
/**
 * @file Drupal callback for dataset display page
 */

/**
 * This page shows an image dataset including the metadata for the user.
 */
function rdp_alm_management_request_dataset_view_form($form = null, &$form_state, $requestid = NULL, $objectid = NULL) {
  $datasetViewManagementPage = new AlmDatasetViewManagementPage();
  return $datasetViewManagementPage->getPageForm($form, $form_state, $requestid, $objectid);
}
