<?php
/**
 * @file Drupal callback for dataset display page
 */

/**
 * This page shows an image dataset including the metadata for the user.
 */
function rdp_alm_request_dataset_view_form($form = null, &$form_state, $requestid = NULL, $objectid = NULL) {
  $datasetViewPage = new AlmDatasetViewPage();
  return $datasetViewPage->getPageForm($form, $form_state, $requestid, $objectid);
  //return $datasetViewPage->getPageHtml($requestid, $objectid);
}

function rdp_alm_dataset_ajax_callback($form, $form_state){
  $page = new AlmDatasetViewPage();
  return $page->almDatasetViewAjaxCallback($form, $form_state);
}

function rdp_alm_dataset_ajax_share($form, &$form_state){
  $page = new AlmDatasetViewPage();
  $page->almDatasetViewAjaxShare($form, $form_state);
}

function rdp_alm_dataset_ajax_revoke($form, &$form_state){
  $page = new AlmDatasetViewPage();
  $page->almDatasetViewAjaxRevoke($form, $form_state);
}

