<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 28.10.2018
 * Time: 19:30
 */

function rdp_alm_request_dataset_comment_form($form = null, &$form_state, $request_id, $dataset_id){

  $request = ALMRequestsRepository::findById($request_id);
  $comment_id = ALMDatasetRepository::findById($dataset_id)->getCommentId();

  $form_state[ALM_REQUEST_ID] = $request_id;
  $form_state['dataset_id'] = $dataset_id;
  $form_state['comment_id'] = $comment_id;

  // Check permission
  if(!$request->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_VIEW_IMAGES)){
    drupal_access_denied();
  }

  $comment_obj = ALMCommentRepository::findCommentByCommentId($comment_id);

  $form['fieldset-image_comment'] = array(
    '#type' => 'fieldset',
    '#title' => '<i class="fa fa-user"></i> ' . t('Comment'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-image_comment']['comment'] = array(
    '#type' => 'textarea',
    '#default_value' => $comment_obj->getMessage(),
    '#field_prefix' => '<div class="row"><div class="col-lg-8">',
    '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
  );

  // cancel this form
  $form['btn-cancel'] = array(
    '#type' => 'markup',
    '#markup' => '<a href="'. sfb_alm_url(RDP_ALM_URL_REQUEST_DATASET_VIEW,  $request_id, $dataset_id) .'" class="btn btn-default" style = "margin-right: 5px"><i class="fa fa-arrow-left"></i> Cancel</a>',
  );

  $form['btn-submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Comment'),
    '#description' => t('Saves the new comment'),
  );

  return $form;
}

function rdp_alm_request_dataset_comment_form_submit($form = null, &$form_state){

  $request_id = $form_state[ALM_REQUEST_ID];
  $dataset_id = $form_state['dataset_id'];
  $comment_id = $form_state['comment_id'];

  $request = ALMRequestsRepository::findById($request_id);

  // Check permission
  if(!$request->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_VIEW_IMAGES)) {
    drupal_access_denied();
  }

  //Update the Withdraw Comment
  $comment = ALMCommentRepository::findCommentByCommentId($comment_id);
  $comment->setMessage($form_state['values']['comment']);
  $comment->setCurrentDate();

  // Sets the CommentType to Submit because than it isn be possible to edit the comment again.
  $comment->setTypeID(ALMCommentType::Withdraw);

  ALMCommentRepository::save($comment);

  // save request in database
  $request->save();

  drupal_set_message('The comment was saved successfully');
  $form_state['redirect'] = sfb_alm_url(RDP_ALM_URL_REQUEST_DATASET_VIEW,  $request_id, $dataset_id) ;
}