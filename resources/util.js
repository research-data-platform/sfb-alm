/**
 * Created by Henke_Christian on 23.09.2016.
 */
function addParameterToURL(parameter, value) {
    if(window.location.search == ''){
        window.location.search = parameter + '=' + value;
    } else{
        window.location.search = window.location.search + '&' + parameter + '=' + value;
    }
}

/**
 * Saves the Y-scroll position in the first Element with the name(not id) scrollPos
 * In general this should be a hidden Element
 */
function saveScrollPosition(){
    scrollPos = (document.documentElement && document.documentElement.scrollTop) ||
        document.body.scrollTop;
    document.getElementsByName('scrollPos')[0].value = scrollPos;
}

/*$('#myform').submit(function() {
    document.getElementById('edit-tabs-consultation-fieldset-general-attending').value = "blaaaaa";
    return true; // return false to cancel form action
});*/