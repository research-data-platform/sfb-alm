<?php

class ALMRequest {

  /**
   * Default ID for empty requests.
   * Empty means, that request is not (yet) stored in the database or
   * does not exists in the database
   *
   * @var int
   */
  const EMPTY_REQUEST_ID = -1;

  /**
   * Default PID prefix
   */
  const DEFAULT_PID_PREFIX = '';

  /**
   * Enumeration with ALMRequest actions
   */
  const ACTION_EDIT = 'edit';

  const ACTION_VIEW = 'view';

  const ACTION_WITHDRAW = 'withdraw';

  const ACTION_REVISE = 'revise';

  const ACTION_ACCEPT = 'accept';

  const ACTION_CONSULT = 'consult';

  const ACTION_CREATE_COVERSLIPS = 'create_coverslips';

  const ACTION_VIEW_IMAGES = 'show_images';

  const ACTION_EDIT_IMAGES = 'edit_images';

  const ACTION_VIEW_CONSULTATION = 'view_consultation';

  const ACTION_CLOSE = 'close_request';

  const ACTION_ADD_STAININGS = 'add_stainings';

  const ACTION_ADD_SAMPLES = 'add_samples';

  /**
   * Enumeration with possible ALM roles
   */
  const ROLE_USER = 'user';

  const ROLE_MANAGER = 'manager';

  /* ------------------------------------------------------------------------ */
  /* ---------------------------- class members ----------------------------- */
  /* ------------------------------------------------------------------------ */

  /* ------------------------ request-form variables ------------------------ */

  /**
   * Unique ID of a request
   *
   * @var int
   */
  private $requestID = ALMRequest::EMPTY_REQUEST_ID;

  /**
   * User UID (owner of this request)
   *
   * @var int
   */
  private $userID;

  /**
   * Position of the user within the research group
   *
   * @see sfb_alm_api_autocomplete()
   * @var int (max. 11 numbers)
   */
  private $position;

  /**
   * Institute where the user is member of
   *
   * @var String (max. 100 characters)
   */
  private $institut;

  /**
   * ID of a working group within th USer
   *
   * @see WorkingGroup
   *
   * @var int
   */
  private $researchGroupID;

  /**
   * ID of a subproject
   *
   * @see Subproject
   * @var int (max. 11 numbers)
   */
  private $subprojectID;

  /**
   * E-Mail adress of the user
   *
   * @var int (max. 11 numbers)
   */
  private $email;

  /**
   * Phone number of the user
   *
   * @var String (max. 100 characters)
   */
  private $phoneNumber;


  /**
   * Imaging goal of the microsocopy request
   *
   * @var String
   */
  private $imagingGoal;

  /**
   * The type of the cells, the researcher intends to investigate
   *
   * @var String (max. 100 characters)
   */
  private $sampleType;

  /**
   * The target structure of the staining
   *
   * @var String (max. 100 characters)
   */
  private $targetStructure;

  /**
   * The condition of the samples
   *
   * @var String (max. 100 characters)
   */
  private $conditions;

  /**
   * Tha staining method, the researcher intends to investigate
   *
   * @var String (max. 100 characters)
   */
  private $stainingMethod;

  /**
   * The descriptive or quantitative data the researcher requires
   *
   * @var String
   */
  private $dataRequired;

  /**
   * Specifies which imaging support the researcher requires from the alm-team
   *
   * IDs with chosen Imaging support options
   *
   * @see getAllImagingSupportChoices
   *
   * @var int[]
   */
  private $imagingSupportIDs;

  /**
   * Specifies which sample health documentation the researcher has selected
   *
   * IDs with chosen Sample health options.
   * All options are defined in ALMRequest::getAllSampleHealthChoices()
   *
   * @see getAllSampleHealthChoices
   * @var int[]
   */
  private $sampleHealthID;

  /**
   * Specifies which imaging modules the researcher has selected.
   *
   * IDs with chosen imaging module options.
   * All options are defined in ALMRequest::getAllImagingModules()
   *
   * @see getAllImagingModules()
   * @var int[]
   */
  private $imagingModuleIDs;

  /**
   * Specifies which analysis modules the researcher has selected
   *
   * IDs with chosen analysis module options.
   * All options are defined in ALMRequest::getAllAnalysisModules()
   *
   * @see getAllAnalysisModules()
   * @var int[]
   */
  private $analysisModuleIDs;

  /**
   * Specifies which kind of introduction the researcher requires
   *
   * IDs with chosen intruction to alm options.
   * All options are defined in ALMRequest::
   *
   * @see getAllIntroductionsToAlmChoices()
   * @var int[] max length: 50
   */
  private $introductionToAlmIDs;


  /* ------------------------ consultation-form variables ------------------------- */

  /**
   * All participians of the consultation meeting
   *
   * @var String (max 250 characters)
   */
  private $consultationAttending;

  /**
   * The documentation of previous practical experiences
   *
   * @var String
   */
  private $consultationPreviousPracticalExperiences;

  /**
   * Documentation, that describes the requested imaging support of the
   * researcher
   *
   * @var
   */
  private $consultationImagingSupport;

  /**
   * Documentation, that describes the requested imaging modules
   *
   * @var Srting
   */
  private $consultationImagingModule;

  /**
   * Documentation, that describes the requested sample health
   */
  private $consultationSampleHealth;

  /**
   * Documentation, that describes the requested analysis modules
   *
   * @var
   */
  private $consultationAnalysisModule;

  /**
   * The ID of the responsible ALM-Manager
   *
   * @var
   */
  private $consultationResponsibleS02MemberID;

  /**
   * Array of the associated Single Staining objects
   *
   * @var ALMRequestStaining[]
   */
  private $consultationSingleStainings;

  /**
   * Array of the associated Dual Staining objects
   *
   * @var ALMRequestStaining[]
   */
  private $consultationDualStainings;

  /**
   * Array of the associated Multi Staining objects
   *
   * @var ALMRequestStaining[]
   */
  private $consultationMultiStainings;

  /**
   * Array of the associated Sample objects
   *
   * @var ALMRequestSample[]
   */
  private $consultationSamples;

  /* ------------------------ Controll variables ------------------------- */

  /**
   * Current State of the request
   *
   * @see ALMRequestState
   * @var ALMRequestState|int
   */
  private $requestState;


  /**
   * UID of the user who lock this request currently.
   * If no user lock this reqeust, this variable isn't set
   *
   * @var int (max. 11)
   */
  private $lockedBy;

  /**
   * The latest updated comment, if the comment has been updated since the last
   * storage into the database.
   *
   * @var ALMComment
   */
  private $updatedComment;

  /* ------------------------------------------------------------------------ */
  /* ------------------------- GETTER / SETTER ------------------------------ */
  /* ------------------------------------------------------------------------ */

  /* --------------------- request-form getter / setter --------------------- */

  /**
   * @return int (max. 11 numbers)
   */
  public function getRequestID() {
    return $this->requestID;
  }

  /**
   * @param int $requestID (max. 11 numbers)
   */
  public function setRequestID($requestID) {
    $this->requestID = $requestID;
  }


  /**
   * @return int (max. 11 numbers)
   */
  public function getUserID() {
    return $this->userID;
  }

  /**
   * @param int $userID (max. 11 numbers)
   */
  public function setUserID($userID) {
    $this->userID = $userID;
  }

  /**
   * @return String (max. 100 character)
   */
  public function getPosition() {
    return $this->position;
  }

  /**
   * @param string $position (max. 100 character)
   */
  public function setPosition($position) {
    $this->position = $position;
  }

  /**
   * @return String  (max. 100 character)
   */
  public function getInstitute() {
    return $this->institut;
  }

  /**
   * @param string $institut (max. 100 character)
   */
  public function setInstitut($institut) {
    $this->institut = $institut;
  }

  /**
   * If a researchgroup ID is set, returns the ID. If not, looks for the first
   * researchgroup of the user an return that ID. If The user has no
   * researchgroup, returns ALM_FORM_SELECT_EMPTY
   *
   * @return int (max. 11 numbers)
   */
  public function getResearchGroupID() {
    if (isset($this->researchGroupID)) {
      return $this->researchGroupID;
    }
    global $user;
    $userObject = UsersRepository::findByUid($user->uid);
    $researchGroups = $userObject->getUserWorkingGroups();
    if (isset($researchGroups[0])) {
      return $researchGroups[0]->getId();
    }
    return ALM_FORM_SELECT_EMPTY;
  }

  /**
   * @param int $researchGroupID (max. 11 numbers)
   */
  public function setResearchGroupID($researchGroupID) {
    $this->researchGroupID = $researchGroupID;
  }

  /**
   * @return int (max. 11 numbers)
   */
  public function getSubprojectID() {
    return $this->subprojectID;
  }

  /**
   * @param int $subprojectID
   */
  public function setSubprojectID($subprojectID) {
    $this->subprojectID = $subprojectID;
  }

  /**
   * @return String (max. 100 character)
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @param string $email (max. 100 character)
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * @return String (max. 100 character)
   */
  public function getPhoneNumber() {
    return $this->phoneNumber;
  }

  /**
   * @param mixed $phoneNumber (max. 100 character)
   */
  public function setPhoneNumber($phoneNumber) {
    $this->phoneNumber = $phoneNumber;
  }

  /**
   * @return String
   */
  public function getImagingGoal() {
    return $this->imagingGoal;
  }

  /**
   * @param String $imagingGoal
   */
  public function setImagingGoal($imagingGoal) {
    $this->imagingGoal = $imagingGoal;
  }

  /**
   * @return String (max. 100 character)
   */
  public function getSampleType() {
    return $this->sampleType;
  }

  /**
   * @param String $sampleType (max. 100 character)
   */
  public function setSampleType($sampleType) {
    $this->sampleType = $sampleType;
  }

  /**
   * @return String (max. 100 character)
   */
  public function getTargetStructure() {
    return $this->targetStructure;
  }

  /**
   * @param String $targetStructure (max. 100 character)
   */
  public function setTargetStructure($targetStructure) {
    $this->targetStructure = $targetStructure;
  }

  /**
   * @return String
   */
  public function getConditions() {
    return $this->conditions;
  }

  /**
   * @param String $conditions (max. 100 character)
   */
  public function setConditions($conditions) {
    $this->conditions = $conditions;
  }

  /**
   * @return String (max. 100 character)
   */
  public function getStainingMethod() {
    return $this->stainingMethod;
  }

  /**
   * @param String $stainingMethod (max. 100 character)
   */
  public function setStainingMethod($stainingMethod) {
    $this->stainingMethod = $stainingMethod;
  }

  /**
   * @return String
   */
  public function getDataRequired() {
    return $this->dataRequired;
  }

  /**
   * @param String $dataRequired
   */
  public function setDataRequired($dataRequired) {
    $this->dataRequired = $dataRequired;
  }

  /**
   * Specifies which imaging support the researcher requires from the alm-team
   *
   * IDs with chosen Imaging support options
   *
   * @see getAllImagingSupportChoices
   *
   * @return int[]
   */
  public function getImagingSupportIDs() {
    return isset($this->imagingSupportIDs) ? $this->imagingSupportIDs : [0];
  }

  /**
   * Specifies which imaging support the researcher requires from the alm-team
   *
   * IDs with chosen Imaging support options
   *
   * @see getAllImagingSupportChoices
   *
   * @param int[] $imagingSupportIDs
   */
  public function setImagingSupportIDs($imagingSupportIDs) {
    $this->imagingSupportIDs = $imagingSupportIDs;
  }

  /**
   * Specifies which sample health documentation the researcher has selected
   *
   * IDs with chosen Sample health options.
   * All options are defined in ALMRequest::getAllSampleHealthChoices()
   *
   * @see getAllSampleHealthChoices
   *
   * @return int[]
   */
  public function getSampleHealthID() {
    return $this->sampleHealthID;
  }

  /**
   * Specifies which sample health documentation the researcher has selected
   *
   * IDs with chosen Sample health options.
   * All options are defined in ALMRequest::getAllSampleHealthChoices()
   *
   * @see getAllSampleHealthChoices
   *
   * @param int[] $sampleHealthID
   */
  public function setSampleHealthID($sampleHealthID) {
    $this->sampleHealthID = $sampleHealthID;
  }

  /**
   * pecifies which imaging modules the researcher has selected.
   *
   * IDs with chosen imaging module options.
   * All options are defined in ALMRequest::getAllImagingModules()
   *
   * @see getAllImagingModules()
   *
   * @return int[]
   */
  public function getImagingModuleIDs() {
    return isset($this->imagingModuleIDs) ? $this->imagingModuleIDs : [0];
  }

  /**
   * pecifies which imaging modules the researcher has selected.
   *
   * IDs with chosen imaging module options.
   * All options are defined in ALMRequest::getAllImagingModules()
   *
   * @see getAllImagingModules()
   *
   * @param int[] $imagingModuleIDs
   */
  public function setImagingModuleIDs($imagingModuleIDs) {
    $this->imagingModuleIDs = $imagingModuleIDs;
  }

  /**
   * Specifies which analysis modules the researcher has selected
   *
   * IDs with chosen analysis module options.
   * All options are defined in ALMRequest::getAllAnalysisModules()
   *
   * @see getAllAnalysisModules()
   *
   * @return int[]
   */
  public function getAnalysisModuleIDs() {
    return isset($this->analysisModuleIDs) ? $this->analysisModuleIDs : [0];
  }

  /**
   * Specifies which analysis modules the researcher has selected
   *
   * IDs with chosen analysis module options.
   * All options are defined in ALMRequest::getAllAnalysisModules()
   *
   * @see getAllAnalysisModules()
   *
   * @param int[] $analysisModuleIDs
   */
  public function setAnalysisModuleIDs($analysisModuleIDs) {
    $this->analysisModuleIDs = $analysisModuleIDs;
  }

  /**
   * Specifies which kind of introduction the researcher requires
   *
   * IDs with chosen intruction to alm options.
   * All options are defined in ALMRequest::
   *
   * @see getAllIntroductionsToAlmChoices()
   *
   * @return int[]
   */
  public function getIntroductionToAlmIDs() {
    return isset($this->introductionToAlmIDs) ? $this->introductionToAlmIDs : [0];
  }

  /**
   * Specifies which kind of introduction the researcher requires
   *
   * IDs with chosen intruction to alm options.
   * All options are defined in ALMRequest::
   *
   * @see getAllIntroductionsToAlmChoices()
   *
   * @param int[] $introductionToAlmIDs
   */
  public function setIntroductionToAlmIDs($introductionToAlmIDs) {
    $this->introductionToAlmIDs = $introductionToAlmIDs;
  }


  /* --------------------- consultation-form getter / setter --------------------- */

  /**
   * @return String (max. 250 character)
   */
  public function getConsultationAttending() {
    return $this->consultationAttending;
  }

  /**
   * @param String (max. 250 character)
   */
  public function setConsultationAttending($consultationAttending) {
    $this->consultationAttending = $consultationAttending;
  }

  /**
   * @return String
   */
  public function getConsultationPreviousPracticalExperiences() {
    return $this->consultationPreviousPracticalExperiences;
  }

  /**
   * @param String
   */
  public function setConsultationPreviousPracticalExperiences($consultationPreviousPracticalExperiences) {
    $this->consultationPreviousPracticalExperiences = $consultationPreviousPracticalExperiences;
  }

  /**
   * @return String
   */
  public function getConsultationImagingSupport() {
    return $this->consultationImagingSupport;
  }

  /**
   * @param String $consultationImagingSupport
   */
  public function setConsultationImagingSupport($consultationImagingSupport) {
    $this->consultationImagingSupport = $consultationImagingSupport;
  }

  /**
   * @return mixed
   */
  public function getConsultationImagingModule() {
    return $this->consultationImagingModule;
  }

  /**
   * @param mixed $consultationImagingModule
   */
  public function setConsultationImagingModule($consultationImagingModule) {
    $this->consultationImagingModule = $consultationImagingModule;
  }

  /**
   * @return mixed
   */
  public function getConsultationSampleHealth() {
    return $this->consultationSampleHealth;
  }

  /**
   * @param mixed $consultationSampleHealth
   */
  public function setConsultationSampleHealth($consultationSampleHealth) {
    $this->consultationSampleHealth = $consultationSampleHealth;
  }

  /**
   * @return mixed
   */
  public function getConsultationAnalysisModule() {
    return $this->consultationAnalysisModule;
  }

  /**
   * @param mixed $consultationAnalysisModule
   */
  public function setConsultationAnalysisModule($consultationAnalysisModule) {
    $this->consultationAnalysisModule = $consultationAnalysisModule;
  }

  /**
   * @return int
   */
  public function getConsultationResponsibleS02MemberID() {
    return $this->consultationResponsibleS02MemberID;
  }

  /**
   * @param int $consultationResponsibleS02MemberID
   */
  public function setConsultationResponsibleS02MemberID($consultationResponsibleS02MemberID) {
    $this->consultationResponsibleS02MemberID = $consultationResponsibleS02MemberID;
  }

  /* --------------------- controll getter / setter --------------------- */

  /**
   * @return ALMRequestState
   */
  public function getRequestState() {
    return $this->requestState;
  }

  /**
   * @param ALMRequestState $requestState
   */
  public function setRequestState($requestState) {
    $this->requestState = $requestState;
  }

  /**
   * Sets the updatedComment Variable, so the next save() will update or create
   * that comment in the Database. Attention! A request object can only store
   * one comment for update. Otherwise please user the  ALMCommentRepository
   * class
   *
   * @param ALMComment $comment
   */
  public function setUpdatedComment($comment) {
    $this->updatedComment = $comment;
  }

  /**
   * @return ALMComment
   */
  public function getUpdatedComment() {
    return $this->updatedComment;
  }

  /* ------------------------------------------------------------------------ */
  /* ------------------------------ Functions ------------------------------- */
  /* ------------------------------------------------------------------------ */

  /* --------------------- Lock functions --------------------- */

  /**
   * Set the lockedBy Variable and saves the (complete request) to the database
   */
  public function lock() {
    global $user;
    $this->lockedBy = $user->uid;
    $this->save(FALSE);
  }

  /**
   * Reset the lockedBy Variable and saves the (complete request) to the
   * database
   */
  public function unlock() {
    $this->lockedBy = NULL;
    $this->save(FALSE);
  }

  /**
   * @return mixed
   */
  public function getLockedBy() {
    return $this->lockedBy;
  }

  /**
   * @param mixed $lockedBy
   */
  public function setLockedBy($lockedBy) {
    $this->lockedBy = $lockedBy;
  }

  /**
   * Checks if the request is locked currently.
   * True: Request is locked
   * False: Request is unlocked (no lock variable is set)
   *
   * @return bool
   */
  public function isLocked() {
    // The request is locked if the locked variable is set.
    if (isset($this->lockedBy) && $this->lockedBy != NULL) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /* --------------------- State functions --------------------- */

  /**
   * Initial the request object for a new Request.
   * New  requestState to: Edit (@see ALMRequestState::Edit)
   *      userID       to: current userID
   *      requestID    to: generate new
   */
  public function setNewRequestAttributes() {
    global $user;
    // set request state to EDIT
    $this->requestState = ALMRequestState::Edit;
    // set userID to current user id
    $this->setUserID($user->uid);
    // set new unique Request ID
    //$this->setRequestID(sfb_alm_generate_new_unique_request_id());
  }

  /**
   * Set the request attributes for submit:
   * New Request State: Pending (@see ALMRequestState::Pending)
   * submit Date: current date
   */
  public function setSubmitAttributes() {
    // set request state to Pending
    $this->requestState = ALMRequestState::Pending;

    sfb_alm_create_log_entry(ALMLog::$ACTION_SUBMIT, 'The request was submitted by the user', $this->requestID);
  }

  /**
   * Set the request atttribute for consulting
   * New Request State: Running (@see ALMRequestState::Running)
   */
  public function setRunAttributes() {
    $this->requestState = ALMRequestState::Running;
    sfb_alm_create_log_entry(ALMLog::$ACTION_CONSULT, 'The request was consulted', $this->requestID);
  }

  /**
   * Set the
   */
  public function setAcceptAttributes() {
    // set request state to Pending
    $this->requestState = ALMRequestState::Accepted;
    // set acceptDate to current
    #$this->acceptDate = date(SFB_ALM_DATEFORMAT);

    sfb_alm_create_log_entry(ALMLog::$ACTION_ACCEPT, 'The request was accepted by an ALM Manager ', $this->requestID);
  }

  /**
   * Set the
   */
  public function setFinishedAttributes() {
    // set request state to Pending
    $this->requestState = ALMRequestState::Finished;
    // set acceptDate to current
    #$this->acceptDate = date(SFB_ALM_DATEFORMAT);

    sfb_alm_create_log_entry(ALMLog::$ACTION_FINISH, 'The request was finished/closed by an ALM Manager ', $this->requestID);
  }

  public function setReviseAttributes() {
    // set request state to Revisiont
    $this->requestState = ALMRequestState::Revision;

    #$this->acceptDate = null;

    sfb_alm_create_log_entry(ALMLog::$ACTION_REVISE, 'The request was revised by an ALM Manager ', $this->requestID);
  }

  public function setWithdrawAttributes() {
    $this->requestState = ALMRequestState::Withdraw;

    sfb_alm_create_log_entry(ALMLog::$ACTION_WITHDRAW, 'The request was withdrawed by', $this->requestID);
  }

  /**
   * Reset a running request to the accepted state.
   * So the request becomes editabe for the managers
   */
  public function resetRunningToAccepted() {
    $this->requestState = ALMRequestState::Accepted;
    sfb_alm_create_log_entry(ALMLog::$ACTION_MANAGER_RESET_TO_ACCEPTED, t('The Request was resetted by an ALM Manager. The request can be edit again.'), $this->requestID);
  }

  /* --------------------- Date functions --------------------- */

  /**
   * @return mixed
   */
  public function getReviseDate() {
    $reviseDate = ALMLogRepository::getLatestActionDateByRequestID($this->requestID, ALMLog::$ACTION_REVISE);
    if ($reviseDate == ALMLogRepository::EMPTY_DATE) {
      return '-';
    }
    return $reviseDate;
  }

  /**
   * @return mixed
   */
  public function getWithdrawDate() {
    $reviseDate = ALMLogRepository::getLatestActionDateByRequestID($this->requestID, ALMLog::$ACTION_WITHDRAW);
    if ($reviseDate == ALMLogRepository::EMPTY_DATE) {
      return '-';
    }
    return $reviseDate;
  }


  /**
   * @return mixed
   */
  public function getAcceptDate() {
    $acceptDate = ALMLogRepository::getLatestActionDateByRequestID($this->requestID, ALMLog::$ACTION_ACCEPT);
    if ($acceptDate == ALMLogRepository::EMPTY_DATE || $this->getReviseDate() > $acceptDate) {
      return '-';
    }
    return $acceptDate;
  }


  /**
   * @return mixed
   */
  public function getSubmitDate() {
    $submittedDate = ALMLogRepository::getLatestActionDateByRequestID($this->requestID, ALMLog::$ACTION_SUBMIT);
    if ($submittedDate == ALMLogRepository::EMPTY_DATE) {
      return '-';
    }
    return $submittedDate;
  }

  public function getConsultationDate() {
    $consultDate = ALMLogRepository::getLatestActionDateByRequestID($this->requestID, ALMLog::$ACTION_CONSULT);
    if ($consultDate == ALMLogRepository::EMPTY_DATE) {
      return '-';
    }
    return $consultDate;
  }

  /**
   * Returns the number of single immunofluorescence stainings.
   * Part of documentation of immunofluorescence stainings.
   *
   * @return int
   */
  function getSingleIfCount() {
    return count(ALMRequestStainingRepository::findAllStainingsByRequestIdAndStainingType($this->requestID, ALMRequestStaining::TYPE_SINGLE));
  }

  /**
   * Returns the number of dual immunofluorescence stainings.
   * Part of documentation of immunofluorescence stainings.
   *
   * @return int
   */
  function getDualIfCount() {
    return count(ALMRequestStainingRepository::findAllStainingsByRequestIdAndStainingType($this->requestID, ALMRequestStaining::TYPE_DUAL));
  }

  /**
   * Returns the number of multi immunofluorescence staining.
   * Part of documentation of immunofluorescence stainings.
   *
   * @return int
   */
  function getMultiIfCount() {
    return count(ALMRequestStainingRepository::findAllStainingsByRequestIdAndStainingType($this->requestID, ALMRequestStaining::TYPE_MULTI));
  }

  /**
   * Returns the number of samples.
   * Part of sample documentation.
   *
   * @return int
   */
  function getSampleCount() {
    return count(ALMRequestSampleRepository::findAllSamplesByRequestID($this->requestID));
  }


  public function getAllComments() {
    return ALMCommentRepository::findAllCommentsByRequestID($this->requestID);
  }

  public function getCommentWithType($commentType) {
    $comment = ALMCommentRepository::findAllCommentByRequestIdAndCommentType($this->requestID, $commentType);
    //If the CommentID is the EMTPY_COMMENT_ID, the comment is not in the database yet, so the type has to set.
    if ($comment->getCommentID() == ALMComment::EMPTY_COMMENT_ID) {
      $comment->setTypeID($commentType);
    }
    return $comment;
  }


  /**
   * Loads all associated samples directly from the database
   *
   * @return ALMRequestSample[]
   */
  public function loadAllSamples() {
    return ALMRequestSampleRepository::findAllSamplesByRequestID($this->requestID);
  }

  /**
   * Returns the Fullname of the responsible S02 member
   *
   * @return string
   */
  public function getConsultationResponsibleS02MemberName() {
    //Looks in the complete Database for the name of the S02 Member because it is possible that the responsible member has no S02 permissions anymore
    return "";
  }


  /**
   * @return mixed
   */
  public function getUserFullname() {
    return $this->getUser()->getFullname(TRUE);
  }

  /**
   * @return mixed
   */
  public function getUserAccountName() {
    return $this->getUser()->getName();
  }

  /**
   * Returns owners User object.
   *
   * @return User
   */
  public function getUser() {

    if (isset($this->userID) && $this->userID > 0) {
      $userc = UsersRepository::findByUid($this->getUserID());
      if ($userc->isEmpty()) {
        return NULL;
      }
      else {
        return $userc;
      }
    }
    return NULL;

  }


  /**
   * Name of the research group
   *
   * @return string
   */
  public function getResearchGroupName() {
    return WorkingGroupRepository::findById($this->researchGroupID)->getName();
  }

  public function getSubProjectAbbreviation() {
    if (isset($this->subprojectID)) {
      return $this->getAllSubprojectAbbreviations()[$this->subprojectID];
    }
    else {
      return '-';
    }
  }

  /**
   * @return string
   */
  public function getSubProjectName() {
    if (isset($this->subprojectID)) {
      return $this->getSubprojectFieldOptions()[$this->subprojectID];
    }
    else {
      return 'No subproject is selected';
    }
  }

  public function getImagingSupportTextArray() {
    if (isset($this->imagingSupportIDs)) {
      return $this->getSelectedValues($this->imagingSupportIDs, $this->getAllImagingSupportChoices());
    }
    else {
      return ['No imaging Support is selected'];
    }
  }

  /**
   * Returns a string with html code containing all imaging support, seperated
   * with </br> tags for new lines
   *
   * @return string
   */
  public function getImagingSupportTextAsMultiLineString() {
    return implode('</br>', $this->getImagingSupportTextArray());
  }

  public function getSampleHealthText() {
    if (isset($this->sampleHealthID)) {
      return $this->getAllSampleHealthChoices()[$this->sampleHealthID];
    }
    else {
      return 'No sample Health is selected';
    }
  }

  /**
   * Returns a string with all imaging support, seperated with </br> tags for
   * new lines
   *
   * @return string
   */
  public function getImagingModuleTextAsMultiLineString() {
    return implode('</br>', $this->getImagingModuleTextArray());
  }

  public function getImagingModuleTextArray() {
    if (isset($this->imagingModuleIDs)) {
      return $this->getSelectedValues($this->imagingModuleIDs, $this->getAllImagingModules());
    }
    else {
      return ['No imaging Module is selected'];
    }
  }

  /**
   * Returns a string with all imaging support, seperated with </br> tags for
   * new lines
   *
   * @return string
   */
  public function getAnalysisModuleTextAsMultiLineString() {
    return implode('</br>', $this->getAnalysisModuleTextArray());
  }

  public function getAnalysisModuleTextArray() {
    if (isset($this->analysisModuleIDs)) {
      return $this->getSelectedValues($this->analysisModuleIDs, $this->getAllAnalysisModules());
    }
    else {
      return ['No analysis Module is selected'];
    }
  }

  /**
   * Returns a string with all imaging support, seperated with </br> tags for
   * new lines
   *
   * @return string
   */
  public function getIntroductionToAlmTextAsMultiLineString() {
    return implode('</br>', $this->getIntroductionToAlmTextArray());
  }

  public function getIntroductionToAlmTextArray() {
    if (isset($this->introductionToAlmIDs)) {
      return $this->getSelectedValues($this->introductionToAlmIDs, $this->getAllIntroductionsToAlmChoices());
    }
    else {
      return ['No introduction to ALM is selected'];
    }
  }

  /**
   * Returns all values from the values array, which value in the
   * selectionField field is bigger then one So a value is selected, when the
   * value in the selectionField is bigger then one
   *
   * @param $selectionField
   * @param $allValues
   *
   * @return array
   */
  private function getSelectedValues($selectionField, $allValues) {
    $selectedValues = [];
    foreach ($selectionField as $id) {
      //0 is the key for a not selected field
      if ($id != 0) {
        $selectedValues[] = $allValues[$id];
      }
    }
    return $selectedValues;
  }


  /**
   * Check if a user has the permission to do an action with the request
   * instance. The Permission distinguish the managerview and the userview
   * because there are other permissions for the same action. In the user view,
   * the user can only view his own requests. PERMISSIONS: See the permission
   * documentation diagram All available actions are stored in const Variables
   * with ACTION_ ALL available views are stored in const Variables with VIEW_
   *
   * @param $action
   *
   * @return bool
   */
  public function permit($role, $action) {
    global $user;
    // First check if the user generally have the permissions for that kind of view.
    if ($role == ALMRequest::ROLE_MANAGER && !user_access(SFB_ALM_PERMISSION_MANAGE)) {
      return FALSE;
    }
    if ($role == ALMRequest::ROLE_USER && $this->userID != $user->uid) {
      return FALSE;
    }

    //Secondly check if the view have the permission for the requested action
    switch ($this->requestState) {
      case ALMRequestState::Edit:
        if ($role == ALMRequest::ROLE_USER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_EDIT) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_WITHDRAW) {
            return TRUE;
          }
          return FALSE;
        }
        if ($role == ALMRequest::ROLE_MANAGER) {
          return FALSE;
        }
        break;
      case ALMRequestState::Revision:
        if ($role == ALMRequest::ROLE_USER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_EDIT) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_WITHDRAW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_CLOSE) {
            return TRUE;
          }
          return FALSE;
        }
        if ($role == ALMRequest::ROLE_MANAGER) {
          return FALSE;
        }
        break;
      case ALMRequestState::Pending:
        if ($role == ALMRequest::ROLE_USER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_WITHDRAW) {
            return TRUE;
          }
          return FALSE;
        }
        if ($role == ALMRequest::ROLE_MANAGER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_EDIT) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_ACCEPT) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_REVISE) {
            return TRUE;
          }
          return FALSE;
        }
        break;
      case ALMRequestState::Accepted:
        if ($role == ALMRequest::ROLE_USER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_WITHDRAW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_CONSULT &&
            ALMUserQualificationRepository::getQualificationLevelOfUserId($this->getUserID()) ==
            ALMUserQualificationRepository::$QUALIFICATION_LEVEL_USER_CONSULTATION) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_VIEW_CONSULTATION &&
            ALMUserQualificationRepository::getQualificationLevelOfUserId($this->getUserID()) ==
            ALMUserQualificationRepository::$QUALIFICATION_LEVEL_USER_CONSULTATION) {
            return TRUE;
          }
          return FALSE;
        }
        if ($role == ALMRequest::ROLE_MANAGER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_EDIT) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_CONSULT) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_WITHDRAW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_REVISE) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_VIEW_CONSULTATION){
            return TRUE;
          }
          return FALSE;
        }
        break;
      case ALMRequestState::Finished:
        if ($role == ALMRequest::ROLE_USER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_VIEW_CONSULTATION){
            return TRUE;
          }
        }
        return FALSE;
      case ALMRequestState::Running:
        if ($role == ALMRequest::ROLE_USER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_WITHDRAW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_CREATE_COVERSLIPS) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_VIEW_IMAGES) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_EDIT_IMAGES) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_VIEW_CONSULTATION) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_ADD_STAININGS  &&
            ALMUserQualificationRepository::getQualificationLevelOfUserId($this->getUserID()) ==
            ALMUserQualificationRepository::$QUALIFICATION_LEVEL_USER_CONSULTATION){
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_ADD_SAMPLES  &&
            ALMUserQualificationRepository::getQualificationLevelOfUserId($this->getUserID()) ==
            ALMUserQualificationRepository::$QUALIFICATION_LEVEL_USER_CONSULTATION){
            return TRUE;
          }

          return FALSE;
        }
        if ($role == ALMRequest::ROLE_MANAGER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_CLOSE) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_ADD_STAININGS){
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_ADD_SAMPLES){
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_VIEW_CONSULTATION) {
            return TRUE;
          }
          if ($action == ALMRequest::ACTION_VIEW_IMAGES) {
            return TRUE;
          }
          return FALSE;
        }
        break;
      case ALMRequestState::Withdraw:
        if ($role == ALMRequest::ROLE_USER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          return FALSE;
        }
        if ($role == ALMRequest::ROLE_MANAGER) {
          return FALSE;
        }
        break;
      case ALMRequestState::Deleted:
        if ($role == ALMRequest::ROLE_USER) {
          if ($action == ALMRequest::ACTION_VIEW) {
            return TRUE;
          }
          return FALSE;
        }
        if ($role == ALMRequest::ROLE_MANAGER) {
          return FALSE;
        }
        break;
      default:
        return FALSE;

    }

    return FALSE;
  }

  /**
   * Checks whether this object is empty.
   * Means that this request instance has not been stored yet, or there is no
   * corresponding entity in the database.
   *
   * @return bool
   */
  public function isEmpty() {
    if ($this->requestID == ALMRequest::EMPTY_REQUEST_ID) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @return ALMRequestStaining[]
   */
  public function getConsultationSingleStainings() {
    return $this->consultationSingleStainings;
  }

  /**
   * @param ALMRequestStaining[] $consultationSingleStainings
   */
  public function setConsultationSingleStainings($consultationSingleStainings) {
    $this->consultationSingleStainings = $consultationSingleStainings;
  }

  /**
   * @return ALMRequestStaining[]
   */
  public function getConsultationDualStainings() {
    return $this->consultationDualStainings;
  }

  /**
   * @param ALMRequestStaining[] $consultationDualStainings
   */
  public function setConsultationDualStainings($consultationDualStainings) {
    $this->consultationDualStainings = $consultationDualStainings;
  }

  /**
   * @return ALMRequestStaining[]
   */
  public function getConsultationMultiStainings() {
    return $this->consultationMultiStainings;
  }

  /**
   * @param ALMRequestStaining[] $consultationMultiStainings
   */
  public function setConsultationMultiStainings($consultationMultiStainings) {
    $this->consultationMultiStainings = $consultationMultiStainings;
  }

  /**
   * @return ALMRequestSample[]
   */
  public function getConsultationSamples() {
    return $this->consultationSamples;
  }

  /**
   * @param ALMRequestSample[] $consultationSamples
   */
  public function setConsultationSamples($consultationSamples) {
    $this->consultationSamples = $consultationSamples;
  }


  /**
   * Adds a staining Object (with a Staining type) to the staining array
   *
   * @param $staining ALMRequestStaining
   *
   * @throws Exception If the staining objects does not contain a valid
   *   staining type
   */
  public function addStaining($staining) {

    // creates a new stainng array if the array is empty (=null) otherwise adds the staining to the end of the array
    switch ($staining->getType()) {
      case ALMRequestStaining::TYPE_SINGLE:
        if (empty($this->consultationSingleStainings)) {
          $this->consultationMultiStainings = [$staining];
        }
        else {
          array_push($this->consultationSingleStainings, $staining);
        }
        break;
      case ALMRequestStaining::TYPE_DUAL:
        if (empty($this->consultationMultiStainings)) {
          $this->consultationMultiStainings = [$staining];
        }
        else {
          array_push($this->consultationDualStainings, $staining);
        }
        break;
      case ALMRequestStaining::TYPE_MULTI:
        if (empty($this->consultationMultiStainings)) {
          $this->consultationMultiStainings = [$staining];
        }
        else {
          array_push($this->consultationMultiStainings, $staining);
        }
        break;
      default:
        throw new Exception('No valid staining type');
    }
  }

  /**
   * Adds a sample object to the sample documentation array of the request
   *
   * @param $sample ALMRequestSample
   */
  public function addSample($sample) {
    if (empty($this->consultationSamples)) {
      $this->consultationSamples = [$sample];
    }
    else {
      array_push($this->consultationSamples, $sample);
    }
  }


  /**
   * Returns all (immunofluorescence) stainings of the request
   *
   * @return ALMRequestStaining[]
   */
  public function loadAllStainings() {
    //load the array from the database
    return ALMRequestStainingRepository::findAllStainingsByRequestID($this->requestID);
  }

  /**
   * Returns all single (immunofluorescence) stainings of the request
   *
   * @return ALMRequestStaining[]
   */
  public function loadAllSingleStainings() {
    //load the array from the database
    return ALMRequestStainingRepository::findAllStainingsByRequestIdAndStainingType($this->requestID, ALMRequestStaining::TYPE_SINGLE);
  }

  /**
   * Returns all dual (immunofluorescence) stainings of the request
   *
   * @return ALMRequestStaining[]
   */
  public function loadAllDualStainings() {
    //load the array from the database
    return ALMRequestStainingRepository::findAllStainingsByRequestIdAndStainingType($this->requestID, ALMRequestStaining::TYPE_DUAL);
  }

  /**
   * Returns all multi (immunofluorescence) stainings of the request
   *
   * @return ALMRequestStaining[]
   */
  public function loadAllMultiStainings() {
    //load the array from the database
    return ALMRequestStainingRepository::findAllStainingsByRequestIdAndStainingType($this->requestID, ALMRequestStaining::TYPE_MULTI);
  }


  /**
   * @param bool $doLog
   *
   * @return string
   */
  public function save($doLog = TRUE) {
    $insertRequestID = ALMRequestsRepository::save($this);

    // since auto increment is used for request id, this request's id must
    // be updated
    if ($this->requestID == ALMRequest::EMPTY_REQUEST_ID) {
      $this->requestID = $insertRequestID;
    }
    if ($insertRequestID != 0 && $this->requestID != $insertRequestID) {
      watchdog('sfb_alm', 'Inconsistency detected. ALM request with ' . $this->requestID . ' got new ID ' . $insertRequestID);
      drupal_set_message('Warning:  Error detected by storing request: Request ID mismatch. (Code: ' . SFB_ALM_ERROR_INSERTID_MISMATCH . ')', 'warning');
    }


    // Check if there is a comment to update
    if (isset($this->updatedComment)) {
      // Check if the comment object has a request ID, because perhaps a comment of a new request, has no request id
      // If the comment has no request ID, the request ID will set before the comment object store into the database
      if ($this->updatedComment->getCommentID() == ALMComment::EMPTY_COMMENT_ID) {
        // Check if the request has a request ID, when not, take the lastInsertID
        if (isset($this->requestID)) {
          $this->updatedComment->setRequestID($this->requestID);
        }
        else {
          // normally this should not be called because the request get the ID before this method is called
          $this->updatedComment->setRequestID($insertRequestID);
        }
      }
      // Save the comment to the database
      ALMCommentRepository::save($this->updatedComment);
    }

    $this->saveAllStainings();
    $this->saveAllSamples();

    // Generates the Log entry for the save of the request.
    // Except the invoker avoid this by setting the doLog parameter to false
    if ($doLog) {
      sfb_alm_create_log_entry(ALMLog::$ACTION_SAVE, 'The request was saved ', $this->requestID);
    }

    return $insertRequestID;
  }

  /**
   * Saves all stainings of the $consultationSingleStainings,
   * $consultationDualStainings and $consultationMultiStainings array in the
   * database.
   */
  private function saveAllStainings() {

    if (!empty($this->consultationSingleStainings)) {
      foreach ($this->consultationSingleStainings as $singleStaining) {
        $singleStaining->save();
      }
    }

    if (!empty($this->consultationDualStainings)) {
      foreach ($this->consultationDualStainings as $dualStaining) {
        $dualStaining->save();
      }
    }

    if (!empty($this->consultationMultiStainings)) {
      foreach ($this->consultationMultiStainings as $multiStaining) {
        $multiStaining->save();
      }
    }
  }

  private function saveAllSamples() {
    if (!empty($this->consultationSamples)) {
      foreach ($this->consultationSamples as $sample) {
        $sample->save();
      }
    }
  }

  /* -------------------------------------------- FORM FUNCTIONS ---------------------------------------------------- */

  /*
   * The Form function are define Drupal Forms, that are used to display the request on the webpages
   */

  public function getFormFieldUserAccountName() {
    return [
      '#type' => 'textfield',
      '#title' => t('User name'),
      '#default_value' => $this->getUserAccountName(),
      //'#autocomplete_path' => 'user/autocomplete',
      '#attributes' => ['placeholder' => t('e.g.') . ': max.mustermann'],
      '#disabled' => TRUE,
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
    ];
  }

  public function getFormFieldUserFullname() {
    return [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $this->getUserFullname(),
      '#attributes' => ['placeholder' => t('e.g.') . ': Max Mustermann'],
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
      '#disabled' => TRUE,
    ];
  }

  public function getFormFieldPosition() {
    return [
      '#type' => 'textfield',
      '#title' => t('Position'),
      '#default_value' => $this->position,
      '#attributes' => ['placeholder' => t('PI/Postdoc/PhD student') . ': '],
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">The Position in your Group</div></div>',
      '#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_POSITION,
      '#maxlength' => '100',
    ];
  }

  public function getFormFieldInstitute() {
    return [
      '#type' => 'textfield',
      '#title' => t('Institute'),
      '#default_value' => $this->institut,
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
      '#maxlength' => '100',
    ];
  }

  /**
   * Returns an array with all researcher group names. The array can be used
   * for a select field.
   *
   * @return array
   */
  private function getResearchgroupFieldOptions() {
    $researchgroups = WorkingGroupRepository::findAll();
    $researchgroupSelect = [];
    foreach ($researchgroups as $researchgroup) {
      $researchgroupSelect[$researchgroup->getId()] = $researchgroup->getName();
    }
    // Sort alphanumerically
    asort($researchgroupSelect);
    // Insert emtpy value as first item in array
    $researchgroupOptions[WorkingGroup::EMPTY_GROUP_ID] = 'Please select research group';
    foreach ($researchgroupSelect as $id => $researchgroup){
      $researchgroupOptions[$id] = $researchgroup;
    }
    return $researchgroupOptions;
  }

  public function getFormFieldResearchgroup() {
    return [
      '#type' => 'select',
      '#title' => t('Research Group'),
      '#options' => $this->getResearchgroupFieldOptions(),
      '#default_value' => $this->getResearchGroupID(),
      '#description' => t('Select your research group.'),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
    ];
  }

  /**
   * Returns an array with all subproject names. The array can be used for a
   * select field.
   *
   * @return array
   */
  private function getSubprojectFieldOptions() {
    $subprojects = SubprojectsRepository::findAll();

    $subprojects_arr = [];
    foreach ($subprojects as $subproject) {
      $subprojects_arr[$subproject->getId()] = $subproject->getAbbreviation() . ' ' . $subproject->getTitle();
    }

    // Sort alphanumerically
    asort($subprojects_arr);
    // Insert emtpy value as first item in array
    $subprojectsOptions[ALM_SUBPROJECT_EMPTY] = "Please select your project";
    foreach ($subprojects_arr as $id => $subproject) {
      $subprojectsOptions[$id] = $subproject;
    }

    return $subprojectsOptions;
  }

  /**
   * Creates an array that associates the subprojectIDs with the subprojectAbbreviation
   *
   * The ID: 0 is associated with the a empty subproject (not selected)
   *
   * @return array
   */
  private function getAllSubprojectAbbreviations() {
    $subprojects = SubprojectsRepository::findAll();

    $subprojects_arr = [ALM_SUBPROJECT_EMPTY => "Please select your project"];
    foreach ($subprojects as $subproject) {
      $subprojects_arr[$subproject->getId()] = $subproject->getAbbreviation();
    }

    return $subprojects_arr;
  }

  public function getFormFieldSubprojects() {
    return [
      '#type' => 'select',
      '#title' => t('Project'),
      '#options' => $this->getSubprojectFieldOptions(),
      '#default_value' => isset($this->subprojectID) ? $this->subprojectID : ALM_FORM_SELECT_EMPTY,
      '#description' => t('Select your Project.'),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">Your Project</div></div>',
    ];
  }

  public function getFormFieldEmail() {
    return [
      '#type' => 'textfield',
      '#title' => t('E-mail'),
      '#default_value' => $this->email,
      '#attributes' => ['placeholder' => t('e.g.') . ': max.mustermann@email.com'],
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">Your email address</div></div>',
      '#maxlength' => '100',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldPhoneNumber() {
    return [
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#default_value' => $this->phoneNumber,
      '#attributes' => ['placeholder' => t('e.g.') . ': +49 1234 1234567'],
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">Your phonenumber</div></div>',
      '#maxlength' => '100',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldImagingGoal() {
    return [
      '#type' => 'textarea',
      '#default_value' => $this->imagingGoal,
      //'#description' => 'description',
      //'#required' => TRUE,
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">Please shortly describe your scientific question and the goal of your requested imaging project.</div></div>',
    ];
  }

  public function getFormFieldSampleType() {
    return [
      '#type' => 'textfield',
      '#title' => t('Sample Type(s)'),
      '#default_value' => $this->sampleType,
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">cultured cells (HEK, HeLa) / primary cells (cardiomyocytes, fibroblasts) / tissue (heart)/EHM</div></div>',
      '#maxlength' => '100',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldTargetStructure() {
    return [
      '#type' => 'textfield',
      '#title' => t('Target Structure(s)'),
      '#default_value' => $this->targetStructure,
      //'#attributes' => array('placeholder' => t('e.g.').': max.mustermann@email.com'),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">protein of interest / calcium / membranes</div></div>',
      '#maxlength' => '100',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldSampleConditions() {
    return [
      '#type' => 'textfield',
      '#title' => t('Conditions'),
      '#default_value' => $this->conditions,
      //'#attributes' => array('placeholder' => t('e.g.').': max.mustermann@email.com'),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">live cells / fixed cells / fixed tissue slices / live whole heart</div></div>',
      '#maxlength' => '100',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldStainingMethod() {
    return [
      '#type' => 'textfield',
      '#title' => t('Staining Method'),
      '#default_value' => $this->stainingMethod,
      //'#attributes' => array('placeholder' => t('e.g.').': max.mustermann@email.com'),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">immunostain / live membrane stain / calcium indicator</div></div>',
      '#maxlength' => '100',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldDataRequired() {
    return [
      '#type' => 'textarea',
      '#title' => t('Descriptive or Quantitative Data required'),
      '#default_value' => $this->dataRequired,
      //'#attributes' => array('placeholder' => t('e.g.').': max.mustermann@email.com'),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">Only descriptive data required: e.g. localization of protein xy in cell type z.
<p/>Quantitative data required: e.g. comparison of protein localization / membrane architecture in TAC vs. Sham mice 4 weeks after intervention.
</div></div>',
      //'#required' => TRUE,
    ];
  }

  /**
   * Returns an array with all "imaging support" choices (including disabled).
   * The array can be used for checkboxes.
   *
   * @return string[]
   */
  public function getAllImagingSupportChoices() {
    $imagingSupportOptions = new ALMSelectOptions('alm_imaging_support', AlmDefaultValues::IMAGING_SUPPORT_ARR);
    $allImagingSupportOptions = $imagingSupportOptions->getAllOptions();

    return $allImagingSupportOptions;
  }

  /**
   * Returns an array with all selectable "imaging support" choices. The array
   * can be used for checkboxes.
   *
   * @return string[]
   */
  public function getEnabledImagingSupportChoices() {
    $imagingSupportOptions = new ALMSelectOptions('alm_imaging_support', AlmDefaultValues::IMAGING_SUPPORT_ARR);
    $enabledImagingSupportOptions = $imagingSupportOptions->getEnabledOptions();

    return $enabledImagingSupportOptions;
  }

  private function getInfoModulesAndProtocolsLinkIcon($target_id = '') {
    return '<a target="_blank" rel="noopener noreferrer" href=' . sfb_alm_url(RDP_ALM_URL_INFO_MODULES_AND_PROTOCOLS, $target_id) . '><i class="fa fa-question-circle-o"></i></a>';
  }


  /**
   *
   * If no imaging support choices are enabled, the returned array is empty!
   * @return array
   */
  public function getFormFieldImagingSupport() {
    $enabledImagingSupportChoices = $this->getEnabledImagingSupportChoices();
    if(empty($enabledImagingSupportChoices)){
      return array();
    }

    return [
      '#type' => 'checkboxes',
      '#options' => $enabledImagingSupportChoices,
      '#title' => t('Imaging support:'),
      '#default_value' => $this->getImagingSupportIDs(),
      '#field_suffix' => '</br>',
    ];
  }

  /**
   * Returns an array with all selectable "sample health" choices. The array
   * can be used for checkboxes or radio buttons.
   *
   * @return string[]
   */
  private function getAllSampleHealthChoices() {
    return [
      //Attention: Don't start with 0 because 0 is the key for not selected!
      1 => "No documentation of sample health",
      2 => "Documentation of sample health",
    ];
  }

  public function getFormFieldSampleHealth() {
    return [
      '#type' => 'radios',
      '#options' => $this->getAllSampleHealthChoices(),
      '#title' => t('Sample Health') . $this->getInfoModulesAndProtocolsLinkIcon(AlmInfoModulesAndProtocolsViewPage::HEADLINE_SAMPLE_HEALTH_ID) . ' :',
      '#field_suffix' => '</br>',
      '#default_value' => $this->getSampleHealthID(),
    ];
  }

  //TODO: warum heissen die Funktionen mit und ohne Choices?

  /**
   * Returns an array with all selectable "imaging modules". The array can be
   * used for checkboxes or radio buttons.
   *
   * @return string[]
   */
  public function getAllImagingModules() {
    $imagingOptions = new ALMSelectOptions('alm_imaging_modules', AlmDefaultValues::IMAGING_MODULE_ARR);
    $allImagingOptions = $imagingOptions->getAllOptions();

    return $allImagingOptions;
  }

  /**
   * Returns an array with all selectable "imaging modules". The array can be
   * used for checkboxes or radio buttons.
   *
   * @return string[]
   */
  public function getEnabledImagingModules() {
    $imagingOptions = new ALMSelectOptions('alm_imaging_modules', AlmDefaultValues::IMAGING_MODULE_ARR);
    $enabledImagingOptions = $imagingOptions->getEnabledOptions();

    return $enabledImagingOptions;
  }


  public function getFormFieldImagingModule() {
    $enabledImagingModules = $this->getEnabledImagingModules();
    if(empty($enabledImagingModules)){
      return array();
    }

    return [
      '#type' => 'checkboxes',
      '#options' => $enabledImagingModules,
      '#title' => t('Imaging Module') . $this->getInfoModulesAndProtocolsLinkIcon(AlmInfoModulesAndProtocolsViewPage::HEADLINE_IMAGING_MODULES_ID) . ' :',
      '#default_value' => $this->getImagingModuleIDs(),
      '#field_suffix' => '</br>',
    ];
  }

  /**
   * Returns an array with all "analysis modules" (including disabled). The array can be
   * used for checkboxes or radio buttons.
   *
   * @return string[]
   */
  public function getAllAnalysisModules() {
    $analysisModuleOptions = new ALMSelectOptions('alm_analysis_modules', AlmDefaultValues::ANALYSIS_MODULE_ARR);
    $allAnalysisModuleOptions = $analysisModuleOptions->getAllOptions();

    return $allAnalysisModuleOptions;
  }

  /**
   * Returns an array with all selectable "analysis modules". The array can be
   * used for checkboxes or radio buttons.
   *
   * @return string[]
   */
  public function getEnabledAnalysisModules() {
    $analysisModuleOptions = new ALMSelectOptions('alm_analysis_modules', AlmDefaultValues::ANALYSIS_MODULE_ARR);
    $enabledAnalysisModuleOptions = $analysisModuleOptions->getEnabledOptions();

    return $enabledAnalysisModuleOptions;
  }

  public function getFormFieldAnalysisModule() {
    $enabledAnalysisModules = $this->getEnabledAnalysisModules();
    if(empty($enabledAnalysisModules)){
      return array();
    }

    return [
      '#type' => 'checkboxes',
      '#options' => $enabledAnalysisModules,
      '#title' => t('Analysis Module') . $this->getInfoModulesAndProtocolsLinkIcon(AlmInfoModulesAndProtocolsViewPage::HEADLINE_ANALYSIS_MODULES_ID) . ' :',
      '#default_value' => $this->getAnalysisModuleIDs(),
      '#field_suffix' => '</br>',
    ];
  }

  /**
   * Returns an array with all selectable "introductions to alm". The array can
   * be used for checkboxes or radio buttons.
   *
   * @return int[]:
   */
  public function getAllIntroductionsToAlmChoices() {
    $introductionToAlmOptions = new ALMSelectOptions('alm_introduction_to_alm', AlmDefaultValues::INTRODUCTION_TO_ALM_ARR);
    $allIntroductionToAlmOptions = $introductionToAlmOptions->getAllOptions();

    return $allIntroductionToAlmOptions;
  }

  /**
   * Returns an array with all selectable "introductions to alm". The array can
   * be used for checkboxes or radio buttons.
   *
   * @return int[]:
   */
  public function getEnabledIntroductionsToAlmChoices() {
    $introductionToAlmOptions = new ALMSelectOptions('alm_introduction_to_alm', AlmDefaultValues::INTRODUCTION_TO_ALM_ARR);
    $enabledIntroductionToAlmOptions = $introductionToAlmOptions->getEnabledOptions();

    return $enabledIntroductionToAlmOptions;
  }

  public function getFormFieldIntroductionToAlm() {
    $enabledIntroductionsToAlmChoices = $this->getEnabledIntroductionsToAlmChoices();
    if(empty($enabledIntroductionsToAlmChoices)){
      return array();
    }
    return [
      '#type' => 'checkboxes',
      '#options' => $enabledIntroductionsToAlmChoices,
      '#title' => t('Introduction to advanced light microscopy (ALM):'),
      '#default_value' => $this->getIntroductionToAlmIDs(),
      '#field_suffix' => '</br>',
    ];
  }

  /**
   * Returns a associative array with all usernames of alm-managern (userID =>
   * username)
   *
   * @return array
   */
  private function getAllS02Member() {
    //Todo Ändern in Datenbankabfrage, die alle UIDs mit der entsprechendne permission sucht.
    $s02users = [0 => 'Please select S02 member'];
    $usersObjects = UsersRepository::findAll();
    $usersObjects[0]->getUserWorkingGroups();
    $users = entity_load('user');
    //$wgLehnart = WorkingGroupRepository::findByShortName('ag_lehnart');
    foreach ($users as $user) {
      //Check if user is a alm-manager
      if (user_access(SFB_ALM_PERMISSION_MANAGE, $user)) {
        //if(user_access(SFB_ALM_PERMISSION_MANAGE, $user->name)){
        // Todo change to fullname
        $s02users[$user->uid] = $user->name;
        #$s02users[$user->uid] = UsersRepository::findByUid($user->uid)->getFullname(); //mit user Fullname
      }

    }
    //global $user;
    //$newUser = UsersRepository::findByUid($user->uid);
    //drupal_set_message(user_has_role(4, $newUser));
    return $s02users;
  }

  public function getFormFieldConsultationResponsibles02Member() {
    return [
      '#type' => 'select',
      '#title' => t('Responsible S02 member:'),
      '#default_value' => isset($this->consultationResponsibleS02MemberID) ? $this->consultationResponsibleS02MemberID : 0,
      '#options' => $this->getAllS02Member(),
      //'#autocomplete_path' => 'user/autocomplete',
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldConsultationAttending() {
    return [
      '#type' => 'textfield',
      '#title' => t('Attending:'),
      '#default_value' => $this->consultationAttending,
      '#maxlength' => 250,
      //'#attributes' => array('placeholder' => t('e.g.').': max.mustermann@email.com'),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
      //'#required' => TRUE,
    ];
  }


  public function getFormFieldComment($commentType) {
    return [
      '#type' => 'textarea',
      '#title' => t(ALMCommentType::getText($commentType)),
      '#default_value' => $this->getCommentWithType($commentType)->getMessage(),
      //'#attributes' => array('placeholder' => t('e.g.').': max.mustermann@email.com'),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldConsultationPreviousPracticalExperiences() {
    return [
      '#type' => 'textarea',
      '#title' => t('Previous practical experience in sample preparation and imaging:'),
      '#default_value' => $this->consultationPreviousPracticalExperiences,
      //'#attributes' => array('placeholder' => t('e.g.').': max.mustermann@email.com'),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4"></div></div>',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldConsultationImagingSupport() {
    return [
      '#type' => 'textarea',
      '#title' => t('Imaging Support:'),
      '#default_value' => $this->consultationImagingSupport,
      //'#attributes' => array('placeholder' => t('e.g. '),
      //'#description' => t('User request: </br>' .$this->getImagingSupportTextAsMultilineString()),
      '#description' => t('User request: ' . str_replace("</br>", " || ", $this->getImagingSupportTextAsMultiLineString())),

      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">What type of imaging support is needed<p/>Brief description of the intended training program <p/>Date of first training session</div></div>',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldConsultationSampleHealth() {
    return [
      '#type' => 'textarea',
      '#title' => t('Sample Health:'),
      '#default_value' => $this->consultationSampleHealth,
      //'#attributes' => array('placeholder' => t('e.g. '),
      '#description' => t('User request: ' . $this->getSampleHealthText()),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">Documentation of sample health intended or not<p/>How can sample health be documented for this project</div></div>',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldConsultationImagingModule() {
    return [
      '#type' => 'textarea',
      '#title' => t('Imaging Module(s):'),
      '#default_value' => $this->consultationImagingModule,
      //'#attributes' => array('placeholder' => t('e.g. '),
      '#description' => t('User request: ' . str_replace("</br>", " || ", $this->getImagingModuleTextAsMultiLineString())),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">Continue with free text, if live cell imaging should be performed<p/>continue with IF table, if single-, dual- or multi-color IF should be performed</div></div>',
      //'#required' => TRUE,
    ];
  }

  public function getFormFieldConsultationAnalysisModule() {
    return [
      '#type' => 'textarea',
      '#title' => t('Analysis Module(s):'),
      '#default_value' => $this->consultationAnalysisModule,
      //'#attributes' => array('placeholder' => t('e.g. '),
      '#description' => t('User request: ' . str_replace("</br>", " || ", $this->getAnalysisModuleTextAsMultiLineString())),
      '#field_prefix' => '<div class="row"><div class="col-lg-8">',
      '#field_suffix' => '</div><div class="col-lg-4">What type of analysis should be performed<p/>Which software / macro / routine has to be used<p/>Next steps</div></div>',
      //'#required' => TRUE,
    ];
  }

  //////////////////////////////////////////////View Tabs////////////////////////////////////////////////////////




  /**
   * Returns an array with log entries of this request
   *
   * @return \ALMLog[]
   */
  public function getLogs() {
    return ALMLogRepository::findAllVisibleLogEntriesByRequestID($this->getRequestID());
  }


  /* -------------------------- Field views --------------------------------- */

  public function getElementAcceptDate($empty_text = '-') {
    if (empty($this->getAcceptDate())) {
      return $empty_text;
    }

    return $this->getAcceptDate();
  }

  public function getElementResearchgroupIcon() {
    $researchgroup = WorkingGroupRepository::findById($this->getResearchGroupID());

    //TODO: Ausgabe, falls datei fehlt
    $wgName = $researchgroup->getName();

    return '<img src="' . $researchgroup->getIconPath() . '" alt="" data-toggle="tooltip" title="' . $wgName . '" />';
  }

  /**
   * @return string Absolute external URL of a Request instance's public
   *   landing page.
   */
  public function getLandingPageUrl() {
    $url = sfb_alm_url(RDP_ALM_URL_REQUEST_LANDING, $this->requestID);
    return $url;
  }

}


