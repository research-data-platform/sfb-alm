<?php
/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 30.08.2016
 * Time: 15:18
 */

/**
 * Class ALMRequestSampleRepository
 */
class ALMRequestSampleRepository{
  static $databaseFields = array(
    'id' => 'id',
    'requestId' => 'request_id',
    'sampleId' => 'sample_id',
    'serialDescription' => 'serial_description',
    'serialAbbreviation' => 'serial_abbreviation',
    'speciesDescription' => 'species_description',
    'speciesAbbreviation' => 'species_abbreviation',
    'speciesIdDescription' => 'species_id_description',
    'speciesIdAbbreviation' => 'species_id_abbreviation',
    'typeDescription' => 'type_description',
    'typeAbbreviation' => 'type_abbreviation',
    'genotypeDescription' => 'genotype_description',
    'genotypeAbbreviation' => 'genotype_abbreviation',
    'treatmentDescription' => 'treatment_description',
    'treatmentAbbreviation' => 'treatment_abbreviation',
    'stainingTypeDescription' => 'staining_type_description',
    'stainingTypeAbbreviation' => 'staining_type_abbreviation',
    'highestAssignedCoverslipNo' => 'highest_assigned_coverslip_no',
    'editable' => 'editable',
  );


  const DATABASE_NAME = 'alm_request_samples';


  /**
   * @param $result
   * @return ALMRequestSample
   */
  private static function databaseResultToALMSample($result) {
    $sample = new ALMRequestSample();
    if(empty($result)){
      $sample->setId(ALMRequestSample::EMPTY_SAMPLE_ID);
      return $sample;
    }
    
    /**
     * Sets the values to the ALMRequestSample Object
     */

    $sample->setId($result->id);
    $sample->setRequestID($result->request_id);
    $sample->setLabelName($result->sample_id);
    $sample->setSerialDescription($result->serial_description);
    $sample->setSerial($result->serial_abbreviation);
    $sample->setSpeciesDescription($result->species_description);
    $sample->setSpeciesAbbreviation($result->species_abbreviation);
    $sample->setSpeciesIdDescription($result->species_id_description);
    $sample->setSpeciesIdAbbreviation($result->species_id_abbreviation);
    $sample->setTypeDescription($result->type_description);
    $sample->setTypeAbbreviation($result->type_abbreviation);
    $sample->setGenotypeDescription($result->genotype_description);
    $sample->setGenotypeAbbreviation($result->genotype_abbreviation);
    $sample->setTreatmentDescription($result->treatment_description);
    $sample->setTreatmentAbbreviation($result->treatment_abbreviation);
    $sample->setStainingTypeDescription($result->staining_type_description);
    $sample->setStainingTypeAbbreviation($result->staining_type_abbreviation);
    $sample->setHighestAssignedCoverslipNo($result->highest_assigned_coverslip_no);
    $sample->setEditable($result->editable);

    return $sample;
  }

  /**
   * Returns the last ID that inserted in the Database.
   *
   * @param $sample ALMRequestSample
   * @return string last ID that inserted in the Database.
   *
   * @throws Exception
   * @throws InvalidMergeQueryException
   */
  public static function save($sample) {
    if (isset($sample)) {
      db_merge(self::DATABASE_NAME)->key(array('id' => $sample->getId()))
        ->fields(array(
          //TODO: FOREIGN key

          self::$databaseFields['id'] => self::setEmptyVariablesToNull($sample->getId()),
          self::$databaseFields['requestId'] => self::setEmptyVariablesToNull($sample->getRequestID()),
          self::$databaseFields['sampleId'] => self::setEmptyVariablesToNull($sample->getLabelName()),
          self::$databaseFields['serialDescription'] => self::setEmptyVariablesToNull($sample->getSerialDescription()),
          self::$databaseFields['serialAbbreviation'] => self::setEmptyVariablesToNull($sample->getSerial()),
          self::$databaseFields['speciesDescription'] => self::setEmptyVariablesToNull($sample->getSpeciesDescription()),
          self::$databaseFields['speciesAbbreviation'] => self::setEmptyVariablesToNull($sample->getSpeciesAbbreviation()),
          self::$databaseFields['speciesIdDescription'] => self::setEmptyVariablesToNull($sample->getSpeciesIdDescription()),
          self::$databaseFields['speciesIdAbbreviation'] => self::setEmptyVariablesToNull($sample->getSpeciesIdAbbreviation()),
          self::$databaseFields['typeDescription'] => self::setEmptyVariablesToNull($sample->getTypeDescription()),
          self::$databaseFields['typeAbbreviation'] => self::setEmptyVariablesToNull($sample->getTypeAbbreviation()),
          self::$databaseFields['genotypeDescription'] => self::setEmptyVariablesToNull($sample->getGenotypeDescription()),
          self::$databaseFields['genotypeAbbreviation'] => self::setEmptyVariablesToNull($sample->getGenotypeAbbreviation()),
          self::$databaseFields['treatmentDescription'] => self::setEmptyVariablesToNull($sample->getTreatmentDescription()),
          self::$databaseFields['treatmentAbbreviation'] => self::setEmptyVariablesToNull($sample->getTreatmentAbbreviation()),
          self::$databaseFields['stainingTypeDescription'] => self::setEmptyVariablesToNull($sample->getStainingTypeDescription()),
          self::$databaseFields['stainingTypeAbbreviation'] => self::setEmptyVariablesToNull($sample->getStainingTypeAbbreviation()),
          self::$databaseFields['highestAssignedCoverslipNo'] => self::setEmptyVariablesToNull($sample->getHighestAssignedCoverslipNo()),
          self::$databaseFields['editable'] => (int) $sample->isEditable(), // Typecast to int because mySql does not accept true/false

        ))->execute();

      return Database::getConnection()->lastInsertId();
    }
  }

  /**
   * Checks if a variable is empty. returns NULL when the value is '' or ALM_FORM_SELECT_EMPTY
   *
   * @param $input
   * @return null or $input
   */
  private static function setEmptyVariablesToNull($input) {
    if ($input == ALM_FORM_SELECT_EMPTY || $input == '') {
      return NULL;
    } else {
      return $input;
    }
  }

  /**
   * @param $requestID
   * @return ALMRequestSample[]
   */
  public static function findAllSamplesByRequestID($requestID){
    $result = db_select(self::DATABASE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['requestId'], $requestID, '=')
      ->fields('r', self::$databaseFields)
      ->execute();

    return Self::databaseResultsToALMSamples($result);
  }

  /**
   * @param $sampleID
   * @return ALMRequestSample
   */
  public static function findBySampleId($sampleID){
    $result = db_select(self::DATABASE_NAME, 'r')
    ->condition('r.' . self::$databaseFields['id'], $sampleID, '=')
      ->fields('r', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return Self::databaseResultToALMSample($result);
  }
  

  /**
   * @param $results
   * @return ALMRequestSample[]
   */
  private static function databaseResultsToALMSamples($results) {
    $samples = array();
    foreach($results as $result) {
      $samples[] = self::databaseResultToALMSample($result);
    }

    return $samples;
  }


  /**
   * Deletes the sample with the id $id from the database
   * @param $id
   */
  public static function deleteById($id){
    db_delete(self::DATABASE_NAME)
      ->condition(self::$databaseFields['id'], $id)
      ->execute();
  }

}