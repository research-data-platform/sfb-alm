<?php

/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 31.08.2016
 * Time: 11:33
 */
class ALMRequestStainingRepository {
  /**
   * @deprecated
   * @var array
   */
  static $databaseFields = array(
    'id' => 'id',
    'requestId' => 'request_id',
    'type' => 'type',
    'labelName' => 'label_name',
    'primaryAntibody1' => 'primary_antibody_1',
    'primDilution1' => 'prim_dilution_1',
    'secondaryAntibody1' => 'secondary_antibody_1',
    'secDilution1' => 'sec_dilution_1',
    'primaryAntibody2' => 'primary_antibody_2',
    'primDilution2' => 'prim_dilution_2',
    'secondaryAntibody2' => 'secondary_antibody_2',
    'secDilution2' => 'sec_dilution_2',
    'primaryAntibody3' => 'primary_antibody_3',
    'primDilution3' => 'prim_dilution_3',
    'secondaryAntibody3' => 'secondary_antibody_3',
    'secDilution3' => 'sec_dilution_3',
    'primaryAntibody4' => 'primary_antibody_4',
    'primDilution4' => 'prim_dilution_4',
    'secondaryAntibody4' => 'secondary_antibody_4',
    'secDilution4' => 'sec_dilution_4',
    'primaryAntibody5' => 'primary_antibody_5',
    'primDilution5' => 'prim_dilution_5',
    'secondaryAntibody5' => 'secondary_antibody_5',
    'secDilution5' => 'sec_dilution_5',

  );
  /**
   * @deprecated
   */
  const DATABASE_NAME = 'alm_request_stainingsold';

  const REQUEST_STAININGS_DATABASE_NAME = 'alm_request_stainings';
  static $requestStainingDatabaseFields = array(
    'id' => 'id',
    'type' => 'type',
    'labelName' => 'label_name',
    'owner' => 'owner',
    'requestId' => 'request_id',
    'editable' => 'editable',
  );

  const ANTIBODY_STAINING_DATABASE_NAME = 'alm_staining_antibody';
  static $antibodyDatabaseFields = array(
    'id' => 'id',
    'stainingId' => 'staining_id',
    'primaryAntibodyId' => 'primary_antibody_id',
    'primaryAntibodyDilution' => 'primary_antibody_dilution',
    'secondaryAntibodyId' => 'secondary_antibody_id',
    'secondaryAntibodyDilution' => 'secondary_antibody_dilution',
    'order' => 'orderNo',
  );

  const ADDITIONAL_STAINING_DATABASE_NAME = 'alm_staining_additional_staining';
  static $additionalStainingDatabaseFields = array(
    'id' => 'id',
    'stainingId' => 'staining_id',
    'primaryAdditionalStaining' => 'primary_additional_staining',
    'primaryAdditionalStainingDilution' => 'primary_add_staining_dilution',
    'secondaryAdditionalStaining' => 'secondary_additional_staining',
    'secondaryAdditionalStainingDilution' => 'secondary_add_staining_dilution',
    'order' => 'orderNo',
  );

  /**
   * Stores a ALMRequestStaining to the database.
   *
   * @param $staining ALMRequestStaining
   *
   * @throws Exception
   * @throws InvalidMergeQueryException
   */
  public static function save($staining) {
    if (isset($staining)) {
      $stainingSaver = new ALMRequestStainingSaver();
      $stainingSaver->save($staining);
    }
  }

  /**
   * Checks if a variable is empty. returns NULL when the value is '' or ALMRequestStaining::EMPTY_IF_ID
   *
   * @param $input
   * @return null or $input
   */
  static function setEmptyVariablesToNull($input) {
    if (!isset($input) || $input == ALMRequestStaining::EMPTY_IF_ID || $input == '') {
      return NULL;
    } else {
      return $input;
    }
  }

  /**
   * Searchs in the database for ALMRequestStainings with the given requestId and StainingType
   *
   * @param $requestId int ID of an ALMRequest (@see ALMRequest)
   * @param $stainingType int Type of ALMRequestStaining (@see ALMRequestStaining)
   * @return ALMRequestStaining[]
   */
  public static function findAllStainingsByRequestIdAndStainingType($requestId, $stainingType) {
    $result = db_select(self::REQUEST_STAININGS_DATABASE_NAME, 's')
      ->condition('s.' . self::$requestStainingDatabaseFields['requestId'], $requestId, '=')
      ->condition('s.' . self::$requestStainingDatabaseFields['type'], $stainingType, '=')
      ->fields('s', array(self::$requestStainingDatabaseFields['id']))
      ->execute();

    // Array with the staining IDs (the database results contains only the stainingIds)
    $stainingIds = $result->fetchCol();

    // Load the stainings as Array
    $stainings = self::findAllStainingsByIds($stainingIds);

    return $stainings;
  }

  /**
   * @param $requestId
   * @return ALMRequestStaining[]
   */
  public static function findAllStainingsByRequestID($requestId) {
    $result = db_select(self::REQUEST_STAININGS_DATABASE_NAME, 's')
      ->condition('s.' . self::$requestStainingDatabaseFields['requestId'], $requestId, '=')
      ->fields('s', array(self::$requestStainingDatabaseFields['id']))
      ->execute();

    // Array with the staining IDs (the database results contains only the stainingIds)
    $stainingIds = $result->fetchCol();

    // Load the stainings as Array
    $stainings = self::findAllStainingsByIds($stainingIds);

    return $stainings;
  }

  /**
   * Finds a staining by the staining id in the database
   * @param $stainingId
   * @return ALMRequestStaining
   */
  public static function findByStainingId($stainingId){
    $stainingLoader = new ALMRequestStainingLoader();

    return $stainingLoader->load($stainingId);
  }

  /**
   * Deletes the immunofluoroscence staining with the id $id from the database
   * @param $id
   */
  public static function deleteById($id) {
    db_delete(self::REQUEST_STAININGS_DATABASE_NAME)
      ->condition(self::$requestStainingDatabaseFields['id'], $id)
      ->execute();

    db_delete(self::ANTIBODY_STAINING_DATABASE_NAME)
      ->condition(self::$antibodyDatabaseFields['stainingId'], $id)
      ->execute();

    db_delete(self::ADDITIONAL_STAINING_DATABASE_NAME)
      ->condition(self::$additionalStainingDatabaseFields['stainingId'], $id)
      ->execute();
  }

  /**
   * Loads all Stainings with the given Ids and returns an array with the stainings as ALMRequestStaining objects
   * @param $stainingIds array with stainingIds as values
   * @return ALMRequestStaining[]
   */
  private static function findAllStainingsByIds($stainingIds) {
    // create new stainings array
    $stainings = array();
    // create new StainingLoader, which loads stainings with a given stainingId from the database
    $stainingLoader = new ALMRequestStainingLoader();

    foreach ($stainingIds as $stainingId) {
      $stainings[] = $stainingLoader->load($stainingId);
    }

    return $stainings;
  }

}

/**
 * Class StainingSaver
 * Saves a Staining object to database
 */
class ALMRequestStainingSaver {

  /**
   * The Staining that will be stored to the database
   * @var ALMRequestStaining
   */
  private $staining;
  private $stainingId;

  /**
   * Saves an ALMRequestStaining to the database
   * @param $staining ALMRequestStaining
   * @return int the id of the inserted staining
   */
  public function save($staining) {
    $this->staining = $staining;

    $insertId = $this->saveValuesOfStainingTable();

    // If the staining is a new staining, it gets its ID when it is store the first time to the database
    if ($staining->hasId()) {
      $this->stainingId = $staining->getId();
    } else {
      $this->stainingId = $insertId;
      $this->staining->setId($insertId);
    }

    $this->saveValuesOfAntibodyStainingTable();
    $this->saveValuesOfAdditionalStainingsTable();

    return $this->stainingId;
  }

  /**
   * Stores the values id, type, labelName and requestId to the ALMRequestStainingRepository::REQUEST_STAININGS_DATABASE_NAME database table
   * @return int
   */
  private function saveValuesOfStainingTable() {
    $db_row = array(
      ALMRequestStainingRepository::$requestStainingDatabaseFields['id'] => $this->staining->getId(),
      ALMRequestStainingRepository::$requestStainingDatabaseFields['type'] => $this->staining->getType(),
      //ALMRequestStainingRepository::$stainingDatabaseFields['owner'] => $staining->getOwner(),
      ALMRequestStainingRepository::$requestStainingDatabaseFields['labelName'] => $this->staining->getLabelName(),
      ALMRequestStainingRepository::$requestStainingDatabaseFields['requestId'] => $this->staining->getRequestId(),
      ALMRequestStainingRepository::$requestStainingDatabaseFields['editable'] => (int) $this->staining->isEditable(), // Typecast to int because mySql does not accept true/false
    );

    db_merge(ALMRequestStainingRepository::REQUEST_STAININGS_DATABASE_NAME)->key(array('id' => $this->staining->getId()))
      ->fields($db_row)->execute();

    return Database::getConnection()->lastInsertId();
  }

  /**
   * Stores each Antibody Staining Row in one row of the AntibodyStainings database (@see ALMRequestStainingRepository::ANTIBODY_STAINING_DATABASE_NAME)
   */
  private function saveValuesOfAntibodyStainingTable() {
    $antibodyStainings = $this->staining->getAntibodyDefinitions();

    foreach ($antibodyStainings as $order => $antibodyStainingRow) {

      $primaryAntibodyId = ALMRequestStainingRepository::setEmptyVariablesToNull($antibodyStainingRow['primaryAntibodyId']);
      $primaryAntibodyDilution = ALMRequestStainingRepository::setEmptyVariablesToNull($antibodyStainingRow['primaryAntibodyDilution']);
      $secondaryAntibodyId = ALMRequestStainingRepository::setEmptyVariablesToNull($antibodyStainingRow['secondaryAntibodyId']);
      $secondaryAntibodyDilution = ALMRequestStainingRepository::setEmptyVariablesToNull($antibodyStainingRow['secondaryAntibodyDilution']);

      $db_row = array(
        ALMRequestStainingRepository::$antibodyDatabaseFields['stainingId'] => $this->stainingId,
        ALMRequestStainingRepository::$antibodyDatabaseFields['primaryAntibodyId'] => $primaryAntibodyId,
        ALMRequestStainingRepository::$antibodyDatabaseFields['primaryAntibodyDilution'] => $primaryAntibodyDilution,
        ALMRequestStainingRepository::$antibodyDatabaseFields['secondaryAntibodyId'] => $secondaryAntibodyId,
        ALMRequestStainingRepository::$antibodyDatabaseFields['secondaryAntibodyDilution'] => $secondaryAntibodyDilution,
        ALMRequestStainingRepository::$antibodyDatabaseFields['order'] => $order,
      );

      db_merge(ALMRequestStainingRepository::ANTIBODY_STAINING_DATABASE_NAME)
        ->key(array(
          ALMRequestStainingRepository::$antibodyDatabaseFields['stainingId'] => $this->stainingId,
          ALMRequestStainingRepository::$antibodyDatabaseFields['order'] => $order,))
        ->fields($db_row)->execute();
    }
  }

  /**
   * Stores each Additional Staining Row in one row of the AdditionalStainings database (@see ALMRequestStainingRepository::ADDITIONAL_STAINING_DATABASE_NAME)
   */
  private function saveValuesOfAdditionalStainingsTable() {
    $additionalStainings = $this->staining->getAdditionalStainingDefinitions();

    foreach ($additionalStainings as $order => $additionalStainingRow) {

      $primAdditionalStaining = ALMRequestStainingRepository::setEmptyVariablesToNull($additionalStainingRow['primaryAdditionalStaining']);
      $primAdditionalStainingDiltution = ALMRequestStainingRepository::setEmptyVariablesToNull($additionalStainingRow['primaryAdditionalStainingDilution']);
      $secAdditionalStaining = ALMRequestStainingRepository::setEmptyVariablesToNull($additionalStainingRow['secondaryAdditionalStaining']);
      $secAdditionalStainingDilution = ALMRequestStainingRepository::setEmptyVariablesToNull($additionalStainingRow['secondaryAdditionalStainingDilution']);

      $db_row = array(
        ALMRequestStainingRepository::$additionalStainingDatabaseFields['stainingId'] => $this->stainingId,
        ALMRequestStainingRepository::$additionalStainingDatabaseFields['primaryAdditionalStaining'] => $primAdditionalStaining,
        ALMRequestStainingRepository::$additionalStainingDatabaseFields['primaryAdditionalStainingDilution'] => $primAdditionalStainingDiltution,
        ALMRequestStainingRepository::$additionalStainingDatabaseFields['secondaryAdditionalStaining'] => $secAdditionalStaining,
        ALMRequestStainingRepository::$additionalStainingDatabaseFields['secondaryAdditionalStainingDilution'] => $secAdditionalStainingDilution,
        ALMRequestStainingRepository::$additionalStainingDatabaseFields['order'] => $order,
      );

      db_merge(ALMRequestStainingRepository::ADDITIONAL_STAINING_DATABASE_NAME)
        ->key(array(
          ALMRequestStainingRepository::$additionalStainingDatabaseFields['stainingId'] => $this->stainingId,
          ALMRequestStainingRepository::$additionalStainingDatabaseFields['order'] => $order,))
        ->fields($db_row)->execute();
    }
  }
}

/**
 * Class ALMRequestStainingLoader
 * Loads a Staining object from the database
 *
 * Staining Objects are stored in three databasetables:
 *
 */
class ALMRequestStainingLoader {
  /**
   * @var ALMRequestStaining
   */
  private $stainingObject;

  /**
   * The ID of the staining that will load from the database
   * @var int
   */
  private $stainingId;

  /**
   * @param $stainingId
   */
  public function load($stainingId) {
    $this->stainingId = $stainingId;
    $this->stainingObject = new ALMRequestStaining();

    $this->stainingObject->setId($this->stainingId);

    $staingingTableResults = $this->loadValuesFromStainingTable();
    $this->addStainingTableResultsToStainingObject($staingingTableResults);

    $antibodyStaingingTableResults = $this->loadValuesFromAntibodyStainingTable();
    $this->addAntibodyStainingTableResultsToStainingObject($antibodyStaingingTableResults);

    $additionalStainingTableResults = $this->loadValuesFromAdditionalStainingTable();
    $this->addAdditionalStainingTableResultsToStainingObject($additionalStainingTableResults);

    return $this->stainingObject;
  }

  /**
   *
   * @return mixed Databaseresult with
   */
  private function loadValuesFromStainingTable() {
    $result = db_select(ALMRequestStainingRepository::REQUEST_STAININGS_DATABASE_NAME, 's')
      ->condition('s.' . ALMRequestStainingRepository::$requestStainingDatabaseFields['id'], $this->stainingId, '=')
      ->fields('s', ALMRequestStainingRepository::$requestStainingDatabaseFields)
      ->execute()
      ->fetch();

    return $result;
  }

  private function addStainingTableResultsToStainingObject($staingingTableResult) {
    // convert the database results to associative arrays
    $staingingTableResultsArr = get_object_vars($staingingTableResult);

    $this->stainingObject->setLabelName($staingingTableResultsArr[ALMRequestStainingRepository::$requestStainingDatabaseFields['labelName']]);
    $this->stainingObject->setType($staingingTableResultsArr[ALMRequestStainingRepository::$requestStainingDatabaseFields['type']]);
    $this->stainingObject->setRequestId($staingingTableResultsArr[ALMRequestStainingRepository::$requestStainingDatabaseFields['requestId']]);
    $this->stainingObject->setEditable($staingingTableResultsArr[ALMRequestStainingRepository::$requestStainingDatabaseFields['editable']]);
  }

  private function loadValuesFromAntibodyStainingTable() {
    $result = db_select(ALMRequestStainingRepository::ANTIBODY_STAINING_DATABASE_NAME, 'a')
      ->condition('a.' . ALMRequestStainingRepository::$antibodyDatabaseFields['stainingId'], $this->stainingId, '=')
      ->fields('a', ALMRequestStainingRepository::$antibodyDatabaseFields)
      ->execute();

    return $result;
  }

  private function addAntibodyStainingTableResultsToStainingObject($antibodyStaingingTableResults) {
    $antibodyStainings = array();

    foreach ($antibodyStaingingTableResults as $antibodyStaingingTableResult) {
      $antibodyStaingingTableResultArr = get_object_vars($antibodyStaingingTableResult);
      $order = $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$antibodyDatabaseFields['order']];

      $antibodyStainings[$order] = array(
        'primaryAntibodyId' => $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$antibodyDatabaseFields['primaryAntibodyId']],
        'primaryAntibodyDilution' => $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$antibodyDatabaseFields['primaryAntibodyDilution']],
        'secondaryAntibodyId' => $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$antibodyDatabaseFields['secondaryAntibodyId']],
        'secondaryAntibodyDilution' => $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$antibodyDatabaseFields['secondaryAntibodyDilution']]
      );
    }

    $this->stainingObject->setAntibodyDefinitions($antibodyStainings);

  }

  private function loadValuesFromAdditionalStainingTable() {
    $result = db_select(ALMRequestStainingRepository::ADDITIONAL_STAINING_DATABASE_NAME, 'a')
      ->condition('a.' . ALMRequestStainingRepository::$additionalStainingDatabaseFields['stainingId'], $this->stainingId, '=')
      ->fields('a', ALMRequestStainingRepository::$additionalStainingDatabaseFields)
      ->execute();

    return $result;
  }

  private function addAdditionalStainingTableResultsToStainingObject($additionalStainingTableResults) {
    $additionalStainings = array();

    foreach ($additionalStainingTableResults as $additionalStaingingTableResult) {
      $antibodyStaingingTableResultArr = get_object_vars($additionalStaingingTableResult);
      $order = $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$additionalStainingDatabaseFields['order']];

      $additionalStainings[$order] = array(
        'primaryAdditionalStaining' => $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$additionalStainingDatabaseFields['primaryAdditionalStaining']],
        'primaryAdditionalStainingDilution' => $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$additionalStainingDatabaseFields['primaryAdditionalStainingDilution']],
        'secondaryAdditionalStaining' => $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$additionalStainingDatabaseFields['secondaryAdditionalStaining']],
        'secondaryAdditionalStainingDilution' => $antibodyStaingingTableResultArr[ALMRequestStainingRepository::$additionalStainingDatabaseFields['secondaryAdditionalStainingDilution']]
      );
    }

    $this->stainingObject->setAdditionalStainingDefinitions($additionalStainings);
  }
}