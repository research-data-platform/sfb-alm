<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 13.06.18
 * Time: 11:51
 */

class ALMDatasetRepository {

  const DATABASE_NAME = 'alm_request_dataset';

  private static $database_fields = [
    'dataset_id',
    'request_id',
    'comment_id',
    'modified_by',
  ];

  /**
   * @param $request_id
   * @return ALMDataset[]
   */
  public static function findAllByRequestId($request_id) {

    $query = db_select(self::DATABASE_NAME, 't1');
    $query->join(ArchiveObjectRepository::getTableName(), 't2', 't1.dataset_id = t2.id');
    $results = $query->fields('t1', self::$database_fields)
      ->fields('t2', ArchiveObjectRepository::getDatabaseFields())
      ->condition('t1.request_id', $request_id)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);

  }

  /**
   * @param $request_id
   * @return ALMDataset
   */
  public static function findById($dataset_id) {

    $query = db_select(self::DATABASE_NAME, 't1');
    $query->join(ArchiveObjectRepository::getTableName(), 't2', 't1.dataset_id = t2.id');
    $results = $query->fields('t1', self::$database_fields)
      ->fields('t2', ArchiveObjectRepository::getDatabaseFields())
      ->condition('t1.dataset_id', $dataset_id)
      ->execute()
      ->fetch();

    return self::databaseResultToObject($results);

  }

  public static function save(ALMDataset $dataset) {
    /**
     * Check if the dataset has been saved before.
     */
    $new_upload = $dataset->isEmpty() ? TRUE : FALSE;


    /**
     * Save @see \ArchiveObject first, on success store auxiliary information.
     */
    if ($id = ArchiveObjectRepository::save($dataset)) {
      try {
        db_merge(self::DATABASE_NAME)
          ->key(['dataset_id' => $id])
          ->fields([
            'request_id' => $dataset->getRequestId(),
            'comment_id' => $dataset->getCommentId(),
            'modified_by' => $dataset->getModifiedBy(),
          ])
          ->execute();
      } catch (Exception $exception) {
        // handle Exception
        watchdog_exception(__CLASS__, $exception);
      }

      // Create log entry on initial upload
      if ($new_upload) {
        $msg = t("A new dataset has been uploaded. (%n)", ['%n' => $dataset->getDisplayName()]);
        sfb_alm_create_log_entry(ALMLog::$ACTION_DATASET_UPLOAD, $msg, $dataset->getRequestId());
      }

      return $id;
    }
    else {
      /**
       * Something went wrong when saving @see \ArchiveObject
       */
      return ALMDataset::EMPTY_ID;
    }
  }

  private static function databaseResultsToObjects($results) {
    $objects = [];
    if (is_array($results)) {
      foreach ($results as $result) {
        $objects[] = self::databaseResultToObject($result);
      }
    }
    return $objects;
  }

  private static function databaseResultToObject($result) {
    $dataset = new ALMDataset();

    if (empty($result)) {
      return $dataset;
    }

    $dataset->setId($result->id);
    $dataset->setArchiveId($result->archive_id);
    $dataset->setCdstarObjectId($result->cdstar_object_id);
    $dataset->setPidId($result->pid_id);
    $dataset->setSharingLevel($result->sharing_level);
    $dataset->setContext($result->context);
    $dataset->setRequestId($result->request_id);
    $dataset->setCommentId($result->comment_id);
    $dataset->setModifiedBy($result->modified_by);

    return $dataset;
  }

  public static function delete(ALMDataset $dataset) {
    $id = $dataset->getId();
    $request_id = $dataset->getRequestId();
    $name = $dataset->getDisplayName();

    if (ArchiveObjectRepository::delete($dataset)) {
      try {
        db_delete(self::DATABASE_NAME)
          ->condition('dataset_id', $id)
          ->execute();

        $msg = t("A dataset has been deleted. (%n)", ['%n' => $name]);
        sfb_alm_create_log_entry(ALMLog::$ACTION_DATASET_DELETE, $msg, $request_id);

        return TRUE;
      } catch (Exception $exception) {
        // handle Exception
        watchdog_exception(__CLASS__, $exception);
        return FALSE;
      }
    }
  }
}