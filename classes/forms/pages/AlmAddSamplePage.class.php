<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 05.03.2019
 * Time: 16:14
 */
class AlmAddSamplePage implements iAlmFormPage {

  /**
   * @var ALMRequest
   */
  protected $requestObj;
  /**
   * @var ALMRequestStaining
   */
  protected $stainingObj;

  /**
   * Name of submit functions which are used in page.inc file.
   */
  const CANCEL_SUBMIT_FUNCTION_NAME = 'rdp_alm_request_view_add_sample_cancel';
  const ADD_SUBMIT_FUNCTION_NAME = 'rdp_alm_request_view_add_sample_submit';


  /**
   * Returns an form array for drupal_get_form forms
   * @param null $form
   * @param $form_state
   * @param null $request_id
   * @return null
   */
  public function getPageForm($form = null, &$form_state, $request_id = null) {
    $form_state['request_id'] = $request_id;

    $this->requestObj = ALMRequestsRepository::findById($request_id);
    if (!$this->userIsPermitted()) {
      drupal_access_denied();
      exit();
    }


    // create an new (empty) staining object
    $empty_sample_obj = $this->initSampleObj();

    $sampleForm = new AlmSampleEditForm($empty_sample_obj);

    $form['new_sample'] = $sampleForm->getForm();

    //$form['new_sample'] = $consultation_forms->getFormFieldSampleDoc(null, $empty_sample_obj, false);

    $form['confirm-dialog'] = $this->confirmDialog();

    // uses called class to access class constants (also if extended)
    $class = get_called_class();

    $form['btn-cancel'] = [
      '#type' => 'submit',
      '#value' => 'Cancel',
      '#description' => 'Go back to the project view. Attention: The new sample will be withdrawn! It will not be available anymore',
      '#submit' => array($class::CANCEL_SUBMIT_FUNCTION_NAME),
    ];

    $form['btn-submit'] = [
      '#type' => 'submit',
      '#value' => 'Add sample',
      '#description' => 'Add the new sample to the alm-project. Attention: This cannot to be undone! The added sample will not be editable anymore',
      '#attributes' => array('id' => 'btn-submit',), // the id is necessary for the confirm dialog
      '#submit' => array($class::ADD_SUBMIT_FUNCTION_NAME),
    ];

    return $form;

  }

  /**
   * Check if user is permitted to access page
   * @return bool
   */
  protected function userIsPermitted(){
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_ADD_SAMPLES);
  }

  /**
   * Creates a new initial (empty) staining object, which contains the AB / Additional Staining rows for the given staining type.
   * @param $staining_type
   * @return ALMRequestSample
   */
  protected function initSampleObj() {
    $sample = new ALMRequestSample();
    $sample->setEditable(true);

    return $sample;
  }

  /**
   * returns a drupal_form array for the confirm dialog of the submit button
   * (This content is normally not visible, that's why it does not depend where this code is inserted)
   * @return array
   */
  protected function confirmDialog() {
    $confirm_dialog = new ConfirmDialog('btn-submit');
    $confirm_dialog->setAnswTextYes('Continue');
    $confirm_dialog->setBody('
      <h4>Are you sure that you want to add the sample now?</h4>
      <h4 class="small">Hint: The sample (serial) number will be given automatically</h4>
      <div class="panel panel-warning">
        <div class="panel-heading">
        <h3 class="panel-title">Attention:</h3> 
        </div>
        <div class="panel-body">
          <p>If you add a new sample to a running request, this cannot be undone! Once the sample is added it will not be possible to edit the sample anymore!</p>
        </div>
      </div>
    ');


    return [
      '#type' => 'markup',
      '#markup' => $confirm_dialog->getDialogAsHtml(),
    ];
  }

  /**
   * Closes the "new staining" view and redirected back to the request overview
   * @param $form
   * @param $form_state
   */
  public function cancelSubmitFunction($form, &$form_state) {
    $request_id = $form_state['request_id'];

    $form_state['redirect'] = $this->requestViewUrl($request_id);
  }


  /**
   * Creates a new staining with the user input and save it (linked to the request) in the db.
   * @param $form
   * @param $form_state
   */
  public function addSubmitFunction($form, &$form_state) {
    $request_id = $form_state['request_id'];

    $this->requestObj = ALMRequestsRepository::findById($request_id);
    if (!$this->userIsPermitted()) {
      drupal_access_denied();
      exit();
    }


    // create new empty staining and add the request ID
    $sample_obj = $this->initSampleObj();
    $sample_obj->setRequestID($request_id);
    $sample_obj->assignSerialNumber();
    $consultation_forms = new ALMConsultationEditForms($request_id);

    // merges the values of the inputted Staining Table into the (new created) staining object and save it to the db
    $sample_obj = AlmSampleEditForm::mergeFormStateInDO($sample_obj, $form_state['values']['new_sample']);
    $sample_obj->save();

    sfb_alm_create_log_entry(ALMLog::$ACTION_ADD_SAMPLE, 'New sample was added on state running. Added sample: ' . $sample_obj->getLabelName(), $request_id);

    // redirect to selected staining type on the request view
    $form_state['redirect'] = $this->requestViewUrl($request_id);
  }

  protected function requestViewUrl($request_id){
    return sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request_id, '1', 'sample-field');
  }
}