<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 20.05.2019
 * Time: 16:46
 */
class AlmDatasetViewPage implements iAlmFormPage {

  const AJAX_CALLBACK = 'rdp_alm_dataset_ajax_callback';
  const AJAX_SHARE = 'rdp_alm_dataset_ajax_share';
  const AJAX_REVOKE = 'rdp_alm_dataset_ajax_revoke';


  /**
   * @var int
   */
  protected $datasetId;

  /**
   * @var ALMDataset
   */
  protected $datasetObj;

  /**
   * @var int
   */
  protected $requestId;

  /**
   * @var ALMRequest
   */
  protected $requestObj;

  /**
   * @var array
   */
  protected $pageForm;


  public function getPageForm($form = null, &$form_state, $requestId = NULL, $datasetId = NULL) {
    $form_state['dataset_id'] = $datasetId;
    $form_state['request_id'] = $requestId;

    $this->pageForm = $form;

    $this->requestId = $requestId;
    $this->datasetId = $datasetId;

    $this->createForm();

    return $this->pageForm;
  }

  /**
   * Returns the html for an view page of an alm dataset
   *
   * @param null $requestId
   * @param null $datasetId
   * @return String
   */
  public function getPageHtml($requestId, $datasetId) {
    $this->requestId = $requestId;
    $this->datasetId = $datasetId;

    $this->pageForm = array();
    $this->createForm();

    return drupal_render($this->pageForm);
  }

  private function createForm() {
    $this->loadDatasetObj();

    $this->checkPageAccess();

    $this->pageForm['page-fieldset'] = [
      '#type' => 'fieldset'
    ];

    $this->createIntroductionForm();
    $this->createCommentForm();
    $this->createFilesForm();
    $this->createBackButton();

    /*$box_header = ['title' => ''];
    $box_body = ['data' => $this->pageForm];
    $box_footer = ['data' => ''];
    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
    */
  }

  protected function loadDatasetObj() {
    $this->datasetObj = ALMDatasetRepository::findById($this->datasetId);
  }

  private function createIntroductionForm() {
    $this->pageForm['page-fieldset']['introduction'] = ['#markup' => '<div class="row row-eq-height"><div class="col-sm-12 col-md-6"><p>This page gives an overview over an ALM Dataset. The files are separated into 3 sections:</p>
                   <p> 1. XML Files, which usually contains the metadata and some organizational information.</p>
                   <p> 2. Image Files, which contains the raw images with a preview picture</p>
                   <p> 3. Further unsupported data tyes, which contains all files that are not an supported XML or image file.</p>
                   <p>You can download each single file as well as the complete (original) dataset. </p></br></div>'];
  }

  private function createCommentForm() {
    $comment_msg = ALMCommentRepository::findCommentByCommentId($this->datasetObj->getCommentId())
      ->getMessage();
    $comment_edit_link = '<a href="' . sfb_alm_url(RDP_ALM_URL_REQUEST_DATASET_COMMENT, $this->requestId, $this->datasetId) . '">' . CdstarResources::GLYPHICON_EDIT . '</a>';

    $this->pageForm['page-fieldset']['comment'] = ['#markup' => '<div class="col-sm-12 col-md-6"> <h4>Comment ' . $comment_edit_link . ':</h4><p><pre class="prettyprint" style="overflow: scroll; height:120px; max-height: 120px">' . $comment_msg . '</pre></p></div></div>'];
  }

  private function createFilesForm() {
    $cdStarObj = CdstarObjectRepository::findById($this->datasetObj->getCdstarObjectId());
    $files = $cdStarObj->getFiles();

    $htmlXmlContent = '';
    $htmlImages = '';
    $htmlUnsupportedFiles = '';

    foreach ($files as $file) {
      $fileType = $file->getContentType();
      if (in_array($fileType, ALMDataset::$allowedDataTypes)) {
        if ($file->getContentType() == 'image/tiff') {
          $imageFile = new ALMImageFile($file, $this->requestId, $this->datasetObj->getId());
          $htmlImages .= $imageFile->getHtmlImageField();
        } else { // supported xml files

          $download_icon = l(CdstarResources::GLYPHICON_DOWNLOAD, $file->url('download'), ['html' => TRUE]);
          $header = '<strong>' . $file->getFilename() . '</strong> ' . $download_icon;

          $xml = $file->getBinaryFileStream();
          $htmlXmlContent .= '<p>' . $header . '<pre class="prettyprint linenums" style="overflow: scroll; max-height: 400px">
                    <code class="language-xml">' . htmlspecialchars($xml) . '</code></pre></p>';
        }
      } else {
        $htmlUnsupportedFiles .= $this->datasetObj->fileHtmlDisplay($file);
      }
    }

    $this->pageForm['page-fieldset']['dataset-info'] = ['#markup' => '<div class="row row-eq-height"><div class="col-sm-12 col-md-6">' . $this->datasetObj->displayInfoblock()];

    $this->createDatasetDownloadFieldset($cdStarObj);
    $this->createShareFieldset();

    $this->createXMLFileFieldset($htmlXmlContent);
    $this->createImageFilesFieldset($htmlImages);
    $this->createUnsupportedFilesFieldset($htmlUnsupportedFiles);
  }

  private function createDatasetDownloadFieldset($cd_obj) {
    $this->pageForm['page-fieldset']['dataset-download-fieldset'] = [
      '#type' => 'fieldset',
      '#value' => l(CdstarResources::GLYPHICON_DOWNLOAD, $this->datasetObj->url('download'), ['html' => TRUE])
        . " Download complete dataset: <strong>" . $cd_obj->getDisplayName() . "</strong>",
      '#children' => '<div>',
      '#collapsible' => FALSE,
      '#suffix' => '<p></p></div>',
      //'#collapsed' => true,
    ];
  }

  private function createShareFieldset() {
    $this->pageForm['page-fieldset']['share-fieldset'] = [
      '#type' => 'fieldset',
      '#title' => 'Share Dataset',
      '#prefix' => '<div id="fieldset-share-wrapper" class="col-sm-12 col-md-6">',
      '#suffix' => '</div></div>',
      '#collapsible' => false,
    ];

    $this->pageForm['page-fieldset']['share-fieldset']['share-with-field'] = [
      '#prefix' => '<div class="row"><div class="col-md-10 col-sm-12 col-xs-12" style="margin: 5px 5px 5px 5px; padding-bottom: 15px"><table><tr>',
      'research_group_select' => [
        '#prefix' => '<td>',
        '#type' => 'textfield',
        '#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_RESEARCH_GROUPS,
        '#attributes' => ['placeholder' => t('ag_lehnart')],
        '#title' => t('Research Group'),
        '#suffix' => '</td>',
      ],
      'btn-share' => [
        '#prefix' => '<td>',
        '#type' => 'submit',
        '#submit' => [self::AJAX_SHARE],
        '#value' => 'Share',
        '#attributes' => ['style' => 'float: right;margin-top: 25px;margin-left:15px'],
        '#suffix' => '</td>',
        '#ajax' => [
          'callback' => self::AJAX_CALLBACK,
          'wrapper' => 'fieldset-share-wrapper',
        ],
      ],
      'btn-revoke' => [
        '#prefix' => '<td>',
        '#type' => 'submit',
        '#submit' => [self::AJAX_REVOKE],
        '#value' => 'Revoke',
        '#attributes' => ['style' => 'float: right;margin-top: 25px;margin-left:15px'],
        '#suffix' => '</td>',
        // enable ajax functionalities for the button
        '#ajax' => [
          'callback' => self::AJAX_CALLBACK,
          'wrapper' => 'fieldset-share-wrapper',
        ],
      ],
      '#suffix' => '</tr></table></div></div>',
    ];
    $this->pageForm['page-fieldset']['share-fieldset']['shared-research-groups'] = [
      '#markup' => '<h5>Dataset is shared with:</h5>' . $this->getSharedLabelsHtml()];
  }


  private function getSharedLabelsHtml(){
    $workingGroupNames = $this->datasetObj->getSharedWorkingGroupShortNames();
    $sharedLabelsHtml = '';

    if(isset($workingGroupNames)) {

      $sharedLabelsHtml = '<h4>';
      foreach ($workingGroupNames as $workingGroupName) {
        $sharedLabelsHtml .= '<span class="label label-primary">' . $workingGroupName . '</span> ';
      }
      $sharedLabelsHtml .= '</h4>';
    }

    return $sharedLabelsHtml;
  }
  /*
 * Ajax callback function for share or revoke
 * Function reloads the whole share fieldset
 */
  public function almDatasetViewAjaxCallback($form, $form_state) {
    return $form['page-fieldset']['share-fieldset'];
  }

  public function almDatasetViewAjaxShare($form, &$form_state){
    $datasetObj = ALMDatasetRepository::findById($form_state['dataset_id']);

    $workingGroupShortName = $form_state['values']['research_group_select'];
    $workingGroupObj = WorkingGroupRepository::findByShortName($workingGroupShortName);

    if($this->checkIfValidWorkingGroup($workingGroupObj)){
      $datasetObj->addSharedWorkingGroup($workingGroupObj->getId());
    } else {
      drupal_set_message('Your input: "' . $workingGroupShortName . '" is not a valid Research Group. <br>Please select a valid Research Group', 'warning');
    }

    $form_state['rebuild'] = TRUE;
  }

  /**
   * @param $workingGroupObj WorkingGroup
   * @return true if the workingGroupObj is an existing working Group
   */
  private function checkIfValidWorkingGroup($workingGroupObj){
    if($workingGroupObj->getId() == -1) return false;
    return true;
  }

  public function almDatasetViewAjaxRevoke($form, &$form_state){
    $datasetObj = ALMDatasetRepository::findById($form_state['dataset_id']);

    $workingGroupShortName = $form_state['values']['research_group_select'];
    $workingGroupObj = WorkingGroupRepository::findByShortName($workingGroupShortName);

    if($this->checkIfValidWorkingGroup($workingGroupObj)){
      $datasetObj->revokeSharedWorkingGroup($workingGroupObj->getId());
    } else {
      drupal_set_message('Your input: "' . $workingGroupShortName . '" is not a valid Research Group. <br>Please select a valid Research Group', 'warning');
    }

    $form_state['rebuild'] = TRUE;
  }

  private function createXMLFileFieldset($htmlXmlContent) {
    $this->pageForm['page-fieldset']['xml-file-fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('XML Files ') . (empty($htmlXmlContent) ? '---' : '<i class="fa fa-caret-down"></i>'),
      '#value' => $htmlXmlContent,
      '#children' => '<div>',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
  }

  private function createImageFilesFieldset($htmlImages) {
    $this->pageForm['page-fieldset']['image-files-fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Image Files ') . (empty($htmlImages) ? '---' : '<i class="fa fa-caret-down"></i>'),
      '#value' => $htmlImages,
      '#children' => '<div>',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
  }

  private function createUnsupportedFilesFieldset($htmlUnsupportedFiles) {
    $this->pageForm['page-fieldset']['unsupported-files-fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Unsupported Files ') . (empty($htmlUnsupportedFiles) ? '---' : '<i class="fa fa-caret-down"></i>'),
      '#value' => $htmlUnsupportedFiles,
      '#children' => '<div>',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
  }

  private function createBackButton() {
    $this->pageForm['page-fieldset']['btn-back'] = ['#markup' => '<a href="' . $this->getBackButtonUrl() . '" class="btn btn-default" style="margin-right: 5px"><span class="fa fa-arrow-left"></span> Back</a>'];
  }

  /**
   * Returns the target url for the back button. Usually this should be the request overview (or manager request overview)
   * @return string
   */
  protected function getBackButtonUrl() {
    return sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $this->requestId, 3);
  }

  protected function checkPageAccess() {
    if ($this->datasetObj->isEmpty()) {
      /**
       * Handle non-existant Dataset ID exception
       */
      drupal_goto(sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $this->requestId));
    }
    $this->requestObj = ALMRequestsRepository::findById($this->requestId);
    if ($this->requestObj->isEmpty()) {
      /**
       * Handle non-existant Request ID exception
       */
      drupal_not_found();
    }

    //todo chris 20/05/19 Why we redirect here to the landing page?
    if ($this->datasetObj->getRequestId() !== $this->requestId) {
      /**
       * Handle invalid Request/Dataset association exception
       */
      $this->accessDenied();
    }

    $this->checkPageAccessPermission();
  }

  /**
   * If the user does not have ALM Module permission to view the dataset also
   * check if the dataset is public - otherwise send to public landing page
   */
  protected function checkPageAccessPermission() {
    if (!$this->hasAccessAsOwner() && !$this->hasAccessDueToSharePermission()) {
      // If the current user has no user access rights as owner or share permissions but manager access rights,
      // he should be redirected to the management page
        if ($this->hasAccessAsManger()){
          $this->redirectToManagementPage();
        } else {
          $this->accessDenied();
        }
      }
  }

  /**
   * Check if the current user has the permission to access the dataset as owner.
   * Return true if the user has access as owner
   * @return bool
   */
  protected function hasAccessAsOwner(){
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_VIEW_IMAGES)
      || $this->datasetObj->checkReadPermission();
  }

  /**
   * Check if the current user has the permission to access the dataset as manager.
   * Return true if the user has access as manager
   * @return bool
   */
  protected function hasAccessAsManger(){
    // Check also the global permission to see user datasets. Not all alm-manager might have it.
    if($this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW_IMAGES)
      && user_access(SFB_ALM_PERMISSION_ACCESS_USER_UPLOADED_DATA)){
      return true;
    }
    return false;
  }

  /**
   * Check if the current user has the permission to access the dataset due to a given share permission.
   * Returns true if the user has access due to share permission
   * @return bool
   */
  protected function hasAccessDueToSharePermission(){
    return $this->datasetObj->checkSharedPermission();
  }

  /**
   * Redirects to the manager page of the dataset
   */
  private function redirectToManagementPage(){
    drupal_goto(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_DATASET_VIEW, $this->requestId, $this->datasetId));
  }

  /**
   * Handles what happens if no access to the page is granted.
   *
   * Redirect the user to the landing page of the request.
   */
  protected function accessDenied(){
    drupal_goto(sfb_alm_url(RDP_ALM_URL_REQUEST_LANDING, $this->requestId));
  }

}