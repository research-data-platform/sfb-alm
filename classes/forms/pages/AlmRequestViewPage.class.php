<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 15.05.2019
 * Time: 23:30
 */
class AlmRequestViewPage implements iAlmFormPage {

  /**
   * Submit functions (including ajax functions):
   * Unnamed submit:
   *    - hook_submit
   *    - coverslipLabelsAjaxCallback
   *    - coverslipLabelAjaxAdd
   *    - CoverslipLabelAjaxDelete
   *    - coverslipLabelAjaxHide
   *    - coverslipLabelSubmitPrint
   */

  const COVERSLIP_LABELS_AJAX_CALLBACK = 'rdp_alm_coverslip_labels_create_ajax_links_callback';

  const COVERSLIP_LABELS_AJAX_ADD = 'rdp_alm_coverslip_label_create_ajax_add';

  const COVERSLIP_LABELS_AJAX_DELETE = 'rdp_alm_coverslip_label_create_ajax_delete';

  const COVERSLIP_LABELS_AJAX_HIDE = 'rdp_alm_coverslip_label_create_ajax_hide';

  const COVERSLIP_LABELS_SUBMIT_PRINT = 'rdp_alm_coverslip_label_create_submit_print';

  /**
   * @var int
   */
  protected $requestId;

  /**
   * @var ALMRequest
   */
  protected $requestObj;

  /**
   * @var array
   */
  protected $form;

  public function getPageForm($form = null, &$form_state, $requestId = null) {
    $this->form = $form;
    $this->requestObj = ALMRequestsRepository::findById($requestId);
    $this->requestId = $requestId;
    $form_state[ALM_REQUEST_ID] = $requestId;

    $this->checkPageAccess();
    $this->createLogAccessMessage();

    $this->createTabs();
    $this->createPageButtons();

    return $this->form;
  }

  /**
   *
   */
  protected function createLogAccessMessage() {
    //do nothing in user view
  }

  /**
   * Creates all Tabs that the current user is allowed to see in the current request state
   */
  private function createTabs() {
    $this->initTabs();

    // Request Overview
    $this->createRequestOverviewTab();

    // Consultation
    if ($this->permitConsultationTab()) {
      $this->createConsultationTab();
    }

    // Create Coverslip Labels
    if ($this->permitCoverslipLabelTab()) {
      $this->createCoverslipLabelTab();
    }

    // Alm Image Overview
    if ($this->permitAlmImagesTab()) {
      $this->createAlmImagesTab();
    }

    // Comments and Logs
    $this->createCommentsAndLogTab();
  }

  /**
   * Init the tabs array and add button to tab bar
   */
  private function initTabs() {
    $this->form['tabs'] = [
      '#tree' => TRUE,
      '#theme' => 'sfb_alm_dashboard_tab_form',
      '#active' => isset($_GET['tab']) ? $_GET['tab'] : 0,
    ];

    $this->createDownloadBtn();
  }

  /**
   * Creates the download button on the right side of the tab bar
   */
  private function createDownloadBtn() {
    $this->form['tabs']['#btn-right'] = [
      "title" => "Request form PDF",
      "url" => sfb_alm_url(RDP_ALM_URL_DOWNLOAD_PDF_REQUEST, $this->requestObj->getRequestID()),
      "icon-class" => "fa fa-file",
      "icon-color" => "#72afd2",
    ];
  }

  private function createRequestOverviewTab() {
    $request_tab = new AlmRequestDataViewTab($this->requestObj);
    $this->form['tabs']['Request Overview'] = [
      '#type' => 'markup',
      '#markup' => $request_tab->getHtml(),
    ];
  }

  /**
   * Checks if the current user is allowed to access the consultation tab in the current state
   *
   * @return bool
   */
  protected function permitConsultationTab() {
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_VIEW_CONSULTATION);
  }

  private function createConsultationTab() {
    $consultation_tab = $this->getAlmConsultationViewTabObject();
    $this->form['tabs']['Consultation'] = [
      '#type' => 'markup',
      '#markup' => $consultation_tab->getHtml(),
    ];
  }

  protected function getAlmConsultationViewTabObject() {
    return new AlmConsultationViewTab($this->requestObj);
  }

  /**
   * Checks if the current user is allowed to access the create coverslip tab in the current request state
   *
   * @return bool
   */
  protected function permitCoverslipLabelTab() {
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CREATE_COVERSLIPS);
  }

  private function createCoverslipLabelTab() {
    $class = get_called_class();

    $tabCoverslipLabels = new AlmCoverslipLabelCreateTab($this->requestId);

    $tabCoverslipLabels->setAjaxCallbackFunction($class::COVERSLIP_LABELS_AJAX_CALLBACK);
    $tabCoverslipLabels->setAjaxSubmitAddFunction($class::COVERSLIP_LABELS_AJAX_ADD);
    $tabCoverslipLabels->setAjaxSubmitDeleteFunction($class::COVERSLIP_LABELS_AJAX_DELETE);
    $tabCoverslipLabels->setAjaxSubmitHideFunction($class::COVERSLIP_LABELS_AJAX_HIDE);
    $tabCoverslipLabels->setSubmitPrintFunction($class::COVERSLIP_LABELS_SUBMIT_PRINT);

    $this->form['tabs']['Coverslip Lables'] = $tabCoverslipLabels->getForm();
  }

  /**
   * Check if the current user is allowed to access the Alm Image Overview tab in the current request state.
   *
   * @return bool
   */
  protected function permitAlmImagesTab() {
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_VIEW_IMAGES);
  }

  private function createAlmImagesTab() {
    $tabImages = $this->getAlmImageOverviewTabObject();
    $this->form['tabs']['ALM Images'] = [
      '#type' => 'markup',
      '#markup' => $tabImages->getHtml(),
    ];
  }

  /**
   * Returns an new AlmImageOverviewTab Object
   *
   * @return AlmImageOverviewTab
   */
  protected function getAlmImageOverviewTabObject() {
    return new AlmImageOverviewTab($this->requestObj);
  }

  private function createCommentsAndLogTab() {
    $tabCommentsAndLog = new AlmCommentsAndLogsTab($this->requestObj);
    $this->form['tabs']['Comments and Log'] = $tabCommentsAndLog->getForm();
  }

  protected function createPageButtons() {
    $this->createBackButton();

    if ($this->permitEditButton()) {
      $this->createEditButton();
    }

    if ($this->permitSubmitButton()) {
      $this->createSubmitButton();
    }

    if ($this->permitConsultButton()) {
      $this->createConsultButton();
    }
  }

  protected function createBackButton() {
    $this->form['btn-back'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_REQUEST) . '" class="btn btn-default" style="margin-right:5px"><span class="fa fa-arrow-left"></span> Back to overview</a>',
    ];
  }

  private function createEditButton() {
    $this->form['btn-edit'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT,
          $this->requestId) . '" class="btn btn-default"><span class="fa fa-edit"></span> Edit request</a>',
    ];
  }

  /**
   * Check if the current user is allowed to edit the request in the current request state.
   *
   * @return bool
   */
  private function permitEditButton() {
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT);
  }

  private function createSubmitButton() {
    $this->form['btn-submit'] = [
      '#type' => 'submit',
      '#attributes' => array(
        'class' => array('btn-primary', 'btn-lg'),
        'style' => array('float: right;'),
      ),
      '#value' => '<i class="fa fa-send-o"></i> ' . t('Submit request'),
    ];
  }

  /**
   * Check if the current user is allowed to submit the request in the current request state.
   *
   * @return bool
   */
  private function permitSubmitButton() {
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT);
  }

  private function createConsultButton() {
    $this->form['btn-consult'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_REQUEST_EDIT, $this->requestId) .
        '?active=1" class="btn btn-primary btn-lg" style="float: right;" ><span class="fa fa-edit"></span> ' . t('Consult request') . '</a>',
    ];
  }

  /**
   * Check if the current user is allowed to consult the request in the current request state.
   *
   * @return bool
   */
  private function permitConsultButton() {
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_CONSULT);
  }

  /**
   * Check if the required request is available an the user has permission to access it.
   * Terminate page build if not.
   */
  protected function checkPageAccess() {
    // Check if request Object is available
    if ($this->requestObj->isEmpty()) {
      drupal_not_found();
      exit();
    }

    $this->checkPageAccessPermission();
  }

  /**
   * Checks if the user has the permission to view the request
   * Link to manager_view if the user has only the permission to view the request as manager.
   */
  protected function checkPageAccessPermission() {
    if (!$this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_VIEW)) {

      if ($this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW)) {
        drupal_goto(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $this->requestObj->getRequestID()));
      }
      else {
        drupal_access_denied();
        exit();
      }
    }
  }

  /*
  * Ajax callback function for adding or deleting label rows
  * Function reloads the whole label_table fieldset
  */
  public function coverslipLabelsAjaxCallback($form, $form_state) {
    return $form['tabs']['Coverslip Lables']['fieldset-label_table'];
  }

  /**
   * (Ajax) Submit function for adding a new row for labels
   *
   * @param $form
   * @param $form_state
   */
  function coverslipLabelAjaxAdd($form, &$form_state) {
    $input_fields = $form_state['values']['tabs']['Coverslip Lables']['fieldset-add_new_label']['input_fields'];

    $number_of_coverslips = $input_fields['sample']['coverslip_number'];
    $sample_id = $input_fields['sample']['sample_id'];
    $staining_id = $input_fields['staining']['staining_id'];

    $sample = ALMRequestSampleRepository::findBySampleId($sample_id);

    //creates for each coverslip a coverslipLabel object and store it to the database.
    for ($i = 0; $i < $number_of_coverslips; $i++) {
      $new_coverslipLabel = new ALMRequestCoverslipLabel();
      $new_coverslipLabel->initNew();
      // the requestID must be equals to the sample
      $new_coverslipLabel->setRequestId($sample->getRequestID());
      $new_coverslipLabel->setSampleId($sample_id);
      $new_coverslipLabel->setStainingId($staining_id);
      // incremented number
      $new_coverslipLabel->setCoverslipNo($sample->assignNewCoverslipNo());

      // Create and register Persistent Identifier
      $new_coverslipLabel->createPid();

      $new_coverslipLabel->save();
    }


    $sample->save();
    $form_state['rebuild'] = TRUE;
  }

  /**
   * (Ajax) Submit function for deleting a row of the label table
   *
   * @param $form
   * @param $form_state
   */
  public function CoverslipLabelAjaxDelete($form, &$form_state) {
    // set to true, if the foreach detect a printed label
    $selected_contain_printed = FALSE;

    // identify the database IDs of the labels that should be deleted
    foreach ($form_state['values']['tabs']['Coverslip Lables']['fieldset-label_table']['table'] as $label_db_id) {
      If ($label_db_id != 0) {
        $label = ALMRequestCoverslipLabelRepository::findById($label_db_id);
        if ($label->isPrinted()) {
          $selected_contain_printed = TRUE;
        }
        else {
          $label->setState(ALMRequestCoverslipLabel::$STATE_DELETED);
          $label->save();
        }
      }
    }

    // Shows an alert dialog if the user tres to
    if ($selected_contain_printed) {
      drupal_set_message("Printed labels cannot deleted anymore. You can hide the labels instead.", 'warning');
    }


    $form_state['rebuild'] = TRUE;
  }

  /**
   * (Ajax) Submit function for deleting a row of the label table
   *
   * @param $form
   * @param $form_state
   */
  function coverslipLabelAjaxHide($form, &$form_state) {
    // identify the database IDs of the labels that should be deleted
    foreach ($form_state['values']['tabs']['Coverslip Lables']['fieldset-label_table']['table'] as $label_db_id) {
      If ($label_db_id != 0) {
        $label = ALMRequestCoverslipLabelRepository::findById($label_db_id);
        $label->setState(ALMRequestCoverslipLabel::$STATE_HIDDEN);
        $label->save();
      }
    }

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Creates for all selected values the template for the label printer
   *
   * @param $form
   * @param $form_state
   */
  function coverslipLabelSubmitPrint($form, &$form_state) {
    $request = ALMRequestsRepository::findById($form_state[ALM_REQUEST_ID]);
    $print_job = new ALMCoverslipLabelsPrintJob();
    $print_job->setRequestId($form_state[ALM_REQUEST_ID]);
    // identify the database IDs of selected Labels
    foreach ($form_state['values']['tabs']['Coverslip Lables']['fieldset-label_table']['table'] as $label_db_id) {
      If ($label_db_id != 0) {
        // evtl einfach label object an printjob übergeben
        $label = ALMRequestCoverslipLabelRepository::findById($label_db_id);
        // check if the label belongs the current user
        $print_job->addLabel([
          'projectName' => $request->getSubProjectAbbreviation(),
          'sampleName' => $label->getSampleName() . '_' . $label->getFormattedCoverslipNo(),
          'userName' => $request->getUser()->getSurname(),
          'date' => date('Y-m-d'),
          'stainingName' => $label->getStainingName(),
          'handleUrl' => (new ALMPidStub($label->getPid()))->getAccessUrl(),
        ]);

        // set Label to printed and lock the sample and staining objects (set editable to false) to ensure, that they
        // could not changed anymore
        $label->setPrinted(TRUE);
        $label->save();

        $sample_obj = ALMRequestSampleRepository::findBySampleId($label->getSampleId());
        $sample_obj->setEditable(FALSE);
        $sample_obj->save();

        $staining_obj = ALMRequestStainingRepository::findByStainingId($label->getStainingId());
        $staining_obj->setEditable(FALSE);
        $staining_obj->save();

      }
    }

    // If no labels were selected, the printJob is empty and no redirect should executed
    if (!$print_job->isEmpty()) {
      $jobId = $print_job->save();

      $form_state['rebuild'] = TRUE;
      drupal_goto(sfb_alm_url(RDP_ALM_URL_COVERSLIP_LABELS_DOWNLOAD, $form_state[ALM_REQUEST_ID]) . '?jobId=' . $jobId);
    }
    $form_state['rebuild'] = TRUE;
  }

  /**
   * Submit handler for sfb_alm_request_view_form.
   *
   * Submits the current request to ALM-Team.
   * Sets request to pending state.
   *
   * @param $form
   * @param $form_state
   */
  function submit($form, &$form_state) {
    $requestID = $form_state[ALM_REQUEST_ID];
    // update request
    // get request from database
    $request = ALMRequestsRepository::findById($requestID);

    // Check if the user has permission to submit(edit) the request
    if (!$request->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT)) {
      watchdog('sfb_alm', 'Submission error in rdp_alm_request_view.inc');
      drupal_access_denied();
      return;
    }

    //  Sets submitDate and RequestState
    $request->setSubmitAttributes();

    // Set the comment type to from submitEdit to submit because then it isnt possible to edit the comment again.
    $comment = $request->getCommentWithType(ALMCommentType::SubmitEdit);
    $comment->setTypeID(ALMCommentType::Submit);

    //Set the updated user Comment to the updatedComment Variable, so the next save invoke will store it to the DB
    $request->setUpdatedComment($comment);

    // save request in database
    $request->save(false);

    $msg = 'Request ' . l($requestID, sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $requestID)) . ' submitted successfully.';
    if (variable_get(SFB_ALM_VAR_CONFIG_MANAGER_MODE, 1)) {
      // Only send notifications to service unit team, if module is configured in "active manager mode"
      $msg .= ' E-mail notifications have been sent to the service team.';
    }
    else {
      /**
       * Auto-accept request if "active manager mode" is deactivated
       */
      if ($request->getRequestState() == ALMRequestState::Pending) {
        $request->setRequestState(ALMRequestState::Accepted);
        $request->save(FALSE);
        ALMUserQualificationRepository::setQualificationLevelOfUserId($request->getUserID(),
          ALMUserQualificationRepository::$QUALIFICATION_LEVEL_USER_CONSULTATION);
      }
    }
    drupal_set_message($msg);
    // Send an e-mail to user (and alm team
    sfb_alm_mail_send_new_request_notification($request);


    // redirect to overview form
    $form_state['redirect'] = array(sfb_alm_url(RDP_ALM_URL_REQUEST));
  }
}