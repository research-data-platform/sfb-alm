<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 03.03.2019
 * Time: 07:24
 *

 */
class AlmAddStainingPage implements iAlmFormPage {

  /**
   * Name of submit functions which are used in page.inc file.
   */
  const CANCEL_SUBMIT_FUNCTION_NAME = 'rdp_alm_request_view_add_staining_cancel';
  const ADD_SUBMIT_FUNCTION_NAME = 'rdp_alm_request_view_add_staining_submit';
  const ADD_VALIDATE_FUNCTION_NAME = 'rdp_alm_request_view_add_staining_submit_validate';

  /**
   * @var ALMRequest
   */
  protected $requestObj;
  /**
   * @var ALMRequestStaining
   */
  protected $stainingObj;

  /**
   * Returns an form array for drupal_get_form forms
   * @param null $form
   * @param $form_state
   * @param null $request_id
   * @return null
   */
  function getPageForm($form = null, &$form_state, $request_id = null){
    $form_state['request_id'] = $request_id;
    $this->requestObj = ALMRequestsRepository::findById($request_id);

    // Check the permission and denied access if not allowed
    if(!$this->userIsPermitted()){
      drupal_access_denied();
      exit();
    }

    //get the url parameters
    $params = drupal_get_query_parameters();
    if(isset($params['type'])){
      $staining_type = $params['type'];
    } else{
      drupal_set_message('No type parameter found');
      drupal_access_denied();
    }

    $form_state['staining_type'] = $staining_type;

    // create an new (empty) staining object
    $this->initStainingObj($staining_type);

    $staining_form = new AlmStainingEditForm($this->stainingObj);
    $staining_form->setRemoveBtnVisible(false);

    $form['new_staining'] = $staining_form->getForm();


    $form['confirm-dialog']  = $this->confirm_dialog();

    // uses called class to access class constants (also if extended)
    $class = get_called_class();

    $form['btn-cancel'] = [
      '#type' => 'submit',
      '#value' => 'Cancel',
      '#description' => 'Go back to the project view. Attention: The new staining will be withdrawn! It is not available anymore',
      '#submit' => array($class::CANCEL_SUBMIT_FUNCTION_NAME),
    ];

    $form['btn-submit'] = [
      '#type' => 'submit',
      '#value' => 'Add staining',
      '#description' => 'Add the new staining to the alm-project. Attention: This cannot to be undone! The added staining is not editable anymore',
      '#submit' => array($class::ADD_SUBMIT_FUNCTION_NAME),
      '#attributes' => array('id' => 'btn-submit',), // the id is necessary for the confirm dialog
      '#validate' => array($class::ADD_VALIDATE_FUNCTION_NAME),
    ];

    return $form;
  }

  /**
   * returns a drupal_form array for the confirm dialog of the submit button
   * (This content is normally not visible, that's why it does not depend where this code is inserted)
   * @return array
   */
  protected function confirm_dialog(){
    $confirm_dialog =  new ConfirmDialog('btn-submit');
    $confirm_dialog->setAnswTextYes('Continue');
    $confirm_dialog->setBody('
      <h4>Are you sure that you want to add the staining now?</h4>
      <h4 class="small">Hint: Ensure that the "short label name" field is not empty</h4>
      <div class="panel panel-warning">
        <div class="panel-heading">
        <h3 class="panel-title">Attention:</h3> 
        </div>
        <div class="panel-body">
          <p>If you add a new staining to a running request, this cannot be undone! Once the staining is added it will not be possible to edit the staining anymore!</p>
        </div>
      </div>
          ');


    return [
      '#type' => 'markup',
      '#markup' => $confirm_dialog->getDialogAsHtml(),
    ];
  }

  /**
   * Creates a new initial (empty) staining object, which contains the AB / Additional Staining rows for the given staining type.
   * @param $staining_type
   */
  protected function initStainingObj($staining_type){
    $this->stainingObj = new ALMRequestStaining();
    $this->stainingObj->setType($staining_type);
    $this->stainingObj->initialDefaultStainingRows();
    $this->stainingObj->setEditable(true);
  }

  /**
   * Closes the "new staining" view and redirected back to the request overview
   * @param $form
   * @param $form_state
   */
  function cancelSubmitFunction($form, &$form_state){
    $request_id = $form_state['request_id'];
    $staining_type = $form_state{'staining_type'};

    $form_state['redirect'] = $this->requestViewUrl($request_id, $staining_type);
  }

  /**
   * Check if the staining has a label name
   * @param $form
   * @param $form_state
   */
  function addValidateFunction($form, &$form_state) {
    // check if the short_label field contains any text, if not set an form error.
    if ( empty($form_state['values']['new_staining']['pre_table_form']['short_label'])) {
      $element_id = 'new_staining][pre_table_form][short_label';
      form_set_error($element_id, 'Please enter a name for the "short label name field" of the staining <b>and check your Antibodies!</b>');
    }
  }

  /**
   * Creates a new staining with the user input and save it (linked to the request) in the db.
   * @param $form
   * @param $form_state
   */
  function addSubmitFunction($form, &$form_state){
    $request_id = $form_state['request_id'];
    $staining_type = $form_state{'staining_type'};

    $this->requestObj = ALMRequestsRepository::findById($request_id);
    if(!$this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_ADD_STAININGS)){
      drupal_access_denied();
    }


    // create new empty staining and add the request ID
    $this->initStainingObj($staining_type);
    $this->stainingObj->setRequestId($request_id);

    // merges the values of the inputted Staining Table into the (new created) staining object and save it to the db
    $this->stainingObj = AlmStainingEditForm::mergeFormStateInDO($this->stainingObj, $form_state['values']['new_staining']);
    $this->stainingObj->save();

    sfb_alm_create_log_entry(ALMLog::$ACTION_ADD_STAINING, 'New staining was added on state running. Added staining: ' . $this->stainingObj->getLabelName(), $request_id);

    // redirect to selected staining type on the request view
    $form_state['redirect'] = $this->requestViewUrl($request_id, $staining_type);
  }

  protected function requestViewUrl($request_id, $staining_type){
    return sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request_id, '1', 'staining-field-' . $staining_type);
  }

  /**
   * Check if the current user is permitted to access the page in the user role.
   * @return mixed
   */
  protected function userIsPermitted(){
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_ADD_STAININGS);
  }

}