<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 11.06.2019
 * Time: 23:56
 */
class AlmDatasetLandingPage implements iAlmFormPage {
  /**
   * @var array
   */
  private $form;

  /**
   * @var array
   */
  private $form_state;

  /**
   * @var int
   */
  private $requestId;

  /**
   * todo: Is this necessary?
   * @var ALMRequest
   */
  private $requestObj;

  /**
   * @var int
   */
  private $datasetId;

  /**
   * @var ALMDataset
   */
  private $datasetObj;

  /**
   * @var CdstarObject
   */
  private $cdStarObj;

  /**
   * AlmDatasetLandingPage constructor.
   * @param int $requestId
   * @param int $datasetId
   */
  public function __construct($requestId, $datasetId) {
    $this->requestId = $requestId;
    $this->datasetId = $datasetId;

    $this->loadDatasetObj();
    $this->loadRequestObj();
    $this->loadCdStarObj();
  }


  /**
   * @param null $form
   * @param $form_state
   * @return array associative array for drupal_get_form function
   */
  public function getPageForm($form = null, &$form_state) {
    $this->form = $form;
    $this->form_state = $form_state;

    $this->createForms();

    return $this->form;
  }

  private function createForms(){

    $this->form['btn_back'] = [
      '#markup' => l('<span class="glyphicon glyphicon-circle-arrow-left"></span>&nbsp;Back',
        $this->requestObj->getLandingPageUrl(),
        [
          'html' => TRUE,
          'attributes' => ['class' => 'btn btn-info btn-sm pull-right', 'style' => ['margin-bottom: 20px;']],
        ]),
      '#prefix' => '<div class="container-fluid">',
      '#suffix' => '</div>',
    ];

    $this->form['page-fieldset'] = [
      '#type' => 'fieldset'
    ];

    $this->createGeneralInfoForm();
    // if the coverslip data is not shown, the divs of the row have to be closed
    if($this->userIsPermitToViewExtendedData()) $this->createCoverslipObjectForm(); else $this->createEmptyCoverslipObjectForm();

    if($this->userIsPermitToViewExtendedData()) {
      $this->createImagingGoalForm();
      $this->createDownloadForm();

      $this->createFilesForm();

      $this->createAntibodyForm();
    }
  }

  private function createGeneralInfoForm(){
    $this->form['page-fieldset']['dataset-info'] = ['#markup' => '<div class="row row-eq-height"><div class="col-sm-12 col-md-6">' . $this->datasetObj->displayInfoblock()];
  }

  private function getCoverslipPIDsuffix(){
    $coverslipPid = $this->datasetObj->getPidId();
    $prefix = variable_get("alm_coverslip_label_pid_prefix", ALMRequestCoverslipLabel::DEFAULT_PID_PREFIX);
    $suffix = str_replace($prefix, "", $coverslipPid);

    return $suffix;
  }

  private function createCoverslipObjectForm(){
    $pidSuffix = $this->getCoverslipPIDsuffix();
    $coverslip = ALMRequestCoverslipLabelRepository::findByPID($pidSuffix);

    $sample = $coverslip->getSampleName();
    $staining = $coverslip->getStainingName();

    $text = '<p>Sample: '. $sample . '</p>
             <p>Staining: '. $staining . '</p>';

    $this->form['page-fieldset']['coverslip-object'] = [
      '#type' => 'fieldset',
      '#title' => t('Coverslip Object'),
      '#collapsible' => false,
      '#value' => $text,
      '#prefix' => '</div><div class="col-sm-12 col-md-6">',
      '#suffix' => '</div></div>',
    ];
  }

  /**
   * This function is needed to close the row of the Fieldsets if the coverslip information should ne be displayed
   * (because the general information and coverslip information are beside in the same row)
   */
  private function createEmptyCoverslipObjectForm(){
    $this->form['page-fieldset']['coverslip-object'] = ['#markup' => '</div></div>'];
  }

  private function createImagingGoalForm(){
    $this->form['page-fieldset']['imaging-goal'] = [
      '#type' => 'fieldset',
      '#title' => t('Imaging Goal and Research Question'),
      '#collapsible' => false,
      '#value' => '<p>' . $this->requestObj->getImagingGoal() . '</p>',
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
  }

  /**
   * If there are associated antibodies, creates an Fieldset with a table for the associated antibodies.
   */
  private function createAntibodyForm(){
    $antibodyIDs = $this->getAssociatedAntibodyIds();

    if(!empty($antibodyIDs)) {
      $antibodyTable = ALMAntibodyAdapter::getAntibodyViewTable($antibodyIDs);
      $htmlAntibodyTable = theme('table', $antibodyTable['table']);

      $this->form['page-fieldset']['Antibodies-fieldset'] = [
        '#type' => 'fieldset',
        '#title' => t('Associated Antibodies'),
        '#value' => $htmlAntibodyTable,
        '#children' => '<div>',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      ];
    }
  }

  /**
   * Returns an array of ALMAntibody objects of all associated antibodies
   * @return ALMAntibody[]
   */
  private function getAssociatedAntibodyObjects(){
    $antibodyIDs = $this->getAssociatedAntibodyIds();

    $antibodies = array();
    foreach ($antibodyIDs as $antibodyID){
      $antibodies[] = ALMAntibodyAdapter::getALMAntibodyObjectById($antibodyID);
    }

    return $antibodies;
  }

  /**
   * Returns an array of IDs of all associated antibodies
   * @return array
   */
  private function getAssociatedAntibodyIds(){
    //the antibodies are associated with the coverslip, which can have several datasets
    $coverslipPidSuffix = $this->getCoverslipPIDsuffix();

    $coverslip = ALMRequestCoverslipLabelRepository::findByPID($coverslipPidSuffix);

    $stainingId = $coverslip->getStainingId();
    $staining = ALMRequestStainingRepository::findByStainingId($stainingId);

    // user array_filter to remove "null" entries
    $antibodyIDs = array_filter(array_unique($staining->getAssociatedAntibodyIds()));

    return $antibodyIDs;
  }

  private function createFilesForm(){
    $files = $this->cdStarObj->getFiles();

    $htmlXmlContent = '';
    $htmlImages = '';
    $htmlUnsupportedFiles = '';

    foreach ($files as $file) {
      $fileType = $file->getContentType();
      if (in_array($fileType, ALMDataset::$allowedDataTypes)) {
        if ($file->getContentType() == 'image/tiff') {
          $imageFile = new ALMImageFile($file, $this->requestId, $this->datasetObj->getId());
          $htmlImages .= $imageFile->getHtmlImageField();
        } else { // supported xml files

          $download_icon = l(CdstarResources::GLYPHICON_DOWNLOAD, $file->url('download'), ['html' => TRUE]);
          $header = '<strong>' . $file->getFilename() . '</strong> ' . $download_icon;

          $xml = $file->getBinaryFileStream();
          $htmlXmlContent .= '<p>' . $header . '<pre class="prettyprint linenums" style="overflow: scroll; max-height: 400px">
                    <code class="language-xml">' . htmlspecialchars($xml) . '</code></pre></p>';
        }
      } else {
        $htmlUnsupportedFiles .= $this->datasetObj->fileHtmlDisplay($file);
      }
    }

    $this->form['page-fieldset']['dataset-info'] = ['#markup' => '<div class="row row-eq-height"><div class="col-sm-12 col-md-6">' . $this->datasetObj->displayInfoblock()];

    $this->createXMLFileFieldset($htmlXmlContent);
    $this->createImageFilesFieldset($htmlImages);
    $this->createUnsupportedFilesFieldset($htmlUnsupportedFiles);

  }

  private function createDownloadForm() {
    $this->form['page-fieldset']['dataset-download-fieldset'] = [
      '#type' => 'fieldset',
      '#value' => l(CdstarResources::GLYPHICON_DOWNLOAD, $this->datasetObj->url('download'), ['html' => TRUE])
        . " Download complete dataset: <strong>" . $this->cdStarObj->getDisplayName() . "</strong>",
      '#children' => '<div>',
      '#collapsible' => FALSE,
      '#suffix' => '<p></p>',
      //'#collapsed' => true,
    ];
  }

  private function createXMLFileFieldset($htmlXmlContent) {
    $this->form['page-fieldset']['xml-file-fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('XML Files ') . (empty($htmlXmlContent) ? '---' : '<i class="fa fa-caret-down"></i>'),
      '#value' => $htmlXmlContent,
      '#children' => '<div>',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
  }

  private function createImageFilesFieldset($htmlImages) {
    $this->form['page-fieldset']['image-files-fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Image Files ') . (empty($htmlImages) ? '---' : '<i class="fa fa-caret-down"></i>'),
      '#value' => $htmlImages,
      '#children' => '<div>',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
  }

  private function createUnsupportedFilesFieldset($htmlUnsupportedFiles) {
    $this->form['page-fieldset']['unsupported-files-fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Unsupported Files ') . (empty($htmlUnsupportedFiles) ? '---' : '<i class="fa fa-caret-down"></i>'),
      '#value' => $htmlUnsupportedFiles,
      '#children' => '<div>',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
  }



  protected function loadDatasetObj() {
    $this->datasetObj = ALMDatasetRepository::findById($this->datasetId);
  }

  protected function loadRequestObj(){
    $this->requestObj = ALMRequestsRepository::findById($this->requestId);
  }

  private function loadCdStarObj(){
    $this->cdStarObj = CdstarObjectRepository::findById($this->datasetObj->getCdstarObjectId());
  }

  // permissions
  private function userIsPermitToViewExtendedData(){
    return $this->datasetObj->checkReadPermission();
  }

}