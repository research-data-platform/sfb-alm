<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 05.03.2019
 * Time: 12:52
 */
class AlmAddStainingManagementPage extends AlmAddStainingPage {

  /**
   * Overwrites super constants
   * Name of submit functions which are used in page.inc file.
   */
  const CANCEL_SUBMIT_FUNCTION_NAME = 'rdp_alm_management_request_view_add_staining_cancel';
  const ADD_SUBMIT_FUNCTION_NAME = 'rdp_alm_management_request_view_add_staining_submit';
  const ADD_VALIDATE_FUNCTION_NAME = 'rdp_alm_management_request_view_add_staining_submit_validate';


  /**
   * Overwrites super functions.
   * Returns the url to the according request view page (including position on the page)
   * @param $request_id
   * @param $staining_type
   * @return string
   */
  protected function requestViewUrl($request_id, $staining_type){
    return sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $request_id, '1', 'staining-field-' . $staining_type);
  }

  /**
   * Overwrites the super function.
   * Check if the current user is permitted to access the page in the Manager role instead of the user role.
   * @return boolean
   */
  protected function userIsPermitted(){
    return $this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_ADD_STAININGS);
  }

}