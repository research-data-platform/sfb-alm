<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 17.05.2019
 * Time: 01:46
 */
class AlmRequestViewManagementPage extends AlmRequestViewPage {


  /**
   * Overwrites super function
   *
   * Creates the (hidden) log entry that the admin access the request. This is used to display the "new" button
   */
  protected function createLogAccessMessage(){
    // Create a (hidden) log entry, that the manager view the request.
    sfb_alm_create_log_entry(ALMLog::$ACTION_MANAGER_VIEW, "View request", $this->requestObj->getRequestID(), 0);
  }

  /**
   * Overwrites super function
   *
   * @return AlmManagementConsultationViewTab
   */
  protected function getAlmConsultationViewTabObject(){
    return new AlmManagementConsultationViewTab(($this->requestObj));
  }

  /**
   * Overwrites super function
   *
   * Returns an new AlmImageOverviewTab Object
   * @return AlmImageOverviewTab
   */
  protected function getAlmImageOverviewTabObject(){
    return new AlmManagementImageOverviewTab($this->requestObj);
  }

  /**
   * Overwrites super function
   *
   * Check if the user is allowed to view the consultation tab of the request as a manager
   * @return bool
   */
  protected function permitConsultationTab() {
    return $this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW_CONSULTATION);
  }

  /**
   * Overwrites super function
   *
   * Check if the user is allowed to view the image overview tab of the request as manager
   * @return bool
   */
  protected function permitAlmImagesTab() {
    return $this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW_IMAGES);
  }

  /**
   * Overwrites super function
   *
   * Check if the user is allowed to view the coverslip label tab of the request as manager
   * @return bool
   */
  protected function permitCoverslipLabelTab() {
    return $this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW_IMAGES);
  }


  /**
   * Overwrites super function
   *
   * Creates all page button depending on the current request state and user permissions
   */
  protected function createPageButtons(){
    $this->createBackButton();
    if ($this->permitWithdrawButton()) $this->createWithdrawButton();
    if ($this->permitReviseButton()) $this->createReviseButton();
    if ($this->permitEditButton()) $this->createEditButton();
    if ($this->permitAcceptButton()) $this->createAcceptButton();
    if ($this->permitConsultButton()) $this->createConsultButton();
  }

  /**
   * Overwrites super function
   *
   * Creates a button that links back to the alm manager overview
   */
  protected function createBackButton(){
    $this->form['btn-back'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST) .
        '" class="btn btn-default" style="margin-right:5px"><span class="fa fa-arrow-left" ></span> Back to Overview</a>',
      ];
  }

  /**
   * Creates a button that links to the withdraw page of the request
   */
  private function createWithdrawButton(){
    $this->form['btn-withdraw'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_WITHDRAW, $this->requestId) .
        '" class="btn btn-default" style="margin-right:5px" ><span class="fa fa-undo"></span> Withdraw request</a>',
    ];
  }

  /**
   * Checks if the current user is allowed to withdraw the request in the current request state
   * @return bool
   */
  private function permitWithdrawButton(){
    return ($this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_WITHDRAW));
  }

  /**
   * Create a button that links ti the revise page of the request
   */
  private function createReviseButton(){
    $this->form['btn-revise'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_REVISE, $this->requestId) .
      '" class="btn btn-default" style="margin-right:5px" ><span class="fa fa-retweet"></span> Revise request</a>',
    ];
  }

  /**
   * Checks if the current user is allowed to revise the request in the current request state
   * @return bool
   */
  private function permitReviseButton(){
    return $this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_REVISE);
  }

  /**
   * Craetes a button that links to the edit page of the request
   */
  private function createEditButton(){
    $this->form['btn-edit'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $this->requestId) .
        '" class="btn btn-default"><span class="fa fa-edit"></span> ' . t('Edit request') . '</a>',    ];
  }

  /**
   * Check if the edit button should be visible.
   * This required that the user is allowed to edit the request AND the request state is accepted. Otherwise the edit
   * button should named "consult" instead of edit.
   * @return bool
   */
  private function permitEditButton(){
    return (($this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_EDIT)) &&
      !($this->requestObj->getRequestState() == ALMRequestState::Accepted));
  }

  /**
   * Creates a accept button
   */
  private function createAcceptButton(){
    $this->form['btn-accept'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_ACCEPT, $this->requestId) .
        '" class="btn btn-primary btn-lg" style="margin-right:5px; float: right; " ><span class="fa fa-thumbs-o-up"></span> Accept request</a>',
    ];
  }

  /**
   * Checks if the accept button should be visible depending on the current request state
   * @return bool
   */
  private function permitAcceptButton(){
    return $this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_ACCEPT);
  }

  /**
   * Creates a button that links to the consult page of the request
   */
  private function createConsultButton(){
    $this->form['btn-consult'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_EDIT, $this->requestId) .
        '?active=1" class="btn btn-primary btn-lg" style="float: right;" ><span class="fa fa-edit"></span> ' . t('Consult request') . '</a>',
    ];
  }

  /**
   * Checks if the consut button should be visible. Depending on the current request state
   * @return bool
   */
  private function permitConsultButton(){
    return $this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_CONSULT);
  }

  /**
   * Overwrites super function
   *
   * Check if the current user is allowed to view the request in the current state as manager
   */
  protected function checkPageAccessPermission(){
    if (!$this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW)) {

      if ($this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_VIEW)) {
        drupal_goto(sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $this->requestObj->getRequestID()));
      } else {
        drupal_access_denied();
        exit();
      }
    }
  }

}