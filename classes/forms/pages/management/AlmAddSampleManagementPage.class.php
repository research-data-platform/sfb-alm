<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 05.03.2019
 * Time: 22:19
 * Description:
 * Extends the AlmAddSampleFormPage and fits it for the management page.
 * Including the submit function names (that are used in the page.inc file), the correct ViewURL (for the manager page)
 * and the permit function.
 *
 */
class AlmAddSampleManagementPage extends AlmAddSamplePage {
  /**
   * Overwrites super constants
   * Name of submit functions which are used in page.inc file.
   */
  const CANCEL_SUBMIT_FUNCTION_NAME = 'rdp_alm_management_request_view_add_sample_cancel';
  const ADD_SUBMIT_FUNCTION_NAME = 'rdp_alm_management_request_view_add_sample_submit';

  /**
   * Overwrites super functions.
   * Returns the url to the according request view page (including position on the page).
   * @param $request_id
   * @return string
   */
  protected function requestViewUrl($request_id){
    return sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $request_id, '1', 'sample-field');
  }

  /**
   * Overwrites the super function.
   * Check if the current user is permitted to access the page in the Manager role instead of the user role.
   * @return boolean
   */
  protected function userIsPermitted(){
    return $this->requestObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_ADD_SAMPLES);
  }
}