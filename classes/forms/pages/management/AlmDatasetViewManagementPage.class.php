<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 20.05.2019
 * Time: 22:41
 */
class AlmDatasetViewManagementPage extends AlmDatasetViewPage {
  protected function getBackButtonUrl() {
    return sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW, $this->requestId, 3);
  }

  /**
   * Overwrites super function
   *
   * load the dataset object and set the managerView variable.
   */
  protected function loadDatasetObj() {
    parent::loadDatasetObj();
    $this->datasetObj->setManagerView(true);
  }

  /**
   * If the user does not have ALM Module permission to view the dataset also
   * check if the dataset is public - otherwise send to public landing page
   */
  protected function checkPageAccessPermission() {
    if (!$this->hasAccessAsManger()){
      $this->accessDenied();
    }
  }

  /**
   * Handles what happens if no access is granted
   * Shows the Drupal accessDenied page.
   */
  protected function accessDenied() {
    drupal_access_denied();
    exit();
  }
}