<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 08.05.2019
 * Time: 16:46
 */
class AlmCommentsAndLogsTab extends AlmForm {

  /**
   * Request Object which is associated to the Comments / Logs
   * @var ALMRequest
   */
  private $requestObj;

  /**
   * AlmCommentsAndLogsViewTab constructor.
   * @param ALMRequest $requestObj
   */
  public function __construct(ALMRequest $requestObj) {
    $this->requestObj = $requestObj;
  }


  /**
   * Returns a Comment field and a Log field side by side
   *
   * @return string
   */
  public function getHtml() {
    return '
        
  <div class="row">
    <div class="col-md-6">'
      . $this->getRequestLogHtml() . '
    </div>
    <div class="col-md-6">'
      . $this->getRequestCommentsHtml() . '
    </div>
  </div>  
  ';
  }

  public function getForm() {
    return [
      '#type' => 'markup',
      '#markup' => $this->getHtml(),
    ];
  }


  /**
   * Return html code for a box with a table with the Request Log
   *
   * @return string
   * @throws Exception
   */
  private function getRequestLogHtml() {
    // get all log entries belonging to this request
    $log = ALMLogRepository::findAllVisibleLogEntriesByRequestID($this->requestObj->getRequestID());
    // sets the headlines for the "log" theme table
    $table_header = [t('Date'), t('Action'), t('Message'), t('User')];

    // theme table rows
    $table_rows = [];

    // creates the rows for the "Log" theme table
    foreach ($log as $logentry) {
      $table_rows[] = [
        t($logentry->getDate()),
        t($logentry->getAction()),
        t($logentry->getMessage()),
        t($logentry->getUserName()),
      ];
    }
    // format the values of the "Log" table as a formatted (theme) table
    $formatted_table_log = theme('table', [
      'header' => $table_header,
      'rows' => $table_rows,
    ]);

    // Format the "Log" (theme) table in a alm_dashboard_box
    $box_header = ['title' => '<i class="fa fa-edit"></i> ' . t('Request Log')];
    $box_body = [
      'data' => $formatted_table_log,
      'class' => ['" style="height:350px; overflow:auto;'],
    ];
    $box_footer = ['data' => '']; // empty footer

    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }

  /**
   * Return html code for a formatted box with the comments of the request
   * @return string
   * @throws Exception
   */
  private function getRequestCommentsHtml() {
    // get all comments belonging to this request
    $comments = $this->requestObj->getAllComments();

    // sets the headlines for the "comment log" theme table
    $table_header = ['Date', 'Comment'];

    // theme table rows
    $table_rows = [];

    // creates the rows for the "comment log" theme table
    foreach ($comments as $comment) {
      $table_rows[] = [
        $comment->getDate(),
        $comment->getTypeText(),
        $comment->getMessage(),
      ];
    }

    // format the values of the "comment log" table as a formatted (theme) table
    $formatted_table_comment = theme('table', [
      'header' => $table_header,
      'rows' => $table_rows,
    ]);

    // Format the "comment log" (theme) table in a alm_dashboard_box
    $box_header = ['title' => '<i class="fa fa-edit"></i> ' . t('Comments')];
    $box_body = [
      'data' => $formatted_table_comment,
      'class' => ['" style="height:350px; overflow:auto;'],
    ];
    $box_footer = ['data' => '']; // empty footer

    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }


}