<?php
define('RDP_COVERSLIP_LABELS_CREATE_PAGE_DESCRIPTION_TEXT',
  '<p>On this page you can prepare printable labels to be attached to coverslips.</p>
            <p>Select a defined sample and staining. Provide the number of coverslips for every sample-staining combination.</p>
            <p>Then click on the "Add new labels" button to add the new labels to the List.</p>
            <p>Select labels and click on "Print selected". The created csv-file can be used to print the labels with the S02 label printer.</p>
            <p>Attention: Please do not edit / modify the csv file to avoid broken or incorrect hyperlinks redirecting to the Research Data Platform.</p>');



/**
 * Created by PhpStorm.
 * User: chris
 * Date: 27.01.2019
 * Time: 12:51
 */
class AlmCoverslipLabelCreateTab extends AlmForm {

  private $requestId;

  /**
   * Name of the ajax callback function which should contain the part of the form array that needs to rebuild
   * The Callback function has to be implemented in the inc file of the page
   * @var String
   */
  private $ajaxCallbackFunction;

  /**
   * Name of the ajax submit function that will be run after clicking the add button
   * The ajax submit function has to be implemented in the inc file of the page.
   *
   * (Hint: $form_state['rebuild'] = TRUE; should be part of the ajax submit function)
   * @var String
   */
  private $ajaxSubmitAddFunction;

  /**
   * Name of the ajax function that will be run after clicking the delete button
   * The ajax submit function has to be implemented in the inc file of the page.
   *
   * (Hint: $form_state['rebuild'] = TRUE; should be part of the ajax submit function)
   * @var String
   */
  private $ajaxSubmitDeleteFunction;

  /**
   * Name of the ajax function that will be run after clicking the hide button
   * The ajax submit function has to be implemented in the inc file of the page.
   *
   * (Hint: $form_state['rebuild'] = TRUE; should be part of the ajax submit function)
   * @var String
   */
  private $ajaxSubmitHideFunction;

  /**
   * Name of the function that will be run after clicking the print button
   * The submit function has to be implemented in the inc file of the page.
   *
   * (Hint: This is not an ajax function!)
   * @var String
   */
  private $submitPrintFunction;


  /**
   * AlmCreateCoverslipLabelForm constructor.
   */
  public function __construct($requestId) {
    $this->requestId = $requestId;
  }

  /**
   * Returns a renderable form array for creating coverslip labels.
   * The form contains ajax functions. The name of the function has to be set previously
   * @return array
   */
  public function getForm() {

    $form['#theme'] = [
      '#theme' => 'alm_dashboard_box_form',
    ];

    //
    // create option arrays for select Fields
    //
    $samples = ALMRequestSampleRepository::findAllSamplesByRequestID($this->requestId);
    $sample_options = [];
    foreach ($samples as $sample) {
      $sample_options[$sample->getId()] = $sample->getLabelName();
    }

    $stainings = ALMRequestStainingRepository::findAllStainingsByRequestID($this->requestId);
    $staining_options = [];
    foreach ($stainings as $staining) {
      $staining_options[$staining->getId()] = $staining->getLabelName();
    }

    $options = [
      'sample_options' => $sample_options,
      'staining_options' => $staining_options,
    ];

    //
    // Create renderable forms array
    //

    $form['introduction'] = [
      '#type' => 'markup',
      '#markup' => '
        <img src="' . base_path() . drupal_get_path('module', 'sfb_alm') . '/' . SFB_ALM_PATH_RESOURCES . '/alm_coverslip_label_create.png' . '" style="float:left; padding-right: 30px;" alt="Advanced Light Microscopy" />
        <blockquote>'. RDP_COVERSLIP_LABELS_CREATE_PAGE_DESCRIPTION_TEXT .'</blockquote>  ',
      '#prefix' => '<div class="no-print"><p style="margin-top: 10px; margin-bottom: 20px;">',
      '#suffix' => '</p></div>',
    ];

    $form['fieldset-add_new_label']['input_fields'] = [
      '#prefix' => '<div class="row"><div class="col-md-10 col-sm-12 col-xs-12" style="margin: 10px 5px 5px 5px; padding-bottom: 10px">',
      'sample' => [
        '#prefix' => '<div class="col-md-5 col-sm-5 col-xs-12"><table><tr>',
        'sample_id' => [
          //
          // Sample Field
          //
          '#prefix' => '<td width="100%">',
          '#type' => 'select',
          '#options' => $options['sample_options'],
          '#title' => t('Sample'),
          '#default_value' => '',
          '#attributes' => ['style' => '100%'],
          '#suffix' => '</td>',
        ],
        'coverslip_number' => [
          //
          // Sample # Field
          //
          '#prefix' => '<td width="50px" style="padding-left: 10px">',
          '#type' => 'textfield',
          '#title' => t('coverslips'),
          '#description' => t('The number of coverslips of this sample with the selected staining. NOT a range of numbers'),
          '#size' => 30,
          '#maxlength' => 3,
          '#default_value' => 1,
          '#suffix' => '</td>',
        ],

        '#suffix' => '</tr></table></div>',
      ],
      'staining' => [
        '#prefix' => '<div class="col-md-7 col-sm-5 col-xs-12"><table><tr>',
        'staining_id' => [
          //
          // Staining Field
          //
          '#prefix' => '<td width="100%">',
          '#type' => 'select',
          '#options' => $options['staining_options'],
          '#default_value' => '',
          '#title' => t('Staining ID'),
          '#attributes' => ['style' => '100%'],
          '#suffix' => '</td>',
        ],
        'btn-add_new_labels' => [
          '#prefix' => '<td>',
          '#type' => 'submit',
          '#value' => 'Add new labels',
          '#submit' => [$this->ajaxSubmitAddFunction],
          // button is used for ajax submit
          '#ajax' => [
            'callback' => $this->ajaxCallbackFunction,
            'wrapper' => 'fieldset-label-table-wrapper',
          ],
          '#attributes' => ['style' => 'float: right;margin-top: 25px;margin-left:15px'],
          '#suffix' => '</td>',
        ],

        '#suffix' => '</tr></table></div>',
      ],
      '#suffix' => '</div></div>',

    ];

    // table that displays all current labels
    $form['fieldset-label_table'] = [
      '#prefix' => '<div id="fieldset-label-table-wrapper">',
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#suffix' => '</div>',
    ];

    // sets the headlines for the "sample-staining" table
    $table_header = [t('sample'), t('staining')];

    $table_rows = [];
    // load coverslip_labels from the database
    $coverslip_labels = ALMRequestCoverslipLabelRepository::findAllVisibleByRequestId($this->requestId);
    foreach ($coverslip_labels as $coverslip_label) {
      $sample_name = $coverslip_label->getSampleName();
      $coverslipNo = $coverslip_label->getFormattedCoverslipNo();
      $staining_name = $coverslip_label->getStainingName();

      $printed_tag = '';
      if ($coverslip_label->isPrinted()) {
        $printed_tag = ' <span class="label label-default">printed</span>';
      }

      $table_rows[$coverslip_label->getDbId()] = [
        $sample_name . '_' . $coverslipNo,
        $staining_name . $printed_tag,
      ];
    }

    $form['fieldset-label_table']['btn-print_selected_label'] = [
      '#type' => 'submit',
      '#value' => 'Print selected',
      '#submit' => [$this->submitPrintFunction],
      '#attributes' => [
        'class' => ['btn-primary'],
        'style' => 'margin-top: 15px',
      ],
    ];

    $form['fieldset-label_table']['btn-hide_selected_label'] = [
      '#type' => 'submit',
      '#submit' => [$this->ajaxSubmitHideFunction],
      '#value' => 'Hide selected',
      '#attributes' => ['style' => 'margin-top: 15px'],
      '#ajax' => [
        'callback' => $this->ajaxCallbackFunction,
        'wrapper' => 'fieldset-label-table-wrapper',
      ],
    ];

    $form['fieldset-label_table']['btn-delete_selected_label'] = [
      '#type' => 'submit',
      '#submit' => [$this->ajaxSubmitDeleteFunction],
      '#value' => 'Delete selected',
      '#attributes' => ['style' => 'margin-top: 15px'],
      '#ajax' => [
        'callback' => $this->ajaxCallbackFunction,
        'wrapper' => 'fieldset-label-table-wrapper',
      ],
    ];

    $form['fieldset-label_table']['table'] = [
      '#type' => 'tableselect',
      '#header' => $table_header,
      '#options' => $table_rows,
    ];


    return $form;

  }

  /**
   * @param mixed $requestId
   */
  public function setRequestId($requestId) {
    $this->requestId = $requestId;
  }

  /**
   * @return String
   */
  public function getAjaxCallbackFunction() {
    return $this->ajaxCallbackFunction;
  }

  /**
   * @param String $ajaxCallbackFunction
   */
  public function setAjaxCallbackFunction($ajaxCallbackFunction) {
    $this->ajaxCallbackFunction = $ajaxCallbackFunction;
  }

  /**
   * @return String
   */
  public function getAjaxSubmitAddFunction() {
    return $this->ajaxSubmitAddFunction;
  }

  /**
   * @param String $ajaxSubmitAddFunction
   */
  public function setAjaxSubmitAddFunction($ajaxSubmitAddFunction) {
    $this->ajaxSubmitAddFunction = $ajaxSubmitAddFunction;
  }

  /**
   * @return String
   */
  public function getAjaxSubmitDeleteFunction() {
    return $this->ajaxSubmitDeleteFunction;
  }

  /**
   * @param String $ajaxSubmitDeleteFunction
   */
  public function setAjaxSubmitDeleteFunction($ajaxSubmitDeleteFunction) {
    $this->ajaxSubmitDeleteFunction = $ajaxSubmitDeleteFunction;
  }

  /**
   * @return String
   */
  public function getAjaxSubmitHideFunction() {
    return $this->ajaxSubmitHideFunction;
  }

  /**
   * @param String $ajaxSubmitHideFunction
   */
  public function setAjaxSubmitHideFunction($ajaxSubmitHideFunction) {
    $this->ajaxSubmitHideFunction = $ajaxSubmitHideFunction;
  }

  /**
   * @return String
   */
  public function getSubmitPrintFunction() {
    return $this->submitPrintFunction;
  }

  /**
   * @param String $submitPrintFunction
   */
  public function setSubmitPrintFunction($submitPrintFunction) {
    $this->submitPrintFunction = $submitPrintFunction;
  }



}