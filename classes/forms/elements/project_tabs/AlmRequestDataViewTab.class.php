<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 08.03.2019
 * Time: 08:23
 */
class AlmRequestDataViewTab implements iAlmView {

  /**
   * @var ALMRequest
   */
  protected $requestObj;

  /**
   * @var string
   */
  protected $htmlRequestDataTab;

  /**
   * Object to creaete the views of the data elements
   * @var AlmRequestDataViews
   */
  protected $views;

  /**
   * AlmRequestDataTabView constructor.
   * @param ALMRequest $requestObj
   */
  public function __construct(ALMRequest $requestObj) {
    $this->requestObj = $requestObj;
    $this->views = new AlmRequestDataViews($requestObj);
  }


  public function getHtml() {
    $this->htmlRequestDataTab = '';
    //Latest Comment
    $this->createLatestCommentBox();
    // Researcher
    $this->createResearcherBox();
    // Imaging Goal and Scientific Question
    $this->createImagingGoalBox();
    // Sample Information
    $this->createSampleInformationBox();
    // Request support amd service
    $this->createRequestSupportBox();

    return $this->htmlRequestDataTab;
  }

  /**
   * Returns HTML for a formatted table with the latest submitted comment of
   * the request in a formatted alm_dashboard_box If the comment is emtpy or
   * there is no comment, it returns an emtpy string.
   *
   * @
   * @return string
   * @throws Exception
   */

  public function createLatestCommentBox() {
    $this->htmlRequestDataTab .= $this->views->getLatestCommentBox();
  }


  /**
   * Returns HTML for a formatted table with the researcher parameter in a
   * formatted alm_dashboard_box
   *
   * @return string
   * @throws Exception
   */

  public function createResearcherBox() {
    $this->htmlRequestDataTab .= $this->views->getResearcherBox();
  }

  /**
   * Returns HTML for a alm_dashboard_box with the Imaging Goal and Scientific
   * Question
   *
   * @return string
   * @throws Exception
   */
  function createImagingGoalBox() {
    $this->htmlRequestDataTab .= $this->views->getImagingGoalBox();
  }

  /**
   * Returns HTML for a alm_dashbord_box with the "sample information" in a
   * formatted table
   *
   * @return string
   * @throws Exception
   */
  function createSampleInformationBox() {
    $this->htmlRequestDataTab .= $this->views->getSampleInformationBox();
  }


  /**
   * Returns HTML for a alm_dashbord_box with the "reqeust support and service"
   * paramteres in a formatted table
   *
   * @return string
   * @throws Exception
   */
  function createRequestSupportBox() {
    $this->htmlRequestDataTab .= $this->views->getRequestSupportBox();
  }

}