<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 04.03.2019
 * Time: 11:39
 */
class AlmManagementConsultationViewTab extends AlmConsultationViewTab {
  /**
   * Overwrites super function.
   * Returns new StainingView object with stainings of the request and selected type.
   * Uses ManagerStainingsView
   * @param $stainingType
   * @return AlmManagementStainingsView
   */
  protected function getNewStainingViewObj($stainingType){
    $stainings = $this->loadStaingsOfType($stainingType);
    return new  AlmManagementStainingsView($stainings, $stainingType, $this->requestId);
  }

  /**
   * Overwrites super function.
   * Returns new SampleView object with Samples of the request-
   * Uses ManagerSampleView instead of normal SampleView
   * @return AlmManagementSamplesView
   */
  protected function getNewSampleViewObj(){
    $samples = $this->requestObj->loadAllSamples();
    return new AlmManagementSamplesView($this->requestId, $samples);
  }
}