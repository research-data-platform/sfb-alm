<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 20.05.2019
 * Time: 13:46
 */
class AlmManagementImageOverviewTab extends AlmImageOverviewTab {


  /**
   * AlmManagementImageOverviewTab constructor.
   */
  public function __construct($requestObj) {
    parent::__construct($requestObj);
  }

  /**
   * Checks if the user is permitted to upload images in the current request state
   * @return bool
   */
  protected function permitUploadButton() {
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT_IMAGES);
  }



    /**
   * Load the associated AlmDatasets and set the managerView parameter.
   * @return ALMDataset[]
   */
  protected function getAssociatedALMDatasets(){
    $datasets =  ALMDatasetRepository::findAllByRequestId($this->requestObj->getRequestID());
    foreach ($datasets as $dataset){
      $dataset->setManagerView(true);
    }
    return $datasets;
  }
}