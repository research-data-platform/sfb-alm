<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 08.05.2019
 * Time: 16:21
 */
class AlmImageOverviewTab implements iAlmView {

  /**
   * @var ALMRequest
   */
  protected $requestObj;


  /**
   * AlmImageOverview constructor.
   * @param $requestObj ALMRequest
   */
  public function __construct($requestObj) {
    $this->requestObj = $requestObj;
  }

  /**
   * Creates a new drupal Forms Fieldset that shows the associated Files of the
   * (local) request object and has two buttons to add new images. The buttons
   * call the function with the name of the $addFunctionName parameter
   *
   * @param $addFunctionName String Name of the function, which the "add image"
   *   Button should call.
   */
  public function getFormFieldset($addFunctionName) {
    $fieldsetImageOverview = [
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-edit"></i> ' . t('Images'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    // Create add button if an addFunctionName is available.
    if ($addFunctionName != NULL) {
      $fieldsetImageOverview['btn-add_image_container'] = [
        '#type' => 'submit',
        '#value' => t(' Add Image'),
        '#description' => t('Adds a new ALM image as ZIP archiv'),
        '#attributes' => [
          'class' => ['btn-default'],
          'style' => ['margin-bottom: 10px;',],
        ],
        '#submit' => [$addFunctionName],
      ];
    }

    $fieldsetImageOverview['image_table'] = [
      '#type' => 'markup',
      '#markup' => $this->getHTMLImageTable(),
    ];

  }

  /**
   * Returns html for an tab that shows a list of all images that are associated to the request
   * @return string
   */
  public function getHtml() {

    $upperAddButton = '';
    $bottomAddButton = '';

    $imageTable = $this->getHTMLImageTable();

    $request_status_blacklist = [
      ALMRequestState::Archived,
      ALMRequestState::Deleted,
      ALMRequestState::Finished,
      ALMRequestState::Withdraw,
    ];
    if ($this->permitUploadButton() && !in_array($this->requestObj->getRequestState(), $request_status_blacklist)) {
      $bottomAddButton = $this->getHtmlUploadButton();
    }

    $box_header = ['title' => ''];
    $box_body = ['data' => $upperAddButton . $imageTable . $bottomAddButton];
    $box_footer = ['data' => ''];
    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }

  private function getHTMLImageTable() {
    $datasets = $this->getAssociatedALMDatasets();
    $imageTable = ALMDataset::table($datasets);
    return $imageTable;
  }

  protected function getAssociatedALMDatasets(){
    return ALMDatasetRepository::findAllByRequestId($this->requestObj->getRequestID());
  }

  /**
   * Returns the html for a complete back button which links back to the overview
   * @return string
   */
  private function getHtmlUploadButton(){
    return '<a href="' . $this->getUploadButtonTargetURL()
      . '" class="btn btn-primary"> <span class="fa fa-upload"></span> Upload new image</a>';
  }

  /**
   * Checks if the user is permitted to upload images in the current request state
   * @return bool
   */
  protected function permitUploadButton(){
    return $this->requestObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_EDIT_IMAGES);
  }

  /**
   * Returns the target URL for the BackButton
   * @return string
   */
  protected function getUploadButtonTargetURL(){
    return sfb_alm_url(RDP_ALM_URL_REQUEST_DATASET_UPLOAD, $this->requestObj->getRequestID());
  }

}