<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 04.03.2019
 * Time: 10:59
 */
class AlmConsultationViewTab implements iAlmView {

  const CANCEL_SUBMIT_FUNCTION_NAME = 'rdp_alm_request_view_add_staining_cancel';
  const ADD_SUBMIT_FUNCTION_NAME = 'rdp_alm_request_view_add_staining_submit';

  /**
   * @var ALMRequest
   */
  protected $requestObj;

  /**
   * @var int
   */
  protected $requestId;

  /**
   * AlmRequestConsultationView constructor.
   * @param $request
   */
  public function __construct($request) {
    $this->requestObj = $request;
    $this->requestId=$this->requestObj->getRequestID();
  }

  /**
   * Returns the html of the complete consultation tab of a request
   * @return string
   */
  public function getHtml() {
    $viewTabConsultation = $this->consultationHeadBox();

    $viewTabConsultation .= $this->consultationGeneralBox();

    $viewTabConsultation .= $this->getNewStainingViewObj(ALMRequestStaining::TYPE_SINGLE)->getHtml();
    $viewTabConsultation .= $this->getNewStainingViewObj(ALMRequestStaining::TYPE_DUAL)->getHtml();
    $viewTabConsultation .= $this->getNewStainingViewObj(ALMRequestStaining::TYPE_MULTI)->getHtml();

    $viewTabConsultation .= $this->getNewSampleViewObj()->getHtml();

    return $viewTabConsultation;
  }

  /**
   * Returns new StainingView object with stainings of the request and selected type
   * @param $stainingType
   * @return AlmStainingsView
   */
  protected function getNewStainingViewObj($stainingType){
    $stainings = $this->loadStaingsOfType($stainingType);
    return new  AlmStainingsView($stainings, $stainingType, $this->requestId);
  }

  /**
   * Returns new SampleView object with Samples of the request
   * @return AlmSamplesView
   */
  protected function getNewSampleViewObj(){
    $samples = $this->requestObj->loadAllSamples();
    return new AlmSamplesView($this->requestId, $samples);
  }

  /**
   * Loads the stainings of the request with a selected staining type
   * @param $stainingType
   * @return ALMRequestStaining[]
   */
  protected function loadStaingsOfType($stainingType){
    switch ($stainingType){
      case ALMRequestStaining::TYPE_SINGLE:
        return $this->requestObj->loadAllSingleStainings();
        break;
      case ALMRequestStaining::TYPE_DUAL:
        return $this->requestObj->loadAllDualStainings();
        break;
      case ALMRequestStaining::TYPE_MULTI:
        return $this->requestObj->loadAllMultiStainings();
        break;
    }
  }


  /**
   * Returns HTML for a alm_dashbord_box with the consultation head parameteres
   * in a formatted table including fields:
   * - responsible s02 member
   * - attending (people)
   *
   * @return string
   */
  function consultationHeadBox() {
    // sets the headlines for the "head" theme table
    $table_header = [t(''), t('')]; //empty header -> the header are not visible

    // theme table rows
    $table_rows = [];

    // creates the rows for the "head" table
    $table_rows[] = [
      'data' => [
        [
          'data' => t('Responsible S02 member'),
          'style' => 'font-weight: bold; width: 30%;',
        ],
        t($this->requestObj->getConsultationResponsibleS02MemberName()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        ['data' => t('Attending'), 'style' => 'font-weight: bold;'],
        t($this->requestObj->getConsultationAttending()),
      ],
    ];

    // format the values of the "head" table as a formatted (theme) table
    $formattedTableConsultationHeader = theme('table', [
      'header' => $table_header,
      'rows' => $table_rows,
    ]);

    // Format the "head" formatted (theme) table in a alm_dashboard_box
    $box_header = ['title' => ''];
    $box_body = ['data' => $formattedTableConsultationHeader];
    $box_footer = ['data' => '']; // empty footer

    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }


  /**
   * Returns HTML for the general consultation fields as concatinated
   * alm_dashboard_boxes. Including fields:
   * - previous practical experiences
   * - Imaging support
   * - sample health
   * - Imaging Module
   * - Analysis Module
   *
   * @return string
   */

  function consultationGeneralBox() {
    $boxes = '';

    // alm_dashboard_box previous practical experiences
    $previousPracticalExperiences = $this->requestObj->getConsultationPreviousPracticalExperiences();

    $box_header = ['title' => '<i class="fa fa-lightbulb-o"></i> ' . t('Previous practical experiences in sample preparation and imaging')];
    $box_body = ['data' => isset($previousPracticalExperiences) ? nl2br($previousPracticalExperiences) : '-'];

    $box_footer = ['data' => ''];

    $boxes .= theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);

    // alm_dashboard_box Imaging support
    $imagingSupport = $this->requestObj->getConsultationImagingSupport();

    $box_header = ['title' => '<i class="fa fa-lightbulb-o"></i> ' . t('Imaging Support')];
    $box_body = ['data' => isset($imagingSupport) ? nl2br($imagingSupport) : '-'];
    $box_footer = ['data' => ''];

    $boxes .= theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);

    // alm_dashboard_box sample health
    $sampleHealth = $this->requestObj->getConsultationSampleHealth();

    $box_header = ['title' => '<i class="fa fa-lightbulb-o"></i> ' . t('Sample health')];
    $box_body = ['data' => isset($sampleHealth) ? nl2br($sampleHealth) : '-'];
    $box_footer = ['data' => ''];

    $boxes .= theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);

    // alm_dashboard_box Imaging Module
    $imagingModule = $this->requestObj->getConsultationImagingModule();

    $box_header = ['title' => '<i class="fa fa-lightbulb-o"></i> ' . t('Imaging Module(s)')];
    $box_body = ['data' => isset($imagingModule) ? nl2br($imagingModule) : '-'];
    $box_footer = ['data' => ''];

    $boxes .= theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);

    // alm_dashboard_box Analysis Module
    $analysisModule = $this->requestObj->getConsultationAnalysisModule();

    $box_header = ['title' => '<i class="fa fa-lightbulb-o"></i> ' . t('Analysis Module(s)')];
    $box_body = ['data' => isset($analysisModule) ? nl2br($analysisModule) : '-'];
    $box_footer = ['data' => ''];

    $boxes .= theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);

    return $boxes;
  }





}