<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 05.03.2019
 * Time: 23:35
 */
class AlmSamplesView implements iAlmView {

  /**
   * @var string
   */
  protected $htmlSampleView;
  /**
   * @var int
   */
  protected $requestId;
  /**
   * @var ALMRequestSample[]
   */
  protected $samples;


  /**
   * AlmSamplesView constructor.
   * @param $requestId int
   * @param $samples ALMRequestSample[]
   */
  public function __construct($requestId, array $samples) {
    $this->requestId = $requestId;
    $this->samples = $samples;
  }

  /**
   * Returns html string of a complete (alm_dashboard_box themed) view table with all samples containing to the request object.
   * Includs also button to add new samples if user is allowed to do.
   * @return string
   */
  public function getHtml() {
    $this->createHtmlSampleView();
    $add_new_button = $this->newSampleButton();

    // Format all the Samples in (one) alm_dashboard_box
    $box_header = ['title' => '<i class="fa fa-eyedropper" id="sample-field"></i> ' . t('Samples')];
    $box_body = ['data' => $add_new_button.$this->htmlSampleView];
    $box_footer = ['data' => '']; // empty footer


    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }

  /**
   * Create the html table of all samples in the htmlSampleView variable.
   */
  protected function createHtmlSampleView(){
    $this->htmlSampleView = '';
    foreach ($this->samples as $index => $sample) {
      // Add the Sample of this iteration as (theme) formatted table to the previous (formatted) samples
      $this->addSampleTable($index, $sample);
    }
  }

  /**
   * Creates a button to add new samples, if the current user is allowed to add samples to this request(used during running state)
   * @return string html
   */
  protected function newSampleButton(){
    if($this->isPermittedToAddSample()) {
      $add_new_button = '<a href="' . $this->addSampleUrl(). '" style="display: inline-block; float: right;"> <i class="fa fa-plus"></i> Add new</a></br>';
    } else{
      return '';
    }
    return $add_new_button;
  }

  /**
   * Check if the current user is permitted to add Samples.
   * @return boolean
   */
  protected function isPermittedToAddSample(){
    $requetObj = ALMRequestsRepository::findById($this->requestId);
    return $requetObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_ADD_SAMPLES);
  }

  /**
   * Returns the url of the page to add new samples.
   * @return string
   */
  protected function addSampleUrl(){
    return sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW_ADD_SAMPLE, $this->requestId);
  }

  /**
   * Adds a new sample table (of one sample) to the htmlSampleView variable
   * @param $index int
   * @param $sampleObj ALMRequestSample
   */
  protected function addSampleTable($index, $sampleObj){
    $sampleTable = new AlmSampleTableView($sampleObj);
    $sampleTable->setTableName($index+1);
    $this->htmlSampleView .= $sampleTable->getHtml();
  }


}