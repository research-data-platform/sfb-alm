<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 06.03.2019
 * Time: 14:51
 */
class AlmManagementSamplesView extends AlmSamplesView {

  /**
   * Overwrites the super function.
   * Check if the current user is permitted to add Samples in the Manager role instead of the user role.
   * @return boolean
   */
  protected function isPermittedToAddSample(){
    $requetObj = ALMRequestsRepository::findById($this->requestId);
    return $requetObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_ADD_SAMPLES);
  }

    /**
     * Overwrites the super function.
     * Returns the url of the page to add new samples.
     * @return string
     */
  protected function addSampleUrl(){
    return sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW_ADD_SAMPLE, $this->requestId);
  }
}