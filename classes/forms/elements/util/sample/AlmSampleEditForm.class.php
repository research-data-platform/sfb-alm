<?php

/**
 * User: chris
 * Date: 18.01.2019
 * Time: 18:09
 */
class AlmSampleEditForm extends AlmDOInputForm {
  /**
   * @var ALMRequestSample
   */
  private $sampleObj;
  /**
   * @var String
   */
  private $headline = 'Sample Documentation';
  /**
   * The default value that will be used if the sample has no serial
   * @var string
   */
  private $serial_default = 'Will be set automatically';

  private $removeBtnVisible = false;
  private $removeBtnSubmitFunction = '';

  private $copyBtnVisible = false;
  private $copyBtnSubmitFunction = '';

  /**
   * AlmStainingForm constructor.
   * @param $sample_obj
   */
  public function __construct($sample_obj) {
    $this->sampleObj = $sample_obj;
  }


  /**
   *
   * @param $sampleObj ALMRequestSample Sample Object in that the data from the form state should be merged
   * @param $form_state array The part of the form_state values that contains the data of the AlmSampleForm
   * @return ALMRequestSample
   */
  public static function mergeFormStateInDO(&$sampleObj, $form_state) {
    $sampleObj->setSpeciesDescription($form_state['species']['description']);
    $sampleObj->setSpeciesAbbreviation($form_state['species']['abbreviation']);
    $sampleObj->setSpeciesIdDescription($form_state['species_id']['description']);
    $sampleObj->setSpeciesIdAbbreviation($form_state['species_id']['abbreviation']);
    $sampleObj->setTypeDescription($form_state['type']['description']);
    $sampleObj->setTypeAbbreviation($form_state['type']['abbreviation']);
    $sampleObj->setGenotypeDescription($form_state['genotype']['description']);
    $sampleObj->setGenotypeAbbreviation($form_state['genotype']['abbreviation']);
    $sampleObj->setTreatmentDescription($form_state['treatment']['description']);
    $sampleObj->setTreatmentAbbreviation($form_state['treatment']['abbreviation']);
    $sampleObj->setStainingTypeDescription($form_state['staining_type']['description']);
    $sampleObj->setStainingTypeAbbreviation($form_state['staining_type']['abbreviation']);

    return $sampleObj;
  }

  /**
   * Creates a new AlmRequestSample object with the data of the form_state array
   * @param $form_state array The part of the form_state values that contains the data of the AlmSampleForm
   * @return ALMRequestSample
   */
  public static function createDOFromFormState($form_state) {
    $sampleObj = new ALMRequestSample();
    return self::mergeFormStateInDO($sampleObj, $form_state);
  }


  /**
   * Return a renderable array for one sample documentation
   * @param int $count
   * @return array
   */
  public function getForm() {
    // create the renderable array for one sample documentation
    $tableForm = array(
      '#theme' => 'alm_dashboard_editable_table_form_box',
      '#tree' => TRUE,
      '#title' => $this->headline,
      '#header' => array('', t('Description'), t('Abbreviation'),),
    );

    $tableForm['pre_table_form'] = $this->preTablePart();

    // creates the editable table that will be themed by theme alm_dashboard_editable_table_form_box.
    // The editable table consists of (drupal form) textfields, the first column is not editabel, so
    // it is markup instead of textfield
    // The table has 3 Columns (Name, description, abbreviation) and 9 rows

    // 2nd row (species)
    $tableForm['species'] = array(
      // first field
      'species' => array(
        '#type' => 'markup',
        '#markup' => 'Species',
      ),
      // second field
      'description' => array(
        '#type' => 'textfield',
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getSpeciesDescription() : '',
        '#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_SPECIES,
      ),
      // third field
      'abbreviation' => array(
        '#type' => 'textfield',
        '#description' => t('max. 2 character'),
        '#maxlength' => 2,
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getSpeciesAbbreviation() : '',
        '#width' => '120px',
        #'#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_SPECIES_ABBREVIATION,
      ),
    );
    // 3rd row (id)
    $tableForm['species_id'] = array(
      // first field
      'id' => array(
        '#type' => 'markup',
        '#markup' => 'Species ID',
      ),
      // second field
      'description' => array(
        '#type' => 'textfield',
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getSpeciesIdDescription() : '',
        '#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_ID,
      ),
      // third field
      'abbreviation' => array(
        '#type' => 'textfield',
        '#description' => t('max. 7 characters'),
        '#maxlength' => 7,
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getSpeciesIdAbbreviation() : '',
      ),
    );
    // 4th row (type)
    $tableForm['type'] = array(
      // first field
      'type' => array(
        '#type' => 'markup',
        '#markup' => 'Type',
      ),
      // second field
      'description' => array(
        '#type' => 'textfield',
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getTypeDescription() : '',
        '#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_TYPE,
      ),
      // third field
      'abbreviation' => array(
        '#type' => 'textfield',
        '#description' => t('max. 4 characters'),
        '#maxlength' => 4,
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getTypeAbbreviation() : '',
        #'#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_TYPE_ABBREVIATION,
      ),
    );
    // 5th row (genotype)
    $tableForm['genotype'] = array(
      // first field
      'genotype' => array(
        '#type' => 'markup',
        '#markup' => 'Genotype',
      ),
      // second field
      'description' => array(
        '#type' => 'textfield',
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getGenotypeDescription() : 'Pyrat or Tierbase name',
        '#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_GENOTYPE,
      ),
      // third field
      'abbreviation' => array(
        '#type' => 'textfield',
        '#description' => t('max. 6 characters'),
        '#maxlength' => 6,
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getGenotypeAbbreviation() : '',
        #'#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_GENOTYPE_ABBREVIATION,
      ),
    );
    // 6th row (treatment)
    $tableForm['treatment'] = array(
      // first field
      'treatment' => array(
        '#type' => 'markup',
        '#markup' => 'Treatment',
      ),
      // second field
      'description' => array(
        '#type' => 'textfield',
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getTreatmentDescription() : '',
        '#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_TREATMENT,
      ),
      // third field
      'abbreviation' => array(
        '#type' => 'textfield',
        '#description' => t('max. 4 characters'),
        '#maxlength' => 4,
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getTreatmentAbbreviation() : '',
        #'#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_TREATMENT_ABBREVIATION,
      ),
    );
    // 7th row (staining type)
    $tableForm['staining_type'] = array(
      // first field
      'staining_type' => array(
        '#type' => 'markup',
        '#markup' => 'Staining Type',
      ),
      // second field
      'description' => array(
        '#type' => 'textfield',
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getStainingTypeDescription() : '',
        '#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_STAININGTYPE,
      ),
      // third field
      'abbreviation' => array(
        '#type' => 'textfield',
        '#description' => t('max. 4 characters'),
        '#maxlength' => 4,
        '#default_value' => isset($this->sampleObj) ? $this->sampleObj->getStainingTypeAbbreviation() : '',
        #'#autocomplete_path' => SFB_ALM_API_AUTOCOMPLETE_SAMPLE_STAININGTYPE_ABBREVIATION,
      ),
    );

    return $tableForm;
    }

    private function preTablePart(){
      /**
       * Creates Remove and Copy button
       */
      //Split the page, left is the short label name and right the remove button
      $preTableForm['left_side'] = array('#markup' => '<div class="row"><div class="col-md-8">',);
      //creates the field for the serial number on the left side
      $serial = isset($this->sampleObj) ? $this->sampleObj->getFormattedSerial() : t($this->serial_default);
      $preTableForm['serial'] = array(
        '#markup' => '<p style="font-size:20px"><b>Serial: </b>' . $serial . '</p>',
      );
      $preTableForm['middle'] = array('#markup' => '</div><div class="col-md-4">',);
      
      if($this->removeBtnVisible) {
        // create the remove buttn
        $preTableForm['btn_' . $this->sampleObj->getId()] = array(
          '#type' => 'submit',
          '#value' => '<i class="fa fa-trash"></i> ',
          '#submit' => array($this->removeBtnSubmitFunction,),
          '#name' => $this->sampleObj->getId(),
          '#attributes' => array('style' => array('float: right'), 'onmouseover' => array('saveScrollPosition()',),),
          '#prefix' => '',
          '#suffix' => '',
        );
      }
      
      if($this->copyBtnVisible) {
        // create copy button
        $preTableForm['btn_copy' . $this->sampleObj->getId()] = array(
          '#type' => 'submit',
          '#value' => '<i class="fa fa-copy"></i> Copy ',
          '#description' => 'Add a new Sample Documentation by copying this Sample. The new Sample gets a new Serial',
          '#submit' => array($this->copyBtnSubmitFunction,),
          '#name' => $this->sampleObj->getId(),
          '#attributes' => array('style' => array('margin-right:5px; float: right'), 'onmouseover' => array('saveScrollPosition()',),),
          '#prefix' => '',
          '#suffix' => '</br></br>',
        );
      }

      $preTableForm['right_side'] = array('#markup' => '</div></div>',);

      return $preTableForm;
    }

  /**
   * Shows a remove btn which executes the given submit function for the sample
   * The remove btn has the sample id as name
   * @param $removeBtnSubmitFunction
   */
  public function addRemoveBtn($removeBtnSubmitFunction){
    $this->removeBtnVisible = true;
    $this->removeBtnSubmitFunction=$removeBtnSubmitFunction;
  }

  /**
   * Shows a copy btn which executes the given submit function for the sample
   * The copy btn has the sample id as name
   * @param $removeBtnSubmitFunction
   */
  public function addCopyBtn($removeBtnSubmitFunction){
    $this->copyBtnVisible = true;
    $this->copyBtnSubmitFunction=$removeBtnSubmitFunction;
  }

  /**
   * @return String
   */
  public function getHeadline() {
    return $this->headline;
  }

  /**
   * @param String $headline
   */
  public function setHeadline($headline) {
    $this->headline = $headline;
  }

  /**
   * @return bool
   */
  public function isRemoveBtnVisible() {
    return $this->removeBtnVisible;
  }

  /**
   * @param bool $removeBtnVisible
   */
  public function setRemoveBtnVisible($removeBtnVisible) {
    $this->removeBtnVisible = $removeBtnVisible;
  }

  /**
   * @return string
   */
  public function getRemoveBtnSubmitFunction() {
    return $this->removeBtnSubmitFunction;
  }

  /**
   * @param string $removeBtnSubmitFunction
   */
  public function setRemoveBtnSubmitFunction($removeBtnSubmitFunction) {
    $this->removeBtnSubmitFunction = $removeBtnSubmitFunction;
  }

  /**
   * @return bool
   */
  public function isCopyBtnVisible() {
    return $this->copyBtnVisible;
  }

  /**
   * @param bool $copyBtnVisible
   */
  public function setCopyBtnVisible($copyBtnVisible) {
    $this->copyBtnVisible = $copyBtnVisible;
  }

  /**
   * @return string
   */
  public function getCopyBtnSubmitFunction() {
    return $this->copyBtnSubmitFunction;
  }

  /**
   * @param string $copyBtnSubmitFunction
   */
  public function setCopyBtnSubmitFunction($copyBtnSubmitFunction) {
    $this->copyBtnSubmitFunction = $copyBtnSubmitFunction;
  }



}