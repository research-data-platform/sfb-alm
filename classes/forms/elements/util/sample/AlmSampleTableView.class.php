<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 05.03.2019
 * Time: 23:34
 */
class AlmSampleTableView implements iAlmView {
  /**
   * @var ALMRequestSample
   */
  private $sampleObj;
  /**
   * @var array
   */
  private $header;
  /**
   * @var array
   */
  private $rows;

  /**
   * AlmSampleTableView constructor.
   * @param $sampleObj ALMRequestSample
   */
  public function __construct($sampleObj) {
    $this->sampleObj = $sampleObj;
  }

  /**
   * Returns a (themed) html table for one Sample
   * @return string
   */
  public function getHtml() {
    $this->createRows();
    return theme('table', [
      'header' => $this->header,
      'rows' => $this->rows,
    ]);
  }


  /**
   * Creates default headers (unnamed)
   */
  function createDefaultHeaders(){
    $this->header = ['', t('Description'), t('Abbreviation'),];
  }

  /**
   * Set a table name to the table header
   * @param $name string
   */
  public function setTableName($name){
    $this->header[0]['data'] = t($name);
  }

  /**
   * Creates the rows which contains the parameter of the sample object.
   */
  function createRows(){
    // Sets the headlines for the columns for one sample

    //Create array with the values of all rows for one sample
    $this->rows = [
      // 1st row (Serial)
      [
        'data' => [
          [
            'data' => t(ALMRequestSample::getRowNames()[0]),
            'style' => 'width: 100px',
          ],
          ['data' => t($this->sampleObj->getSerialDescription()),],
          #'style' => ' width: 8%'),
          [
            'data' => t($this->sampleObj->getFormattedSerial()),
            'style' => ' width: 100px',
          ],
        ],
      ],
      // 2nd row (species)
      [
        'data' => [
          ['data' => t(ALMRequestSample::getRowNames()[1]),],
          ['data' => t($this->sampleObj->getSpeciesDescription()),],
          ['data' => t($this->sampleObj->getSpeciesAbbreviation()),],
        ],
      ],
      // 3rd row (ID)
      [
        'data' => [
          ['data' => t(ALMRequestSample::getRowNames()[2]),],
          ['data' => t($this->sampleObj->getSpeciesIdDescription()),],
          ['data' => t($this->sampleObj->getSpeciesIdAbbreviation()),],
        ],
      ],
      // 4th row (Type)
      [
        'data' => [
          ['data' => t(ALMRequestSample::getRowNames()[3]),],
          ['data' => t($this->sampleObj->getTypeDescription()),],
          ['data' => t($this->sampleObj->getTypeAbbreviation()),],
        ],
      ],
      // 5th row (Genotype)
      [
        'data' => [
          ['data' => t(ALMRequestSample::getRowNames()[4]),],
          ['data' => t($this->sampleObj->getGenotypeDescription()),],
          ['data' => t($this->sampleObj->getGenotypeAbbreviation()),],
        ],
      ],
      // 6th row (Treatment)
      [
        'data' => [
          ['data' => t(ALMRequestSample::getRowNames()[5]),],
          ['data' => t($this->sampleObj->getTreatmentDescription()),],
          ['data' => t($this->sampleObj->getTreatmentAbbreviation()),],
        ],
      ],
      // 7th row (Staining Type)
      [
        'data' => [
          ['data' => t(ALMRequestSample::getRowNames()[6]),],
          ['data' => t($this->sampleObj->getStainingTypeDescription()),],
          ['data' => t($this->sampleObj->getStainingTypeAbbreviation()),],
        ],
      ],
    ];
  }
}