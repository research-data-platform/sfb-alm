<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 22.11.2019
 * Time: 12:54
 */
class AlmSelectOptionsAdminView {
  /**
   * @var ALMSelectOptions
   */
  private $selectOptions;

  /**
   * AlmSelectOptionsAdminView constructor.
   * @param ALMSelectOptions $selectOptions
   */
  public function __construct(ALMSelectOptions $selectOptions) {
    $this->selectOptions = $selectOptions;
  }

  public function getViewTable(){
    $tableHeader = ['Name', 'Status', ''];
    $tableData = array();
    foreach ($this->selectOptions->getFullOptionArr() as $id => $selectOption) {
        $tableData[] = $this->getRow($id, $selectOption);
    }

    return theme('table', ['header' => $tableHeader, 'rows' => $tableData]);
  }

  private function getRow($id, $selectOption){
    $name = $selectOption['name'];
    $enabled = $selectOption['enabled'] ? t('Enabled') : t('Disabled');
    $editButton = l(t('Edit'), sfb_alm_url(RDP_ALM_URL_CONFIG_REQUEST_SETTINGS_EDIT_SELECT_OPTION, $this->selectOptions->getSelectOptionsFieldName(), $id));

    // glyphicon: <span class="glyphicon glyphicon-edit">
    return [$name, $enabled, $editButton];
  }

  public function getEditForm(){

  }

}