<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 03.03.2019
 * Time: 11:15
 */
class AlmStainingsView implements iAlmView {

  /**
   * @var
   */
  protected $stainingType;
  /**
   * @var string
   */
  protected $htmlStainingsView;
  /**
   * @var int
   */
  protected $requestId;
  /**
   * True if the staining array is empty
   * @var boolean
   */
  protected $empty;
  /**
   * @var ALMRequestStaining[]
   */
  protected $stainings;

  /**
   * AlmStainingsView constructor.
   * @param $stainingType
   * @param $requestId int
   * @param $stainings ALMRequestStaining[]
   */
  public function __construct(array $stainings, $stainingType, $requestId) {
    $this->stainingType = $stainingType;
    $this->requestId = $requestId;
    $this->stainings = $stainings;
  }

  /**
   * Returns html string of a complete (alm_dashboard_box themed) view table with all stainings of a type containing to the request object.
   * Includs also button to add new samples if user is allowed to do.
   * @return string
   */
  public function getHtml() {
    // Create a formatted (theme) table with all single stainings of this request
    $this->createHtmlStainingsView();
    $add_new_button = $this->newStainingButton();

    // Format the "Single IF" (theme) table in a alm_dashboard_box
    $box_header = ['title' => '<i class="fa fa-flask" id="staining-field-' . $this->stainingType .'"></i> ' . t($this->boxheadlineText())];
    $box_body = ['data' =>  $add_new_button . $this->htmlStainingsView];
    $box_footer = ['data' => '']; // empty footer

    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);

  }

  /**
   * Returns the Headline text of the stainings table. Considers the staining type
   * @return string
   */
  protected function boxheadlineText(){
    switch ($this->stainingType){
      case ALMRequestStaining::TYPE_SINGLE:
        return 'Single Immunofluorescence Stainings';
        break;
      case ALMRequestStaining::TYPE_DUAL:
        return 'Dual Immunofluorescence Stainings';
        break;
      case ALMRequestStaining::TYPE_MULTI:
        return 'Multi Immunofluorescence Stainings';
        break;
      default:
        return '';
    }
  }

  /**
   * Returns an "Add Staining" Button if the user is allowed to add stainings to the current request. Otherwise returns empty string.
   * @return string
   */
  protected function newStainingButton(){
    if($this->isPermittedToAddStainings()) {
      $addNewButton = '<a href="' . $this->addStainingUrl() . '" style="display: inline-block; float: right;"> <i class="fa fa-plus"></i> Add new</a>';
    } else{
      return '';
    }
    return $addNewButton;
  }

  /**
   * returns the url of the page to add a new staining to the requeest
   * @return string
   */
  protected function addStainingUrl(){
    return sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW_ADD_STAINING, $this->requestId, $this->stainingType);
  }

  /**
   * Check if the current user is permitted to add new stainings.
   * @return boolean
   */
  protected function isPermittedToAddStainings(){
    $requetObj = ALMRequestsRepository::findById($this->requestId);
    return $requetObj->permit(ALMRequest::ROLE_USER, ALMRequest::ACTION_ADD_STAININGS);
  }

  /**
   * Create the html table of all stainings in the htmlStainingsView variable.
   */
  protected function createHtmlStainingsView() {
    $this->htmlStainingsView = '';

    //create for every staining a (theme) table
    foreach ($this->stainings as $index => $staining) {
      $this->addLabelName($staining);
      $this->addStainingTable($index, $staining);
    }
  }

  /**
   * Add the label name to the stainingsview
   * @param $staining ALMRequestStaining
   */
  protected function addLabelName($staining){
    // Add the Label name as HTML code to the return string.
    $this->htmlStainingsView .= '<p></p><b>Short name for label:</b> ' . $staining->getLabelName() . '</p>';

  }

  /**
   * Add a (themed)  html table for the current staining to the Stainingsview
   * @param $index
   * @param $staining
   */
  protected function addStainingTable($index, $staining){
    // create the rows for one staining table
    $stainingTable = new AlmStainingTableView($staining);
    $stainingTable->setTableName($index+1);
    // Add the table of one staining as a formatted (theme) table to the return string.
    $this->htmlStainingsView .= $stainingTable->getHtml();
  }


}