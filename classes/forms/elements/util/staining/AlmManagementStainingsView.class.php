<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 05.03.2019
 * Time: 14:41
 */
class AlmManagementStainingsView extends AlmStainingsView {
  /**
   * Overwrites the super function.
   * Check if the current user is permitted to add stainings in the Manager role instead of the user role.
   * @return boolean
   */
  protected function isPermittedToAddStainings(){
    $requetObj = ALMRequestsRepository::findById($this->requestId);
    return $requetObj->permit(ALMRequest::ROLE_MANAGER, ALMRequest::ACTION_ADD_STAININGS);
  }

  /**
   * Overwrites the super function.
   * Returns the url of the page to add a new staining to the requeest
   * @return string
   */
  protected function addStainingUrl(){
    return sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW_ADD_STAINING, $this->requestId, $this->stainingType);
  }
}