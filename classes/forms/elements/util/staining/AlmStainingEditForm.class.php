<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 15.01.2019
 * Time: 20:54
 */
class AlmStainingEditForm extends AlmDOInputForm {
  /**
   * @var ALMRequestStaining
   */
  private $stainingObj;
  /**
   * @var String
   */
  private $headline = 'Alm Staining';

  private $removeBtnVisible = false;
  private $removeBtnSubmitFunction = '';

  /**
   * AlmStainingForm constructor.
   * @param $stainng_obj
   */
  public function __construct($stainng_obj) {
    $this->stainingObj = $stainng_obj;
  }


  /**
   * Merges the data of an staining input field into a given staining object
   * @param ALMRequestStaining $stainingObj ALMRequestStaining Staining Object in that the data from the form state should be merged
   * @param array $form_state
   * @return ALMRequestStaining
   */
  public static function mergeFormStateInDO(&$stainingObj, $form_state) {
    $stainingObj->setLabelName($form_state['pre_table_form']['short_label']);
    // Delete the pre Table form values from the view_staining_table array
    unset($form_state['pre_table_form']);

    // Merge antibody stainings  ---------------------
    $amountOfAntibodyRows = $stainingObj->getAmountOfAntibodyStainingRows();
    $antibodyRows = $stainingObj->getAntibodyDefinitions();

    for ($index = 0; $index < $amountOfAntibodyRows; $index++) {
      $antibodyRow = $form_state[$index];
      // The select2 field returns a associative array which contains the selected antibody id as key and value.
      $antibodyRows[$index]['primaryAntibodyId'] = self::extractAntibodyIdOrNull($antibodyRow['prim_anti']);
      $antibodyRows[$index]['primaryAntibodyDilution'] = $antibodyRow['prim_dilution'];
      $antibodyRows[$index]['secondaryAntibodyId'] = self::extractAntibodyIdOrNull($antibodyRow['sec_anti']);
      $antibodyRows[$index]['secondaryAntibodyDilution'] = $antibodyRow['sec_dilution'];
    }
    $stainingObj->setAntibodyDefinitions($antibodyRows);

    // Merge additional stainings  ---------------------
    $amountOfAdditionalStainingRows = $stainingObj->getAmountOfAdditionalStainingRows();
    $additionalStainingRows = $stainingObj->getAdditionalStainingDefinitions();

    for ($index = 0; $index < $amountOfAdditionalStainingRows; $index++) {
      // $index + $amount_of_antibody_staining_rows because the first rows where the anitbody stainings.
      $additionalStainingRow = $form_state[$index + $amountOfAntibodyRows];

      $additionalStainingRows[$index]['primaryAdditionalStaining'] = ($additionalStainingRow['prim_anti']);
      $additionalStainingRows[$index]['primaryAdditionalStainingDilution'] = $additionalStainingRow['prim_dilution'];
      $additionalStainingRows[$index]['secondaryAdditionalStaining'] = ($additionalStainingRow['sec_anti']);
      $additionalStainingRows[$index]['secondaryAdditionalStainingDilution'] = $additionalStainingRow['sec_dilution'];
    }
    $stainingObj->setAdditionalStainingDefinitions($additionalStainingRows);

    return $stainingObj;
  }

  /**
   * @param $antibodies
   *
   * returns ONE antibody id from a given antibody ID array
   * If the array does not contain an allowed antibody ID, returns null.
   * @return null or antibody id
   */
  private static function extractAntibodyIdOrNull($antibodies){
    foreach ($antibodies as $antibody){
      if(ALMAntibodyAdapter::existsAntibdyId($antibody))
        return $antibody;
    }
    return null;
  }

  /**
   * Creates a new ALMRequestStaining Object with the data of an staining input field
   * @param array $form_state
   * @return ALMRequestStaining
   */
  public static function createDOFromFormState($form_state) {
    $newStaining = new ALMRequestStaining();
    return self::mergeFormStateInDO($newStaining, $form_state);
  }

  /**
   * Shows a remove btn which executes the given submit function for the staining.
   * The remove btn has the staining id a s name
   * @param $removeBtnSubmitFunction
   */
  public function addRemoveBtn($removeBtnSubmitFunction){
    $this->removeBtnVisible = true;
    $this->removeBtnSubmitFunction=$removeBtnSubmitFunction;
  }

  /**
   * Return a renderable formField for an complete (ImmunoFluorescence) staining.
   * @return array
   */
  public function getForm()
  {
    //$stainingList = $this->requestObj->getAllMultiStainings();

    // create the renderable array for one multi IF staining
    $multiStainingTableForm = array(
      '#theme' => 'alm_dashboard_editable_table_form_box',
      '#tree' => TRUE,
      '#title' => $this->headline,
      '#header' => array('primary antibody', 'dilution', 'secondary antibody', 'dilution'),
    );

    $multiStainingTableForm['pre_table_form'] = $this->preTablePart();

    // creates the editable table that will be themed by theme alm_dashboard_editable_table_form_box.
    // The editable table consists of (drupal form) textfields and (antibody) select2 fields for the antibody-links

    //
    // A Single IF Staining has five rows: three antibody staining rows and two additional staining rows:
    //

    // antibody stainings:
    $antibodyDefinitions = $this->stainingObj->getAntibodyDefinitions();
    foreach ($antibodyDefinitions as $index=> $antibodyDefinition){
      $multiStainingTableForm[$index] = $this->tableRowWithAntibodyStaining($antibodyDefinition);
    }

    // additional stainings
    $additionalStainingDefintions = $this->stainingObj->getAdditionalStainingDefinitions();
    // needs the number of antibodies because the whole table is written in one array that should include first the
    // antibody stainings and then after that the additional stainings
    $amountOfAntibodyStainings = $this->stainingObj->getAmountOfAntibodyStainingRows();

    foreach ($additionalStainingDefintions as $index=>$additionalStainingDefinition){
      // uses $amoundOfAntibodyStainings as start index
      $multiStainingTableForm[$amountOfAntibodyStainings+$index] = $this->tableRowWithAdditionalStaining($additionalStainingDefinition);
  }

    return $multiStainingTableForm;
  }

  /**
   * Returns the pre table part, which includes the short label name and the remove btn (if visible)
   * @return mixed
   */
  private function preTablePart(){
    //Split the page, left is the short label name and right the remove button
    $preTableForm['left_side'] = array('#markup' => '<div class="row"><div class="col-md-8">',);
    //creates the textfield for the short label name before the table for the antibodys
    $preTableForm['short_label'] = $this->shortLabelNameField();
    $preTableForm['middle'] = array('#markup' => '</div><div class="col-md-4">',);


    // create the remove btn if it should be displayed
    if($this->removeBtnVisible) {
      $preTableForm['btn_' . $this->stainingObj->getId()] = $this->removeBtn();
    }
    $preTableForm['right_side'] = array('#markup' => '</div></div>',);

    return $preTableForm;
  }

  /**
   * Creates tha remove btn for the staining
   * @return array|string
   */
  private function removeBtn(){
    return array(
      '#type' => 'submit',
      '#value' => '<i class="fa fa-trash"></i> ',
      '#submit' => array($this->removeBtnSubmitFunction,),
      '#name' => $this->stainingObj->getId(),
      '#attributes' => array('style' => array('float: right'), 'onmouseover' => array('saveScrollPosition()',),),
    );
  }

  /**
   * Creates a renderable (drupal_form) array for a row for a editable (IF) Staining Table with antibody Stainings
   *
   * @param $antibodyDefinition array with index:
   *  primaryAntibodyId : Antibody ID
   *  primaryAntibodyDilution
   *  secondaryAntibodyId : Antibody ID
   *  secondaryAntibodyDilution
   *
   * @return array (renderable array)
   */
  private function tableRowWithAntibodyStaining($antibodyDefinition){
    $antibodySelectFieldOptions= array(
      'maximumSelectionLength' => '1',        // Only one antibody can be selected
      'title' => null,                            // No Title before the antibody (select-) field
      'style' => array('max-width: 100%;'),          // The antibody field use the whole (table) cell
    );

    $primAntibodyId = isset($antibodyDefinition['primaryAntibodyId']) ? $antibodyDefinition['primaryAntibodyId'] : '';
    $primDilution = isset($antibodyDefinition['primaryAntibodyDilution']) ? $antibodyDefinition['primaryAntibodyDilution'] : '';
    $secAntibodyId = isset($antibodyDefinition['secondaryAntibodyId']) ? $antibodyDefinition['secondaryAntibodyId'] : '';
    $secDilution = isset($antibodyDefinition['secondaryAntibodyDilution']) ? $antibodyDefinition['secondaryAntibodyDilution'] : '';

    return array(
      // first field  -- Antibody
      'prim_anti' => ALMAntibodyAdapter::createRenderablePrimaryAntibodySelect2Field($primAntibodyId, $antibodySelectFieldOptions),

      // second field  -- Dilution  (free-text)
      'prim_dilution' => array(
        '#type' => 'textfield',
        '#attributes' => array('placeholder' => t('1:xxx')),
        '#default_value' => $primDilution,
        '#width' => '10%',
      ),

      // third field -- Antibody
      'sec_anti' => ALMAntibodyAdapter::createRenderableSecondaryAntibodySelect2Field($secAntibodyId, $antibodySelectFieldOptions),

      // fourth field -- Dilution
      'sec_dilution' => array(
        '#type' => 'textfield',
        '#attributes' => array('placeholder' => t('1:xxx')),
        '#default_value' => $secDilution,
        '#width' => '10%',
      ),
    );
  }

  /**
   * Creates a renderable array for a row for a editable (IF) Staining Table with additional stainings
   * @param $additionalStainingDefinition array with index:
   *  primaryAdditionalStaining,
   *  primaryAdditionalStainingDilution,
   *  secondaryAdditionalStaining,
   *  secondaryAdditionalStainingDilution
   *
   * @return array (renderable array)
   */
  private function tableRowWithAdditionalStaining($additionalStainingDefinition){
    $primAdditionalStaining = $additionalStainingDefinition['primaryAdditionalStaining'];
    $primAdditionalStainingDilution = $additionalStainingDefinition['primaryAdditionalStainingDilution'];
    $secAdditionalStaining = $additionalStainingDefinition['secondaryAdditionalStaining'];
    $secAdditionalStainingDilution = $additionalStainingDefinition['secondaryAdditionalStainingDilution'];

    return array(
      // first field  -- Staining  (free-text)
      'prim_anti' => array(
        '#type' => 'textfield',
        '#attributes' => array('placeholder' => t('additional staining (e.g. DAPI) - company, order number #')),
        '#default_value' => $primAdditionalStaining,
      ),

      // second field   -- Dilution  (free-text)
      'prim_dilution' => array(
        '#type' => 'textfield',
        '#attributes' => array('placeholder' => t('1:xxx')),
        '#default_value' => $primAdditionalStainingDilution,
      ),

      // third field   -- Staining  (free-text)
      'sec_anti' => array(
        '#type' => 'textfield',
        '#attributes' => array('placeholder' => t('')),
        '#default_value' => $secAdditionalStaining,
      ),

      // fourth field   -- Dilution  (free-text)
      'sec_dilution' => array(
        '#type' => 'textfield',
        '#attributes' => array('placeholder' => t('1:xxx')),
        '#default_value' => $secAdditionalStainingDilution,
      ),
    );
  }

  /**
   * Return a renderable array, that contains a textfield for the short label name of a (immunofluorescecne) staining.
   * @param $staining ALMRequestStaining the associated stainng object
   * @return array
   */
  private function shortLabelNameField()
  {
    // return the rendable array for the textfield with the label of the selected if as default value
    return array(
      '#type' => 'textfield',
      '#title' => 'short name for label:',
      '#field_prefix' => '<div class="row"><div class="col-lg-4">',
      '#field_suffix' => '</div></div>',
      '#default_value' => (null !== $this->stainingObj->getLabelName()) ? $this->stainingObj->getLabelName() : '',
      '#required' => false, // it is not possible to use required because save, new sample etc. are submits too.
    );
  }

  /**
   * Getter & Setter
   */

  /**
   * @return bool
   */
  public function isRemoveBtnVisible() {
    return $this->removeBtnVisible;
  }

  /**
   * @param bool $removeBtnVisible
   */
  public function setRemoveBtnVisible($removeBtnVisible) {
    $this->removeBtnVisible = $removeBtnVisible;
  }

  /**
   * @return string
   */
  public function getRemoveBtnSubmitFunction() {
    return $this->removeBtnSubmitFunction;
  }

  /**
   * @param string $removeBtnSubmitFunction
   */
  public function setRemoveBtnSubmitFunction($removeBtnSubmitFunction) {
    $this->removeBtnSubmitFunction = $removeBtnSubmitFunction;
  }

  /**
   * @return String
   */
  public function getHeadline() {
    return $this->headline;
  }

  /**
   * @param String $headline
   */
  public function setHeadline($headline) {
    $this->headline = $headline;
  }


}