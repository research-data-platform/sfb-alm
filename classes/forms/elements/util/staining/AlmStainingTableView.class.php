<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 03.03.2019
 * Time: 12:57
 */
class AlmStainingTableView implements iAlmView {

  /**
   * @var ALMRequestStaining
   */
  private $stainingObj;
  /**
   * Sets the headlines for the "immunofluorescence staining table" theme table
   * @var array
   */
  private $header = [
    [
      'data' => 0,
      'style' => 'border-style: solid;border-color: black; border-width: 0px;',
    ],
    [
      'data' => ('primary antibody'),
      'style' => 'border-style: solid;border-color: black; border-width: 1px;',
    ],
    [
      'data' => ('dilution'),
      'style' => 'border-style: solid;border-color: black; border-width: 1px;',
    ],
    [
      'data' => ('secondary antibody'),
      'style' => 'border-style: solid;border-color: black; border-width: 1px;',
    ],
    [
      'data' => ('dilution'),
      'style' => 'border-style: solid;border-color: black; border-width: 1px;',
    ],
  ];

  /**
   * @var array
   */
  private $rows = array();

  /**
   * AlmStainingTableView constructor.
   * @param $stainingObj ALMRequestStaining
   */
  public function __construct($stainingObj) {
    $this->stainingObj = $stainingObj;
  }

  /**
   * Returns a (themed) html table for one Staining
   * @return string
   */
  public function getHtml() {
    $this->createAntibodyRows();
    $this->createAdditionalStainingRows();


    return theme('table', [
      'header' => $this->header,
      'rows' => $this->rows,
    ]);
  }

  /**
   * Set a table name to the table header
   * @param $name string
   */
  public function setTableName($name){
    $this->header[0]['data'] = t($name);
  }

  /**
   * Create the rows with the antibody stainings
   */
  private function createAntibodyRows(){
    // Antibody Staining Rows:
    $antibodyStainingRows = $this->stainingObj->getAntibodyDefinitions();

    foreach ($antibodyStainingRows as $antibodyStainingRow) {
      // values for the cells
      $primAnti = $this->createHTMLAntibodyViewCell($antibodyStainingRow['primaryAntibodyId']);
      $primDilution = isset($antibodyStainingRow['primaryAntibodyDilution']) ? $antibodyStainingRow['primaryAntibodyDilution'] : '-';
      $secAnti = $this->createHTMLAntibodyViewCell($antibodyStainingRow['secondaryAntibodyId']);
      $secDilution = isset($antibodyStainingRow['secondaryAntibodyDilution']) ? $antibodyStainingRow['secondaryAntibodyDilution'] : '-';

      // Add the values as a row to the row array
      $this->rows[] = [
        'data' => [
          ['data' => t(''), 'style' => 'width: 4%;'],
          [
            'data' => t($primAnti),
            'style' => 'width: 38%;border-style: solid;border-width: 1px;border-color: black;',
          ],
          [
            'data' => t($primDilution),
            'style' => ' width: 8%;border-style: solid;border-width: 1px;border-color: black;',
          ],
          [
            'data' => t($secAnti),
            'style' => ' width: 38%;border-style: solid;border-width: 1px;border-color: black;',
          ],
          [
            'data' => t($secDilution),
            'style' => ' width: 8%;border-style: solid;border-width: 1px;border-color: black;',
          ],
        ],
      ];
    }
  }

  /**
   * Create the rows with the additional staining rows
   */
  private function createAdditionalStainingRows(){
    // Additional Staining Rows:
    $additionalStainingRows = $this->stainingObj->getAdditionalStainingDefinitions();

    foreach ($additionalStainingRows as $additionalStainingRow) {

      $primAdditionalStaining = isset($additionalStainingRow['primaryAdditionalStaining']) ? $additionalStainingRow['primaryAdditionalStaining'] : '-';
      $primAdditionalStainingDilution = isset($additionalStainingRow['primaryAdditionalStainingDilution']) ? $additionalStainingRow['primaryAdditionalStainingDilution'] : '-';
      $secAdditionalStaining = isset($additionalStainingRow['secondaryAdditionalStaining']) ? $additionalStainingRow['secondaryAdditionalStaining'] : '-';
      $secAdditionalStainingDilution = isset($additionalStainingRow['secondaryAdditionalStainingDilution']) ? $additionalStainingRow['secondaryAdditionalStainingDilution'] : '-';

      $this->rows[] = [
        'data' => [
          ['data' => t(''), 'style' => 'width: 4%;'],
          [
            'data' => t($primAdditionalStaining),
            'style' => 'width: 38%;border-style: solid;border-width: 1px;border-color: black;',
          ],
          [
            'data' => t($primAdditionalStainingDilution),
            'style' => ' width: 8%;border-style: solid;border-width: 1px;border-color: black;',
          ],
          [
            'data' => t($secAdditionalStaining),
            'style' => ' width: 38%;border-style: solid;border-width: 1px;border-color: black;',
          ],
          [
            'data' => t($secAdditionalStainingDilution),
            'style' => ' width: 8%;border-style: solid;border-width: 1px;border-color: black;',
          ],
        ],
      ];
    }

  }

  /** Returns HTML antibody cell for one antibody (id)
   * @param $antibodyId
   * @return string
   */
  private function createHTMLAntibodyViewCell($antibodyId) {
    if (!isset($antibodyId)) {
      return '-';
    }

    try {
      $antibody = ALMAntibodyAdapter::getALMAntibodyObjectById($antibodyId);
      return $this->createHTMLshortAntibodyView($antibody);

    } catch (NoAntibodyFoundException $e) {
      drupal_set_message($e->getMessage(), 'warning');
      return 'Antibody not found!';
    }
  }

  /**
   * Creates Html AntibodyView as summary for one antibody
   * @param $antibody ALMAntibody
   * @return string
   */
  private function createHTMLshortAntibodyView($antibody) {
    $antigenHTML = '<b> Antigen Symbol: </b>' . $antibody->getAntigenSymbol();
    $nameHTML = '<b> Name: </b>' . $antibody->getName();
    // empty string if RaisedIn is not set
    $raisedInHTML = empty($antibody->getRaisedIn()) ? '' : '<b> Raised in: </b>' . $antibody->getRaisedIn();
    // empty string  if catalogNo is not set
    $catalogNoHTML = empty($antibody->getCatalogNo()) ? '' : '<b>Catalog No.: </b>' . $antibody->getCatalogNo();
    $antibodyLinkHTML = $antibody->getViewBadge();

    $shortAntibodyHTML =
      // line 1
      $antigenHTML . ' ' . $nameHTML . '<br>' .
      // line 2
      $raisedInHTML . ' ' . $catalogNoHTML . '<br>' .
      // line 3
      $antibodyLinkHTML;

    return $shortAntibodyHTML;
  }

}