<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 08.03.2019
 * Time: 11:42
 */
class AlmRequestDataViews {

  /**
   * @var ALMRequest
   */
  protected $requestObj;

  /**
   * AlmRequestDataViews constructor.
   * @param ALMRequest $requestObj
   */
  public function __construct(ALMRequest $requestObj) {
    $this->requestObj = $requestObj;
  }

  /**
   * Returns HTML for a formatted table with the latest submitted comment of
   * the request in a formatted alm_dashboard_box If the comment is emtpy or
   * there is no comment, it returns an emtpy string.
   *
   * @
   * @return string
   * @throws Exception
   */

  public function getLatestCommentBox() {
    // Load the latest comment from this request, with a commenttype, which is smaller than the SubmitEdit Comment type(@see ALMCommentType::SubmitEdit)
    // Use SubmitEdit as SmallerThanCommentType because it is the lowest Edit type, so the comment can not be an editable comment
    $comment = ALMCommentRepository::findLatestCommentByRequestIDAndSmallerThanCommentType($this->requestObj->getRequestID(), ALMCommentType::SubmitEdit);

    // If there is no comment, return empty HTML string, so in the output the complete field is not visible
    if ($comment == NULL || $comment->getMessage() == "") {
      return '';
    }
    // sets empty headlines for the "latest comment" table -> no headlines are visible
    $table_header = ['', '', ''];

    // because the comment has only one row, this creates the columns for the "latest comment" table
    $table_rows = [];
    $table_rows[] = [
      'data' => [
        // Date, when the comment was submitted
        ['data' => t($comment->getDate()), 'style' => 'width: 10%;'],
        // Type of the comment
        ['data' => t($comment->getTypeText()), 'style' => 'width: 10%;'],
        // The message of the comment
        t($comment->getMessage()),
      ],
    ];

    // format the values of the "latest comment" table as a formatted table
    $formatted_table_researcher = theme('table', [
      'header' => $table_header,
      'rows' => $table_rows,
    ]);

    // Format the "latest comment" table in a alm_dashboard_box
    $box_header = ['title' => '<i class="fa fa-user"></i> ' . t('Latest Comment')];
    $box_body = ['data' => $formatted_table_researcher];
    $box_footer = ['data' => '']; // empty footer

    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }


  /**
   * Returns HTML for a formatted table with the researcher parameter in a
   * formatted alm_dashboard_box
   *
   * @return string
   * @throws Exception
   */

  public function getResearcherBox() {
    // sets the headlines for the "researcher" table
    $table_header = [t(''), t('')];

    // creates the rows for the "researcher" table
    $table_rows = [];
    $table_rows[] = [
      'data' => [
        ['data' => 'State', 'style' => 'font-weight: bold; width: 30%;'],
        ALMRequestState::getRequestStateIcon($this->requestObj->getRequestState()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        ['data' => 'User name', 'style' => 'font-weight: bold;'],
        t($this->requestObj->getUserAccountName()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        ['data' => 'Name', 'style' => 'font-weight: bold;'],
        t($this->requestObj->getUserFullname()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        ['data' => 'Position', 'style' => 'font-weight: bold;'],
        t($this->requestObj->getPosition()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        ['data' => 'Institute', 'style' => 'font-weight: bold;'],
        t($this->requestObj->getInstitute()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        ['data' => 'Research Group', 'style' => 'font-weight: bold;'],
        t($this->requestObj->getResearchGroupName()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        ['data' => 'Project', 'style' => 'font-weight: bold;'],
        t($this->requestObj->getSubProjectName()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        ['data' => 'E-mail', 'style' => 'font-weight: bold;'],
        t($this->requestObj->getEmail()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        ['data' => 'Phone', 'style' => 'font-weight: bold;'],
        t($this->requestObj->getPhoneNumber()),
      ],
    ];

    // format the values of the "researcher" table as a formatted table
    $formatted_table_researcher = theme('table', [
      'header' => $table_header,
      'rows' => $table_rows,
    ]);

    // Format the formatted "researcher" table in a alm_dashboard_box
    $box_header = ['title' => '<i class="fa fa-user"></i> ' . t('Researcher')];
    $box_body = ['data' => $formatted_table_researcher];
    $box_footer = ['data' => '']; // empty footer

    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }

  /**
   * Returns HTML for a alm_dashboard_box with the Imaging Goal and Scientific
   * Question
   *
   * @return string
   * @throws Exception
   */
  function getImagingGoalBox() {
    // because of checking the "isset", load imagingGoal in local variable
    $goal = $this->requestObj->getImagingGoal();

    // Format the "imaging goal" as a alm_dashboard_box
    $box_header = ['title' => '<i class="fa fa-lightbulb-o"></i> ' . t('Imaging Goal and Scientific Question')];
    $box_body = ['data' => isset($goal) ? $goal : '-'];
    $box_footer = ['data' => ''];

    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }

  /**
   * Returns HTML for a alm_dashbord_box with the "sample information" in a
   * formatted table
   *
   * @return string
   * @throws Exception
   */
  public function getSampleInformationBox() {
    // sets the headlines for the "sample information" table
    $table_header = [t(''), t('')];

    // creates the rows for the "sample information" table
    $table_rows = [];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_SAMPLE_TYPE),
          'style' => 'font-weight: bold; width: 30%;',
        ],
        t($this->requestObj->getSampleType()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_TARGET_STRUCTURE),
          'style' => 'font-weight: bold;',
        ],
        t($this->requestObj->getTargetStructure()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_CONDITIONS),
          'style' => 'font-weight: bold;',
        ],
        t($this->requestObj->getConditions()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_STAINING_METHOD),
          'style' => 'font-weight: bold;',
        ],
        t($this->requestObj->getStainingMethod()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_DATA_REQUIRED),
          'style' => 'font-weight: bold;',
        ],
        t(nl2br($this->requestObj->getDataRequired())),
      ],
    ];

    // format the values for the "sample information" table as a formatted table
    $formatted_table_sample_information = theme('table', [
      'header' => $table_header,
      'rows' => $table_rows,
    ]);

    // Format the "sample information" table in a alm_dashboard_box
    $box_header = ['title' => '<i class="fa fa-info"></i> ' . t('Sample Information')];
    $box_body = ['data' => $formatted_table_sample_information];
    $box_footer = ['data' => '']; // empty footer

    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }


  /**
   * Returns HTML for a alm_dashbord_box with the "reqeust support and service"
   * paramteres in a formatted table
   *
   * @return string
   * @throws Exception
   */
  public function getRequestSupportBox() {
    // sets the headlines for the "request support and service" table
    $table_header = [t(''), t('')];

    // creates the rows for the "request support and service" table
    $table_rows = [];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_IMAGING_SUPPORT),
          'style' => 'font-weight: bold; width: 30%;',
        ],
        t($this->requestObj->getImagingSupportTextAsMultiLineString()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_SAMPLE_HEALTH),
          'style' => 'font-weight: bold;',
        ],
        t($this->requestObj->getSampleHealthText()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_IMAGING_MODULE),
          'style' => 'font-weight: bold;',
        ],
        t($this->requestObj->getImagingModuleTextAsMultiLineString()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_ANALYSIS_MODULE),
          'style' => 'font-weight: bold;',
        ],
        t($this->requestObj->getAnalysisModuleTextAsMultiLineString()),
      ],
    ];
    $table_rows[] = [
      'data' => [
        [
          'data' => t(SFB_ALM_REQUEST_ITEMTITLE_INTRODUCTION_TO_ALM),
          'style' => 'font-weight: bold;',
        ],
        t($this->requestObj->getIntroductionToAlmTextAsMultiLineString()),
      ],
    ];
    // format the values for the "request support and service" table as a formatted table
    $formatted_table_requested_support_and_service = theme('table', [
      'header' => $table_header,
      'rows' => $table_rows,
    ]);

    // Format the "request support and service" table in a alm_dashboard_box
    $box_header = ['title' => '<i class="fa fa-edit"></i> ' . t('Requested Support and Service')];
    $box_body = ['data' => $formatted_table_requested_support_and_service];
    $box_footer = ['data' => '']; // empty footer

    return theme('alm_dashboard_box', [
      'header' => $box_header,
      'body' => $box_body,
      'footer' => $box_footer,
    ]);
  }
}