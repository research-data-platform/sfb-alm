<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 11.01.2019
 * Time: 23:49
 */
class ConfirmDialog {

  protected $dialogId;
  protected $btnId;
  protected $body = 'Are you sure, you want to continue?';
  protected $answTextYes = 'Yes';
  protected $answTestNo = 'Cancel';
  protected $title = 'Confirm';

  /**
   * ConfirmDialog constructor.
   */
  public function __construct($btnId = 'anonymous') {
    $this->btnId = $btnId;
    $this->dialogId = $btnId . '-conirm-dialog';
  }

  /**
   * Defines the form of the popup dialog
   * @return string
   */
  protected function createDialog() {

    return '
    <div class="modal fade" id="' . $this->dialogId . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">' . $this->title .'</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"></span>
            </button>
          </div>
          <div class="modal-body">
            ' . $this->body .'
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" id="modal-btn-close" data-dismiss="modal">' . $this->answTestNo .'</button>
            <button type="button" class="btn btn-primary" id="modal-btn-save">' . $this->answTextYes .'</button>
          </div>
        </div>
      </div>
    </div>
    
    <!-- css to show the dialog in the center-->
    <style>
    .modal-content {
    width:100%;
    }
    .modal-dialog-centered {
      display:-webkit-box;
      display:-ms-flexbox;
      display:flex;
      -webkit-box-align:center;
      -ms-flex-align:center;
      align-items:center;
      min-height:calc(100% - (.5rem * 2));
    }

    @media (min-width: 576px) {
      .modal-dialog-centered {
        min-height:calc(100% - (1.75rem * 2));
      }
    }
    </style>
    ';
  }

  /**
   * Creates the functionality for the confirm dialoag. Contains jQuery in a <script> tag.
   * @return string
   */
  protected function createJQuery() {
    return '
  <script type="text/javascript">
    var blockSubmit = true;
    var submitBtn;

    // Add onclick functions to submit-button and modal dialog buttons
    jQuery(document).ready(function(){
        
        // Prevent the normal (drupal) submit function
        jQuery("#' . $this->btnId . '").on(\'click\', function(e){
            if (blockSubmit) {
            e.preventDefault();
        }
   
        jQuery("#' . $this->dialogId . '").modal(); 
        submitBtn = this;
    })
    
    jQuery("#modal-btn-save").on(\'click\', function(e){
        doSubmit();    
    })
    jQuery("#modal-btn-close").on(\'click\', function(e){
        jQuery("#' . $this->dialogId . '").modal(\'hide\')  
    })
  });
  
  // Runs the drupal submit function
  function doSubmit() {
      blockSubmit=false;
      submitBtn.click();
  }
  </script>
  ';
  }

  /**
   *
   * Returns html for a confirm dialog including all jquery for the functionality.
   * The dialog will be shown when the button with $btnId is clicked.
   * The submit function of the button is blocked until the confirm dialog is confirmed
   * @return string
   */
  public function getDialogAsHtml() {
    return $this->createDialog() . $this->createJQuery();
  }

  /**
   * Set the body of the dialog. This can be html
   * @param string $body
   */
  public function setBody($body) {
    $this->body = $body;
  }

  /**
   * Set the text of the "yes" button in the dialog
   * @param string $answTextYes
   */
  public function setAnswTextYes($answTextYes) {
    $this->answTextYes = $answTextYes;
  }

  /**
   * Set the text of the "no" button in the dialog
   * @param string $answTestNo
   */
  public function setAnswTestNo($answTestNo) {
    $this->answTestNo = $answTestNo;
  }

  /**
   * Set the text of the title button
   * @param string $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }



}