<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 13.01.2019
 * Time: 22:26
 */
class RequestBlockedDialog extends ConfirmDialog {

  protected function createJQuery(){
    return '
    <script type="text/javascript">

    // Add onclick functions to submit-button and modal dialog buttons
    jQuery(document).ready(function(){
        jQuery("#' . $this->dialogId . '").modal({keyboard: false, backdrop:false}); 
    })
    
    jQuery("#modal-btn-save").on(\'click\', function(e){
        addParameterToURL("unlock", "true");    
    })
    jQuery("#modal-btn-close").on(\'click\', function(e){
        history.back();
    })
  
  </script>
  ';
  }

}