<?php

/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 07.07.2016
 * Time: 12:57
 */
class ALMRequestsRepository
{
  /**
   * Array with database fields.
   *
   * @var array
   */
  static $databaseFields = array(
      'id' => 'id',
      'userid' => 'userid',
      'position' => 'position',
      'institut' => 'institut',
      'researchgroup_id' => 'workinggroup_id',
      'subproject' => 'subproject_id',
      'email' => 'email',
      'phone' => 'phone',
      'imaging_goal' => 'imaging_goal',
      'sample_type' => 'sample_type',
      'target_structure' => 'target_structure',
      'conditions' => 'conditions',
      'staining_method' => 'staining_method',
      'data_required' => 'data_required',
      'imaging_support' => 'imaging_support',
      'sample_health' => 'sample_health',
      'imaging_module' => 'imaging_module',
      'analysis_module' => 'analysis_module',
      'introduction_to_alm' => 'introduction_to_alm',

      'consultationAttending' => 'consultation_attending',
      'consultationPrevPracticalExperiences' => 'consultation_prev_practical_experiences',
      'consultationResponsibleS02Member' => 'consultation_responsible_s02_member',
      'consultationImagingSupport' => 'consultation_imaging_support',
      'consultationImagingModule' => 'consultation_imaging_module',
      'consultationSampleHealth' => 'consultation_sample_health',
      'consultationAnalysisModule' => 'consultation_analysis_module',

      'request_state' => 'request_state',
      'lockedBy' => 'locked_by',
  );

  const TABLE_NAME = 'alm_requests';




  /**
   * Returns the last ID that insterted in the Database.
   *
   * @param $request ALMRequest
   * @return string last ID that insterted in the Database.
   *
   * @throws Exception
   * @throws InvalidMergeQueryException
   */
  public static function save($request) {
    $id = NULL;

    if ($request->getRequestID() != ALMRequest::EMPTY_REQUEST_ID)
      $id = $request->getRequestID();

    db_merge('alm_requests')->key(array('id' => $id))
        ->fields(array(
          //TODO: USER to Database, FOREIGN key

          //self::$databaseFields['id'] => self::setEmptyVariablesToNull($request->getRequestID()),
            self::$databaseFields['position'] => self::setEmptyVariablesToNull($request->getPosition()),
            self::$databaseFields['userid'] => self::setEmptyVariablesToNull($request->getUserID()),
            self::$databaseFields['institut'] => self::setEmptyVariablesToNull($request->getInstitute()),
            self::$databaseFields['researchgroup_id'] => self::setEmptyVariablesToNull($request->getResearchGroupID()),
            self::$databaseFields['subproject'] => self::setEmptyVariablesToNull($request->getSubprojectID()),
            self::$databaseFields['email'] => self::setEmptyVariablesToNull($request->getEmail()),
            self::$databaseFields['phone'] => self::setEmptyVariablesToNull($request->getPhoneNumber()),
            self::$databaseFields['imaging_goal'] => self::setEmptyVariablesToNull($request->getImagingGoal()),
            self::$databaseFields['sample_type'] => self::setEmptyVariablesToNull($request->getSampleType()),
            self::$databaseFields['target_structure'] => self::setEmptyVariablesToNull($request->getTargetStructure()),
            self::$databaseFields['conditions'] => self::setEmptyVariablesToNull($request->getConditions()),
            self::$databaseFields['staining_method'] => self::setEmptyVariablesToNull($request->getStainingMethod()),
            self::$databaseFields['data_required'] => self::setEmptyVariablesToNull($request->getDataRequired()),
            self::$databaseFields['imaging_support'] => implode(';', $request->getImagingSupportIDs()), // convert array to string for database
            self::$databaseFields['sample_health'] => self::setEmptyVariablesToNull($request->getSampleHealthID()),
            self::$databaseFields['imaging_module'] => implode(';', $request->getImagingModuleIDs()),   // convert array to string for database
            self::$databaseFields['analysis_module'] => implode(';', $request->getAnalysisModuleIDs()),   // convert array to string for database
            self::$databaseFields['introduction_to_alm'] => implode(';', $request->getIntroductionToAlmIDs()),    // convert array to string for database


            self::$databaseFields['consultationResponsibleS02Member'] => self::setEmptyVariablesToNull($request->getConsultationResponsibleS02MemberID()),
            self::$databaseFields['consultationAttending'] => self::setEmptyVariablesToNull($request->getConsultationAttending()),
            self::$databaseFields['consultationPrevPracticalExperiences'] => self::setEmptyVariablesToNull($request->getConsultationPreviousPracticalExperiences()),
            self::$databaseFields['consultationImagingSupport'] => self::setEmptyVariablesToNull($request->getConsultationImagingSupport()),
            self::$databaseFields['consultationImagingModule'] => self::setEmptyVariablesToNull($request->getConsultationImagingModule()),
            self::$databaseFields['consultationSampleHealth'] => self::setEmptyVariablesToNull($request->getConsultationSampleHealth()),
            self::$databaseFields['consultationAnalysisModule'] => self::setEmptyVariablesToNull($request->getConsultationAnalysisModule()),

            self::$databaseFields['request_state'] => self::setEmptyVariablesToNull($request->getRequestState()),
            self::$databaseFields['lockedBy'] => self::setEmptyVariablesToNull($request->getLockedBy()),

        ))->execute();


    return Database::getConnection()->lastInsertId();
  }

  /**
   * Prüft ob eine Variable einen Inhalt hat. Wenn die Variable '' oder ALM_FORM_SELECT_EMPTY ist, wird sie null gesetzt.
   *
   * @param $input
   * @return null
   */
  private static function setEmptyVariablesToNull($input) {
    if ($input == ALM_FORM_SELECT_EMPTY || $input == '') {
      return NULL;
    } else {
      return $input;
    }
  }

  /**
   * @param $id
   * @return ALMRequest
   *
   */
  public static function findById($id) {
    $result = db_select(self::TABLE_NAME, 'r')
        ->condition('r.' . self::$databaseFields['id'], $id, '=')
        ->fields('r', self::$databaseFields)
        ->range(0, 1)
        ->execute()
        ->fetch();

    return self::databaseResultsToALMRequest($result);
  }

  /**
   * Return all requests of a given user.
   * Separates the requests in pages with $limit requests per pages
   * Order the requests firstly by request states and secondly by the last change date (the last existing log entry)
   *
   * @param $userid
   * @return ALMRequest[]
   */
  public static function findAllCurrentRequestsByUserIDAddPageLimit($userid, $limit = 25) {
    $query = db_select(self::TABLE_NAME, 'r');
    // join with Log Database over request id
    $query->join(ALMLogRepository::DATABASE_NAME, 'l', 'r.id = l.request_id');
    $query
        //->condition('l.action', 'Submit request', '=') //WHERE l.action = Submit request
        ->condition('r.' . self::$databaseFields['userid'], $userid, '=')
        ->condition('r.' . self::$databaseFields['request_state'], ALMRequestState::Deleted, '!=')
        ->fields('r', self::$databaseFields);
    $query
        ->groupBy('l.request_id')
        ->orderBy(self::$databaseFields['request_state'], 'ASC')
        ->orderBy('MAX(l.date)', 'DESC');         //ORDER BY MAX(l.date)

    $result = $query->extend('PagerDefault')
        ->limit($limit)
        ->execute();
    return ALMRequestsRepository::databaseResultsToALMRequests($result);
  }

  /**
   * Returns users requests with a given state
   *
   * @param ALMRequestState $requestState
   * @param Integer $userId
   * @return \ALMRequest[]
   */
  public static function findAllRequestsByUserIdAndState($userId, $requestState) {
    $result = db_select(self::TABLE_NAME, 'r')
        ->condition('r.' . self::$databaseFields['request_state'], $requestState, '=')
        ->condition('r.' . self::$databaseFields['userid'], $userId, '=')
        ->fields('r', self::$databaseFields)
        ->execute();

    return ALMRequestsRepository::databaseResultsToALMRequests($result);
  }

  /**
   * Returns array of ALMRequest Objects of all requests with the given state.
   * @param ALMRequestState $requestState
   * @return ALMRequest[]
   */
  public static function findAllRequestsByState($requestState){
    $result = db_select(self::TABLE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['request_state'], $requestState, '=')
      ->fields('r', self::$databaseFields)
      ->execute();

    $requestsArray = ALMRequestsRepository::databaseResultsToALMRequests($result);
    return $requestsArray;
  }

  /**
   * Returns users requests with a given state
   * Seperates the requests in pages with $limit requests per pages
   *
   * @param ALMRequestState $requestState
   * @param Integer $userId
   * @return \ALMRequest[]
   */
  public static function findAllRequestsByUserIdAndStateAddPageLimit($userId, $requestState, /*$header, */
                                                                     $limit = 25) {
    $query = db_select(self::TABLE_NAME, 'r')
        //->extend('TableSort')
        ->condition('r.' . self::$databaseFields['request_state'], $requestState, '=')
        ->condition('r.' . self::$databaseFields['userid'], $userId, '=')
        ->fields('r', self::$databaseFields);
    $result = $query
      ->extend('PagerDefault')
      ->limit($limit)
        //->orderByHeader($header)
      ->execute();

    return ALMRequestsRepository::databaseResultsToALMRequests($result);
  }

  /**
   * @return ALMRequest[]
   */
  public static function findAllCurrentRequests() {
    $query = db_select(self::TABLE_NAME, 'r');
    $query->join(ALMLogRepository::DATABASE_NAME, 'l', 'r.id = l.request_id');
    $query
        ->condition('l.' . ALMLogRepository::$databaseFields['action'], ALMLog::$ACTION_SUBMIT, '=')// WHERE l.action = Submit request
        ->condition('r.' . self::$databaseFields['request_state'], ALMRequestState::Deleted, '!=')
        ->fields('r', self::$databaseFields);
    $query
        ->groupBy('r.' . self::$databaseFields['id'])
        ->orderBy('MAX(l.' . ALMLogRepository::$databaseFields['date'] . ')', 'DESC');  // ORDER BY MAX(l.date)

    $result = $query->execute();

    return ALMRequestsRepository::databaseResultsToALMRequests($result);
  }

  /**
 * Returns request array, except that who have the states deleted,edit or withdraw. The array is sorted by the request_state and the submitted_date
 *
 * @return ALMRequest[]
 */
  public static function findAllManageableRequestsAddPageLimit($limit) {
    /**
     * SELECT r.*
     * FROM alm_requests r, alm_request_log l
     * WHERE l.action = "Submit request" AND r.id = l.request_id AND ...
     * GROUP BY request_id
     * ORDER BY r.request_state DESC, MAX(l.date) ASC;
     */
    $query = db_select(self::TABLE_NAME, 'r');
    $query->join(ALMLogRepository::DATABASE_NAME, 'l', 'r.' . self::$databaseFields['id'] . ' = l.' . ALMLogRepository::$databaseFields['request_id']);
    $query
      ->condition('l.' . ALMLogRepository::$databaseFields['action'], ALMLog::$ACTION_SUBMIT, '=')//WHERE l.action = Submit request , Da am Ende nach (submitted) dates sortiert wird, dürfen nur die submitted dates beachtet werden
      ->condition('r.' . self::$databaseFields['request_state'], ALMRequestState::Deleted, '!=')
      ->condition('r.' . self::$databaseFields['request_state'], ALMRequestState::Edit, '!=')
      ->condition('r.' . self::$databaseFields['request_state'], ALMRequestState::Withdraw, '!=')
      ->condition('r.' . self::$databaseFields['request_state'], ALMRequestState::Revision, '!=')
      ->fields('r', self::$databaseFields);
    $query
      ->groupBy('r.' . self::$databaseFields['id'])
      ->orderBy(self::$databaseFields['request_state'], 'ASC')
      ->orderBy('MAX(l.' . ALMLogRepository::$databaseFields['date'] . ')', 'DESC'); //ORDER BY MAX(l.date) , max because it is possible that one request has more than one submitted date
    $result = $query
      ->extend('PagerDefault')
      ->limit($limit)
      //->orderByHeader($header)
      ->execute();

    $requests = ALMRequestsRepository::databaseResultsToALMRequests($result);
    return $requests;
  }

  /**
   * Returns request array, with all manageable requests with a given request state. The array is sorted by the submitted_date
   *
   * @return ALMRequest[]
   */
  public static function findManageableRequestsByRequestStateAddPageLimit($requestState, $limit) {
    // Check if requestState is not manageable
    /*
    if(ALMRequestState::Deleted == $requestState|| 
      ALMRequestState::Edit == $requestState || 
      ALMRequestState::Withdraw == $requestState || 
      ALMRequestState::Revision == $requestState || 
      ALMRequestState::Archived == $requestState || 
      ALMRequestState::Finished == $requestState ){
      return ALMRequestsRepository::databaseResultsToALMRequests(array());
    }
    */
    
    // if requestState is manageable, create query
    $query = db_select(self::TABLE_NAME, 'r');
    $query->join(ALMLogRepository::DATABASE_NAME, 'l', 'r.' . self::$databaseFields['id'] . ' = l.' . ALMLogRepository::$databaseFields['request_id']);
    $query
      ->condition('l.' . ALMLogRepository::$databaseFields['action'], ALMLog::$ACTION_SUBMIT, '=')//WHERE l.action = Submit request , Da am Ende nach (submitted) dates sortiert wird, dürfen nur die submitted dates beachtet werden
      ->condition('r.' . self::$databaseFields['request_state'], $requestState, '=')
      ->fields('r', self::$databaseFields);
    $query
      ->groupBy('r.' . self::$databaseFields['id'])
      ->orderBy(self::$databaseFields['request_state'], 'ASC')
      ->orderBy('MAX(l.' . ALMLogRepository::$databaseFields['date'] . ')', 'DESC'); //ORDER BY MAX(l.date) , max because it is possible that one request has more than one submitted date
    $result = $query
      ->extend('PagerDefault')
      ->limit($limit)
      //->orderByHeader($header)
      ->execute();

    $requests = ALMRequestsRepository::databaseResultsToALMRequests($result);
    return $requests;
  }

  /**
   * Parse the database results of various requests to an array containing ALMRequest objects
   *
   * @param $results
   * @return ALMRequest[]
   */
  private static function databaseResultsToALMRequests($results) {
    $requests = array();
    foreach ($results as $result) {
      $requests[] = ALMRequestsRepository::databaseResultsToALMRequest($result);
    }

    return $requests;
  }

  /**
   * Parse the Database result of one ALMRequest to an ALMRequest object
   *
   * @param $results
   * @return ALMRequest
   */
  private static function databaseResultsToALMRequest($results) {
    $request = new ALMRequest();

    if (empty($results)) {
      $request->setRequestID(ALMRequest::EMPTY_REQUEST_ID);
      return $request;
    }

    // convert results object to an associative array
    $results_arr = get_object_vars($results);

    // set request variables with data from database result
    $request->setRequestID($results_arr[self::$databaseFields['id']]);
    $request->setPosition($results_arr[self::$databaseFields['position']]);
    $request->setUserID($results_arr[self::$databaseFields['userid']]);
    $request->setInstitut($results_arr[self::$databaseFields['institut']]);
    $request->setResearchGroupID($results_arr[self::$databaseFields['researchgroup_id']]);
    $request->setSubprojectID($results_arr[self::$databaseFields['subproject']]);
    $request->setEmail($results_arr[self::$databaseFields['email']]);
    $request->setPhoneNumber($results_arr[self::$databaseFields['phone']]);
    $request->setImagingGoal($results_arr[self::$databaseFields['imaging_goal']]);
    $request->setSampleType($results_arr[self::$databaseFields['sample_type']]);
    $request->setTargetStructure($results_arr[self::$databaseFields['target_structure']]);
    $request->setConditions($results_arr[self::$databaseFields['conditions']]);
    $request->setStainingMethod($results_arr[self::$databaseFields['staining_method']]);
    $request->setDataRequired($results_arr[self::$databaseFields['data_required']]);
    $request->setImagingSupportIDs(explode(';', $results_arr[self::$databaseFields['imaging_support']])); //convert database values to array
    $request->setSampleHealthID($results_arr[self::$databaseFields['sample_health']]);
    $request->setImagingModuleIDs(explode(';', $results_arr[self::$databaseFields['imaging_module']]));   //convert database values to array
    $request->setAnalysisModuleIDs(explode(';', $results_arr[self::$databaseFields['analysis_module']]));   //convert database values to array
    $request->setIntroductionToAlmIDs(explode(';', $results_arr[self::$databaseFields['introduction_to_alm']]));    //convert database values to array

    $request->setConsultationResponsibleS02MemberID($results_arr[self::$databaseFields['consultationResponsibleS02Member']]);
    $request->setConsultationAttending($results_arr[self::$databaseFields['consultationAttending']]);
    $request->setConsultationPreviousPracticalExperiences($results_arr[self::$databaseFields['consultationPrevPracticalExperiences']]);
    $request->setConsultationImagingSupport($results_arr[self::$databaseFields['consultationImagingSupport']]);
    $request->setConsultationImagingModule($results_arr[self::$databaseFields['consultationImagingModule']]);
    $request->setConsultationSampleHealth($results_arr[self::$databaseFields['consultationSampleHealth']]);
    $request->setConsultationAnalysisModule($results_arr[self::$databaseFields['consultationAnalysisModule']]);

    $request->setRequestState($results_arr[self::$databaseFields['request_state']]);
    $request->setLockedBy($results_arr[self::$databaseFields['lockedBy']]);

    return $request;
  }

}