<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 05.06.2017
 * Time: 19:15
 */
class ALMAntibody {
  /**
   * @var Antibody
   */
  private $antibodyObject;

  /**
   * ALMAntibody constructor.
   * @param $inputAntibodyObject
   */
  public function __construct($inputAntibodyObject) {
    $this->antibodyObject = $inputAntibodyObject;
  }

  /**
   * @return string
   */
  public function getAntigenSymbol() {
    return $this->antibodyObject->getAntigenSymbol();
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->antibodyObject->getName();
  }

  /**
   * @return string
   */
  public function getViewUrl() {
    return $this->antibodyObject->getViewUrl();
  }

  public function getViewBadge(){
    return '<a href="' . $this->getViewUrl() . '"><span class="badge" style="background-color: #9dc340;">' . $this->getElementPID() . '</span></a>';
  }

  /**
   * @return string
   */
  public function getElementPID() {
    return $this->antibodyObject->getElementPID();
  }

  /**
   * @return string
   */
  public function getRaisedIn() {
    return $this->antibodyObject->getElementRaisedIn();
  }

  /**
   * @return string
   */
  public function getCatalogNo() {
    return $this->antibodyObject->getElementCatalogNo();
  }
}