<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 31.05.2018
 * Time: 00:01
 */
class ALMRequestEditForms {

  /**
   * @var ALMRequest
   */
  private $requestObj;

  function __construct($requestObject) {
    $this->requestObj = $requestObject;
  }

  public function getFieldsetUserProfile() {
    return array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-user"></i> ' . t('User Profile'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,

      ALM_REQUEST_USER_USER => $this->requestObj->getFormFieldUserAccountName(),
      ALM_REQUEST_USER_NAME => $this->requestObj->getFormFieldUserFullname(),
      ALM_REQUEST_USER_POSITION => $this->requestObj->getFormFieldPosition(),
      ALM_REQUEST_USER_INSTITUTE => $this->requestObj->getFormFieldInstitute(),
      ALM_REQUEST_USER_RESEARCHGROUP => $this->requestObj->getFormFieldResearchgroup(),
      ALM_REQUEST_USER_SUBPROJECT => $this->requestObj->getFormFieldSubprojects(),
      ALM_REQUEST_USER_EMAIL => $this->requestObj->getFormFieldEmail(),
      ALM_REQUEST_USER_PHONE => $this->requestObj->getFormFieldPhoneNumber(),
    );
  }

  public function getFieldsetImagingGoal() {
    return array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-lightbulb-o"></i> ' . t('Imaging Goal and Scientific Question'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => 'Please describe here the imaging goal and the scientific question',

      ALM_REQUEST_IMAGING_GOAL_AND_SCIENTIFIC_QUESTION => $this->requestObj->getFormFieldImagingGoal(),

    );
  }

  public function getFieldsetSampleInformation() {
    return array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-info"></i> ' . t('Sample Information'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,

      ALM_REQUEST_SAMPLE_TYPE => $this->requestObj->getFormFieldSampleType(),
      ALM_REQUEST_SAMPLE_TARGET_STRUCTURE => $this->requestObj->getFormFieldTargetStructure(),
      ALM_REQUEST_SAMPLE_CONDITIONS => $this->requestObj->getFormFieldSampleConditions(),
      ALM_REQUEST_SAMPLE_STAINING_METHOD => $this->requestObj->getFormFieldStainingMethod(),
      ALM_REQUEST_SAMPLE_DESCRIPTIVE_OR_QUANTITATIVE_DATA_REQUIRED => $this->requestObj->getFormFieldDataRequired(),

    );
  }

  public function getFieldsetRequestedSupport() {
    return array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-edit"></i> ' . t('Requested Support and Service'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      //'#description' => 'Please describe here the imaging goal and the scientific question',

      //todo (Nov.2019) Changed to one column because of scaling problems if the number of checkboxes are varyin
      //The markup elements generates a view with 3 rows and two colums
      //'row_1_begin' => array('#markup' => '<div class="row"><div class="col-md-4">',),
      ALM_REQUEST_IMAGING_SUPPORT => $this->requestObj->getFormFieldImagingSupport(),
      //'row_1_column_2' => array('#markup' => '</div><div class="col-md-4">',),
      ALM_REQUEST_INTRODUCTION_TO_ALM => $this->requestObj->getFormFieldIntroductionToAlm(),
      //'row_1_end_row_2_begin' => array('#markup' => '</div></div><div class="row"><div class="col-md-4">',),
      ALM_REQUEST_IMAGING_MODULE => $this->requestObj->getFormFieldImagingModule(),
      //'row_2_column_2' => array('#markup' => '</div><div class="col-md-4">',),
      ALM_REQUEST_ANALYSIS_MODULE => $this->requestObj->getFormFieldAnalysisModule(),
      //'row_2_end' => array('#markup' => '</div></div>',),
      ALM_REQUEST_SAMPLE_HEALTH => $this->requestObj->getFormFieldSampleHealth(),

    );
  }

  public function getFieldsetUserComment() {
    return array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-comment"></i> ' . t('User Comment'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => 'Place for additional user comments',

      ALM_REQUEST_USER_COMMENT => $this->requestObj->getFormFieldComment(ALMCommentType::SubmitEdit),
    );
  }

  /**
   * Takes data from the form_state array of the user-profile fieldset and writes them to the associated request object
   * @param $fieldsetUserProfileFormState
   */
  public function collectDataFromFieldsetUserProfile($fieldsetUserProfileFormState){
    
    $this->requestObj->setPosition($fieldsetUserProfileFormState[ALM_REQUEST_USER_POSITION]);
    $this->requestObj->setInstitut($fieldsetUserProfileFormState[ALM_REQUEST_USER_INSTITUTE]);
    $this->requestObj->setSubprojectID($fieldsetUserProfileFormState[ALM_REQUEST_USER_SUBPROJECT]);
    $this->requestObj->setResearchGroupID($fieldsetUserProfileFormState[ALM_REQUEST_USER_RESEARCHGROUP]);
    $this->requestObj->setEmail($fieldsetUserProfileFormState[ALM_REQUEST_USER_EMAIL]);
    $this->requestObj->setPhoneNumber($fieldsetUserProfileFormState[ALM_REQUEST_USER_PHONE]);
  }

  /**
   * Takes data from the form_state array of the Imaging-Goal fieldset and writes them to the associated request object
   * @param $fieldsetImagingGoalFormState
   */
  public function collectDataFromFieldsetImagingGoal($fieldsetImagingGoalFormState){
    $this->requestObj->setImagingGoal($fieldsetImagingGoalFormState[ALM_REQUEST_IMAGING_GOAL_AND_SCIENTIFIC_QUESTION]);
  }

  /**
   * Takes data from the form_state array of the Sample-Information fieldset and writes them to the associated request object
   * @param $fieldsetSampleInformationFormState
   */
  public function collectDataFromFieldsetSampleInformation($fieldsetSampleInformationFormState){
    
    $this->requestObj->setSampleType($fieldsetSampleInformationFormState[ALM_REQUEST_SAMPLE_TYPE]);
    $this->requestObj->setTargetStructure($fieldsetSampleInformationFormState[ALM_REQUEST_SAMPLE_TARGET_STRUCTURE]);
    $this->requestObj->setConditions($fieldsetSampleInformationFormState[ALM_REQUEST_SAMPLE_CONDITIONS]);
    $this->requestObj->setStainingMethod($fieldsetSampleInformationFormState[ALM_REQUEST_SAMPLE_STAINING_METHOD]);
    $this->requestObj->setDataRequired($fieldsetSampleInformationFormState[ALM_REQUEST_SAMPLE_DESCRIPTIVE_OR_QUANTITATIVE_DATA_REQUIRED]);
  }

  /**
   * Takes data from the form_state array of the Requested-Support fieldset and writes them to the associated request object
   * @param $fieldsetRequestedSupportFormState
   */
  public function collectDataFromFIeldsetRequestedSupport($fieldsetRequestedSupportFormState){
    
    $this->requestObj->setImagingSupportIDs($fieldsetRequestedSupportFormState[ALM_REQUEST_IMAGING_SUPPORT]);
    $this->requestObj->setSampleHealthID($fieldsetRequestedSupportFormState[ALM_REQUEST_SAMPLE_HEALTH]);
    $this->requestObj->setImagingModuleIDs($fieldsetRequestedSupportFormState[ALM_REQUEST_IMAGING_MODULE]);
    $this->requestObj->setAnalysisModuleIDs($fieldsetRequestedSupportFormState[ALM_REQUEST_ANALYSIS_MODULE]);
    $this->requestObj->setIntroductionToAlmIDs($fieldsetRequestedSupportFormState[ALM_REQUEST_INTRODUCTION_TO_ALM]);
  }

  public function collectDataFromFieldsetUserComment($fieldsetUserCommentFormState){
    // TODO 29.may.18 Commentar über Forms Klasse speichern.
    //Update the User Comment
    $comment = $this->requestObj->getCommentWithType(ALMCommentType::SubmitEdit);
    $comment->setMessage($fieldsetUserCommentFormState[ALM_REQUEST_USER_COMMENT]);
    $comment->setCurrentDate();

    //Set the updated user Comment to the updatedComment Variable, so the next save invoke will store it to the DB
    $this->requestObj->setUpdatedComment($comment);
  }
}