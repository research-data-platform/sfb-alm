<?php

/**
 * Class ALMDatasetUploadForms
 *
 * Provides handling for a Drupal form to upload (zipped) files as ALM datasets
 */
class ALMDatasetUploadForms {

  /**
   * @var ALMRequest
   */
  private $requestObj;

  /**
   * ALMDatasetUploadForms constructor.
   *
   * @param $requestObj ALMRequest
   *
   * @return ALMDatasetUploadForms
   */
  public function __construct($requestObj) {
    $this->requestObj = $requestObj;
  }

  public function getFormUpload() {
    $request_id = $this->requestObj->getRequestID();
    /**
     * Fetch coverslip labels and produce a select field options array for
     * those coverslips that have a PID assigned.
     */
    $coverslips = ALMRequestCoverslipLabelRepository::findAllVisibleByRequestId($request_id);

    // Select field options
    $coverslip_options = [];

    // Loop through coverslips and check if they have PID assigned.
    foreach ($coverslips as $coverslip) {
      if ($pid = $coverslip->getPidObject()) {
        /**
         * Add PID to options label (Only PID is needed later to register
         * with the @see \ArchiveObject that will be created)
         */
        $coverslip_options[$pid->getId()] = $coverslip->getCoverslipDescripingName();
      }
    }

    /**
     * Fetch the current user's (first...) personal Archive instance.
     */
    $user = User::getCurrent();
    $userArchives = ArchiveRepository::findAllPersonalArchivesByUser($user->getUid());
    if (sizeof($userArchives) == 0) {
      throw new NoUserArchiveFoundException(t("No personal archiv of the current user was found. Please create and enable a personal research data archiv for the user."));
    }
    $archive = $userArchives[0];

    // Initialize form
    $form = [];

    /**
     * Coverslip select field.
     */
    $form['select_coverslip'] = [
      '#type' => 'select',
      '#title' => t('Choose a Coverslip for which to upload dataset'),
      '#options' => $coverslip_options,
      '#required' => TRUE,
    ];


    /**
     * Display info into which Archive the dataset will be stored.
     */
    $form['info_archive_name'] = [
      '#type' => 'markup',
      '#markup' => '<h5>Dataset will be uploaded to <em>' . $archive->getName() . '</em></h5>',
    ];

    /**
     * Initialize empty @see \ArchiveObject instance and get the basic upload form.
     */
    $object = new ALMDataset();
    $form = array_merge($form, $object->getForm(ArchiveObject::FORM_UPLOAD));

    $form['comment'] = [
      '#type' => 'textarea',
      '#title' => 'Comments',
      '#description' => '',
      '#size' => 4,
      '#maxlength' => 1023,
      '#required' => FALSE,
    ];

    //
    // Cancel button
    // Returns back to ALM Images Tab of request view
    //
    $form['cancel'] = [
      '#markup' => '<a href="' . sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $request_id, 3) . '" class="btn btn-default" style="margin-right: 5px"><span class="fa fa-arrow-left"></span> Cancel</a>',
    ];

    /**
     * Overwrite submit button. (Needs to be overwritten because the form was merged wuth the FORM_UPLOAD of the ArchiveObject
     */
    unset($form['submit']);
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Upload Dataset',
    ];

    /**
     * Set form theme.
     */
    $form['#tree'] = TRUE;
    $form['#theme'] = [
      '#theme' => 'alm_dashboard_box_form',
    ];

    return $form;
  }

}

/**
 * Class NoUserArchiveFoundException
 *
 * ToDo: Move to RDP Archive module.
 */
class NoUserArchiveFoundException extends Exception {

}
