<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 21.09.2018
 * Time: 15:52
 *
 * Description:
 * Object for an ALM Image File. Uses a CdstarFile object as input.
 *
 * Provides functions for to get the image as JPG and to create an html tag
 * that shows the image (uses data-uri so the image is added as base64 code in
 * the html file).
 *
 */
class ALMImageFile {

  /**
   * @var CdstarFile
   */
  private $cdstarFileObj;

  /**
   * Binary Image from CdStar
   *
   * @var String
   */
  private $binaryImage;

  /**
   * @var int ID of parent ALMRequest
   */
  private $request_id;

  /**
   * @var int ID of parent ALMDataset
   */
  private $dataset_id;

  function __construct($cdstarFile, $request_id, $dataset_id) {
    $this->cdstarFileObj = $cdstarFile;
    $this->binaryImage = $this->cdstarFileObj->getBinaryFileStream();
    $this->request_id = $request_id;
    $this->dataset_id = $dataset_id;
  }

  /**
   * Returns the image in jpg format as binary image String (NOT Base64!!)
   */
  function getJPGImageBlob() {
    $image = $this->getJPGImagick();
    return $image->getImageBlob();
  }

  private function getJPGImagick() {
    try {
      $image = new Imagick();
      $image->readImageBlob($this->binaryImage);
    } catch (Exception $e) {
      watchdog_exception(__CLASS__, $e);
    }
    //loads the binary stream in an Imagick object
    $image->setFormat('JPEG');  // convert TIFF zu JPG

    return $image;
  }

  /**
   * Returns a HTML-Tag, that displays the image in the given size
   *
   * @param int $x
   * @param int $y
   *
   * @return string returns a html <img ...> Tag containing the complete image
   *   as String.
   */
  function getScaledHtmlImage($x = 250, $y = 200, $padding = '10px') {
    $image = $this->getJPGImagick();
    try {
      $image->scaleImage($x, $y, TRUE);
    } catch (Exception $e) {
      watchdog_exception(__CLASS__, $e);
    }

    $imageBlob = $image->getImageBlob(); // gets the image blob (string representation of the binary image)
    $base64Image = base64_encode($imageBlob);  //converts the imageblob to a base64 String
    $mime = 'image/jpg'; // set the mimi type for the data uri.
    $base64ImageForHtmlImgTag = 'data:' . $mime . ';base64,' . $base64Image;  // Create the data-uri source of a base64 image (the String is used instead of the source url)

    $htmlImage = '<img src="' . $base64ImageForHtmlImgTag . '" style="padding: ' . $padding . ';display: block; margin:0 auto; max-width: 100%; height:auto;"/>'; // creates the complete hmtl data-uri image tag.

    return $htmlImage;
  }

  /**
   * Returns html-code for a field (col-sm-4 col-md-3 col-lg-2) with the
   * name,size, download/delete button and the image
   *
   * @return string
   */
  function getHtmlImageField() {
    $download_icon = l(CdstarResources::GLYPHICON_DOWNLOAD, $this->url(), ['html' => TRUE]);

    //todo: can be deleted
    $delete_icon = l(CdstarResources::GLYPHICON_DELETE, $this->cdstarFileObj->url('delete'), ['html' => TRUE]);


    $boxStyles = 'style="font-size: smaller; padding: 2px; margin: 2px; background-color: #f5f5f5; border: 1px solid #dddddd;"';

    $output = '<div class="col-sm-6 col-md-4 col-lg-3"><div class="container-fluid table-bordered" ' . $boxStyles . '>
        <div class="col-xs-12">' . '<strong>' . $this->cdstarFileObj->getFilename() . '</strong> ['
      . format_size($this->cdstarFileObj->getFilesize()) . ']'
      . $download_icon . ' '
      . '</div>';

    $output .= '<div class="clo-xs-12">' . $this->getScaledHtmlImage() . '</div></div></div>';

    return $output;
  }

  function maxScaleImage() {
    // todo
  }

  public function url() {
    return sfb_alm_url(RDP_ALM_URL_DOWNLOAD_FILE,
      $this->request_id,
      $this->dataset_id,
      $this->cdstarFileObj->getId());
  }

}