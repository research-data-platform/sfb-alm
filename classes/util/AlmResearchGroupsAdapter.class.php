<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 28.05.2019
 * Time: 21:58
 */
class AlmResearchGroupsAdapter {

  public static function getAllResearchGroupShortNames(){
    $researchgroups = WorkingGroupRepository::findAll();
    $researchgroupNames = [];

    foreach ($researchgroups as $researchgroup) {
      $researchgroupNames[$researchgroup->getId()] = $researchgroup->getShortName();
    }
    return $researchgroupNames;
  }

}