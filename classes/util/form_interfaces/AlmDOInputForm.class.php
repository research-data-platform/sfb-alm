<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 15.01.2019
 * Time: 20:56
 */
abstract class AlmDOInputForm implements iAlmDOInputForm  {
  /**
   * Returns the form as html
   * Todo: Check if working well
   * @return string
   */
  public function getHtml() {
    return drupal_render($this->getForm());
  }
}