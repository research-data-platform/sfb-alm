<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 15.01.2019
 * Time: 20:32
 * Description:
 * An AlmForm object for a dataobject.
 * The AlmDOInputForm object has to provide functions to handle the form inputs ob the form state values
 */
interface iAlmDOInputForm extends iAlmForm {

  /**
   * Saves the data of the (by getForms function) generated fields into a corresponding dataobject
   * @param $form_state array that part of the $form_state array that contains the values of the DO(which have are the values of the provided forms)
   * @param $dataObject object dataobject (DO) in that the data from the form (form_state value) should be saved.
   * @return mixed
   */
  public static function mergeFormStateInDO(&$dataObject, $form_state);

  /**
   * @param $form_state array that part of the $form_state array that contains the values of the DO(which have are the values of the provided forms)
   * @return mixed
   */
  public static function createDOFromFormState($form_state);
}