<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 15.01.2019
 * Time: 21:10
 */
abstract class AlmForm implements iAlmForm {
  /**
   * Returns the form as html
   * Todo: Check if working well
   * @return string
   */
  public function getHtml() {
    return drupal_render($this->getForm());
  }
}