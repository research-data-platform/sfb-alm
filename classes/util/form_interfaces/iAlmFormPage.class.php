<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 03.03.2019
 * Time: 10:01
 */
interface iAlmFormPage {
  public function getPageForm($form = null, &$form_state);
}