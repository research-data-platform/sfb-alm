<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 06.11.2017
 * Time: 21:41
 */
abstract class ALMRepository {


  /**
   * Saves object to database
   *
   * @param $$object object that should be stored into the database
   * @return mixed
   */
  abstract static public function save($object);

  /**
   * Loads a object with a given ID from the database.
   *
   * @param $id databaseId of object
   * @return mixed
   */
  abstract static public function findById($id);

  /**
   * Checks if a variable is empty. returns NULL when the value is '' or ALM_FORM_SELECT_EMPTY
   *
   * @param $input
   * @return null or $input
   */
  protected static function setEmptyVariablesToNull($input) {
    if ($input == ALM_FORM_SELECT_EMPTY || $input == '') {
      return NULL;
    } else {
      return $input;
    }
  }
}