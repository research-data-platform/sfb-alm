<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 03.05.2018
 * Time: 13:49
 */
class ALMConsultationEditForms {
  /**
   * @var ALMRequest
   */
  private $requestObj;

  function __construct($requestObject) {
    $this->requestObj = $requestObject;
  }

  function getFieldsetGeneral() {
    return array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-edit"></i> ' . t('General'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,

      'responsible_s02_member' => $this->requestObj->getFormFieldConsultationResponsibles02Member(),
      'attending' => $this->requestObj->getFormFieldConsultationAttending(),
      'comment' => $this->requestObj->getFormFieldComment(ALMCommentType::ConsultEdit),
      'practical_experiences' => $this->requestObj->getFormFieldConsultationPreviousPracticalExperiences(),
      'imaging_support' => $this->requestObj->getFormFieldConsultationImagingSupport(),
      'sample_health' => $this->requestObj->getFormFieldConsultationSampleHealth(),
      'imaging_module' => $this->requestObj->getFormFieldConsultationImagingModule(),
      'analysis_module' => $this->requestObj->getFormFieldConsultationAnalysisModule(),
    );
  }

  /**
   * Creates a (drupal_form) fieldset for editable singe staining tables.
   * Remove function:
   * The remove button invokes the rdp_alm_request_submit_remove_staining function.
   * The #name element of the triggering button contains the stainingID
   *
   * @param $addFunctionName String Name of the function which adds a new single Staining (Optional)
   * @return drupal_form array.
   */
  function getFieldsetSingleStaining($addFunctionName = null){
    $fieldsetSingleStaining = array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-flask"></i> ' . t('Single IF Stainings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    // Create add button if an addFunctionName is available.
    if($addFunctionName != null) {
      $fieldsetSingleStaining['btn-add_single_IF'] = array(
        '#type' => 'submit',
        '#value' => t(' Add Single Staining'),
        '#description' => t('Adds a new Single IF Staining Table'),
        //Use onmouseover event for setting the scroll position because the value has to set before the submit
        '#attributes' => array('class' => array('btn-default'), 'style' => array('margin-bottom: 10px;',), 'onmouseover' => array('saveScrollPosition()',),),
        '#submit' => array($addFunctionName),
      );
    }

    // The total number of single stainings
    $singleStainingCount = $this->requestObj->getSingleIfCount();
    $stainingList = $this->requestObj->loadAllSingleStainings();
    // creates a staining Table for each single staining
    for ($count = 0; $count < $singleStainingCount; $count++) {
      $fieldsetSingleStaining['view_staining_table' . $count] = $this->getFormFieldStainingTable($count, $stainingList[$count]);
    }

    // Create second  add button if an addFunctionName is available and at least one singe staining is defined.
    if ($addFunctionName != null && $singleStainingCount > 0) {
      $fieldsetSingleStaining['btn-add_single_IF_2'] = array(
        '#type' => 'submit',
        '#value' => t(' Add Single Staining'),
        '#description' => t('Adds a new Single IF Staining Table'),
        //Use onmouseover event for setting the scroll position because the value has to set before the submit
        '#attributes' => array('class' => array('btn-default'), 'style' => array('margin-bottom: 10px;',), 'onmouseover' => array('saveScrollPosition()',),),
        '#submit' => array($addFunctionName),
      );
    }

    return $fieldsetSingleStaining;
  }

  /**
   * Creates a (drupal_form) fieldset for editable dual staining tables.
   *
   * @param $addFunctionName String Name of the function which adds a new dual Staining (Optional)
   * @return drupal_form array.
   */
  function getFieldsetDualStaining($addFunctionName = null){
    $fieldsetDualStaining = array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-flask"></i> ' . t('Dual IF Staining'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE, #isset($_GET['addDualIF']) ? FALSE : TRUE,
    );

    // Create add button if an addFunctionName is available.
    if($addFunctionName != null) {
      $fieldsetDualStaining['btn-add_dual_IF_'] = array(
        '#type' => 'submit',
        '#value' => t(' Add Dual Staining'),
        '#description' => t('Adds a new Dual IF Staining Table'),
        //Use onmouseover event for setting the scroll position because the value has to set before the submit
        '#attributes' => array('class' => array('btn-default'), 'style' => array('margin-bottom: 10px;',), 'onmouseover' => array('saveScrollPosition()',),),
        '#submit' => array($addFunctionName),
      );
    }

    // The total number of dual stainings
    $dualStainingCount = $this->requestObj->getDualIfCount();
    $stainingList = $this->requestObj->loadAllDualStainings();
    // creates a staining Table for each dual staining
    for ($count = 0; $count < $dualStainingCount; $count++) {
      $fieldsetDualStaining['view_staining_table' . $count] = $this->getFormFieldStainingTable($count, $stainingList[$count]);
    }

    // Create second  add button if an addFunctionName is available and at least one dual staining is defined.
    if ($addFunctionName != null && $dualStainingCount > 0) {
      $fieldsetDualStaining['btn-add_dual_IF_2'] = array(
        '#type' => 'submit',
        '#value' => t(' Add Dual Staining'),
        '#description' => t('Adds a new Dual IF Staining Table'),
        //Use onmouseover event for setting the scroll position because the value has to set before the submit
        '#attributes' => array('class' => array('btn-default'), 'style' => array('margin-bottom: 10px;',), 'onmouseover' => array('saveScrollPosition()',),),
        '#submit' => array($addFunctionName),
      );
    }

    return $fieldsetDualStaining;
  }

  /**
   * Creates a (drupal_form) fieldset for editable multi staining tables.
   *
   * @param addFunctionName String Name of the function which adds a new dual Staining (Optional)
   * @return array (drupal_form).
   */
  public function getFieldsetMultiStaining($addFuncName = null){
    $fieldsetMultiStaining = array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-flask"></i> ' . t('Multi IF Staining'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE, #isset($_GET['addMultiIF']) ? FALSE : TRUE,
    );

    // Create add button if an addFunctionName is available.
    if($addFuncName != null) {
      $fieldsetMultiStaining['btn-add_multi_IF_'] = array(
        '#type' => 'submit',
        '#value' => t(' Add Multi Staining'),
        '#description' => t('Adds a new Multi IF Table'),
        //Use onmouseover event for setting the scroll position because the value has to set before the submit
        '#attributes' => array('class' => array('btn-default'), 'style' => array('margin-bottom: 10px;',), 'onmouseover' => array('saveScrollPosition()',),),
        '#submit' => array($addFuncName),
      );
    }

    // The total number of multi stainings
    $multiStainingCount = $this->requestObj->getMultiIfCount();
    $stainingList = $this->requestObj->loadAllMultiStainings();

    // creates a staining Table for each multi staining
    for ($count = 0; $count < $multiStainingCount; $count++) {
      $fieldsetMultiStaining['view_staining_table' . $count] = $this->getFormFieldStainingTable($count, $stainingList[$count]);
    }

    // Create second  add button if an addFunctionName is available and at least one multi staining is defined.
    if ($addFuncName != null && $multiStainingCount > 0) {
      $fieldsetMultiStaining['btn-add_multi_IF_2'] = array(
        '#type' => 'submit',
        '#value' => t(' Add Multi Staining'),
        '#description' => t('Adds a new Multi IF Staining Table'),
        //Use onmouseover event for setting the scroll position because the value has to set before the submit
        '#attributes' => array('class' => array('btn-default'), 'style' => array('margin-bottom: 10px;',), 'onmouseover' => array('saveScrollPosition()',),),
        '#submit' => array($addFuncName),
      );
    }

    return $fieldsetMultiStaining;
  }

  /**
   * Creates a formField for one (ImmunoFLuoerescence) Staining object (ALMRequestStaining).
   * The number of rows depends from the type of the staining object
   *
   * @param null $index
   * @param $staining ALMRequestStaining
   * @return array
   */
  public function getFormFieldStainingTable($index = null, $staining){

    $stainingForm = new AlmStainingEditForm($staining);
    $stainingForm->addRemoveBtn('rdp_alm_request_submit_remove_staining');
    $stainingForm->setHeadline(t($staining->getTypeText() . ' ' . ($index + 1) . ':'));
    return $stainingForm->getForm();
  }


  public function getFieldsetSampleDocumentation($addFunctionName = null){
    $fieldsetSampleDocumentation = array(
      '#type' => 'fieldset',
      '#title' => '<i class="fa fa-eyedropper"></i> ' . t('Sample Documentation'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    if($addFunctionName != null) {
      $fieldsetSampleDocumentation['btn-add_sample_documentation'] = array(
        '#type' => 'submit',
        '#value' => t(' Add Sample documentation'),
        '#description' => t('Adds a new sample documentation'),
        //Use onmouseover event for setting the scroll position because the value has to set before the submit
        '#attributes' => array('class' => array('btn-default'), 'style' => array('margin-bottom: 10px;',), 'onmouseover' => array('saveScrollPosition()',),),
        '#submit' => array($addFunctionName),
      );
    }


    // The Total number of samples in the request
    $sample_count = $this->requestObj->getSampleCount();
    // Load all samples of the request
    $sampleList = $this->requestObj->loadAllSamples();

    // create sample Documentation table for each sample
    for ($count = 0; $count < $sample_count; $count++) {
      // Check if the sample with the index $count exists, otherwise use null
      //$sample_obj = $count >= 0 && $count < count($sampleList) ? $sampleList[$count] : new ALMRequestSample(); todo auskommentiert 04.mai.18
      //$fieldsetSampleDocumentation['view_staining_table' . $count] = $this->getFormFieldSampleDoc($count, $sampleList[$count]);

      $sampleForm = new AlmSampleEditForm($sampleList[$count]);
      $sampleForm->addRemoveBtn('rdp_alm_request_submit_remove_sample');
      $sampleForm->addCopyBtn('rdp_alm_request_submit_copy_sample');
      $sampleForm->setHeadline(('Sample ' . ($count + 1) . ':'));

      $fieldsetSampleDocumentation['view_sample_table' . $count] =$sampleForm->getForm();
    }

    // Create second add button if an addFunctionName is available and at least one sample Documentation is defined.
    if ($addFunctionName != null && $sample_count > 0) {
      $fieldsetSampleDocumentation['btn-add_sample_documentation_2'] = array(
        '#type' => 'submit',
        '#value' => t(' Add Sample documentation'),
        '#description' => t('Adds a new sample documentation'),
        //Use onmouseover event for setting the scroll position because the value has to set before the submit
        '#attributes' => array('class' => array('btn-default'), 'style' => array('margin-bottom: 10px;',), 'onmouseover' => array('saveScrollPosition()',),),
        '#submit' => array($addFunctionName),
      );
    }

    return $fieldsetSampleDocumentation;
  }


  /**
   * Takes data from the form_state array of the general-fieldset and writes them to the associated request Object
   * @param $fieldsetGeneralFormState
   */
  public function collectDataFromFieldsetGeneral($fieldsetGeneralFormState){
    //Update the User Comment
    $comment = $this->requestObj->getCommentWithType(ALMCommentType::ConsultEdit);

    $comment->setMessage($fieldsetGeneralFormState['comment']);
    $comment->setCurrentDate();

    //Set the updated user Comment to the updatedComment Variable, so the next save invoke will store it to the DB
    $this->requestObj->setUpdatedComment($comment);

    $this->requestObj->setConsultationResponsibleS02MemberID($fieldsetGeneralFormState['responsible_s02_member']);
    $this->requestObj->setConsultationAttending($fieldsetGeneralFormState['attending']);
    $this->requestObj->setConsultationPreviousPracticalExperiences($fieldsetGeneralFormState['practical_experiences']);
    $this->requestObj->setConsultationImagingSupport($fieldsetGeneralFormState['imaging_support']);
    $this->requestObj->setConsultationImagingModule($fieldsetGeneralFormState['imaging_module']);
    $this->requestObj->setConsultationSampleHealth($fieldsetGeneralFormState['sample_health']);
    $this->requestObj->setConsultationAnalysisModule($fieldsetGeneralFormState['analysis_module']);
  }


  /**
   * Collects the data from the form_state of the SingleStaining fieldset and writes them to the associated request Object
   * @param $fieldsetSingleStainingsFormState
   */
  public function collectDataFromFieldsetSingleStaining($fieldsetSingleStainingsFormState){
    $singleStainings = $this->requestObj->loadAllSingleStainings();
    $this->collectDataFromSpecificFieldsetStaining($singleStainings, $fieldsetSingleStainingsFormState);

    $this->requestObj->setConsultationSingleStainings($singleStainings); //todo prüfen ob array als referenz übergebn
  }

  /**
   * Collects the data from the form_state of the DualStaining fieldset and writes them to the associated request Object
   * @param $fieldsetDualStainingsFormState
   */
  public function collectDataFromFieldsetDualStaining($fieldsetDualStainingsFormState){
    $dualStainings = $this->requestObj->loadAllDualStainings();
    $this->collectDataFromSpecificFieldsetStaining($dualStainings, $fieldsetDualStainingsFormState);

    $this->requestObj->setConsultationDualStainings($dualStainings);
  }

  /**
   * Collects the data from the form_state of the MultiStaining fieldset and writes them to the associated request Object
   * @param $fieldsetMultiStainingsFormState
   */
  public function collectDataFromFieldsetMultiStaining($fieldsetMultiStainingsFormState){
    $multiStainings = $this->requestObj->loadAllMultiStainings();
    $this->collectDataFromSpecificFieldsetStaining($multiStainings, $fieldsetMultiStainingsFormState);

    $this->requestObj->setConsultationMultiStainings($multiStainings);
  }

  /**
   * Collects the data from a (universal) stainingField and update the objects of the stainingsArr
   * @param $stainingsArr
   * @param $form_stateFieldsetStainings
   */
  private function collectDataFromSpecificFieldsetStaining($stainingsArr, $form_stateFieldsetStainings){
    foreach ($stainingsArr as $index => $stainingObj){
      // select table of the current stainingObj
      $form_stateStainingTable = $form_stateFieldsetStainings['view_staining_table' . $index];

      $stainingsArr[$index] = AlmStainingEditForm::mergeFormStateInDO($stainingObj, $form_stateStainingTable);
      //$stainingsArr[$index] = $this->mergeFormStateStainingTableIntoStainingObject($stainingObj, $form_stateStainingTable);

    }
  }


  /**
   * Collects data from a form_state of a Fieldset-Sample and writes them to the associated request object
   * @param $fieldsetSampleFormState
   */
  public function collectDataFromFieldsetSample($fieldsetSampleFormState){
    $samplesArr = $this->requestObj->loadAllSamples();
    
    foreach ($samplesArr as $index => $sample){
      $formStateSampleTable = $fieldsetSampleFormState['view_sample_table' . $index];
      $samplesArr[$index] = AlmSampleEditForm::mergeFormStateInDO($sample, $formStateSampleTable);
    }
    $this->requestObj->setConsultationSamples($samplesArr);
    
  }
}