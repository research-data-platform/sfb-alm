<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 07.03.2018
 * Time: 00:50
 *
 * Stub Class for predefining PIDs.
 */
class ALMPidStub {
  private $pidSuffix;
  const ACCESS_URL = 'hdl.handle.net/';
  const PID_PREFIX = '21.11124/';
  const ALM_PREFIX = 'alm01-';

  /**
   * ALMPidStub constructor.
   * Creates a new ALMPid Object with a pidSuffix
   * The PID Suffix is only the part after the general and modul specific prefix
   * @param $pidSuffix String
   */
  function __construct($pidSuffix = '') {
    $this->pidSuffix = $pidSuffix;
  }

  /**
   * !Stub! NO REAL PID RESERVATION
   * returns always true. Set the pidSuffix variable
   * @param $newPidSuffix
   * @return bool
   */
  function register($newPidSuffix){
    $this->pidSuffix = $newPidSuffix;
    return true;
  }

  /**
   * !Stub! THE URL DOES NOT WORK CURRENTLY!!
   * Returns a full handle URL for the PID.
   * @return string
   */
  function getAccessUrl(){
    return Self::ACCESS_URL . self::PID_PREFIX . self::ALM_PREFIX . $this->pidSuffix;
  }

  /**
   * Returns the PidSuffix
   * @return String
   */
  function getPidSuffix(){
    return $this->pidSuffix;
  }

}