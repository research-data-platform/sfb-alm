<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 20.11.2019
 * Time: 14:15
 * Description:
 * This class handles editable arrays for select options like select or checkbox forms.
 * The options are managed with an array that contains an ID, name and editable flag for each option.
 * The returned option arrays have the form id => name, which can be used for drupal forms like select or checkbox
 */
class ALMSelectOptions {

  /**
   * @var String
   */
  private $selectOptionsFieldName;

  /**
   * @var array
   */
  private $optionArr;

  /**
   * ALMSelectOptions constructor.
   */
  public function __construct($selectOptionsFieldName, $defaultOptions = null) {
    $this->selectOptionsFieldName = $selectOptionsFieldName;
    $this->loadOptionArr($defaultOptions);
  }

  /**
   * Loads the option array from the database and convert it as associative array
   * @param $defaultOptions
   */
  private function loadOptionArr($defaultOptions){
    // Load the option array as json array from the database
    $jsonOptions = variable_get($this->selectOptionsFieldName, json_encode($defaultOptions));
    // Decode the json array from the database
    $this->optionArr = json_decode($jsonOptions, true);
  }

  /**
   * Returns an array with all ENABLED options in a form that can be used for drupal forms
   *
   * @return array contains only 'key' => 'option' pairs
   */
  public function getEnabledOptions(){
    $enabledOptions = array();
    foreach ($this->optionArr as $key => $option) {
      if($option['enabled']){
        $enabledOptions[$key] = $option['name'];
      }
    }

    return $enabledOptions;
  }

  /**
   * Returns an array with all options in a form that can be used for drupal forms
   *
   * @return array contains only 'key' => 'option' pairs
   */
  public function getAllOptions(){
    $options = array();
    foreach ($this->optionArr as $key => $option) {
      $options[$key] = $option['name'];
    }

    return $options;
  }

  /**
   * Returns the associative array of all option including the enabled flag
   * @return array ('key' => ('name' => '...', 'enabled' => ''),..
   */
  public function getFullOptionArr(){
    return $this->optionArr;
  }

  /**
   * Returns the name of the select Option Field(which is the name of the database field)
   * @return String
   */
  public function getSelectOptionsFieldName(){
    return $this->selectOptionsFieldName;
  }

  public function existsId($id){
    if($this->optionArr == null) return false;
    if(array_key_exists($id, $this->optionArr)) return true;
    return false;
  }

  /**
   * Store the current optionArr in the database
   */
  public function storeOptions(){
    variable_set($this->selectOptionsFieldName, json_encode($this->optionArr));
  }

  /**
   * Enables the option with the id
   * For persistent, object needs to be stored in the database!
   * @param $id
   */
  public function enableOption($id){
    $this->checkId($id);
    $this->optionArr[$id]['enabled'] = true;
  }

  /**
   * Disables the option with the id
   * For persistent, object needs to be stored in the database!
   *
   * @param $id
   */
  public function disableOption($id){
    $this->checkId($id);
    $this->optionArr[$id]['enabled'] = false;
  }

  /**
   * Check if the option with the $id is enabled. Returns the current status
   * @param $id
   * @return mixed
   */
  public function isEnabled($id){
    $this->checkId($id);
    return $this->optionArr[$id]['enabled'];
  }

  /**
   * Sets the 'enabled' value of the option with the $id
   * @param $id
   * @param $status boolean Enabled: 1; Disabled: 0
   */
  public function setStatus($id, $status){
    $this->checkId($id);
    $this->optionArr[$id]['enabled'] = $status;
  }

  /**
   * Set the name of the option with the id
   * For persistent, object needs to be stored in the database!
   *
   * @param $id
   * @param $name
   */
  public function setOptionName($id, $name){
    $this->checkId($id);
    $this->optionArr[$id]['name'] = $name;
  }

  private function checkId($id){
    if (!$this->existsId($id)){
      throw new AlmSelectOptionIdNotFoundException('The option id is not in the option array of the selectfield options');
    }
  }

  /**
   * Creates a new Option and add is to the optionArr
   * For persistent, object needs to be stored in the database!
   *
   * @param $name
   * @param $enabled
   */
  public function addOption($name, $enabled){
    if($this->optionArr == null){
      $this->createFirstElement($name, $enabled);
    }
    $this->optionArr[] = ['name' => $name, 'enabled' => $enabled];
  }

  /**
   * Init the optionArr and inserts the first element.
   * The first element has the key 1 because 0 is reserved for default values like "Please Select" and the key for
   * adding new options
   * @param $name
   * @param $enabled
   */
  private function createFirstElement($name, $enabled){
    $this->optionArr = array();
    $this->optionArr[1] = ['name' => $name, 'enabled' => $enabled];
  }
}

class AlmSelectOptionIdNotFoundException extends Exception{

}