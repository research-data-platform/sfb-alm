<?php
/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 31.08.2016
 * Time: 11:11
 */

class ALMRequestStaining{

  const EMPTY_IF_ID = -1;
  const TYPE_SINGLE = 2;
  const TYPE_DUAL = 3;
  const TYPE_MULTI = 5;

  // Default Values for AntibodyStainingRows
  private static $defaultAntibodyStainingRow = array(
    'primaryAntibodyId' => '',
    'primaryAntibodyDilution' => '',
    'secondaryAntibodyId' => '',
    'secondaryAntibodyDilution' => '',
  );

  // Default Values for AdditionalStainingRows
  private static $defaultAdditionalStainingRow = array(
    'primaryAdditionalStaining' => '',
    'primaryAdditionalStainingDilution' => '',
    'secondaryAdditionalStaining' => '',
    'secondaryAdditionalStainingDilution'=>'',
  );

  /**
   * @var integer, unique persistent ID of the database object
   */
  private $id;

  /**
   * @var integer, the type of the immunofluorescence staining.
   * 2 = single  , 3 = dual, 5 = multi-color
   * @see ALMRequestStaining::TYPE_SINGLE, ALMRequestStaining::TYPE_DUAL, ALMRequestStaining::TYPE_MULTI
   */
  private $type;

  /**
   * The requestID that owns this IF
   */
  private $requestId;

  /**
   * @var string the name for the label with information about the IF
   */
  private $labelName;

  public function save(){
    ALMRequestStainingRepository::save($this);
  }

  /**
   * Associative Array with:
   * $antibodyStainings[order][primaryAntibodyId]
   * $antibodyStainings[order][primaryAntibodyDilution]
   * $antibodyStainings[order][secondaryAntibodyId]
   * $antibodyStainings[order][secondaryAntibodyDilution]
   *
   * Order begins with:  0
   *
   * @var string[]
   */
  private $antibodyDefinitions = array();

  /**
   * Associative Array with:
   * $additionalStainingRows[order][primaryAdditionalStaining]
   * $additionalStainingRows[order][primaryAdditionalStainingDilution]
   * $additionalStainingRows[order][secondaryAdditionalStaining]
   * $additionalStainingRows[order][secondaryAdditionalStainingDilution]
   *
   * Order begins with: 0
   *
   * @var string[]
   */
  private $additionalStainingDefinitions = array();

  /**
   * @var int the UID of the user that creates the steining
   */
  private $owner;

  /**
   * Only if true, it is possible to editable the sample definition
   * @var boolean
   */
  private $editable;

  /**
   * @return boolean
   */
  public function isEditable() {
    return $this->editable;
  }

  /**
   * @param bool $editable
   */
  public function setEditable($editable) {
    $this->editable = $editable;
  }

  public function hasId(){
    if($this->id == null){
      return false;
    }
    return true;

  }

  /**
   * @return int
   */
  public function getOwner()
  {
    return $this->owner;
  }

  /**
   * @param int $owner
   */
  public function setOwner($owner)
  {
    $this->owner = $owner;
  }


  /**
   * @return string[]
   */
  public function getAntibodyDefinitions()
  {
    return $this->antibodyDefinitions;
  }

  /**
   * Returns an array with all IDs of the associated Antibodies. The list is not distinct
   *
   * @param $antibodyType String Limit the results to primary or secondary antibodies. Use 'primary' or 'secondary'
   * @return array with all IDs of assiciated Antibodies (the IDs are not distinct!)
   */
  public function getAssociatedAntibodyIds($antibodyType = null){
    $associatedAbIds = [];
    foreach ($this->antibodyDefinitions as $antibodyStainingRow){
      if ($antibodyType == null || $antibodyType == 'primary'){

        $associatedAbIds[] = $antibodyStainingRow['primaryAntibodyId'];
      }
      if($antibodyType == null || $antibodyType == 'secondary'){
        $associatedAbIds[] = $antibodyStainingRow['secondaryAntibodyId'];
      }
    }
    return $associatedAbIds;
  }
  /**
   * @param string[] $antibodyDefinitions
   */
  public function setAntibodyDefinitions(array $antibodyDefinitions)
  {
    $this->antibodyDefinitions = $antibodyDefinitions;
  }

  /**
   * @return string[]
   */
  public function getAdditionalStainingDefinitions()
  {
    return $this->additionalStainingDefinitions;
  }

  /**
   * @param string[] $additionalStainingDefinitions
   */
  public function setAdditionalStainingDefinitions(array $additionalStainingDefinitions)
  {
    $this->additionalStainingDefinitions = $additionalStainingDefinitions;
  }




  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param int $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return mixed
   */
  public function getRequestId() {
    return $this->requestId;
  }

  /**
   * @param mixed $requestId
   */
  public function setRequestId($requestId) {
    $this->requestId = $requestId;
  }

  /**
   * @return string
   */
  public function getLabelName() {
    return $this->labelName;
  }

  /**
   * @param string $labelName
   */
  public function setLabelName($labelName) {
    $this->labelName = $labelName;
  }

  /**
   * Returns the Amount of the Antibody Staining Rows of the Staining Table.
   * Those are the rows which contains antibody link.
   * Attention: One row contains two antibody links (primary and secondary)
   * @return int
   */
  public function getAmountOfAntibodyStainingRows(){
    switch ($this->type){
      case ALMRequestStaining::TYPE_SINGLE:
        return 1;
        break;
      case ALMRequestStaining::TYPE_DUAL:
        return 2;
        break;
      case ALMRequestStaining::TYPE_MULTI:
        return 3;
    }
  }

  /**
   * Returns the Amount of the Additional Staining Rows of the Staining Table.
   * Those are the rows which don't contain antibody links
   * @return int
   */
  public function getAmountOfAdditionalStainingRows(){
    switch ($this->type){
      case ALMRequestStaining::TYPE_SINGLE:
        return 1;
        break;
      case ALMRequestStaining::TYPE_DUAL:
        return 1;
        break;
      case ALMRequestStaining::TYPE_MULTI:
        return 2;
    }
  }

  /**
   * Returns the Staining Type as text (Single, Dual or Multi IF)
   */
  public function getTypeText(){
    $typeText='';
    switch ($this->type){
      case ALMRequestStaining::TYPE_SINGLE:
        $typeText = 'Single';
        break;
      case ALMRequestStaining::TYPE_DUAL:
        $typeText = 'Dual';
        break;
      case ALMRequestStaining::TYPE_MULTI:
        $typeText = 'Multi';
    }

    return $typeText.' IF';
  }

  /**
   * Initial the Staining Rows with the default values.
   * The number of rows depends to the staining Type
   *
   * Attention: The current staining will be overwritten!
   */
  public function initialDefaultStainingRows(){
    $this->initialDefaultAntibodyStainingRows();
    $this->initialDefaultAdditionalStainingRows();
  }


  /**
   * Sets the Antibody Staining Rows to default values.
   * The number of rows depends to the staining Type
   */
  private function initialDefaultAntibodyStainingRows(){
    $antibodyStainingRows = array();
    for ($i=0; $i<$this->getAmountOfAntibodyStainingRows();$i++){
      $antibodyStainingRows[$i] = self::$defaultAntibodyStainingRow;
    }
    $this->antibodyDefinitions = $antibodyStainingRows;
  }

  /**
   * Sets the Additional Staining Rows to default values
   * The number of rows depends to the staining Type
   */
  private function initialDefaultAdditionalStainingRows(){
    $additionalStainingRows = array();
    for ($i=0; $i<$this->getAmountOfAdditionalStainingRows();$i++){
      $additionalStainingRows[$i] = self::$defaultAdditionalStainingRow;
    }
    $this->additionalStainingDefinitions = $additionalStainingRows;
  }
}