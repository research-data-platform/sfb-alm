<?php

/**
 * Created by PhpStorm.
 * User: Christian Henke
 * Date: 10.11.2017
 * Time: 12:11
 */
class ALMCoverslipLabelsPrintJob {
  /**
   * The maximum number of characters in one line of the sample name on the label
   */
  const SAMPLE_NAME_MAX_LINE_LENGTH = 16;

  /**
   * The maximum number of characters in one line of the staining name on the label
   */
  const STAINING_NAME_MAX_LINE_LENGTH = 18;

  /**
   * This symbol is used by bartender to insert a line feed on the label.
   * Only one _ will be replaced. The other _ will be visible on the final label.
   */
  const PRINTER_LINE_FEED_SYMBOL = '__';

  /**
   * The database ID of the printjob
   * @var int
   */
  private $dbId;

  /**
   * The request id of the associated request
   * @var int
   */
  private $requestId;
  /**
   * CURRENTLY NOT USED!
   * @var int
   */
  private $state;

  /**
   * @var array
   * Fields:
   *
   * '[]jobId'
   * '[]sampleName'
   * '[]projectName'
   * '[]userName'
   * '[]date'
   * '[]stainingName'
   * '[}handleUrl'
   */
  private $labelsValues = array();

  /**
   * @return int
   */
  public function getRequestId() {
    return $this->requestId;
  }

  /**
   * @param int $requestId
   */
  public function setRequestId($requestId) {
    $this->requestId = $requestId;
  }

  /**
   * @return int
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @param int $state
   */
  public function setState($state) {
    $this->state = $state;
  }

  /**
   * @return array
   *
   * ArrayFields:
   *
   * '[]jobId'
   * '[]sampleName'
   * '[]projectName'
   * '[]userName'
   * '[]date'
   * '[]stainingName'
   * '[]handleUrl'
   */
  public function getLabelsValues() {
    return $this->labelsValues;
  }

  /**
   * @param array $labelsValues
   *
   * ArrayFields:
   *
   * '[]jobId'
   * '[]sampleName'
   * '[]projectName'
   * '[]userName'
   * '[]date'
   * '[]stainingName'
   * '[]handleUrl'
   */
  public function setLabelsValues($labelsValues) {
    $this->labelsValues = $labelsValues;
  }

  /**
   * Checks if the LabelPrintJob is empty. It is empty if no labelValues are defined.
   * @return bool
   */
  public function isEmpty(){
    if(sizeof($this->labelsValues)==0){
      return true;
    }
    return false;
  }

  /**
   * Returns an array containng the values for the labels in the printable form for the Bartender print software
   * Linefeeds are marked with double underlines (__)
   */
  public function getLabelsPrintValues(){
    $labelsPrintValues = array();

    foreach ($this->labelsValues as $labelValues){
      $labelPrintValue = array();
      $labelPrintValue['sampleName'] = $this->addLineFeedSymbolsToString($labelValues['sampleName'], self::SAMPLE_NAME_MAX_LINE_LENGTH);
      $labelPrintValue['projectName'] = $this->removeGermanVowels($labelValues['projectName']);
      $labelPrintValue['userName'] = $this->removeGermanVowels($labelValues['userName']);
      $labelPrintValue['date'] = substr($labelValues['date'], 0, 10);
      $labelPrintValue['stainingName'] = $this->addLineFeedSymbolsToString($labelValues['stainingName'], self::STAINING_NAME_MAX_LINE_LENGTH);
      $labelPrintValue['handleUrl'] = $labelValues['handleUrl'];

      $labelsPrintValues[] = $labelPrintValue;
    }

    return $labelsPrintValues;
  }


  /**
   * Replaces german vowels (ä,ö,ü,ß)
   * @param $str
   * @return mixed
   */
  function removeGermanVowels($str){
    $vowels = array('ä', 'ö', 'ü', 'ß');
    $replacements = array('ae',  'oe', 'ue', 'ss');

    return str_replace($vowels, $replacements, $str);
  }

  /**
   * Ensure that each line is at least $max_line_length characters long and the nameParts (marked by '_') are not splitted
   * Inserts a PRINTER_LINE_FEED_SYMBOL after each line.
   *
   *
   * PRINTER_LINE_FEED_SYMBOL the symbol which mark a line feed in the string (depending from the printer software)
   *
   * @param $name string
   * @param $max_line_length string Maximum length of each line
   * @return string
   */
  private function addLineFeedSymbolsToString($name, $max_line_length) {
    $lineFeededSampleName = '';
    $charsInCurrentLine = 0;
    foreach (explode('_', $name) as $NamePart) {
      if($charsInCurrentLine != 0) {
        $charsInLineAfterNewNamePart = $charsInCurrentLine + strlen($NamePart) +1; // +1 bacause of the '_'
        // Check if linefeed before the next part
        if ($charsInLineAfterNewNamePart <= $max_line_length) {
          $lineFeededSampleName .= '_';
          $charsInCurrentLine += 1;
        } else {
          $lineFeededSampleName .= self::PRINTER_LINE_FEED_SYMBOL;
          $charsInCurrentLine = 0;
        }
      }
      $lineFeededSampleName .= $NamePart;
      $charsInCurrentLine += strlen($NamePart);
    }

    return $lineFeededSampleName;
  }


  /**
   * @return string
   */
  public function getDbId() {
    return $this->dbId;
  }

  /**
   * @param string $dbId
   */
  public function setDbId($dbId) {
    $this->dbId = $dbId;
  }


  /**
   * @param $labelValue array():
   *
   */
  public function addLabel($labelValue) {
    $this->labelsValues[] = $labelValue;
  }

  public function save() {
    // id with 6 characters
    //$this->dbId = substr(uniqid(),0, 6);
    $this->dbId = substr(md5(rand()),0,6);
    // prüfen ob dbId vergeben ist
    // wenn nicht:
    // neue ID generieren und prüfen ob die frei ist. solange wiederholen bis es klappt
    ALMCoverslipLabelsPrintJobRepository::save($this);

    return $this->dbId;

  }
}

class ALMCoverslipLabelsPrintJobRepository extends ALMRepository {

  static $job_database_fields = array(
    'dbId' => 'id',
    'requestId' => 'request_id',
    'state' => 'state',
  );

  static $label_value_database_fields = array(
    'jobId' => 'job_id',
    'sampleName' => 'sample_name',
    'projectName' => 'project_name',
    'userName' => 'user_name',
    'date' => 'date',
    'stainingName' => 'staining_name',
    'handleUrl' => 'handle_url',
  );

  const JOB_DATABASE_NAME = 'alm_print_coverslip_label_jobs';
  const LABEL_VALUES_DATABASE_NAME = 'alm_print_coverslip_label_job_values';
  /**
   * Saves object to database
   *
   * @param $labelsPrintJob ALMCoverslipLabelsPrintJob Object that should be stored into the database
   * @return mixed
   */
  static public function save($labelsPrintJob) {
    db_merge(self::JOB_DATABASE_NAME)->key(array('id' => $labelsPrintJob->getDbId()))
      ->fields(array(
        self::$job_database_fields['dbId'] => $labelsPrintJob->getDbId(),
        self::$job_database_fields['requestId'] => $labelsPrintJob->getRequestId(),
        self::$job_database_fields['state'] => $labelsPrintJob->getState(),
      ))->execute();

    $labelValues = $labelsPrintJob->getLabelsValues();
    foreach ($labelValues as $label){
      db_merge(self::LABEL_VALUES_DATABASE_NAME)->key(array(
        self::$label_value_database_fields['jobId'] => $labelsPrintJob->getDbId(),
        self::$label_value_database_fields['sampleName'] => $label['sampleName'],
        ))
        ->fields(array(
          self::$label_value_database_fields['jobId'] => $labelsPrintJob->getDbId(),
          self::$label_value_database_fields['sampleName'] => $label['sampleName'],
          self::$label_value_database_fields['projectName'] => $label['projectName'],
          self::$label_value_database_fields['userName'] => $label['userName'],
          self::$label_value_database_fields['date'] => $label['date'],
          self::$label_value_database_fields['stainingName'] => $label['stainingName'],
          self::$label_value_database_fields['handleUrl'] => $label['handleUrl'],
      ))->execute();

    }

  }

  /**
   * Deletes a print job in the database (include the print job values)
   * @param $id
   */
  static public function deleteById($id){

    // Delete the print job values
    $deleteQueryJobValues = db_delete(self::LABEL_VALUES_DATABASE_NAME);
    $deleteQueryJobValues
      ->condition(self::$label_value_database_fields['jobId'], $id, '=')
      ->execute();

    // Delete the print job
    $deleteQueryJob = db_delete(self::JOB_DATABASE_NAME);
    $deleteQueryJob
      ->condition(self::$job_database_fields['dbId'], $id, '=')
      ->execute();


  }

  /**
   * @param databaseId $id
   * @return ALMCoverslipLabelsPrintJob
   */
  static public function findById($id) {
    $joinedDatabaseFields = array_merge(self::$job_database_fields, self::$label_value_database_fields);

    $query = db_select(self::JOB_DATABASE_NAME, 'j');
    $query->join(self::LABEL_VALUES_DATABASE_NAME, 'l', 'j.'. self::$job_database_fields['dbId'] . '=l.' . self::$label_value_database_fields['jobId']);
    $result = $query->condition('j.' . self::$job_database_fields['dbId'], $id, '=')
      ->fields('j')
      ->fields('l')
      ->execute();
    $result = $result->fetchAll();

    return self::databaseResultToALMCoverslipLabelPrintJob($result);
  }

  static private function databaseResultToALMCoverslipLabelPrintJob($result){
    if(empty($result)){
      return null;
    }
    $printJob = new ALMCoverslipLabelsPrintJob();

    // single values ara the values that are equal for the whole print job.
    // They containing the job_id, the request_id and the state
    $singleResultsArr = get_object_vars($result[0]);

    $printJob->setDbId($singleResultsArr[self::$job_database_fields['dbId']]);
    $printJob->setRequestId($singleResultsArr[self::$job_database_fields['requestId']]);
    $printJob->setState($singleResultsArr[self::$job_database_fields['state']]);

    // label values:
    $labelVals = array();
    foreach ($result as $label_result){
      $labelArr = get_object_vars($label_result);

      $label['jobId'] = $labelArr[self::$label_value_database_fields['jobId']]; // todo: kann eig weg
      $label['sampleName'] = $labelArr[self::$label_value_database_fields['sampleName']];
      $label['projectName'] = $labelArr[self::$label_value_database_fields['projectName']];
      $label['userName'] = $labelArr[self::$label_value_database_fields['userName']];
      $label['date'] = $labelArr[self::$label_value_database_fields['date']];
      $label['stainingName'] = $labelArr[self::$label_value_database_fields['stainingName']];
      $label['handleUrl'] = $labelArr[self::$label_value_database_fields['handleUrl']];

      $labelVals[] = $label;
    }

    $printJob->setLabelsValues($labelVals);

    return $printJob;
  }
}