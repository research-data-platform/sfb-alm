<?php
/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 30.08.2016
 * Time: 15:17
 */

class ALMRequestSample
{
  const EMPTY_SAMPLE_ID = -1;
  private static $rowNames = array('Serial #', 'Species', 'ID', 'Type', 'Genotype', 'Treatment', 'Staining type',);
  
  private $requestID;
  /**
   * @var integer, the name for the Sample, which includes the sample information
   */
  private $LabelName;
  
  /**
   * @var integer, unique persistent ID of the database object
   */
  private $id;
  
  private $serialDescription;
  private $serial;
  private $speciesDescription;
  private $speciesAbbreviation;
  private $SpeciesIdDescription;
  private $speciesIdAbbreviation;
  private $typeDescription;
  private $typeAbbreviation;
  private $genotypeDescription;
  private $genotypeAbbreviation;
  private $treatmentDescription;
  private $treatmentAbbreviation;
  private $stainingTypeDescription;
  private $stainingTypeAbbreviation;
  /**
   * Only if true, it is possible to editable the sample definition
   * @var boolean
   */
  private $editable;

  /**
   * @return bool
   */
  public function isEditable() {
    return $this->editable;
  }

  /**
   * @param bool $editable
   */
  public function setEditable($editable) {
    $this->editable = $editable;
  }


  /**
   * The highest assigned coverslip Number
   * @var int
   */
  private $highestAssignedCoverslipNo;

  public function save(){
    ALMRequestSampleRepository::save($this);
  }
  
  /**
   * @return mixed
   */
  public function getRequestID() {
    return $this->requestID;
  }

  /**
   * @param mixed $requestID
   */
  public function setRequestID($requestID) {
    $this->requestID = $requestID;
  }

  /**
   * @return The
   */
  public function getLabelName() {
    $this->generateSampleID();
    return $this->LabelName;
  }

  /**
   * @param The $LabelName
   */
  public function setLabelName($LabelName) {
    $this->LabelName = $LabelName;
  }

  /**
   * Generates a sample ID with the value of the abbreviations
   */
  public function generateSampleID() {
    $sampleID = $this->getFormattedSerial();
    $sampleID .= empty($this->speciesAbbreviation) ? '' : '_' . $this->speciesAbbreviation;
    $sampleID .= empty($this->speciesIdAbbreviation) ? '' : '_' . $this->speciesIdAbbreviation;
    $sampleID .= empty($this->typeAbbreviation) ? '' : '_' . $this->typeAbbreviation;
    $sampleID .= empty($this->genotypeAbbreviation) ? '' : '_' . $this->genotypeAbbreviation;
    $sampleID .= empty($this->treatmentAbbreviation) ? '' : '_' . $this->treatmentAbbreviation;
    $sampleID .= empty($this->stainingTypeAbbreviation) ? '' : '_' . $this->stainingTypeAbbreviation;

    $this->LabelName = $sampleID;
  }
  /**
   * @return integer
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param integer $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getSerialDescription() {
    return $this->serialDescription;
  }

  /**
   * @param mixed $serialDescription
   */
  public function setSerialDescription($serialDescription) {
    $this->serialDescription = $serialDescription;
  }

  /**
   * @return mixed
   */
  public function getSerial() {
    return $this->serial;
  }

  /**
   * Returns the serial number as a string with 4 characters
   * @return string
   */
  public function getFormattedSerial(){
    return str_pad($this->serial, 5, '0', STR_PAD_LEFT);
  }

  /**
   * @param mixed $serial
   */
  public function setSerial($serial) {
    $this->serial = $serial;
  }

  /**
   * @return mixed
   */
  public function getSpeciesDescription() {
    return $this->speciesDescription;
  }

  /**
   * @param mixed $speciesDescription
   */
  public function setSpeciesDescription($speciesDescription) {
    $this->speciesDescription = $speciesDescription;
  }

  /**
   * @return mixed
   */
  public function getSpeciesAbbreviation() {
    return $this->speciesAbbreviation;
  }

  /**
   * @param mixed $speciesAbbreviation
   */
  public function setSpeciesAbbreviation($speciesAbbreviation) {
    $this->speciesAbbreviation = $speciesAbbreviation;
  }

  /**
   * @return mixed
   */
  public function getSpeciesIdDescription() {
    return $this->SpeciesIdDescription;
  }

  /**
   * @param mixed $speciesIdDescription
   */
  public function setSpeciesIdDescription($speciesIdDescription) {
    $this->SpeciesIdDescription = $speciesIdDescription;
  }

  /**
   * @return mixed
   */
  public function getSpeciesIdAbbreviation() {
    return $this->speciesIdAbbreviation;
  }

  /**
   * @param mixed $speciesIdAbbreviation
   */
  public function setSpeciesIdAbbreviation($speciesIdAbbreviation) {
    $this->speciesIdAbbreviation = $speciesIdAbbreviation;
  }

  /**
   * @return mixed
   */
  public function getTypeDescription() {
    return $this->typeDescription;
  }

  /**
   * @param mixed $typeDescription
   */
  public function setTypeDescription($typeDescription) {
    $this->typeDescription = $typeDescription;
  }

  /**
   * @return mixed
   */
  public function getTypeAbbreviation() {
    return $this->typeAbbreviation;
  }

  /**
   * @param mixed $typeAbbreviation
   */
  public function setTypeAbbreviation($typeAbbreviation) {
    $this->typeAbbreviation = $typeAbbreviation;
  }

  /**
   * @return mixed
   */
  public function getGenotypeDescription() {
    return $this->genotypeDescription;
  }

  /**
   * @param mixed $genotypeDescription
   */
  public function setGenotypeDescription($genotypeDescription) {
    $this->genotypeDescription = $genotypeDescription;
  }

  /**
   * @return mixed
   */
  public function getGenotypeAbbreviation() {
    return $this->genotypeAbbreviation;
  }

  /**
   * @param mixed $genotypeAbbreviation
   */
  public function setGenotypeAbbreviation($genotypeAbbreviation) {
    $this->genotypeAbbreviation = $genotypeAbbreviation;
  }

  /**
   * @return mixed
   */
  public function getTreatmentDescription() {
    return $this->treatmentDescription;
  }

  /**
   * @param mixed $treatmentDescription
   */
  public function setTreatmentDescription($treatmentDescription) {
    $this->treatmentDescription = $treatmentDescription;
  }

  /**
   * @return mixed
   */
  public function getTreatmentAbbreviation() {
    return $this->treatmentAbbreviation;
  }

  /**
   * @param mixed $treatmentAbbreviation
   */
  public function setTreatmentAbbreviation($treatmentAbbreviation) {
    $this->treatmentAbbreviation = $treatmentAbbreviation;
  }

  /**
   * @return mixed
   */
  public function getStainingTypeDescription() {
    return $this->stainingTypeDescription;
  }

  /**
   * @param mixed $stainingTypeDescription
   */
  public function setStainingTypeDescription($stainingTypeDescription) {
    $this->stainingTypeDescription = $stainingTypeDescription;
  }

  /**
   * @return mixed
   */
  public function getStainingTypeAbbreviation() {
    return $this->stainingTypeAbbreviation;
  }

  /**
   * @param mixed $stainingTypeAbbreviation
   */
  public function setStainingTypeAbbreviation($stainingTypeAbbreviation) {
    $this->stainingTypeAbbreviation = $stainingTypeAbbreviation;
  }

  /**
   * @return int
   */
  public function getHighestAssignedCoverslipNo() {
    return $this->highestAssignedCoverslipNo;
  }

  /**
   * Return a free coverslip number and increment the highestAssignedCoverslipNo Value.
   *
   * This does not automatically store the new value in the database!!
   *
   */
  public function assignNewCoverslipNo(){

    if($this->highestAssignedCoverslipNo == null){
      $this->highestAssignedCoverslipNo = 0;
    }

    $this->highestAssignedCoverslipNo++;
    return $this->highestAssignedCoverslipNo;
  }

  /**
   * Unset the database id.
   * ATTENTION: The object gets a new id if you try to save it.
   */
  public function resetDatabaseId(){
    $this->id = null;
  }

  /**
   * @param int $highestAssignedCoverslipNo
   */
  public function setHighestAssignedCoverslipNo($highestAssignedCoverslipNo) {
    $this->highestAssignedCoverslipNo = $highestAssignedCoverslipNo;
  }

  /**
   *  Assign the next (free) sample serial number to the serial object, if the serial (of the object) is unset currently
   *
   * Throws exception if the serial has been set already
   */
  public function assignSerialNumber(){
    if(isset($this->serial)){
      throw new Exception('The Serial Number has already been set');
    } else {
      $this->assignNewSerialNumber();
    }
  }

  public function assignNewSerialNumber(){
    $newSerial = variable_get('alm_sample_next_serial', 1);
    $this->setSerial($newSerial);
    $newSerial++;
    variable_set('alm_sample_next_serial', $newSerial);
  }
  /**
   * @return array
   */
  public static function getRowNames() {
    return self::$rowNames;
  }



  ///////////// Values for autocomplete ///////////////
  static function getSpeciesDescriptionOptions(){
    return array('human', 'mouse', 'rat', 'cell line');
  }
  
  static function getSpeciesAbbreviationOptions(){
    return array('h','m','r','c');
  }

  static function getIdDescriptionOptions(){
    return array('animal number','patient','passage');
  }

  static function getTypeDescriptionOptions(){
    return array(
      'ventricular myocytes',
      'atrial myocytes',
      'skeletal muscle',
      'fibroblasts',
      'HeLa cells',
      'HEK cells',
      'neonatal rat cardiomyocytes',
      'neonatal rat fibroblasts',
      'induced pluripotent stem cells',
      'C2C12 cells',
      'heart slice',
      );
  }

  static function getTypeAbbreviationOptions(){
    return array(
      'VM',
      'AM',
      'SM',
      'Fib',
      'HeLa',
      'HEK',
      'NRCM',
      'NRF',
      'iPSC',
      'C2C1',
      'hrt',
    );
  }

  static function getGenotypeDescriptionOptions(){
    return array('Pyrat or Tierbase name');
  }

  static function getGenotypeAbbreviationOptions(){
    return array(
      'RyR2D3',
      'JphKO',
      'CavRE1',
    );
  }

  static function getTreatmentDescriptionOptions(){
    return array(
      'TAC operated',
      'MI operated',
      'sham operated',
      'isoprenaline treated',
      'wild type',
      'knock out',
      'homozygous',
      'heterozygous',

    );
  }

  static function getTreatmentAbbreviationOptions(){
    return array(
      'TAC',
      'MI',
      'sham',
      'Iso',
      'wt',
      'KO',
      'hom',
      'het',
    );
  }

  static function getStainingTypeDescriptionOptions(){
    return array('immunofluorescence', 'immunohistochemistry',);
  }

  static function getStainingTypeAbbreviationOptions(){
    return array('IF', 'IHC',);
  }

  static function getUserDescriptionOptions(){
    $userNames = array();
    $users = UsersRepository::findAll();
    foreach($users as $user){
      $userNames[] = $user->getFullname();
    }
    return $userNames;
  }

  static function getCoverSlipDescriptionOptions(){
    return array('consecutive numbers');
  }
}
