<?php
/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 17.05.2018
 * Time: 16:47
 */

class ALMUserQualificationRepository {
  /**
   * Qualification Level, which represents, that a user is allowed to consult his own requests
   * @var int
   */
  public static $QUALIFICATION_LEVEL_USER_CONSULTATION = 200;

  /**
   * Qualification Level, which represents, that a user is NOT allowed to consult his requests without a Manager
   * @var int
   */
  public static $QUALIFICATION_LEVEL_MANAGER_CONSULTATION = 100;

  /**
   * Qualification Level, for users that are not in the database
   * @var int
   */
  public static $QUALIFICATION_LEVEL_UNSET = -1;

  static $databaseFields = array(
    'userId' => 'user_id',
    'qualificationLevel' => 'qualification_level',
  );
  const DATABASE_NAME = 'alm_user_qualification';

  /****************************************************************************/
  public static function getQualificationLevelOfUserId($userId) {
    $result = db_select(self::DATABASE_NAME, 'q')
      ->condition('q.' . self::$databaseFields['userId'], $userId, '=')
      ->fields('q', array('qualification_level'))
      ->range(0, 1)
      ->execute();

    $result_value = $result->fetch();

    if ($result_value) {
      $qualification_lvl = $result_value->qualification_level;
      return $qualification_lvl;
    } else {
      return self::$QUALIFICATION_LEVEL_UNSET;
    }
  }

  public static function setQualificationLevelOfUserId($userId, $level){
    db_merge(self::DATABASE_NAME)->key(array('user_id' => $userId))
      ->fields(array(
        self::$databaseFields['qualificationLevel'] => $level,
        self::$databaseFields['userId'] => $userId)
      )->execute();

    return Database::getConnection()->lastInsertId();
  }

}