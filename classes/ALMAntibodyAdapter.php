<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 05.06.2017
 * Time: 19:03
 */
class ALMAntibodyAdapter
{
    public static function createRenderableAntibodySelect2Field($antibodyId, $options){
        $renderableAntibodySelectField = module_invoke(
            'sfb_antibody',
            'rdp_linking_antibody_antibody2_edit_get',
            array($antibodyId),                       // The antibody is required as array
            $options
            );

        return $renderableAntibodySelectField;
    }

    public static function createRenderablePrimaryAntibodySelect2Field($antibodyId, $options){
      $options['type'] = 'primary';
      return SELF::createRenderableAntibodySelect2Field($antibodyId, $options);
    }

  public static function createRenderableSecondaryAntibodySelect2Field($antibodyId, $options){
    $options['type'] = 'secondary';
    return SELF::createRenderableAntibodySelect2Field($antibodyId, $options);
  }

    /*
     *
     */
    public static function getALMAntibodyObjectById($antibodyId){
        $originalAntibodyObject = module_invoke('sfb_antibody','object_get', $antibodyId);
        if($originalAntibodyObject->getId() == Antibody::EMPTY_ANTIBODY_ID){
            throw new NoAntibodyFoundException('Antibody does not exist in database!');
        }
        return new ALMAntibody($originalAntibodyObject);
    }

  /**
   * Checks if an Antibody ID exists in the Database.
   * Return true if the AntibodyID exists
   * Return false if  the AntibodyID does not exist
   * @param $antibodyId
   * @return bool
   */
    public static function existsAntibdyId($antibodyId){
      try {
        ALMAntibodyAdapter::getALMAntibodyObjectById($antibodyId);
        return true;
      }
      catch (NoAntibodyFoundException $e){
        return false;
      }
    }

  /**
   * Returns an array for an antibody table with:
   * [table][header]
   * [table][rows]
   *
   * use theme('table', arr['table']) to theme the returned table
   *
   * @param $antibodyIds
   * @return array
   */
    public static function getAntibodyViewTable($antibodyIds){
      return module_invoke('sfb_antibody', 'rdp_linking_antibody_antibody2_view_get', $antibodyIds, true);
    }
}

class NoAntibodyFoundException extends Exception {
}