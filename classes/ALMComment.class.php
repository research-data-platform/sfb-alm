<?php

/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 17.08.2016
 * Time: 17:04
 */

/**
 * Class ALMComment
 * Comment Object, every request can have several comments.
 * But every request should only have one comment in every edit state. 
 */
class ALMComment
{
  const EMPTY_COMMENT_ID = -1;
  
  private $commentID = self::EMPTY_COMMENT_ID;
  private $requestID;
  private $date;
  private $message;
  private $typeID;

  /**
   * @return mixed
   */
  public function getCommentID() {
    return $this->commentID;
  }

  /**
   * @param mixed $commentID
   */
  public function setCommentID($commentID) {
    $this->commentID = $commentID;
  }

  /**
   * @return integer
   */
  public function getRequestID() {
    return $this->requestID;
  }

  /**
   * @param mixed $requestID
   */
  public function setRequestID($requestID) {
    $this->requestID = $requestID;
  }

  /**
   * @return mixed
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * @param mixed $date
   */
  public function setDate($date) {
    $this->date = $date;
  }

  /**
   * Sets the current Date to the Date Variable
   */
  public function setCurrentDate() {
    $this->date = date(SFB_ALM_DATEFORMAT);
  }

  /**
   * @return mixed
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * @param mixed $message
   */
  public function setMessage($message) {
    $this->message = $message;
  }

  /**
   * @return int ALMCommentType
   */
  public function getTypeID() {
    return $this->typeID;
  }

  /**
   * @param int $typeID ALMCommentType
   */
  public function setTypeID($typeID) {
    $this->typeID = $typeID;
  }

  public function isRequestIdSet(){
    return isset($this->requestID);
  }

  public function getTypeText(){
    return ALMCommentType::getText($this->typeID);
  }

  /**
   * Test if instance has been saved to repository.
   *
   * @return bool
   */
  public function isEmpty() {
    return $this->getCommentID() == self::EMPTY_COMMENT_ID ? TRUE : FALSE;
  }

  /**
   * Store instance in repository. Update ID if instance has not been stored before.
   */
  public function save() {
    $id = ALMCommentRepository::save($this);
    if ($this->isEmpty()) {
      $this->setCommentID($id);
    }
  }

}