<?php
/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 11.08.2016
 * Time: 14:28
 */

class ALMLog {
  private $action;
  private $message;
  private $userID;
  private $date;
  private $requestID;
  private $logID;
  /**
   * @var boolean, declares if a log entry is visible for the users.
   * The default value is true.
   */
  private $isVisible = 1;

  // Log actions
  /**
   * @var string A new request is saved the first time
   */
  static $ACTION_CREATE = 'Create request';
  static $ACTION_SUBMIT = 'Submit request';
  static $ACTION_SAVE = 'Save request';
  static $ACTION_REVISE = 'Revise request';
  static $ACTION_WITHDRAW = 'Withdraw request';
  static $ACTION_ACCEPT = 'Accept request';

  static $ACTION_FINISH = 'Finish request';
  /**
   * @var string The request was loaded by the user, so the view or the edit page was loaded
   */
  static $ACTION_USER_VIEW = 'view request';
  /**
   * @var string The request was loaded by a manager, so the view or the edit page was loaded
   */
  static $ACTION_MANAGER_VIEW = 'Manager view request';
  /**
   * @var string An accepted request is ready consulted and the state change to run
   */
  static $ACTION_CONSULT = 'Consult request';
  /**
   * @var string a running request is resettet to the consultation state and becomes editabel again.
   */
  static $ACTION_MANAGER_RESET_TO_ACCEPTED = 'Reset the request to accepted state';

  /**
   * @var string A dataset is uploaded.
   */
  static $ACTION_DATASET_UPLOAD = 'Dataset upload';

  static $ACTION_DATASET_DELETE = 'Dataset deletion';

  /**
   * @var string A staining was added in state running
   */
  static $ACTION_ADD_STAINING = 'Staining added';

  /**
   * @var string A sample was added in state running
   */
  static $ACTION_ADD_SAMPLE = 'Sample added';

  /**
   * @return string
   */
  public function getUserName(){
    return UsersRepository::findByUid($this->getUserID())->getFullname();
}
  /**
   * @return mixed
   */
  public function getRequestID() {
    return $this->requestID;
  }

  /**
   * @param mixed $requestID
   */
  public function setRequestID($requestID) {
    $this->requestID = $requestID;
  }

  /**
   * @return mixed
   */
  public function getLogID() {
    return $this->logID;
  }

  /**
   * @param mixed $logID
   */
  public function setLogID($logID) {
    $this->logID = $logID;
  }
  
  /**
   * @return mixed
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * @param mixed $date
   */
  public function setDate($date) {
    $this->date = $date;
  }

  /**
   * Set date to the current date.
   */
  public function setCurrentDate() {
    $this->date = date(SFB_ALM_DATEFORMAT);
  }

  /**
   * @return mixed
   */
  public function getAction() {
    return $this->action;
  }

  /**
   * @param mixed $action
   */
  public function setAction($action) {
    $this->action = $action;
  }

  /**
   * @return mixed
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * @param mixed $message
   */
  public function setMessage($message) {
    $this->message = $message;
  }

  /**
   * @return mixed
   */
  public function getUserID() {
    return $this->userID;
  }

  /**
   * @param mixed $userID
   */
  public function setUserID($userID) {
    $this->userID = $userID;
  }

  /**
   *
   * @return mixed
   */
  public function getIsVisible() {
    return $this->isVisible;
  }

  /**
   * @param mixed $isVisible
   */
  public function setIsVisible($isVisible) {
    $this->isVisible = $isVisible;
  }

  /**
   * Saves the log entry in the database.
   */
  public function save(){
    ALMLogRepository::save($this);
  }


}