<?php

/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 16.08.2016
 * Time: 15:15
 */
class ALMLogRepository
{
  static $databaseFields = array(
    'id' => 'id',
    'request_id' => 'request_id',
    'date' => 'date',
    'action' => 'action',
    'message' => 'message',
    'user_id' => 'user_id',
    'is_visible' => 'is_visible',
  );

  const DATABASE_NAME = 'alm_request_log';
  const EMPTY_DATE = -1;


  /**
   * @param $result
   * @return ALMLog
   */
  private static function databaseResultToALMLog($result) {
    $log = new ALMLog();

    // convert results object to an associative array
    $results_arr = get_object_vars($result);
    // set log variables with data from database result
    $log->setLogID($results_arr[self::$databaseFields['id']]);
    $log->setRequestID($results_arr[self::$databaseFields['request_id']]);
    $log->setDate($results_arr[self::$databaseFields['date']]);
    $log->setAction($results_arr[self::$databaseFields['action']]);
    $log->setMessage($results_arr[self::$databaseFields['message']]);
    $log->setUserID($results_arr[self::$databaseFields['user_id']]);
    $log->setIsVisible($results_arr[self::$databaseFields['is_visible']]);

    return $log;
  }

  /**
   * Saves the given log object to the database. If the database contains a object with the same id, the object will be overwritten.
   * Returns the last ID that insterted in the database.
   *
   * @param $log ALMLog
   * @return string last ID that insterted in the database.
   *
   * @throws Exception
   * @throws InvalidMergeQueryException
   */
  public static function save($log) {
    db_merge(self::DATABASE_NAME)->key(array('id' => $log->getLogID()))
      ->fields(array(
        //TODO: FOREIGN key

        self::$databaseFields['id'] => self::setEmptyVariablesToNull($log->getLogID()),
        self::$databaseFields['request_id'] => self::setEmptyVariablesToNull($log->getRequestID()),
        self::$databaseFields['date'] => self::setEmptyVariablesToNull($log->getDate()),
        self::$databaseFields['action'] => self::setEmptyVariablesToNull($log->getAction()),
        self::$databaseFields['message'] => self::setEmptyVariablesToNull($log->getMessage()),
        self::$databaseFields['user_id'] => self::setEmptyVariablesToNull($log->getUserID()),
        self::$databaseFields['is_visible'] => self::setEmptyVariablesToNull($log->getIsVisible()),

      ))->execute();

    return Database::getConnection()->lastInsertId();
  }

  /**
   * Sets empty variable (when the variabl is '' or ALM_FORM_SELECT_EMPTY) to null
   *
   * @param $input
   * @return null
   */
  private static function setEmptyVariablesToNull($input) {
    if ($input == ALM_FORM_SELECT_EMPTY || $input == '') {
      return NULL;
    } else {
      return $input;
    }
  }

  /**
   * Search in the log database for all entries with a given requestID which are visible (is_visible = 1).
   * @param $requestID
   * @return ALMLog[]
   */
  public static function findAllVisibleLogEntriesByRequestID($requestID) {
    $result = db_select(self::DATABASE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['request_id'], $requestID, '=')
      ->condition('r.' . self::$databaseFields['is_visible'], true, '=')
      ->orderBy(self::$databaseFields['date'], 'DESC')
      ->fields('r', self::$databaseFields)
      ->execute();

    return ALMLogRepository::databaseResultsToALMLogs($result);
  }

  /**
   * Search in the log database for all entries with a given requestID.
   * @param $requestID
   * @return ALMLog[]
   */
  public static function findAllLogEntriesByRequestID($requestID) {
    $result = db_select(self::DATABASE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['request_id'], $requestID, '=')
      ->orderBy(self::$databaseFields['date'], 'DESC')
      ->fields('r', self::$databaseFields)
      ->execute();

    return ALMLogRepository::databaseResultsToALMLogs($result);
  }


  /**
   * Returns array with log objects for the given database results
   * If results is emtpy, the return value is an empty array.
   * @param $results
   * @return ALMRequest[]
   */
  private static function databaseResultsToALMLogs($results) {
    $log = array();
    // Check if the results are empty, if it is, returns an empty array.
    if (empty($results)) {
      return $log;
    }

    foreach ($results as $result) {
      $log[] = ALMLogRepository::databaseResultToALMLog($result);
    }

    return $log;
  }

  /**
   * Returns log entrys from a specific user.
   * Currently this method is not in use.
   * @param $logAction
   * @param $userId
   * @param $requestId
   * @return ALMRequest[]
   */
  public static function getAllLogEntriesWithActionUserIdAndRequestId($logAction, $userId, $requestId) {
    $result = db_select(self::DATABASE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['action'], $logAction, '=')
      ->condition('r.' . self::$databaseFields['user_id'], $userId, '=')
      ->condition('r.' . self::$databaseFields['request_id'], $requestId, '=')
      ->fields('r', self::$databaseFields)
      ->execute();

    return ALMLogRepository::databaseResultsToALMLogs($result);
  }


  /**
   * Search in the log database for the latest date of an action with a requestID.
   * @param $requestID
   * @param $logAction
   * @return string Date oder @see ALMLogRepository::
   */
  public static function getLatestActionDateByRequestID($requestID, $logAction) {
    $result = db_select(self::DATABASE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['request_id'], $requestID, '=')
      ->condition('r.' . self::$databaseFields['action'], $logAction, '=')
      ->orderBy(self::$databaseFields['date'], 'DESC')
      ->fields('r', array(self::$databaseFields['date']))
      ->range(0, 1)
      ->execute()
      ->fetch();

    if (is_object($result)) {
      $results_arr = get_object_vars($result);
      return $results_arr[self::$databaseFields['date']];
    }

    return self::EMPTY_DATE;
  }


}
