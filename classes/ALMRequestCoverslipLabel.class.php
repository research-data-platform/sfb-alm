<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 06.11.2017
 * Time: 19:34
 *
 * An ALMRequestCoverslipLabel Object represents the information for one
 * coverslip, which contains the staining and the sample
 */
class ALMRequestCoverslipLabel {

  /**
   * Default PID prefix
   */
  const DEFAULT_PID_PREFIX = '';

  static $STATE_VISIBLE = 10;

  static $STATE_HIDDEN = 30;

  static $STATE_DELETED = 100;


  /**
   * The ID of the row in the alm_request_coverslip_labels database.
   *
   * @var integer
   */
  private $dbId;

  /**
   * The unique PID of the coverslip
   *
   * @var string
   */
  private $pid;

  /**
   * The ID of the associated request
   *
   * @var integer
   */
  private $requestId;

  /**
   * The ID of the associated sample
   *
   * @var integer
   */
  private $sampleId;

  /**
   * The number of the coverslip, to distinguish from other equal coverslips
   * (same sample and staining). (All equal coverslips are counted)
   *
   * @var integer
   */
  private $coverslipNo;

  /**
   * The ID of the associated staining
   *
   * @var integer
   */
  private $stainingId;

  /**
   * The current state of a coverslip label.
   * 10: Visible
   * ??: Hidden
   * ??: Deleted
   *
   * @var integer
   */
  private $state;

  /**
   * Shows if a label was printed or not. If a label was printed, is it not
   * allowed to delete it anymore.
   *
   * @var boolean
   */
  private $printed;

  /**
   * @return int
   */
  public function getDbId() {
    return $this->dbId;
  }

  /**
   * @param int $dbId
   */
  public function setDbId($dbId) {
    $this->dbId = $dbId;
  }

  /**
   * @return string
   */
  public function getPid() {
    return $this->pid;
  }

  /**
   * @param string $pid
   */
  public function setPid($pid) {
    $this->pid = $pid;
  }

  /**
   * @return int
   */
  public function getRequestId() {
    return $this->requestId;
  }

  /**
   * @param int $requestId
   */
  public function setRequestId($requestId) {
    $this->requestId = $requestId;
  }

  /**
   * @return int
   */
  public function getSampleId() {
    return $this->sampleId;
  }

  /**
   * @param int $sampleId
   */
  public function setSampleId($sampleId) {
    $this->sampleId = $sampleId;
  }

  /**
   * @return int
   */
  public function getCoverslipNo() {
    return $this->coverslipNo;
  }

  public function getFormattedCoverslipNo() {
    return str_pad($this->coverslipNo, 2, '0', STR_PAD_LEFT);
  }

  /**
   * @param int $coverslipNo
   */
  public function setCoverslipNo($coverslipNo) {
    $this->coverslipNo = $coverslipNo;
  }

  /**
   * @return int
   */
  public function getStainingId() {
    return $this->stainingId;
  }

  /**
   * @param int $stainingId
   */
  public function setStainingId($stainingId) {
    $this->stainingId = $stainingId;
  }

  /**
   * @return int
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @param int $state
   */
  public function setState($state) {
    $this->state = $state;
  }

  /**
   * @return bool
   */
  public function isPrinted() {
    return $this->printed;
  }

  /**
   * @param bool $printed
   */
  public function setPrinted($printed) {
    $this->printed = $printed;
  }


  ///////////// Functions /////////////

  /**
   * Generates the PID with the associated Data
   * The PID contains the formatted serial xxxxx concatenated with the formatted
   * coverslip No xx
   */
  public function generatePidSuffix() {
    $sample = ALMRequestSampleRepository::findBySampleId($this->sampleId);
    $newPid = $sample->getFormattedSerial() . '-' . $this->getFormattedCoverslipNo();

    // Throws Exception if the PID is longer than the max length of PID
    if (strlen($newPid) == 8) {
      return $newPid;
    }
    else {
      throw new Exception('Wrong length of PID');
    }
  }

  /**
   * Returns the String representation of the staining
   *
   * @return string
   */
  public function getSampleName() {
    $sample = ALMRequestSampleRepository::findBySampleId($this->sampleId);
    return $sample->getLabelName();
  }

  /**
   * Returns the String representation of the staining
   *
   * @return string
   */
  public function getStainingName() {
    $staining = ALMRequestStainingRepository::findByStainingId($this->stainingId);
    return $staining->getLabelName();
  }

  /*
   * Init a new Coverslip Label:
   * Set printed to false
   * Set state to visible
   */
  public function initNew() {
    $this->state = self::$STATE_VISIBLE;
    $this->printed = FALSE;
  }

  public function makeVisible() {
    $this->state = self::$STATE_VISIBLE;
  }

  /**
   * Changes the state of the coverslip label to Hide
   */
  public function hide() {
    $this->state = self::$STATE_HIDDEN;
  }

  /**
   * Create and register Persistent Identifier
   */
  public function createPid() {
    //
    // ------  ------
    //
    // generates the PID with the stainingID and the CoverslipNo
    $pidSuffix = $this->generatePidSuffix();

    if (module_exists("epic_pid")) {

      //
      // Get system setting variables to build PID
      //
      $prefix = variable_get("alm_coverslip_label_pid_prefix", self::DEFAULT_PID_PREFIX);
      $pidServiceId = variable_get("alm_coverslip_label_pid_service", EpicPidService::EMPTY_ID);

      //
      // Create the PID object and set the target URL to
      // ALMRequest public landing page
      //
      $pid = new EpicPid();
      $pid->setId($prefix . $pidSuffix);
      $pidService = EpicPidServiceRepository::findById($pidServiceId);
      $pid->setServiceId($pidService->getId());
      $request = ALMRequestsRepository::findById($this->requestId);
      $pid->setTargetUrl($request->getLandingPageUrl());

      // ToDo: Error handling!
      $pid->save();
    }
    else {
      // ToDo: How should PID service failures be handled?
      $pid = new ALMPidStub();
      $pid->register($pidSuffix);
    }

    $this->setPid($pidSuffix);
  }

  /**
   * Fetch and return the instance of @see \EpicPid that is
   * registered for this coverslip.
   *
   * @return \EpicPid
   */
  public function getPidObject() {
    if (module_exists("epic_pid") && !empty($this->getPid())) {
      $prefix = variable_get("alm_coverslip_label_pid_prefix", self::DEFAULT_PID_PREFIX);
      $suffix = $this->getPid();
      $pid_object = EpicPidRepository::findById($prefix . $suffix);
      if (!$pid_object->isEmpty()) {
        return $pid_object;
      }
    }
    else {
      // Todo: 29/June/18 wäre hier nicht eine Exception angebrachter?
      return NULL;
    }
  }

  /**
   * Tries to change the state of the coverslip label to deleted.
   * Throws exception if the coverslip label cannot deleted (if it was printed
   * already)
   */
  public function delete() {
    if ($this->printed) {
      throw CannotDeletePrintedLabelException('Printed Labels cannot deleted anymore');
    }
    else {
      $this->state = self::$STATE_DELETED;
    }
  }


  /**
   * Createrns a describing name for the coverslip, which contains the (unique) sampleId with coverslip id and the used
   * staining.
   * @return string
   */
  public function getCoverslipDescripingName(){
    return $this->getSampleName().'_'.$this->getFormattedCoverslipNo().' (Staining: '. $this->getStainingName().')';
  }

  public function save() {
    ALMRequestCoverslipLabelRepository::save($this);
  }
}

class CannotDeletePrintedLabelException extends Exception {

}

class ALMRequestCoverslipLabelRepository extends ALMRepository {

  static $databaseFields = [
    'id' => 'id',
    'pid' => 'pid',
    'requestId' => 'request_id',
    'sampleId' => 'sample_id',
    'coverslip' => 'coverslip',
    'stainingId' => 'staining_id',
    'state' => 'state',
    'printed' => 'printed_flag',
  ];

  const DATABASE_NAME = 'alm_request_coverslip_labels';

  /**
   * Finds all CoverslipLabels that are associated with a requestID in the
   * database. Returns array of ALMRequestCoverslipLabel Objects
   *
   * @param $requestId int
   */
  public static function findAllByRequestId($requestId) {
    // todo
    drupal_set_message('ALMRequestCoverslipLable->findAllByRequestId is not implemented yet');
  }

  /**
   * Finds all CoverslipLabels that are associated with the $requestID and have
   * the $state in the Database
   *
   * @param $requestId
   * @param $state
   *
   * @return ALMRequestCoverslipLabel[]
   */
  public static function findAllByRequestIdAndState($requestId, $state) {
    $results = db_select(self::DATABASE_NAME, 'l')
      ->condition('l.' . self::$databaseFields['requestId'], $requestId, '=')
      ->condition('l.' . self::$databaseFields['state'], $state, '=')
      ->fields('l', self::$databaseFields)
      ->execute();

    return self::databaseResultsToALMCoverslipLabel($results);
  }

  /**
   * Finds all CoverslipLabels with state 'Visible' that are associated with
   * the $requestID in the Database
   *
   * @param $requestId
   *
   * @return ALMRequestCoverslipLabel[]
   */
  public static function findAllVisibleByRequestId($requestId) {
    return self::findAllByRequestIdAndState($requestId, ALMRequestCoverslipLabel::$STATE_VISIBLE);
  }

  public static function findById($coverslipLabelDbId) {
    $result = db_select(self::DATABASE_NAME, 'l')
      ->condition('l.' . self::$databaseFields['id'], $coverslipLabelDbId, '=')
      ->fields('l', self::$databaseFields)
      ->execute()
      ->fetch();

    return self::databaseResultToALMCoverslipLabel($result);
  }

  /**
   * Retrieve a CoverslipLabel by a given Persistent Identifier ID-string
   *
   * @param string $pid
   *
   * @return \ALMRequestCoverslipLabel
   */
  public static function findByPID(string $pid) {
    $result = db_select(self::DATABASE_NAME, 'l')
      ->condition('l.' . self::$databaseFields['pid'], $pid, '=')
      ->fields('l', self::$databaseFields)
      ->execute()
      ->fetch();

    return self::databaseResultToALMCoverslipLabel($result);
  }

  /**
   * Stores a ALMRequestCoverslipLabel Object in the database
   *
   * @param $coverslipLabel ALMRequestCoverslipLabel
   *
   * @return int returns the lastInputID of the database
   */
  public static function save($coverslipLabel) {
    if (isset($coverslipLabel)) {
      db_merge(self::DATABASE_NAME)
        ->key(['id' => $coverslipLabel->getDbId()])
        ->fields([

          self::$databaseFields['id'] => self::setEmptyVariablesToNull($coverslipLabel->getDbId()),
          self::$databaseFields['pid'] => self::setEmptyVariablesToNull($coverslipLabel->getPid()),
          self::$databaseFields['requestId'] => self::setEmptyVariablesToNull($coverslipLabel->getRequestId()),
          self::$databaseFields['sampleId'] => self::setEmptyVariablesToNull($coverslipLabel->getSampleId()),
          self::$databaseFields['coverslip'] => self::setEmptyVariablesToNull($coverslipLabel->getCoverslipNo()),
          self::$databaseFields['stainingId'] => self::setEmptyVariablesToNull($coverslipLabel->getStainingId()),
          self::$databaseFields['state'] => self::setEmptyVariablesToNull($coverslipLabel->getState()),
          self::$databaseFields['printed'] => (int) $coverslipLabel->isPrinted(),

        ])->execute();

      return Database::getConnection()->lastInsertId();
    }
  }

  /**
   * Creates array of ALMRequestCoverslipLabel objects from the database results
   *
   * @param $results
   *
   * @return ALMRequestCoverslipLabel[]
   */
  private static function databaseResultsToALMCoverslipLabel($results) {
    $coverslipLabels = [];
    foreach ($results as $result) {
      $coverslipLabels[] = self::databaseResultToALMCoverslipLabel($result);
    }

    return $coverslipLabels;
  }

  /**
   * Create ALMRequestCoverslipLabel objects from the database result
   *
   * @param $result
   *
   * @return ALMRequestCoverslipLabel
   */
  private static function databaseResultToALMCoverslipLabel($result) {
    $coverslipLabel = new ALMRequestCoverslipLabel();

    $resultArr = get_object_vars($result);

    $coverslipLabel->setDbId($resultArr[self::$databaseFields['id']]);
    $coverslipLabel->setPid($resultArr[self::$databaseFields['pid']]);
    $coverslipLabel->setRequestId($resultArr[self::$databaseFields['requestId']]);
    $coverslipLabel->setSampleId($resultArr[self::$databaseFields['sampleId']]);
    $coverslipLabel->setCoverslipNo($resultArr[self::$databaseFields['coverslip']]);
    $coverslipLabel->setStainingId($resultArr[self::$databaseFields['stainingId']]);
    $coverslipLabel->setState($resultArr[self::$databaseFields['state']]);
    $coverslipLabel->setPrinted($resultArr[self::$databaseFields['printed']]);

    return $coverslipLabel;
  }

  /**
   * @return \ALMRequestCoverslipLabel[] All ALMRequestCoverslipLabel objects
   *   with a PID value.
   */
  public static function findAllPid() {
    $results = db_select(self::DATABASE_NAME, 'l')
      ->condition('l.' . self::$databaseFields['pid'], "NULL", '!=')
      ->fields('l', self::$databaseFields)
      ->execute();

    return self::databaseResultsToALMCoverslipLabel($results);
  }

}