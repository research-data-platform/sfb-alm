<?php

/**
 * Class ALMMessage
 *
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 *
 */
class ALMMessage {

  /**
   * Array with database fields. Required by ALMMessagesRepository
   *
   * @var array
   */
  static $databaseFields = array('id', 'created', 'readd', 'recipient', 'sender', 'title', 'message', 'responseto', 'archived', 'deleted', 'sender_hide');
  const EMPTY_MESSAGE_ID = -1;

  /**
   * Database message ID
   *
   * @var  int
   */
  private $id = ALMMessage::EMPTY_MESSAGE_ID;

  /**
   * @var string
   */
  private $created_date;

  /**
   * @var string
   */
  private $read_date;

  /**
   * @var int
   */
  private $recipientId = User::USER_NOT_SET;

  /**
   * @var int
   */
  private $senderId = User::USER_NOT_SET;

  /**
   * @var string
   */
  private $title;

  /**
   * @var string
   */
  private $message;

  /**
   * ID of replied message
   *
   * @var int
   */
  private $responseToMessageId = ALMMessage::EMPTY_MESSAGE_ID;

  /**
   * @var boolean
   */
  private $archived = false;

  /**
   * @var boolean
   */
  private $deleted = false;

  /**
   * @var boolean
   */
  private $senderHide = false;

  /**
  * @return int
  */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getCreatedDate() {
    return $this->created_date;
  }

  /**
   * @param string $created_date
   */
  public function setCreatedDate($created_date) {
    $this->created_date = $created_date;
  }

  /**
   * @return string
   */
  public function getReadDate() {
    return $this->read_date;
  }

  /**
   * @param string $read_date
   */
  public function setReadDate($read_date) {
    $this->read_date = $read_date;
  }

  /**
   * @return int
   */
  public function getRecipientId() {
    return $this->recipientId;
  }

  /**
   * @param int $recipientId
   */
  public function setRecipientId($recipientId) {
    $this->recipientId = $recipientId;
  }

  /**
   * @return int
   */
  public function getSenderId() {
    return $this->senderId;
  }

  /**
   * @param int $senderId
   */
  public function setSenderId($senderId) {
    $this->senderId = $senderId;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @param string $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * @return string
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * @param string $message
   */
  public function setMessage($message) {
    $this->message = $message;
  }

  /**
   * @return intr
   */
  public function getResponseToMessageId() {
    return $this->responseToMessageId;
  }

  /**
   * @param intr $responseToMessageId
   */
  public function setResponseToMessageId($responseToMessageId) {
    $this->responseToMessageId = $responseToMessageId;
  }

  /**
   * @return boolean
   */
  public function isArchived() {
    return $this->archived;
  }

  /**
   * @param boolean $archived
   */
  public function setArchived($archived) {
    $this->archived = $archived;
  }

  /**
   * @return boolean
   */
  public function isDeleted() {
    return $this->deleted;
  }

  /**
   * @param boolean $deleted
   */
  public function setDeleted($deleted) {
    $this->deleted = $deleted;
  }

  /**
   * @return boolean
   */
  public function isSenderHide() {
    return $this->senderHide;
  }

  /**
   * @param boolean $senderHide
   */
  public function setSenderHide($senderHide) {
    $this->senderHide = $senderHide;
  }

  /**
   * Returns true if message is empty. That means, that there is no equivalent
   * message in the database.
   * In other case false.
   *
   * @return bool
   */
  public function isEmpty() {
    if($this->id != ALMMessage::EMPTY_MESSAGE_ID) {
      return false;
    }
    return true;
  }

  /**
   * Returns true if user:
   * - is sender (creator) of this message
   * - is recipient of this message
   *
   * In other case false.
   *
   * @param $type
   * @param null $userId
   * @return bool
   */
  public function userHasPermission($type, $userId = null) {
    // there is no edit option, because emails cannot be saved
    if($userId = null) {
      global $user;
      $userId = $user->uid;
    }

    if(user_is_anonymous()) {
      $userId = 'anonymous';
    }

    if($userId == $this->senderId)
      return true;

    if($userId == $this->recipientId)
      return true;

    return false;
  }

  /**
   * Returns an instance of User class with sender user data.
   *
   * @return \User
   */
  public function getSender() {
    return UsersRepository::findByUid($this->senderId);
  }

  /**
   * Returns an instance of User class with recipient user data.
   *
   * @return \User
   */
  public function getRecipient() {
    return UsersRepository::findByUid($this->recipientId);
  }

  /**
   * Stores this message into the database
   */
  public function save() {
    ALMMessagesRespository::save($this);
  }
}

/**
 * Class ALMMessagesRespository
 */
class ALMMessagesRespository {

  /**
   * Returns a new instance of \ALMMessage class with data from database result
   *
   * @param $results
   * @return \ALMMessage
   */
  public static function databaseResultsToALMMessage($results) {
    $message = new ALMMessage();

    if(empty($results))
      return $message;

    $message->setId($results->id);
    $message->setCreatedDate($results->created);
    $message->setReadDate($results->readd);
    $message->setRecipientId($results->recipient);
    $message->setSenderId($results->sender);
    $message->setTitle($results->title);
    $message->setMessage($results->message);
    $message->setResponseToMessageId($results->responseto);
    $message->setArchived($results->archived);
    $message->setDeleted($results->deleted);
    $message->setSenderHide($results->sender_hide);

    return $message;
  }

  /**
   * Returns an array with instances of \LabNotebook class
   *
   * @param $results db_query or db_select results
   * @return \LabNotebook[]
   */
  public static function databaseResultsToALMMessages($results) {
    $messages = array();

    foreach($results as $result) {
      $messages[] = ALMMessagesRespository::databaseResultsToALMMessage($result);
    }

    return $messages;
  }

  /**
   * @param $message \ALMMessage
   * @return bool
   *
   */
  public static function save($message) {

    if($message->isEmpty())
      return false;

    db_merge('alm_message')
      ->key(array('id' => $message->getId()))
      ->fields(array(
        'id' => $message->getId(),
        'created' => $message->getCreatedDate(),
        'readd' => $message->getReadDate(),
        'recipient' => $message->getRecipientId(),
        'sender' => $message->getSenderId(),
        'title' => $message->getTitle(),
        'message' => $message->getMessage(),
        'responseto' => $message->getResponseToMessageId(),
        'archived' => $message->isArchived(),
        'deleted' => $message->isDeleted(),
        'sender_hide' => $message->isSenderHide(),
      ))->execute();

    return true;
  }

  /**
   * @param $id
   * @return \ALMMessage
   */
  public static function findById($id) {
    $result = db_select('alm_message', 'm')
      ->condition('m.id', $id, '=')
      ->fields('m', ALMMessage::$databaseFields)
      ->range(0,1)
      ->execute()
      ->fetch();

    return ALMMessagesRespository::databaseResultsToALMMessage($result);
  }

  /**
   * @param $userId
   * @return \ALMMessage[]
   *
   */
  public static function findByRecipientId($userId) {
    $result = db_select('alm_message', 'm')
      ->condition('m.recipient', $userId, '=')
      ->fields('m', ALMMessage::$databaseFields)
      ->execute();

    return ALMMessagesRespository::databaseResultsToALMMessages($result);
  }

}







