<?php

/**
 * @file
 * Enumerator with states of a ALMRequest
 *
 * Series states:
 * (1) Edit - request is edited
 * (2) Pending - request is waiting for accept (or reject) by Service 02 team
 * (3) Revision - request was not accepted by request responsible (advisor)
 * (4) Accepted - request was accepted by request responsible and waiting for consultation
 * (5) Running - (after consultation) request is carried out, but not done yet
 * (6) Finished - request is finished
 * (7) Archived - optional
 * (8) Withdraw - request was withdrawed in the consultation meeting
 *
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 *
 * @version 0.1
 */

/**
 * Class ALMRequestState
 * Defines all possible states for an ALM request object
 */
class ALMRequestState
{
  /**
   * First state after creating.
   * The user can view, edit, submit and delete the request.
   * The Manager can't see it
   */
  const Edit = '10';
  /**
   * The state after a manager revised a requet.
   * The user can view, edit, submit and delete the request.
   * The manager can't see it. (Same like edit, but after revising)
   */
  const Revision = '11';
  /**
   * The state after a researcher submited a request.
   * The user can view the request.
   * The manager can view, edit, accept and revise the request.
   */
  const Pending = '50';
  /**
   * The state after the request was withdrawed in the consultation.
   * The user can view the request.
   * The manager can not see it.
   */
  const Withdraw = '100';
  /**
   * The state after a manager has accepted the request.
   * The user can view the request.
   * The manager can view, consult(edit), revise and withdraw the request.
   *
   * Depending of the user_qualification lvl, the user is able to consult his requests self
   */
  const Accepted = '30';
  /**
   * The state after a manager consult the request (together with the researcher).
   * The user can view the request.
   * The manager can view the request
   */
  const Running = '40';
  /**
   * Not implemented yet.
   */
  const Finished = '6';
  /**
   * Not implemented yet.
   */
  const Archived = '120';
  /**
   * The state after a user has deleted the request.
   * The user can view the request (only with the link because in current Version [1.0] no overview for deleted requests exists)
   */
  const Deleted = '200';


  /**
   * Gives for each request state a Icon with a short name of the request state.
   * Used to show the Request-States in the overviews (request lists)
   *
   * //TODO: auslagern in theme/template
   * 
   * @param $request_state
   * @return string HTML Icon with Text
   */
  public static function getRequestStateIcon($request_state) {
    switch ($request_state) {
      case ALMRequestState::Edit:
        return '<span class="text-red"><i class="fa fa-unlock"></i> Editable. Not submitted yet!</span>';
      case ALMRequestState::Pending:
        return '<span class="text-yellow"><i class="fa fa-question-circle"></i>  Pending!</span>';
      case ALMRequestState::Accepted:
        return '<span class="text-aqua"><i class="fa fa-battery-2"></i> Accepted, waits for consultation!</span>';
      case ALMRequestState::Withdraw:
        return '<span class="text-red"><i class="fa  fa-remove"></i> Withdraw</span>';
      case ALMRequestState::Running:
        return '<span class="text-green"><i class="fa fa-battery-3"></i> Running</span>';
      case ALMRequestState::Finished:
        return '<span class="text-grey"><i class="fa  fa-thumbs-up"></i> Finished</span>';
      case ALMRequestState::Deleted:
        return '<span class="text-black"><i class="fa  fa-trash"></i> Deleted</span>';
      case ALMRequestState::Revision:
        return '<span class="text-yellow"><i class="fa  fa-retweet"></i> Revised! Please review the request and submit again</span>';
      default:
        return 'STATE';
    }
  }

  /**
   * Returns human readable name of given request state
   *
   * @param $requestState \ALMRequestState
   * @return string
   */
  public static function getRequestStateName($requestState) {
    switch ($requestState) {
      case ALMRequestState::Edit:
        return 'Editable';
      case ALMRequestState::Pending:
        return 'Pending';
      case ALMRequestState::Accepted:
        return 'Accepted';
      case ALMRequestState::Withdraw:
        return 'Withdrawn';
      case ALMRequestState::Running:
        return 'Running';
      case ALMRequestState::Finished:
        return 'Finished';
      case ALMRequestState::Deleted:
        return 'Deleted';
      case ALMRequestState::Revision:
        return 'Revised';
      default:
        return 'Unknown';
    }
  }

  /**
   * Check if a string matches to any existing RequestState
   * @param $requestState
   * @return bool
   */
  public static function isRequestState($requestState){
    if($requestState == ALMRequestState::Edit ||
        $requestState == ALMRequestState::Pending ||
        $requestState == ALMRequestState::Accepted ||
        $requestState == ALMRequestState::Withdraw ||
        $requestState == ALMRequestState::Running ||
        $requestState == ALMRequestState::Finished ||
        $requestState == ALMRequestState::Deleted ||
        $requestState == ALMRequestState::Revision)
    {
      return true;
    }
    return false;
  }
}