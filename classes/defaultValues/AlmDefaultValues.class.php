<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 21.11.2019
 * Time: 13:49
 */
class AlmDefaultValues {

  /**
   * DO NOT DELETE ROWS for a running instance. Just disable them with the flag.
   * Add new rows is possible.
   */
  const IMAGING_SUPPORT_ARR = [
    //Attention: Don't start with 0 because 0 is the key for not selected!
    1 => ['name' => "I only want to register the imaging study / no further support", 'enabled'=>'1'],
    2 => ['name' => "Setting up imaging protocol / workflow", 'enabled'=>'1'],
    3 => ['name' => "Confocal microscopy (Zeiss LSM 710)", 'enabled'=>'1'],
    4 => ['name' => "STED microscopy (Leica TCS SP8 STED)", 'enabled'=>'1'],
    5 => ['name' => "Image data backup (ALM server module)", 'enabled'=>'1'],
  ];

  /**
   * DO NOT DELETE ROWS for a running instance. Just disable them with the flag.
   * Add new rows is possible.
   */
  const IMAGING_MODULE_ARR = [
      //Attention: Don't start with 0 because 0 is the key for not selected!
      1 => ['name'=>"No imaging module required", 'enabled'=>'1'],
      2 => ['name'=>"Single immunofluorescence", 'enabled'=>'1'],
      3 => ['name'=>"Dual immunofluorescence", 'enabled'=>'1'],
      4 => ['name'=>"Triple / multi-color immunofluorescence", 'enabled'=>'1'],
      5 => ['name'=>"Live cell TATS imaging", 'enabled'=>'1'],
      6 => ['name'=>"Live cell calcium imaging", 'enabled'=>'1'],
      7 => ['name'=>"Live cell calcium and TATS imaging", 'enabled'=>'1'],
      8 => ['name'=>"Live heart calcium imaging", 'enabled'=>'1'],
      9 => ['name'=>"Live heart TATS imaging", 'enabled'=>'1'],
      10 => ['name'=>"Cellular electrophysiology and quantitative Calcium measurements", 'enabled'=>'1'],
    ];

  /**
   * DO NOT DELETE ROWS for a running instance. Just disable them with the flag.
   * Add new rows is possible.
   */
  const ANALYSIS_MODULE_ARR = [
    //Attention: Don't start with 0 because 0 is the key for not selected!1 =>
    1 => ['name' => "No analysis module required", 'enabled'=>'1'],
    2 => ['name' => "Colocalization analysis", 'enabled'=>'1'],
    3 => ['name' => "TATS analysis", 'enabled'=>'1'],
    4 => ['name' => "Calcium transient analysis", 'enabled'=>'1'],
    5 => ['name' => "Cellular electrophysiology and quantitative Calcium analysis", 'enabled'=>'1'],
  ];

  /**
   * DO NOT DELETE ROWS for a running instance. Just disable them with the flag.
   * Add new rows is possible.
   */
  const INTRODUCTION_TO_ALM_ARR = [
    //Attention: Don't start with 0 because 0 is the key for not selected!1 =>
    1 => ['name' => "No ALM introduction requested", 'enabled'=>'1'],
    2 => ['name' => "Confocal microscopy introduction (Zeiss LSM 710)", 'enabled'=>'1'],
    3 => ['name' => "STED microscopy introduction (Leica TCS SP8 STED)", 'enabled'=>'1'],
  ];

  static function DefaultSelectOptionsByName($name){
    switch ($name){
      case 'alm_imaging_support':
        return self::IMAGING_SUPPORT_ARR;
      case 'alm_imaging_modules':
        return self::IMAGING_MODULE_ARR;
      case 'alm_analysis_modules':
        return self::ANALYSIS_MODULE_ARR;
      case 'alm_introduction_to_alm':
        return self::INTRODUCTION_TO_ALM_ARR;
    }
  }
}