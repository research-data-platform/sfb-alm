<?php
/**
 * Created by PhpStorm.
 * User: Henke_Christian
 * Date: 17.08.2016
 * Time: 17:23
 */

class ALMCommentRepository {

  static $databaseFields = [
    'id' => 'id',
    'request_id' => 'request_id',
    'date' => 'date',
    'message' => 'message',
    'type' => 'type',
  ];

  const DATABASE_NAME = 'alm_request_comments';


  /**
   * @param $result
   *
   * @return ALMComment
   */
  private static function databaseResultToALMComment($result) {
    $comment = new ALMComment();
    if (empty($result)) {
      $comment->setCommentID(ALMComment::EMPTY_COMMENT_ID);
      return $comment;
    }

    //
    // Weist den Variablen aus dem ALMComment Objekt die Werte aus dem Datenbankfeld zu.
    //

    $comment->setCommentID($result->id);
    $comment->setRequestID($result->request_id);
    $comment->setDate($result->date);
    $comment->setMessage($result->message);
    $comment->setTypeID($result->type);

    return $comment;
  }

  /**
   * Saves an instance of @see \ALMComment to the database and returns the identifier.
   *
   * @param \ALMComment $comment
   *
   * @return string ID of the saved element.
   */
  public static function save($comment) {
    if (isset($comment)) {
      /**
       * If instance has not been stored before, set ID to NULL for merge.
       */
      $id = $comment->isEmpty() ? NULL : $comment->getCommentID();
      try {
        db_merge(self::DATABASE_NAME)->key(['id' => $id])
          ->fields([
            //TODO: FOREIGN key
            self::$databaseFields['request_id'] => self::setEmptyVariablesToNull($comment->getRequestID()),
            self::$databaseFields['date'] => self::setEmptyVariablesToNull($comment->getDate()),
            self::$databaseFields['message'] => self::setEmptyVariablesToNull($comment->getMessage()),
            self::$databaseFields['type'] => self::setEmptyVariablesToNull($comment->getTypeID()),
          ])->execute();

        /**
         * If ID is NULL, update to last inserted ID from database transaction.
         */
        $id = $id == NULL ? Database::getConnection()->lastInsertId() : $id;
      } catch (Exception $exception) {
        /**
         * Handle exception.
         */
        watchdog_exception(__CLASS__, $exception);
      }

      return $id;
    }
  }

  /**
   * Prüft ob eine Variable einen Inhalt hat. Wenn die Variable '' oder
   * ALM_FORM_SELECT_EMPTY ist, wird sie null gesetzt.
   *
   * @param $input
   *
   * @return null
   */
  private static function setEmptyVariablesToNull($input) {
    if ($input == ALM_FORM_SELECT_EMPTY || $input == '') {
      return NULL;
    }
    else {
      return $input;
    }
  }

  /**
   * @param $requestID
   *
   * @return ALMComment[]
   */
  public static function findAllCommentsByRequestID($requestID) {
    $result = db_select(self::DATABASE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['request_id'], $requestID, '=')
      ->orderBy(self::$databaseFields['date'], 'DESC')
      ->fields('r', self::$databaseFields)
      ->execute();

    return ALMCommentRepository::databaseResultsToALMComments($result);
  }

  /**
   * Returns ONE comment object with the requestID and the commentType
   *
   * @param $requestID
   * @param $commentType ALMCommentType
   *
   * @return ALMComment
   */
  public static function findAllCommentByRequestIdAndCommentType($requestID, $commentType) {
    $result = db_select(self::DATABASE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['request_id'], $requestID, '=')
      ->condition('r.' . self::$databaseFields['type'], $commentType, '=')
      ->orderBy(self::$databaseFields['date'], 'DESC')
      ->fields('r', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return ALMCommentRepository::databaseResultToALMComment($result);
  }

  public static function findLatestCommentByRequestIDAndSmallerThanCommentType($requestID, $maxCommentType) {
    /*$query = db_select(self::DATABASE_NAME, 'c')
      ->condition('c.' . self::$databaseFields['request_id'], $requestID, '=')
      ->condition('c.' . self::$databaseFields['type'], $maxCommentType, '<');

    $query->addExpression('MAX('. self::$databaseFields[date()]);
    $result = $query->fields('c', self::$databaseFields)->execute();
    */
    $result = db_select(self::DATABASE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['request_id'], $requestID, '=')
      ->condition('r.' . self::$databaseFields['type'], $maxCommentType, '<')
      ->orderBy(self::$databaseFields['date'], 'DESC')
      ->fields('r', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();
    return ALMCommentRepository::databaseResultToALMComment($result);
  }


  /**
   * Find the comment to the commentId in the database and return the comment object
   * @param $commentId
   * @return ALMComment
   */
  public static function findCommentByCommentId($commentId){
    $result = db_select(self::DATABASE_NAME, 'r')
      ->condition('r.' . self::$databaseFields['id'], $commentId, '=')
      ->fields('r', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();
    return ALMCommentRepository::databaseResultToALMComment($result);
  }


  /**
   * @param $results
   *
   * @return ALMComment[]
   */
  private static function databaseResultsToALMComments($results) {
    $comments = [];
    foreach ($results as $result) {
      $comments[] = ALMCommentRepository::databaseResultToALMComment($result);
    }

    return $comments;
  }
}