<?php

/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 13.06.18
 * Time: 10:35
 */

class ALMDataset extends ArchiveObject implements ArchiveObjectInterface {

  const CONTEXT = "ALM Imaging Dataset";

  private $request_id = ALMRequest::EMPTY_REQUEST_ID;

  private $comment_id = ALMComment::EMPTY_COMMENT_ID;

  private $modified_by;

  private $managerView = false;

  private $sharedWorkingGroupsIds;

  public function getSharedWorkingGroupsIds() {
    if (!isset($this->sharedWorkingGroupsIds)) {
      $this->loadSharedWorkingGroups();
    }
    return $this->sharedWorkingGroupsIds;
  }

  public function getSharedWorkingGroups(){
    $workingGroups = array();
    $workingGroupIds = $this->getSharedWorkingGroupsIds();

    foreach ($workingGroupIds as $workingGroupId) {
      $workingGroups[] = WorkingGroupRepository::findById($workingGroupId);
    }

    return $workingGroups;
  }
  /**
   * Returns array with all (short) names of the shared workingGroups
   */
  public function getSharedWorkingGroupShortNames(){
    $workingGroupShortNames = array();
    $workingGroupIds = $this->getSharedWorkingGroupsIds();

    foreach ($workingGroupIds as $workingGroupId) {
      $workingGroupShortNames[] = WorkingGroupRepository::findById($workingGroupId)->getShortName();
    }

    return $workingGroupShortNames;
  }

  private function loadSharedWorkingGroups() {
    $this->sharedWorkingGroupsIds = AlmDatasetSharedWorkingGroupsRepository::findWorkingGroupsForDatasetId($this->getId());
  }

  /**
   * Saves the shared working groups to the database
   * Attention: This function does not delete further shared working groups in the database!
   */
  private function saveSharedWorkingGroups(){
    AlmDatasetSharedWorkingGroupsRepository::saveSharedWorkingGroups($this->getId(), $this->sharedWorkingGroupsIds);
  }

  /**
   * Adds the workingGroupId to the sharedWorkingGroupIds array and to the database!!
   * @param $workingGroupId
   */
  public function addSharedWorkingGroup($workingGroupId){
    if (isset($workingGroupId)) {
      $this->sharedWorkingGroupsIds[] = $workingGroupId;
      $this->saveSharedWorkingGroups();
    }
  }

  /**
   * Remove the workingGroupId from the sharedWorkingGroupIds array and the database!!
   * @param $workingGroupId
   */
  public function revokeSharedWorkingGroup($workingGroupId){
    $this->sharedWorkingGroupsIds = array_diff($this->sharedWorkingGroupsIds, [$workingGroupId]);
    AlmDatasetSharedWorkingGroupsRepository::deleteSharedWorkingGroup($this->getId(), $workingGroupId);
  }


  /**
   * If actived the url's of the views will use the management views (containing /management/ in the url)
   * @param bool $managerView
   */
  public function setManagerView($managerView) {
    $this->managerView = $managerView;
  }


  public static $allowedDataTypes = [
    'tiff' => 'image/tiff',
    'xlif' => 'application/octet-stream',
  ];

  public function __construct() {
    $this->setContext(self::CONTEXT);
  }

  /**
   * @return bool TRUE if current user has permission to delete datasets
   */
  public function checkDeletePermission() {
    /**
     * Delete-permission must be granted to user role
     */
    if (user_access(SFB_ALM_PERMISSION_DELETE_DATASET)) {
      /**
       * User must have write-access to the Dataset or be ALM-Manager
       */
      if ($this->checkWritePermission() || user_access(SFB_ALM_PERMISSION_MANAGE)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  public function checkSharedPermission(){
    // Get all available groups
    $groups = $this->getSharedWorkingGroups();
// Check membership
    $user = User::getCurrent();

    $is_member = false;
    foreach ($groups as $group) {
        if($user->isMemberOfWorkingGroup( $group)) $is_member = true;
    }
    return $is_member;
  }

  /**
   * @return bool TRUE if current user has permission to download the dataset
   */
  public function checkDownloadPermission() {
    // Checks read permission due to sharing level
    if ($this->checkReadPermission()) {
      return TRUE;
    }

    // Checks download permission due to alm-modul permissions
    $requestObj = ALMRequestsRepository::findById($this->request_id);
    if ($requestObj->permit(ALMRequest::ROLE_MANAGER,
        ALMRequest::ACTION_VIEW_IMAGES) &&
      user_access(SFB_ALM_PERMISSION_ACCESS_USER_UPLOADED_DATA)) {
      return TRUE;
    }

    return FALSE;
  }

  public function setContext($context) {
    if ($context != self::CONTEXT) {
      watchdog(__CLASS__, "Tried to set illegal context in %c setContext()",
        ['%c' => __CLASS__], WATCHDOG_NOTICE);
    }
    parent::setContext(self::CONTEXT);
  }

  /**
   * @param self[] $alm_datasets
   *
   * @return string Redered table or empty string on error.
   */
  public static function table($alm_datasets) {
    $rows = [];
    foreach ($alm_datasets as $alm_dataset) {
      $rows[] = $alm_dataset->tableRow();
    }

    try {
      return theme('table', ['header' => self::tableHeader(), 'rows' => $rows]);
    } catch (Exception $exception) {
      watchdog_exception(__CLASS__, $exception);
    }
    return '';
  }


  /**
   * Defailt(view): Links to the ALM Datasetview
   * If $type = delete, uses the delete function of the archiv function
   *
   * @param string $type
   *
   * @return string
   */
  public function url($type = 'view') {

    // Uses URLs that redirect in the manager view
    if ($this->managerView) {
      switch ($type) {
        case 'download':
          return sfb_alm_url(RDP_ALM_URL_REQUEST_DATASET_ZIP_DOWNLOAD, $this->request_id, $this->getId());
        case 'view':
          return sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_DATASET_VIEW,
            $this->request_id, $this->getId());
        default:
          // do nothing, use non-management routes
      }
    }


    switch ($type) {
      case 'delete':
        return sfb_alm_url(RDP_ALM_URL_REQUEST_DATASET_DELETE,
          $this->request_id,
          $this->getId());
      case 'download':
        return sfb_alm_url(RDP_ALM_URL_REQUEST_DATASET_ZIP_DOWNLOAD,
          $this->request_id, $this->getId());
      case 'landing':
        return sfb_alm_url(RDP_ALM_URL_REQUEST_DATASET_LANDING,
          $this->request_id, $this->getId());
      case 'view':
      default :
        return sfb_alm_url(RDP_ALM_URL_REQUEST_DATASET_VIEW, $this->request_id,
          $this->getId());
    }
  }

  public function tableRow() {

    $actions = '';
    $actions .= '&nbsp;' . l('<span class="glyphicon glyphicon-eye-open"></span>',
        $this->url(), ['html' => TRUE]);
    $actions .= '&nbsp;' . l('<span class="glyphicon glyphicon-download"></span>',
        $this->url('download'), ['html' => TRUE]);
    //$actions .= '&nbsp;<a href=""><span class="glyphicon glyphicon-edit"></span></a>';
    if ($this->checkDeletePermission()) {
      $actions .= '&nbsp;' . l('<span class="glyphicon glyphicon-trash" style="color: red"></span>',
          $this->url('delete'), ['html' => TRUE]);
    }

    return [
      // Staining/Coverslip ID
      l($this->getPidId(), sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $this->request_id)),
      // Dataset display name
      l($this->getDisplayName(), $this->url()),
      // Dataset classification
      //'STED microscopy (Leica TCS SP8 STED)',
      // Number of files in dataset
      $this->getFileCount(),
      // Upload date
      $this->getCreated()->format(SFB_ALM_DATEFORMAT),
      // Owner
      $this->getOwner()->getFullname(),
      // Visibility
      SharingLevel::getHtmlInline($this->getSharingLevel()),
      // Actions
      $actions,
    ];
  }


  /**
   * @return \User The owner/uploader of the dataset.
   */
  private function getOwner() {
    $object = CdstarObjectRepository::findById($this->getCdstarObjectId());
    return UsersRepository::findByUid($object->getOwner());
  }

  public static function tableHeader() {
    return [
      'Identifier',
      'Dataset',
      //'Type',
      'Files',
      'Upload date',
      'Uploaded by',
      'Visibility',
      'Actions',
    ];
  }

  public function save() {
    $datetime = new DateTime("now",
      new DateTimeZone(drupal_get_user_timezone()));
    if ($this->isEmpty()) {
      $this->setCreated($datetime);
    }
    $this->setModified($datetime);
    $this->setModifiedBy(User::getCurrent()->getUid());
    $id = ALMDatasetRepository::save($this);
    if ($this->isEmpty()) {
      $this->setId($id);
    }

    $this->saveSharedWorkingGroups();
  }

  public function delete() {
    $this->setPidId(self::EMPTY_ID);
    ALMDatasetRepository::delete($this);
  }

  /**
   * @return mixed
   */
  public function getModifiedBy() {
    return $this->modified_by;
  }

  /**
   * @param mixed $modified_by
   */
  public function setModifiedBy($modified_by) {
    $this->modified_by = $modified_by;
  }

  /**
   * @return int
   */
  public function getRequestId() {
    return $this->request_id;
  }

  /**
   * @param int $request_id
   */
  public function setRequestId($request_id) {
    $this->request_id = $request_id;
  }

  /**
   * @return int
   */
  public function getCommentId() {
    return $this->comment_id;
  }

  /**
   * @param int $comment_id
   */
  public function setCommentId($comment_id) {
    $this->comment_id = $comment_id;
  }

  /**
   * ALM module specific HTML markup for display of arbitrary files in CDSTAR
   * packages.
   *
   * @param \CdstarFile $file
   *
   * @return string HTML markup
   */
  public function fileHtmlDisplay($file) {
    /**
     * Replaces generic download route with ALM module download route.
     */
    /*
    //todo: can be deleted
    $url = sfb_alm_url_replace_seq(RDP_ALM_URL_DOWNLOAD_FILE, [
      $this->getRequestId(),
      $this->getId(),
      $file->getId(),
    ]);
    */

    $url = sfb_alm_url(RDP_ALM_URL_DOWNLOAD_FILE, $this->getRequestId(),
      $this->getId(), $file->getId());
    return str_replace($file->url("download"), $url, $file->htmlDisplay());
  }

  /**
   * @return string HTML markup of a block containing general information about
   *   the dataset
   */
  public function displayInfoblock() {

    /**
     * Fetch @see \EpicPid object.
     */
    $pid = EpicPidRepository::findById($this->getPidId());

    $request = ALMRequestsRepository::findById($this->getRequestId());
    /**
     * jQuery script to enable "copy to clipboard" links.
     */
    $copy_to_clipboard_javascript = /** @lang javascript */
      <<<SCRIPT
jQuery(document).ready(function(){
	jQuery("a[name=copy_pre]").click(function() {
		var id = jQuery(this).attr('id'); 
		var el = document.getElementById(id);
		var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = el.textContent;
    document.body.appendChild(tempInput);
    tempInput.select();
		document.execCommand('copy');
		return false;
	});
});
SCRIPT;
    drupal_add_js($copy_to_clipboard_javascript, 'inline');

    /**
     * Generate arbitrary but unique id for CSS elements to enable copy-to-clipboard functionality
     */
    $css_element_id = substr(md5($pid->getUrl()), 0, 12);

    $pid_link = $pid->getIconHtmlLink("14") . '&nbsp;'
      . l($pid->getPid(), $pid->getUrl(), [
        'external' => TRUE,
        'attributes' => ['class' => 'badge btn-warning'],
      ]);
    $pid_markup = '<strong>Persistent Identifier</strong>:&nbsp;'
      . $pid_link
      . '&nbsp;<div id="' . $css_element_id . '" class="hidden">' . $pid->getUrl() . '</div>
        <a href="#" id="' . $css_element_id . '" name="copy_pre" title="Copy to clipboard">
            <span class="glyphicon glyphicon-copy"></span></a>';

    $block = '<div class="panel panel-default">
                  <div class="panel-heading">General Information</div>
                  <div class="panel-body">
                    <strong>Owner</strong>: ' . $request->getUserFullname() . ' <a href="mailto:' . $request->getEmail() . '"><span class="glyphicon glyphicon-envelope"></span> </a> <br>
                    <strong>Request ID</strong>: ' . $request->getRequestID() . "&nbsp;"
      . l('<span class="glyphicon glyphicon-eye-open" title="View request"></span>',
        $this->getRequestLinkUrl($request), ['html' => TRUE]) . '<br>
                    ' . $pid_markup . '<br>
                    ' . SharingLevel::getHtmlInline($this->getSharingLevel()) . '
                  </div>
              </div>';
    return $block;
  }

  /**
   * Returns a link to an alm request (view-page) depending on the managerView
   * variable
   *
   * @param $requestObj
   *
   * @return string
   */
  private function getRequestLinkUrl($requestObj) {
    if ($this->managerView) {
      return sfb_alm_url(RDP_ALM_URL_MANAGEMENT_REQUEST_VIEW,
        $requestObj->getRequestID());
    }
    return sfb_alm_url(RDP_ALM_URL_REQUEST_VIEW, $requestObj->getRequestID());
  }
}