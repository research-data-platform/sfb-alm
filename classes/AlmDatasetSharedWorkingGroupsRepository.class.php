<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 28.05.2019
 * Time: 23:32
 */
class AlmDatasetSharedWorkingGroupsRepository {
  const DATABASE_NAME = 'alm_shared_datasets';

  static $databaseFields = array(
    'WorkingGroupId' => 'workinggroup_id',
    'DatasetId' => 'dataset_id',
  );


  private static function databaseResultToArray($results) {
    $workingGroups = [];
    foreach ($results as $result) {
      // convert results object to an associative array
      $results_arr = get_object_vars($result);
      $workingGroups[] = $results_arr[self::$databaseFields['WorkingGroupId']];

    }
    return $workingGroups;
  }


  static public function deleteSharedWorkingGroup($datasetId, $workingGoupId){
    db_delete(self::DATABASE_NAME)
      ->condition(self::$databaseFields['DatasetId'], $datasetId)
      ->condition(self::$databaseFields['WorkingGroupId'], $workingGoupId)
      ->execute();
  }


  /**
   * Saves all shared working groups for one datasetId
   * @param $datasetId
   * @param $workingGroupIds array
   *
   */
  static public function saveSharedWorkingGroups($datasetId, $workingGroupIds) {
    if(isset($workingGroupIds)) {
      foreach ($workingGroupIds as $workingGrId) {
        db_merge(self::DATABASE_NAME)->key(array(self::$databaseFields['WorkingGroupId'] => $workingGrId, self::$databaseFields['DatasetId'] => $datasetId))
          ->fields(array(
            self::$databaseFields['WorkingGroupId'] => $workingGrId,
            self::$databaseFields['DatasetId'] => $datasetId,
          ))->execute();
      }
    }
  }

  static public function findWorkingGroupsForDatasetId($datasetId){
    $result = db_select(self::DATABASE_NAME, 's')
      ->condition('s.' . self::$databaseFields['DatasetId'], $datasetId, '=')
      ->fields('s', array(self::$databaseFields['WorkingGroupId']))
      ->execute();

    return self::databaseResultToArray($result);
  }
}