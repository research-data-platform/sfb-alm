<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 18.06.18
 * Time: 07:27
 */


/**
 * Class ALMCommentType
 * Declares all possible comment types. While a comment is editable, the name
 * of the commenttype ends with a "Edit"
 */
class ALMCommentType {

  /** User comment after submitting the request(the comment is NOT editable anymore)*/
  const Submit = 1;

  /** ALM Manager comment after accepting (the comment is NOT editable anymore) */
  const Accepted = 2;

  /** ALM Manager comment after the consultation the request (the comment is NOT editable anymore) */
  const Consult = 3;

  /** ALM Manager comment after revising the request (the comment is NOT editable anymore) */
  const Revise = 4;

  /** ALM Manager comment after withdrawing the request (the comment is NOT editable anymore) */
  const Withdraw = 5;

  /** User comment before submit (the comment is editable) */
  const SubmitEdit = 101;

  /** ALM Manager comment before accepting the request (the comment is editable) Currently this should not be possible, because you can not save a accept comment without accepting the request*/
  const AcceptEdit = 102;

  /** ALM Manager comment before consulting the request (the comment is editable)*/
  const ConsultEdit = 103;

  /** ALM Manager comment before revising (the comment is editable) Currently this should not be possible, because you can not save a revise comment without revising the request*/
  const ReviseEdit = 104;

  /** ALM Manager comment before withdrawing (the comment is editable) Currently this should not be possible, because you can not save a withdraw comment without withdrawing the request*/
  const WithdrawEdit = 105;

  /** ALM Manager comment before closing request */
  const CloseEdit = 190;

  /** User comment when uploading a dataset */
  const DatasetUpload = 200;

  /**
   * Returns a String text(name) of the commandType
   *
   * @param $commandType
   *
   * @return string
   */
  static function getText($commandType) {
    switch ($commandType) {
      case ALMCommentType::Submit:
        return 'User comment:';
      case ALMCommentType::SubmitEdit:
        return 'User comment:';
      case ALMCommentType::Accepted:
        return 'Accept comment:';
      case ALMCommentType::AcceptEdit:
        return 'Accept comment:';
      case ALMCommentType::ConsultEdit:
        return 'Consultation comment:';
      case ALMCommentType::Consult:
        return 'Consultation comment:';
      case ALMCommentType::Revise:
        return 'Revise comment:';
      case ALMCommentType::ReviseEdit:
        return 'Revise comment:';
      case ALMCommentType::Withdraw:
        return 'Withdraw comment:';
      case ALMCommentType::WithdrawEdit:
        return 'Withdraw comment:';
      case ALMCommentType::DatasetUpload:
        return 'Dataset upload comment:';
      case ALMCommentType::CloseEdit:
        return 'Request closing comment:';
      default:
        return 'Comment Type text is not implemented yet';
    }
  }
}